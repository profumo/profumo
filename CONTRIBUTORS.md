# PROFUMO Contributors

The method has been developed in collaboration with many of the members of the
Analysis Group at [FMRIB](https://www.ndcn.ox.ac.uk/divisions/fmrib) (see e.g.
[here](https://www.ndcn.ox.ac.uk/research/functional-mri-modelling) or
[here](https://www.ndcn.ox.ac.uk/research/connectivity-modelling)). The
following have made contributions to the code, and are the best people to
contact with specific questions.

----------

### Original developer: Sam Harrison

FMRIB Centre,
[Wellcome Centre for Integrative Neuroimaging](https://www.win.ox.ac.uk/),
University of Oxford
 + **Contact:** [www.ndcn.ox.ac.uk/team/samuel-harrison](https://www.ndcn.ox.ac.uk/team/samuel-harrison)


----------

### Current developer and main contact: Rezvan Farahibozorg
FMRIB Centre,
[Wellcome Centre for Integrative Neuroimaging](https://www.win.ox.ac.uk/),
University of Oxford
 + **Contact:** [www.ndcn.ox.ac.uk/team/rezvan-farahibozorg](https://www.ndcn.ox.ac.uk/team/rezvan-farahibozorg)

----------

Written in [CommonMark-flavoured Markdown](http://commonmark.org/) using the
associated [live testing tool](http://try.commonmark.org/).
