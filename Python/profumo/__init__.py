# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2018
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

"""
A Pythonic interface with the output of a PROFUMO analysis.

Abbreviations
-------------
covmat
    Covariance matrix.
corrmat
    Correlation matrix.
precmat
    Precision matrix.
netmat
    Network matrix (i.e. partial correlations).
"""

###############################################################################

from profumo.pfms import PFMs

###############################################################################

import warnings

# More prominent warnings, but without printing the line of code that issued it
def __formatwarning(message, category, filename, lineno, line=None):
    filename = ("..." + filename[-57:]) if len(filename) > 60 else filename
    return "*"*80 + "\n" \
            + "{} [{}:{}]\n".format(category.__name__, filename, lineno) \
            + "{}\n".format(message) \
            + "*"*80 + "\n"

warnings.formatwarning = __formatwarning

###############################################################################
