# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2018
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# extras.py
"""
Here be dragons.
Mostly some interactive summary plots, but minimal guarantees about
usefulness, consistency or stability ;-p
"""

###############################################################################

import numpy, numpy.fft
import scipy.stats

import matplotlib as mpl
import matplotlib.pyplot as plt

import profumo.utilities as utils
import profumo.plotting as pplt

###############################################################################

def plot_maps(maps, title, vmax=None, vmin=None,
              xlabel="Mode", ylabel="Voxel"):
    """
    Plot a set of spatial maps.
    
    Generated plots:
     + Maps as a matrix.
     + Correlations between maps.
     + Summaries of the map mean / variance / skewness.
    """
    
    # Plot maps
    fig,ax = plt.subplots()
    pplt.plot_matrix(
        fig, ax, maps, title=title, vmax=vmax, vmin=vmin,
        xlabel=xlabel, ylabel=ylabel, clabel="Map weights")
    
    # Plot map correlations
    C = utils.similarity(maps, demean=False)
    fig,ax = plt.subplots()
    pplt.plot_correlation_matrix(
        fig, ax, C, title=title+": corrcoefs",
        xlabel=xlabel, ylabel=xlabel)
    
    # Plot map summary stats
    V, M = maps.shape
    map_means = numpy.mean(maps, axis=0)
    map_vars = numpy.var(maps, axis=0)
    #map_vars = numpy.mean(maps**2, axis=0)
    map_skews = scipy.stats.skew(maps, axis=0)
    #map_skews = numpy.mean(maps**3, axis=0)
    fig,ax = plt.subplots()
    plt.plot([0, M-1], [0.0, 0.0], color=pplt.LINE_BG)
    plt.plot(map_means, label="mean")
    plt.plot(map_vars ** (1/2), label="var ** (1/2)")
    # https://stackoverflow.com/a/45384691
    plt.plot(numpy.sign(map_skews) * (numpy.abs(map_skews)) ** (1/3),
             label="skew ** (1/3)")
    ax.set_xlabel(xlabel)
    ax.legend()
    ax.set_title(title + ": summary stats")
    
    return
    
###############################################################################

def plot_time_courses(time_courses, title, vmax=None, vmin=None,
                      xlabel="Time point", ylabel="Mode"):
    """
    Plot a single set of time courses.
    
    Generated plots:
     + Time courses as a matrix.
     + Correlations between time courses.
     + Frequency spectra.
    """
    
    # Plot TCs
    fig,ax = plt.subplots()
    pplt.plot_matrix(
        fig, ax, time_courses, title=title, vmax=vmax, vmin=vmin,
        xlabel=xlabel, ylabel=ylabel)
    
    # Plot correlations
    C = utils.similarity(time_courses.T)
    fig,ax = plt.subplots()
    pplt.plot_correlation_matrix(
        fig, ax, C, title=title+": corrcoefs", xlabel=ylabel, ylabel=ylabel)
    
    # Plot frequency spectra
    fft = numpy.abs(numpy.fft.rfft(time_courses, axis=1))
    fig,ax = plt.subplots()
    pplt.plot_matrix(
        fig, ax, fft, title=title+": FFT", vmin=0.0,
        ylabel=ylabel, xlabel="FFT coefficient")
    
    # Plot mean frequency spectra
    fig, ax = plt.subplots()
    plt.plot(numpy.mean(fft, axis=0))
    ax.set_xlabel("Frequency"); ax.set_ylabel("FFT")
    ax.set_title(title + ": Mean FFT")
    
    return

###############################################################################

def plot_precmat(precmat, title, label="Mode"):
    """
    Plot a precision matrix.
    
    Generated plots:
     + Raw precision matrix.
     + Partial correlation matrix.
    """
    
    # Plot precmat
    fig,ax = plt.subplots()
    pplt.plot_covariance_matrix(
        fig, ax, precmat, title=title+" (precmat)",
        xlabel=label, ylabel=label, clabel="Precision")
    
    # Convert to partial correlations
    netmat = - utils.normalise_covmat(precmat)
    
    # And plot netmat
    fig,ax = plt.subplots()
    pplt.plot_correlation_matrix(
        fig, ax, netmat, title=title+" (netmat)",
        xlabel=label, ylabel=label, clabel="Partial correlations")
    
    return

###############################################################################
