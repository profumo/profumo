# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2019
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# Should be sourced after conda has been activated (i.e. after `fsl.sh`).


export PROFUMODIR="/opt/profumo"
if [ -d "${PROFUMODIR}" ]; then
    PATH="${PATH}:${PROFUMODIR}/C++:${PROFUMODIR}/Python"
    # Activate conda environment, if present
    if command -v conda >/dev/null 2>&1 \
        && $(conda info --envs | grep --silent profumo);
    then
        conda activate profumo
    fi
fi
