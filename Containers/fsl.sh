# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2019
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT


export FSLDIR="/opt/fsl"
if [ -d "${FSLDIR}" ]; then
    PATH="${FSLDIR}/bin:${PATH}"
    # . "${FSLDIR}/etc/fslconf/fsl.sh"
    . "${FSLDIR}/etc/fslconf/fsl-devel.sh"
    # Use fslpython for conda
    . "${FSLDIR}/fslpython/etc/profile.d/conda.sh"
fi
