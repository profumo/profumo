#!/usr/bin/env python
# -*- coding: utf-8 -*-

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

################################################################################

import argparse
parser = argparse.ArgumentParser(description="Change PROFUMO version number.")
parser.add_argument("version")
args = parser.parse_args()

if args.version == 'dev':
    dev = True
    print("Switching to development version.")
else:
    dev = False
    print("Switching to version {v}.".format(v=args.version))

################################################################################

import os
import shutil
import re
import datetime

################################################################################

VERSION = args.version
DATE = datetime.date.today().strftime("%Y-%m-%d")

################################################################################

def replace_tags(filename):
    shutil.copy(filename, filename+'.bak')
    
    with open(filename, 'r') as input_file:
        lines = input_file.readlines()
    
    with open(filename, 'w') as output_file:
        for line in lines:
            line = re.sub('\$VERSION', VERSION, line)
            line = re.sub('\$RELEASE_DATE', DATE, line)
            output_file.write(line)
    
    os.remove(filename+'.bak')
    
    return

################################################################################

if not dev:
    replace_tags('README.md')
    replace_tags('C++/PROFUMO.c++')

################################################################################

# Makefile

shutil.copy("C++/Makefile", "C++/Makefile.bak")


contents = []
with open("C++/Makefile", "r") as inputFile:
    for line in inputFile:
        # install
        if "install: $(if" in line:
            contents.append(line)
            if dev:
                line = "\tcp PROFUMO ${HOME}/bin/PROFUMO_dev_$(TIMESTAMP)\n"
                contents.append(line)
                line = "\tln -sf PROFUMO_dev_$(TIMESTAMP) ${HOME}/bin/PROFUMO_dev\n"
                contents.append(line)
            else:
                line = "\tcp PROFUMO ${{HOME}}/bin/PROFUMO_v{v}_$(TIMESTAMP)\n".format(v=args.version)
                contents.append(line)
                line = "\tln -sf PROFUMO_v{v}_$(TIMESTAMP) ${{HOME}}/bin/PROFUMO_v{v}\n".format(v=args.version)
                contents.append(line)
                line = "\tln -sf PROFUMO_v{v} ${{HOME}}/bin/PROFUMO\n".format(v=args.version)
                contents.append(line)
            # Skip to blank line (this section can be two or three lines)
            while next(inputFile).strip():
                pass
            line = ("\n")
        
        # uninstall
        if "uninstall:" in line:
            contents.append(line); line = next(inputFile)
            if dev:
                line = "\trm -f ${HOME}/bin/PROFUMO_dev*\n"
            else:
                line = "\trm -f ${{HOME}}/bin/PROFUMO_v{v}_* ${{HOME}}/bin/PROFUMO_v{v} ${{HOME}}/bin/PROFUMO\n".format(v=args.version)
        
        # Store!
        contents.append(line)


with open("C++/Makefile", "w") as outputFile:
    for line in contents:
        outputFile.write(line)


os.remove("C++/Makefile.bak")


################################################################################
