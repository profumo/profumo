# SVD Calibration

In PROFUMO, we use the SVD in several places, including for the initialisation
of the subject spatial maps. For that, we calculate the group spatial basis
(i.e. the spatial singular vectors for all the concatenated data), and then run
a Bayesian ICA on that.

The crucial point to note in the above is the disconnect between group- and
subject-level representations: we initialise the *subject* maps based on the
*group* spatial basis. What we want is a decomposition that is more typical of
what we see at the subject level, such that we are not e.g. overconfident and
end up over-splitting modes. This is where the calibration step comes in: can
we transform the group spatial basis into something more akin to a single
subject basis. The second thing we have to be sure of is that our model (or,
more specifically, the priors) are still appropriate for decomposing the basis,
not the raw data. Here, we deal with the latter first.


### Model adjustments

PROFUMO is a matrix-factorisation-based model, loosely `D = P * A + E`, where
the key parameters to estimate are the spatial maps (`P`), timecourses (`A`),
and noise (specifically its variance, `σ^2`). What we want to do is combine
this with the SVD (`D = U * diag(s) * V.T`).

The first step is to combine the two equations and post-multiply by `V`. This
gives `U * diag(s) = P * (A * V) + E * V`. How does this change the model?
 + `P` is unchanged.
 + The prior on `A` needs altering, as we are now no longer inferring a
   timeseries per se. `(A * V)` represents a set of loadings onto the SVD
   basis, so will be (1) strongest in the first few entries and (2) very
   different in terms of autocorrelations etc.
 + `σ^2` is unchanged. If `V` is full rank then it is a pure rotation matrix,
   and `Tr(V.T * E.T * E * V) = Tr(E.T * E)`.

The second step is to truncate the basis by removing the components with the
smallest singular values. If we assume that the SVD has managed to separate
signal and noise reasonably well, then the model will change as below.
 + `P` and `A` are effectively unchanged. While we do remove the latter
   elements of `A`, these are loadings on the 'noise' components of the SVD so
   should have been close to zero anyway. This means that the key expectations
   (e.g. `E[D * A.T]`, `E[A.T * A]`) should be almost identical.
 + `σ^2` is affected. We should be able to explain almost all the variance of
   the top few SVD components, so, equivalently, we discard almost all the
   variance we are not able to explain. This leads us to drastically
   underestimate the noise level.

We address this issue in the next section.


### SVD & SNR

Typically, when we average across multiple subjects (`S`) we expect the SNR of
the data to increase: noise variance reduced by a factor of something like `S`
by averaging: <https://en.wikipedia.org/wiki/Signal_averaging>.

However, this is not quite the case for the SVD! In the example below, we
simulate data from a group of subjects, with a low dimensional structure (all
code is included at the end of the file). The questions are: how much of the
structure is captured by the first few 'signal' components from the SVD; how
clean are these components; and how do the singular values compare to those
from the 'noise' subspace? Below, we plot the key results as we vary the number
of subjects for convenience.

![SNR plot](SNR.png)
![Stratified singular values plot](signal_noise.png)

There are several effects at play here, and we make a distinction between the
variance of two different types of noise: how cleanly we can identify the
components that make up the principal signal subspace ('in') and how the
variance of the components outside of the main subspace changes too ('out').
What we see is that:
 + There are large benefits to adding subjects at the beginning, as this allows
   us to identify the signal subspace more reliably.
 + As we continue to add more subjects, the noise within the top few signal
   components levels off, and we see a concomitant increase in SNR.
 + However, the variance of the pure noise components increases, albeit slower,
   than that of the signal components.

As described in the previous section, the key metric from our point of view is
the noise variance, and we therefore need to quantify how much slower this is
increasing than the signal variance.

To briefly quantify theses effects, note that for the SVD the sum of the
squared singular values is the same as the sum of the squared data, i.e.
`sum(s^2) = sum(D^2) = Tr(D.t * D)`. If `D = [D_1, D_2, ..., D_S]` then this
sum is directly proportional to `S`. Finally, and directly analogously to the
distinction between standard deviation and variance, it is the *squared*
singular values that behave linearly.

##### Noise subspace

The noise-subspace singular values do increase, but not as fast as for the
signal. Intuitively, as we add more subjects `sum(s^2)` increases, but so does
the number of singular values because of the increase in dimensionality. There
is however still some effect of the number of subjects, and it turns out that
this is governed by the
[Marchenko-Pastur distribution](https://en.wikipedia.org/wiki/Marchenko%E2%80%93Pastur_distribution).
This says that if we have a random matrix `X`, of size `[M, N]`, with iid
elements `Xij ~ N(0, σ^2)`, then the eigenvalues of `(1/N) * X * X.T` follow a
specific distribution as `M`, `N` tend to infinity. From our point of view, the
key information is the limiting values, which are
`λ_lim = σ^2 (1 +/- sqrt(M/N))^2`. As the singular values are square root of
the eigenvalues of `X * X.T` (i.e. unnormalised), we have the following
equivalent expression: `s_lim = σ abs(sqrt(N) +/- sqrt(M))`

We can see from the simulations that this accurately predicts the relationship
between the max/min noise singular values and `S`.  Furthermore, we can use the
first singular values outside the signal subspace to calculate the effective
unstructured noise level. Again, we plot these results below:

![Marchenko-Pastur predictions](mp_predictions.png)
![Estimated noise level](estimated_sigma.png)

##### Signal subspace

The dominant effect on the singular values in the signal subspace is the number
of subjects. As the signal subspace is low rank (i.e. the number of components
is fixed), the signal singular values grow in proportion to `sqrt(S)`.

However, the we overestimate the contribution of the signal for two reasons.
 + The 'true' signal maps will soak up some of the variance of the noise
   itself. If we take our random matrix `X` from above, and multiply it with a
   normalised random vector of length `M` (i.e. `wi ~ N(0, 1/M)`) then this
   will contribute to the singular value associated with `w`:
   `s_w^2 = E[Tr(w.T * X * X.T * w)] = N σ^2`.
 + The 'true' maps will be perturbed, and this extra noise also inflates the
   singular value (this is `Noise (in)` in the first figure). Empirically, this
   seems to follow `s^2 = M σ^2`. In these simulations, this is independent of
   the number of subjects as we concatenate along the second dimension.

We can therefore recover the true singular value (`s`) from the observed
singular value (`s_obs`) using the following:
`s_obs^2 = s^2 + σ^2 (N + M)`. Finally, note how the contribution from the
noise, `σ^2 (N + M)`, is halfway between the upper and lower values of
`s_lim^2`: this is what we expect, as the signal is independent of the noise.
Therefore, the signal singular values each soak up what is an 'average' amount
of noise.

##### Structured noise, low rank approximations

While the theory is an excellent fit in the case of unstructured noise, we have
two key differences on real data. We typically have structured (but
predominantly subject-specific) artefacts, and we may take a low-rank
approximation to the individual data. The figures below recapitulate the above
results, but with both of these effects present.

![Marchenko-Pastur predictions](LowRank/mp_predictions.png)
![Estimated noise level](LowRank/estimated_sigma.png)

What is clear is that we overestimate `σ^2`, and this is for two reasons:
firstly, by truncating the subject data we only take the components with
singular values that are by chance larger than average; secondly, the
structured noise increases the variance of these components. We also marginally
underestimate the strength of the signal as some variance is left outside the
identified subject-level subspaces, but this effect is much smaller.

To what extent is this overestimation a problem in practice? This is something
of an empirical question, and it is by no means the only difference between
this and a 'true' subject model. For instance, there is the effect that the
subject decompositions are regularised by the group in PROFUMO, which makes the
real problem easier to some extent, and the fact that correction for `σ^2` adds
back in perfectly unstructured noise (whereas the real noise has structure, and
is therefore more challenging). However, the above should get us in the right
ballpark. Note also that there is a workaround of sorts: if we calculate
enough singular values that we go beyond the subspace of the structured noise
then we should get better estimates of the true unstructured noise level.


### Summary

We initialise the PROFUMO subject maps with an ICA decomposition of the group
basis, but calibrated to reflect the SNR at the subject level. The steps are:
 + Calculate the spatial basis from the concatenated data (including weighting
   by singular values): `U * diag(s) * V.T = [D_1, D_2, ..., D_S]`. We then
   discard (or, in practice, do not even calculate) the SVD components which we
   think represent the noise subspace.
 + Estimate noise, where the variance is determined by the last singular value
   we calculate and a correction for the Marchenko-Pastur distribution.
 + Correct signal singular values for amplification via the noise itself.
 + Add noise to this corrected basis, and decompose this, with a model that has
   relaxed priors on the form of the timecourses.
 + Initialise all subject maps with this decomposition.


### Simulation code

```python
M = 1000; N = 50; S = 100; C = 5
Ss = (np.arange(S) + 1)
structured_noise = False
low_rank = False

def rms(X):
    return np.sqrt(np.mean(X**2))

# Make a simple spatial basis
X = np.zeros([M, C])
for c in range(C):
    n_c = int(M / (C+1))  # Some empty voxels
    X[c*n_c:(c+1)*n_c, c] = np.random.rand(n_c)
plt.figure(); plt.imshow(X, vmin=-1.0, vmax=1.0); plt.colorbar()
plt.title("Spatial maps")

# Make the data (add structured and unstructured noise)
signal = X @ np.random.randn(C, N*S)
noise = 2.5 * np.random.randn(M, N*S)
if structured_noise:
    for i in range(S):
        noise[:,i*N:(i+1)*N] += (1.0 / math.sqrt(C)) * (
            np.random.randn(M, C) @ np.random.randn(C, N))
D = signal + noise
snr = rms(signal) / rms(noise)
print("SNR D: {:.2f}".format(snr))

# Run subject SVDs
if low_rank:
    k = 3  # Take k*C components
    plt.figure(); plt.xlim([-1, N])
    U,s,V = np.linalg.svd(D, full_matrices=False)
    lD = plt.plot(s / math.sqrt(S))
    svds = []
    for i in range(S):
        U,s,V = np.linalg.svd(D[:, i*N:(i+1)*N], full_matrices=False)
        svds.append(U[:,:k*C] * s[:k*C])
        lDs = plt.plot(s, color=[0.7,]*3, zorder=-1)
    svds = np.concatenate(svds, axis=-1)
    U,s,V = np.linalg.svd(svds, full_matrices=False)
    lU = plt.plot(s / math.sqrt(S))
    plt.xlabel("Component"); plt.ylabel("Singular value")
    plt.legend([lDs[0], lD[0], lU[0]], ["Subject", "Group", "Group (LR)"])
    plt.title("Group v subject singular values")

    D = svds; N_orig = N; N = k*C

    # N.B. Two reasons why this procedure enhances the estimated noise
    # variance: we only pick the strongest components, and these will also
    # preferentially contain structured noise. Below is an attempt to adjust
    # for the former, but it doesn't seem particularly effective...
    # Probably need to interpolate in `s**2` space
    s_min = np.abs(1.0 - np.sqrt(N_orig / M))
    s_max = np.abs(1.0 + np.sqrt(N_orig / M))
    s_avg = s_min + (s_max - s_min) * (1.0 - (N/2) / N_orig)
else:
    N_orig = N

# Compute Marchenko-Pastur max/min normalised singular values
mp_upper = np.abs(np.sqrt(M) + np.sqrt(Ss * N))
mp_lower = np.abs(np.sqrt(M) - np.sqrt(Ss * N))

# Run the SVDs and store key metrics
s_signal = np.zeros([S])
s_noise_in = np.zeros([S])
s_noise_out = np.zeros([S])
plt.figure()
lT = plt.plot(rms(noise) * mp_lower, 'k')
lT = plt.plot(rms(noise) * mp_upper, 'k')
lT = plt.plot(np.sqrt(
        rms(signal)**2 * (Ss * N_orig * M / C)
        + rms(noise)**2 * (Ss * N + M)
        ), 'k')
for i in range(S):
    U,s,V = np.linalg.svd(D[:, :(i+1)*N], full_matrices=False)
    lmax = plt.plot(i, s[C], 'C0+')
    lmin = plt.plot(i, s[-1], 'C1+')
    lsig = plt.plot(i, np.sqrt(np.mean(s[:C]**2)), 'C2+')
    
    # Separate signal / noise
    beta = np.linalg.pinv(X) @ U
    U_signal = X @ beta
    U_noise = U - U_signal
    
    #plt.figure(); plt.imshow(Us); plt.colorbar()
    #plt.figure(); plt.imshow(Us_signal); plt.colorbar()
    #plt.figure(); plt.imshow(Us_noise); plt.colorbar()
    
    # Signal v. noise in first C components
    s_signal[i]    = np.mean(s[:C]) * rms(U_signal[:,:C]) * np.sqrt(M)
    s_noise_in[i]  = np.mean(s[:C]) * rms(U_noise[:,:C]) * np.sqrt(M)
    # And noise immediately beyond that
    s_noise_out[i] = np.mean(s[C:2*C]) * rms(U_noise[:,C:2*C]) * np.sqrt(M)
plt.xlabel("Subjects"); plt.ylabel("Singular value")
plt.legend(
        [lT[0], lmax[0], lmin[0], lsig[0]],
        ["Theoretical", "Noise (max)", "Noise (min)", "Signal"])
plt.title("Marchenko-Pastur predictions")
#plt.tight_layout(); plt.savefig('mp_predictions.png')

plt.figure()
plt.plot(rms(signal)**2 * M * (Ss * N_orig) / C + rms(noise)**2 * (Ss * N), 'k')
plt.plot([Ss[0], Ss[-1]], [rms(noise)**2 * M]*2, 'k', label="Theoretical")
plt.plot(s_noise_in**2, '+', label="Noise (in)")
plt.plot(s_noise_out**2, '+', label="Noise (out)")
plt.plot(s_signal**2, '+', label="Signal")
#plt.xscale('log'); plt.yscale('log')
plt.xlabel("Subjects"); plt.ylabel("Squared singular value"); plt.legend()
plt.title("Signal/noise separation")
#plt.tight_layout(); plt.savefig('signal_noise.png')

plt.figure()
plt.plot(s_signal / s_noise_in, '+', label="SNR (in)")
plt.plot(s_signal / s_noise_out, '+', label="SNR (out)")
#plt.xscale('log'); plt.yscale('log')
plt.xlabel("Subjects"); plt.ylabel("SNR (std)"); plt.legend()
plt.title("SNR of signal/noise subspaces")
#plt.tight_layout(); plt.savefig('SNR.png')

plt.figure()
plt.plot([1, S], [rms(noise)]*2, 'k', label="True")
plt.plot(s_noise_out / mp_upper, '+', label="Estimated")
plt.xlabel("Subjects"); plt.ylabel("Noise std"); plt.legend()
plt.title("Estimated noise level")
#plt.tight_layout(); plt.savefig('estimated_sigma.png')
```
