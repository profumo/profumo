#!/usr/bin/env sh

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2019
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# generate_pdfs.sh
# Simple Makefile wrapper for generating documentation PDFs

for file in UpdateRules/*/*.tex; do
    make build file="${file}" TEXTRAS='-pv'
    make clean file="${file}"
done
