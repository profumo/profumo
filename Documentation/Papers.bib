% Encoding: UTF-8

@InProceedings{Bishop1999,
  author    = {Bishop, Christopher M.},
  title     = {Variational Principal Components},
  booktitle = {Ninth International Conference on Artificial Neural Networks: ICANN '99},
  year      = {1999},
  volume    = {1},
  pages     = {509--514},
  abstract  = {One of the central issues in the use of principal component analysis (PCA) for data modelling is that of choosing the appropriate number of retained components. This problem was recently addressed through the formulation of a Bayesian treatment of PCA in terms of a probabilistic latent variable model. A central feature of this approach is that the effective dimensionality of the latent space is determined automatically as part of the Bayesian inference procedure. In common with most non-trivial Bayesian models, however, the required marginalizations are analytically intractable, and so an approximation scheme based on a local Gaussian representation of the posterior distribution was employed. In this paper we develop an alternative, variational formulation of Bayesian PCA, based on a factorial representation of the posterior distribution. This approach is computationally efficient, and unlike other approximation schemes, it maximizes a rigorous lower bound on the marginal log probability of the observed data},
  doi       = {10.1049/cp:19991160},
  issn      = {0537-9989},
  keywords  = {Bayes method; Gaussian representation; covariance matrix; eigenvectors; lower bound; principal component analysis; probability; variational principal components; principal component analysis;},
}

@Book{Bishop2006,
  title     = {Pattern Recognition and Machine Learning},
  publisher = {Springer},
  year      = {2006},
  author    = {Bishop, Christopher M.},
  isbn      = {978-0387-31073-2},
  url       = {http://research.microsoft.com/en-us/um/people/cmbishop/prml/},
}

@InProceedings{Corduneanu2001,
  author    = {Corduneanu, A. and Bishop, Christopher M.},
  title     = {Variational {B}ayesian Model Selection for Mixture Distributions},
  booktitle = {Eighth International Conference on Artificial Intelligence and Statistics},
  year      = {2001},
  editor    = {T. Jaakkola and T. Richardson},
  pages     = {27--34},
  publisher = {Morgan Kaufmann},
  abstract  = {Mixture models, in which a probability distribution is represented as a linear superposition of component distributions, are widely used in statistical modelling and pattern recognition. One of the key tasks in the application of mixture models is the determination of a suitable number of components. Conventional approaches based on cross-validation are computationally expensive, are wasteful of data, and give noisy estimates for the optimal number of components. A fully Bayesian treatment, based on Markov chain Monte Carlo methods for instance, will return a posterior distribution over the number of components. However, in practical applications it is generally convenient, or even computationally essential, to select a single, most appropriate model. Recently it has been shown, in the context of linear latent variable models, that the use of hierarchical priors governed by continuous hyperparameters whose values are set by type-II maximum likelihood, can be used to optimize model complexity. In this paper we extend this framework to mixture distributions by considering the classical task of density estimation using mixtures of Gaussians. We show that, by setting the mixing coefficients to maximize the marginal log-likelihood, unwanted components can be suppressed, and the appropriate number of components for the mixture can be determined in a single training run without recourse to cross-validation. Our approach uses a variational treatment based on a factorized approximation to the posterior distribution.},
  url       = {http://research.microsoft.com/apps/pubs/default.aspx?id=67239},
}

@Article{Dawid1981,
  author   = {Dawid, A. P.},
  title    = {Some matrix-variate distribution theory: Notational considerations and a {B}ayesian application},
  journal  = {Biometrika},
  year     = {1981},
  volume   = {68},
  number   = {1},
  pages    = {265--274},
  abstract = {We introduce and justify a convenient notation for certain matrix-variate distributions which, by its emphasis on the important underlying parameters, and the theory on which it is based, eases greatly the task of manipulating such distributions. Important examples include the matrix-variate normal, t, F and beta, and the Wishart and inverse Wishart distributions. The theory is applied to compound matrix distributions and to Bayesian prediction in the multivariate linear model.},
  doi      = {10.1093/biomet/68.1.265},
  eprint   = {http://biomet.oxfordjournals.org/content/68/1/265.full.pdf+html},
  url      = {http://biomet.oxfordjournals.org/content/68/1/265.abstract},
}

@Book{Demmel1997,
  title     = {Applied Numerical Linear Algebra},
  publisher = {SIAM},
  year      = {1997},
  author    = {Demmel, James W.},
  comment   = {Chapter 4: Nonsymmetric Eigenvalue Problems},
}

@Article{MacKay1995,
  author   = {Mackay, David J. C.},
  title    = {Probable networks and plausible predictions---a review of practical {B}ayesian methods for supervised neural networks},
  journal  = {Network: Computation in Neural Systems},
  year     = {1995},
  volume   = {6},
  number   = {3},
  pages    = {469--505},
  abstract = {Bayesian probability theory provides a unifying framework for data modelling. In this framework the overall aims are to find models that are well-matched to the data, and to use these models to make optimal predictions. Neural network learning is interpreted as an inference of the most probable parameters for the model, given the training data. The search in model space (i.e., the space of architectures, noise models, preprocessings, regularizers and weight decay constants) can then also be treated as an inference problem, in which we infer the relative probability of alternative models, given the data. This review describes practical techniques based on Gaussian approximations for implementation of these powerful methods for controlling, comparing and using adaptive networks.},
  eprint   = {http://informahealthcare.com/doi/abs/10.1088/0954-898X_6_3_011},
  url      = {http://www.inference.phy.cam.ac.uk/mackay/network.pdf},
}

@Book{MacKayITILA,
  title     = {Information Theory, Inference and Learning Algorithms},
  publisher = {Cambridge University Press},
  year      = {2003},
  author    = {MacKay, David J. C.},
  url       = {http://www.inference.phy.cam.ac.uk/mackay/itila/},
}

@Article{McGrory2008,
  author   = {McGrory, C. A. and Titterington, D. M. and Reeves, R. and Pettitt, A. N.},
  title    = {Variational {B}ayes for estimating the parameters of a hidden {P}otts model},
  journal  = {Statistics and Computing},
  year     = {2008},
  volume   = {19},
  number   = {3},
  pages    = {329--340},
  month    = sep,
  issn     = {1573-1375},
  abstract = {Hidden Markov random field models provide an appealing representation of images and other spatial problems. The drawback is that inference is not straightforward for these models as the normalisation constant for the likelihood is generally intractable except for very small observation sets. Variational methods are an emerging tool for Bayesian inference and they have already been successfully applied in other contexts. Focusing on the particular case of a hidden Potts model with Gaussian noise, we show how variational Bayesian methods can be applied to hidden Markov random field inference. To tackle the obstacle of the intractable normalising constant for the likelihood, we explore alternative estimation approaches for incorporation into the variational Bayes algorithm. We consider a pseudo-likelihood approach as well as the more recent reduced dependence approximation of the normalisation constant. To illustrate the effectiveness of these approaches we present empirical results from the analysis of simulated datasets. We also analyse a real dataset and compare results with those of previous analyses as well as those obtained from the recently developed auxiliary variable MCMC method and the recursive MCMC method. Our results show that the variational Bayesian analyses can be carried out much faster than the MCMC analyses and produce good estimates of model parameters. We also found that the reduced dependence approximation of the normalisation constant outperformed the pseudo-likelihood approximation in our analysis of real and synthetic datasets.},
  doi      = {10.1007/s11222-008-9095-6},
}

@TechReport{Penny2001,
  author = {Penny, William D.},
  title  = {{K}ullback-{L}iebler Divergences of Normal, Gamma, {D}irichlet and {W}ishart Densities},
  year   = {2001},
  school = {Wellcome Department of Cognitive Neurology},
  url    = {http://www.fil.ion.ucl.ac.uk/~wpenny/mypubs/reports.html},
}

@Article{Potts1952,
  author    = {Potts, Renfrey B.},
  title     = {Some generalized order-disorder transformations},
  journal   = {Mathematical Proceedings of the Cambridge Philosophical Society},
  year      = {1952},
  volume    = {48},
  number    = {1},
  pages     = {106--109},
  month     = jan,
  abstract  = {In considering the statistics of the {\textquoteleft}no-field{\textquoteright} square Ising lattice in which each unit is capable of two configurations and only nearest neighbours interact, Kramers and Wannier were able to deduce an inversion transformation under which the partition function of the lattice is invariant when the temperature is transformed from a low to a high ({\textquoteleft}inverted{\textquoteright}) value. The important property of this inversion transformation is that its fixed point gives the transition point of the lattice.},
  address   = {Cambridge, UK},
  day       = {24},
  doi       = {10.1017/S0305004100027419},
  publisher = {Cambridge University Press},
  url       = {https://www.cambridge.org/core/article/some-generalized-order-disorder-transformations/5FD50240095F40BD123171E5F76CDBE0},
}

@InCollection{Stegle2011,
  author    = {Stegle, Oliver and Lippert, Christoph and Mooij, Joris and Lawrence, Neil and Borgwardt, Karsten},
  title     = {Efficient inference in matrix-variate {G}aussian models with iid observation noise},
  booktitle = {Advances in Neural Information Processing Systems 24},
  year      = {2011},
  editor    = {J. Shawe-Taylor and R.S. Zemel and P.L. Bartlett and F. Pereira and K.Q. Weinberger},
  pages     = {630--638},
  note      = {Scientific publication Twenty-Fifth Annual Conference on Neural Information Processing Systems, 12--17 December, 2011 Granada (Spain)},
  abstract  = {Inference in matrix-variate Gaussian models has major applications for multioutput prediction and joint learning of row and column covariances from matrixvariate data. Here, we discuss an approach for efficient inference in such models that explicitly account for iid observation noise. Computational tractability can be retained by exploiting the Kronecker product between row and column covariance matrices. Using this framework, we show how to generalize the Graphical Lasso in order to learn a sparse inverse covariance between features while accounting for a low-rank confounding covariance between samples. We show practical utility on applications to biology, where we model covariances with more than 100,000 dimensions. We find greater accuracy in recovering biological network structures and are able to better reconstruct the confounders.},
  url       = {http://papers.nips.cc/paper/4281-efficient-inference-in-matrix-variate-gaussian-models-with-iid-observation-noise},
}

@InCollection{Titsias2011,
  author    = {Titsias, Michalis and L{\'a}zaro-Gredilla, Miguel},
  title     = {Spike and Slab Variational Inference for Multi-Task and Multiple Kernel Learning},
  booktitle = {Advances in Neural Information Processing Systems 24},
  year      = {2011},
  editor    = {J. Shawe-Taylor and R.S. Zemel and P.L. Bartlett and F. Pereira and K.Q. Weinberger},
  pages     = {2339--2347},
  abstract  = {We introduce a variational Bayesian inference algorithm which can be widely applied to sparse linear models. The algorithm is based on the spike and slab prior which, from a Bayesian perspective, is the golden standard for sparse inference. We apply the method to a general multi-task and multiple kernel learning model in which a common set of Gaussian process functions is linearly combined with task-specific sparse weights, thus inducing relation between tasks. This model unifies several sparse linear models, such as generalized linear models, sparse factor analysis and matrix factorization with missing values, so that the variational algorithm can be applied to all these cases. We demonstrate our approach in multioutput Gaussian process regression, multi-class classisication, image processing applications and collaborative filtering.},
  url       = {http://papers.nips.cc/paper/4305-spike-and-slab-variational-inference-for-multi-task-and-multiple-kernel-learning},
}

@Article{Wishart1928,
  author  = {Wishart, John},
  title   = {The Generalised Product Moment Distribution in Samples from a Normal Multivariate Population},
  journal = {Biometrika},
  year    = {1928},
  volume  = {20A},
  pages   = {32--52},
  month   = jul,
  doi     = {10.2307/2331939},
  url     = {http://www.jstor.org/stable/2331939},
}

@Comment{jabref-meta: databaseType:bibtex;}

@Comment{jabref-meta: saveOrderConfig:specified;bibtexkey;false;author;false;year;false;}
