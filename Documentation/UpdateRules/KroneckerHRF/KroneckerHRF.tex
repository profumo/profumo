% For TeXShop, TeXWorks, etc
% !TEX TS-program = LuaLaTeX
% !TEX encoding = UTF-8 Unicode
% !TEX spellcheck = en-GB

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preamble {{{

\documentclass[a4paper, 11pt]{lt_article}

\usepackage{lt_basics}
\usepackage{lt_bib}
\usepackage{lt_floats}
\usepackage{lt_maths}
\usepackage{lt_utilities}
\usepackage{lt_hyperref}

\title{Kronecker HRF Update Rules}
\author{Sam Harrison}
\date{\today}
\header{PROFUMO: Kronecker HRF}

\addbibresource{../../../Documentation/Papers.bib}

% Compatibility with legacy commands
\newcommand*{\bm}[1]{\symbf{#1}}
\newcommand*{\p}{p}
\newcommand*{\q}{q}

\renewcommand*{\expec}[1]{\bigl\langle #1 \bigr\rangle}
\renewcommand*{\expecWRT}[2]{\bigl\langle #1 \bigr\rangle_{#2}}

% }}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

% Generate the title
\maketitle

% Disable microtype locally so table entries line up properly
\microtypesetup{protrusion=false}
\tableofcontents
\microtypesetup{protrusion=true}

\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Distributions} % {{{

%------------------------------------------------------------------------------
\subsection{Matrix normal distribution} % {{{
The matrix normal distribution is a generalisation of the multivariate normal distribution to matrices, for certain covariance structures.
It is used when the covariance structure for the whole matrix can be expressed as the Kronecker product of covariance matrices over the rows and columns of the matrix \autocite{Dawid1981}.

If the random matrix $\bm{X} \in \mathbb{R}^{m \times n}$ follows a matrix normal distribution, with mean $\bm{M} \in \mathbb{R}^{m \times n}$, column covariance $\bm{Σ} \in \mathbb{R}^{m \times m}$ and row covariance $\bm{Ω} \in \mathbb{R}^{n \times n}$, then the following hold:
%Notation
\begin{equation*}
\begin{split}
\bm{X} \sim & \dist{ \matrixNormalDist }{ \bm{M}, \, \bm{Ω}, \, \bm{Σ} } \\
\func{ \vecOp }{ \bm{X} } \sim & \dist{ \normalDist }{ \func{ \vecOp }{ \bm{M} }, \, \bm{Σ} \otimes \bm{Ω} } \\
\end{split}
\end{equation*}

%PDF
\begin{equation*}
\cDist{ \p }{ \bm{X} }{ \bm{M}, \bm{Ω}, \bm{Σ} } = \frac{ \exp \Bigl( -\frac{1}{2} \trace \bigl( \bm{Ω}^{-1} (\bm{X} - \bm{M})^{T} \bm{Σ}^{-1} (\bm{X} - \bm{M}) \bigr) \Bigr)}{(2π)^{\frac{mn}{2}} | \bm{Σ} |^{\frac{n}{2}} | \bm{Ω} |^{\frac{m}{2}}}
\end{equation*}

%Expectations
\begin{equation*}
\expec{ \bm{X} } = \bm{M}
\end{equation*}
\begin{equation*}
\expec{ \bm{X}^{T} \bm{A} \bm{X} } = \bm{M}^{T} \bm{A} \bm{M} +  \bm{Ω} \func{ \trace }{ \bm{Σ} \bm{A} }
\end{equation*}
\begin{equation*}
\expec{ \bm{X} \bm{B} \bm{X}^{T} } = \bm{M} \bm{B} \bm{M}^{T} +  \bm{Σ} \func{ \trace }{ \bm{Ω} \bm{B} }
\end{equation*}
\begin{equation*}
\expec{ \func{ \trace }{ \bm{B} \bm{X}^{T} \bm{A} \bm{X} } } = \func{ \trace }{ \bm{B} \expec{  \bm{X}^{T} \bm{A} \bm{X} } } = \func{ \trace }{ \expec{  \bm{X} \bm{B} \bm{X}^{T} } \bm{A} }
\end{equation*}

% }}}
%------------------------------------------------------------------------------

% }}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\texorpdfstring{$\bm{A}$}{A} updates} % {{{

%Why distribution over At
For computational reasons, we define the prior over $\bm{A}^{\transpose}$ rather than over $\bm{A}$ directly.
This does not change the form of any of the distributions, but gives the posterior covariance matrix a more convenient block structure.
With this formulation, the blocks of the matrix are defined by full time courses, rather than the set of all mode activities at one time point---intuitively, $\func{ \vecOp }{\bm{A}^{\transpose} }$ concatenates time courses rather than time points.

% In general need sum over multiple data
The update rules are only sketched in outline to try and keep the notation relatively simple.
However, in general, we may wish to infer $\bm{A}$ from a range of observations\footnote{The obvious example is multiple subjects doing the same task and therefore having equivalent time courses.}, in which case there may be multiple sets of $ψ$, $\bm{P}$ and $\bm{D}$.
Rather than writing the sum over these sets of variables out in full each time, we will keep these implicit.
Note that this is why $ψ$ does not always commute, even though it is a scalar---in general, it is paired with a set of $\bm{P}$ and $\bm{D}$.

%Prior
\begin{equation}
\begin{split}
\dist{ \p }{ \bm{A} } & = \cDist{ \matrixNormalDist }{ \bm{A}^{\transpose} }{ \bm{0}, \, \bm{K}_{\bm{A}}, \, \bm{α}^{-1} } \\
\dist{ \p }{ \func{ \vecOp }{ \bm{A}^{\transpose} } } & = \dist{ \normalDist }{ \func{ \vecOp }{ \bm{0} }, \, \bm{α}^{-1} \otimes \bm{K}_{\bm{A}} }
\end{split}
\label{A:prior}
\end{equation}

%Posterior
\begin{equation}
\begin{split}
% q(A)
\dist{ \q }{ \func{ \vecOp }{ \bm{A}^{\transpose} } } & = \dist{ \normalDist }{ \func{ \vecOp }{ \bm{M}_{\bm{A}}^{\transpose} }, \, \bm{Σ}_{\bm{A}} } \\[5pt]
% Posterior cov
\bm{Σ}_{\bm{A}} & = \Bigl( \expec{  \bm{α} } \otimes \bm{K}_{\bm{A}}^{-1} + \expec{ \bm{P}^{\transpose} \bm{P} } \otimes \expec{ ψ } \bm{I}_{T} \Bigr)^{-1} \\[5pt]
% Posterior mean
\func{ \vecOp }{ \bm{M}_{\bm{A}}^{\transpose} } & = \bm{Σ}_{\bm{A}} \vecOp\Bigl( \expec{ ψ } \bm{D}^{\transpose} \expec{ \bm{P} } \Bigr)
\end{split}
\label{A:posterior}
\end{equation}

%KL divergence
\begin{equation}
\begin{split}
% Full form
\DKL{ \dist{ \q }{ \bm{A} } }{ \dist{ \p }{ \bm{A} } \bigr) } = & \,\, \frac{1}{2} \left\langle \ln \left( \frac{ \determinant{ \bm{α}^{-1} \otimes \bm{K}_{\bm{A}} } }{ \determinant{ \bm{Σ}_{\bm{A}} } } \right) \right\rangle
+ \frac{1}{2} \left\langle \trace \Bigl( \bigl( \bm{α}^{-1} \otimes \bm{K}_{\bm{A}} \bigr)^{-1} \bm{Σ}_{\bm{A}} \Bigr) \right\rangle \\
& + \frac{1}{2} \func{ \vecOp }{ \bm{M}_{\bm{A}}^{\transpose} }^{\transpose} \left\langle \bigl( \bm{α}^{-1} \otimes \bm{K}_{\bm{A}} \bigr)^{-1} \right\rangle \func{ \vecOp }{ \bm{M}_{\bm{A}}^{\transpose} }
- \frac{TM}{2} \\[10pt]
% Simplified
= & \,\, \frac{M}{2} \logdet{ \bm{K}_{\bm{A}} } - \frac{T}{2} \expec{ \logdet{ \bm{α} } } - \frac{1}{2} \logdet{  \bm{Σ}_{\bm{A}} } \\
& + \frac{1}{2} \trace \Bigl( \bigl( \expec{ \bm{α} } \otimes \bm{K}_{\bm{A}}^{-1} \bigr) \bm{Σ}_{\bm{A}} \Bigr) \\
& + \frac{1}{2} \trace \Bigl( \expec{ \bm{α} } \bm{M}_{\bm{A}} \bm{K}_{\bm{A}}^{-1} \bm{M}_{\bm{A}}^{\transpose} \Bigr)
- \frac{TM}{2} \\[10pt]
% In expectation form
= & \,\, \frac{M}{2} \logdet{ \bm{K}_{\bm{A}} } - \frac{T}{2} \expec{ \logdet{ \bm{α} } } - \frac{1}{2} \logdet{  \bm{Σ}_{\bm{A}} } \\
& + \frac{1}{2} \trace \Bigl( \expec{ \bm{α} } \expec{ \bm{A} \bm{K}_{\bm{A}}^{-1} \bm{A}^{\transpose} } \Bigr) - \frac{TM}{2} \\
\end{split}
\label{A:KL}
\end{equation}

% }}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\texorpdfstring{$\bm{α}$}{Alpha} updates} % {{{

This gives the loose form of the updates necessary to infer a posterior covariance across modes.
The precise way the information from different time courses is pooled to infer the covariance is arbitrary---the updates are simply provided to illustrate the expectations we need to calculate.

%Prior
\begin{equation}
\dist{ \p }{ \bm{α} } = \dist{ \wishartDist{M} }{ a_{\bm{α}}, \bm{B}_{\bm{α}} }
\label{alpha:prior}
\end{equation}

%Posterior
\begin{equation}
\begin{split}
\dist{ \q }{ \bm{α} } & = \dist{ \wishartDist{M} }{ c_{\bm{α}}, \bm{D}_{\bm{α}} } \\[5pt]
%Posterior c
c_{\bm{α}} & = b_{\bm{α}} + T \sum_{s=1}^{S} R(s)  \\[5pt]
%Posterior d
\bm{D}_{\bm{α}} & = \bm{B}_{\bm{α}} + \sum_{s=1}^{S} \sum_{r=1}^{R(s)} \expec{ \bm{A}^{(sr)} \bm{K}_{\bm{A}}^{-1} (\bm{A}^{(sr)})^{\transpose} }
\end{split}
\label{alpha:posterior}
\end{equation}

% }}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Computational approach} % {{{

The above demonstrate that we need to be able to manipulate $\bm{Σ}_{\bm{A}}$, the posterior covariance matrix over all elements in $\bm{A}$.
However, this is a square matrix of side $MT$.
For even relatively modest values, say $M=100$ and $T=1000$, this contains over five billion unique entries, far too large for a na\"ive computational approach.

In this section we detail how it is possible to utilise the structure of this matrix to extract all the necessary expectations, without ever forming the full matrix.

% Need Sigma to:
We need to calculate four separate quantities using $\bm{Σ}_{\bm{A}}$.
% Calculate Ma
Firstly, it is used in \Ref{Equation}{A:posterior} to calculate the posterior mean of $\bm{A}$.
% Calculate <AAt>
Secondly, we need it to calculate $\expec{ \bm{A} \bm{A}^{\transpose} }$, which is needed by the other parts of the matrix factorisation model\footnote{See the separate document on VB inference of matrix factorisation models for more details.}.
% Calculate <AiKAt>
Thirdly, the update rules for $\bm{α}$ and the free energy require $\expec{ \bm{A} \bm{K}_{\bm{A}}^{-1} \bm{A}^{\transpose} }$.
% Determinant for F
Finally, we need its determinant, again for the free energy.

%------------------------------------------------------------------------------
\subsection{Eigendecomposition of \texorpdfstring{$\bm{Σ}_{\bm{A}}$}{SigmaA} using the Kronecker structure} % {{{

For two real, symmetric matrices with accompanying eigendecompositions, $\bm{X} = \bm{U}_{\bm{X}} \bm{\Lambda}_{\bm{X}} \bm{U}_{\bm{X}}^{\transpose}$ and $\bm{Y} = \bm{U}_{\bm{Y}} \bm{\Lambda}_{\bm{Y}} \bm{U}_{\bm{Y}}^{\transpose}$, it is straightforward to show that the Kronecker product takes the form
\begin{equation*}
\bm{X} \otimes \bm{Y} = \bigl( \bm{U}_{\bm{X}} \otimes \bm{U}_{\bm{Y}} \bigr) \bigl( \bm{\Lambda}_{\bm{X}} \otimes \bm{\Lambda}_{\bm{Y}} \bigr) \bigl( \bm{U}_{\bm{X}}^{\transpose} \otimes \bm{U}_{\bm{Y}}^{\transpose} \bigr)
\end{equation*}

The other property we require is a general one of eigendecompositions:
\begin{equation*}
\bm{X} + c \bm{I} = \bm{U}_{\bm{X}} \bigl( \bm{\Lambda}_{\bm{X}} + c \bm{I} \bigr) \bm{U}_{\bm{X}}^{\transpose}
\end{equation*}

As \textcite{Stegle2011} noted, it is possible to utilise these two properties for very efficient solutions of problems involving matrix normal distributions.
The combination of the two yields the following equation, which we can use to invert $\bm{Σ}_{\bm{A}}$.
\begin{equation*}
\bigl( \bm{X} \otimes \bm{Y} + c \bm{I} \bigr)^{-1} = \bigl( \bm{U}_{\bm{X}} \otimes \bm{U}_{\bm{Y}} \bigr) \bigl( \bm{\Lambda}_{\bm{X}} \otimes \bm{\Lambda}_{\bm{Y}} + c \bm{I} \bigr)^{-1} \bigl( \bm{U}_{\bm{X}}^{\transpose} \otimes \bm{U}_{\bm{Y}}^{\transpose} \bigr)
\end{equation*}

Finally, we will also make use of the following property of the matrix inverse:
\begin{equation*}
\big( \bm{X} + \bm{Y} \big)^{-1} = \bm{X}^{-1} \big( \bm{I} + \bm{Y} \bm{X}^{-1} \big)^{-1}
\end{equation*}

We can use the properties listed above to rearrange the formula for $\bm{Σ}_{\bm{A}}$ (\Ref{Equation}{A:posterior}) to give:
\begin{equation*}
\begin{split}
%Original form
\bm{Σ}_{\bm{A}} = & \, \Bigl( \expec{ \bm{α} } \otimes \bm{K}_{\bm{A}}^{-1} + \expec{ \bm{P}^{\transpose} \bm{P} } \otimes \expec{ ψ } \bm{I}_{T} \Bigr)^{-1} \\
%Take matrix out
= & \, \Bigl( \expec{ \bm{α} } \otimes \bm{K}_{\bm{A}}^{-1} \Bigr)^{-1}
\biggl( \Bigl( \expec{ \bm{P}^{\transpose} \bm{P} } \otimes \expec{ ψ } \bm{I}_{T} \Bigr) \Bigl( \expec{ \bm{α} } \otimes \bm{K}_{\bm{A}}^{-1} \Bigr)^{-1} + \bm{I}_{MT} \biggr)^{-1} \\
%Multiply Kronecker's together
= & \, \Bigl( \expec{ \bm{α} }^{-1} \otimes \bm{K}_{\bm{A}} \Bigr)
\biggl( \Bigl( \bigl( \expec{ ψ } \expec{ \bm{P}^{\transpose} \bm{P} } \bigr) \expec{ \bm{α} }^{-1} \Bigr) \otimes \bm{K}_{\bm{A}} + \bm{I}_{MT} \biggr)^{-1} \\
\end{split}
\end{equation*}
Note that we pulled $\expec{ \bm{α} } \otimes \bm{K}_{\bm{A}}^{-1}$ out of the main inverse, rather than $\expec{ \bm{P}^{\transpose} \bm{P} } \otimes \expec{ ψ } \bm{I}_{T}$. While the latter appears to be simpler, it is actually much less well conditioned numerically (e.g.~$\expec{ \bm{P}^{\transpose} \bm{P} }$ can become hard to invert if maps are eliminated by the implicit model complexity penalties).

Finally, we denote the relevant eigendecompositions as 
% K
$ \bm{K}_{\bm{A}} = \bm{U}_{\bm{K}} \bm{\Lambda}_{\bm{K}} \bm{U}_{\bm{K}}^{\transpose} $
% PtP
%$ \expec{ \bm{P}^{\transpose} \bm{P} } = \bm{U}_{\bm{P}} \bm{\Lambda}_{\bm{P}} \bm{U}_{\bm{P}}^{\transpose} $
% alpha
%$ \expec{ \bm{α} } = \bm{U}_{\bm{α}} \bm{\Lambda}_{\bm{α}} \bm{U}_{\bm{α}}^{\transpose} $ and
% alpha PtP
and $ \bigl( \expec{ ψ } \expec{ \bm{P}^{\transpose} \bm{P} } \bigr) \expec{ \bm{α} }^{-1} = \bm{U}_{\bm{P} \bm{α}} \bm{\Lambda}_{\bm{P} \bm{α}} \bm{U}_{\bm{P} \bm{α}}^{-1} $
These can now be combined with the equation above to yield our \enquote{simplified} form of $\bm{Σ}_{\bm{A}}$:
%Just substitute eigen stuff in
\begin{equation}
\begin{split}
\bm{Σ}_{\bm{A}} = & \, \Bigl( \expec{ \bm{α} }^{-1} \otimes \bm{K}_{\bm{A}} \Bigr)
\Bigl( \bm{U}_{\bm{P} \bm{α}} \otimes \bm{U}_{\bm{K}} \Bigr) \Bigl( \bm{\Lambda}_{\bm{P} \bm{α}} \otimes \bm{\Lambda}_{\bm{K}} + \bm{I}_{MT} \Bigr)^{-1} \Bigl( \bm{U}_{\bm{P} \bm{α}}^{-1} \otimes \bm{U}_{\bm{K}}^{\transpose} \Bigr) \\
\end{split}
\label{eigenSigma}
\end{equation}

Now we have expressed $\bm{Σ}_{\bm{A}}$ in terms of the eigendecompositions of its constituents it is possible to simplify the formulation of the necessary expectations.
Furthermore, it is also possible to precompute the eigendecomposition of $\bm{K}_{\bm{A}}$, and the decompositions of the various $M \times M$ matrices should be relatively inexpensive.
This is especially important as, for most problems we are interested in, $T \gg M$.



\subsubsection{Eigendecompositions of non-symmetric matrices}
%http://math.stackexchange.com/questions/326944/evaluating-eigenvalues-of-a-product-of-two-positive-definite-matrices
%http://mathoverflow.net/questions/106191/eigenvalues-of-product-of-two-symmetric-matrices
The representation of $\bm{Σ}_{\bm{A}}$ in \Ref{Equation}{eigenSigma} relies on the eigendecomposition of $\bigl( \expec{ ψ } \expec{ \bm{P}^{\transpose} \bm{P} } \bigr) \expec{ \bm{α} }^{-1}$.
In general, this is not symmetric and, as such, it has slightly more complex properties than the other eigendecompositions we make use of.
However, it is possible to show that the eigenvalues will still be real, though the eigenvectors are not guaranteed to be orthogonal.
This is useful as it means that it is not necessary to consider the general case of complex matrices when doing the various computations.

To show this, we will use the concept of matrix similarity.
Two matrices, $\bm{X}$ and $\bm{Y}$, are considered similar if there exists an invertible matrix $\bm{P}$ such that $\bm{Y} = \bm{P}^{-1} \bm{X} \bm{P}$.
If this holds, then the matrices share a number of properties, including properties of the characteristic polynomial (and hence eigenvalues).

Consider two real, positive semidefinite matrices, $\bm{X}$ and $\bm{Y}$.
Let $\bm{Z} = \bm{P}^{-1} \bm{X} \bm{Y} \bm{P}$, and take $\bm{P} = \bm{Y}^{-\frac{1}{2}}$ (this exists, and is positive semidefinite, because $\bm{Y}$ is positive semidefinite).
Rearranging gives that $\bm{Z} = \bm{Y}^{\frac{1}{2}} \bm{X} \bm{Y}^{\frac{1}{2}}$, which is clearly symmetric.
In other words, the product of two real, positive semidefinite matrices is similar to a real, symmetric matrix, so will have real eigenvalues.



\subsubsection{Computational stability}
All of the above calculations have assumed that all the matrices of interest are invertible.
Is this guaranteed to be the case?

In theory, if the priors are suitably specified then they should guarantee that the posteriors remain well-conditioned.
For example, note how in \Ref{Equation}{alpha:posterior} if $\bm{B}_{\bm{α}}$ is full rank then $\bm{D}_{\bm{α}}$ will be too.
We might reasonably hope that the priors are not pathologically specified in this regard---though it may not be wise to rely on this.

However, it is still likely that we will run into computational issues, even with well-specified priors.
Many of the models we are concerned with will have complex hierarchies of hyperpriors, and will, for example, include ARD priors that aim to eliminate components if they are not supported by the data \autocite{MacKay1995}.
As such, it is very likely that we will encounter situations where some of these matrices are not invertible because there is such a wide range of component amplitudes.

Therefore, it is vital that the computational routines used to calculate all the necessary expectations are made suitably robust.
\textit{Caveat emptor}.

% }}}
%------------------------------------------------------------------------------
\subsection{Calculation of expectations} % {{{



\subsubsection{Vector multiplication}
The posterior mean requires $\bm{Σ}_{\bm{A}}$ to be multiplied by a vector.
To do this we will require the following:
\begin{equation*}
\bigl( \bm{X} \otimes \bm{Y} \bigr) \func{ \vecOp }{ \bm{Z} } = \func{ \vecOp }{ \bm{Y} \bm{Z} \bm{X}^{\transpose} }
\end{equation*}

Using \Ref{Equation}{eigenSigma}, the simplified vector multiplication operation, for $\bm{Z} \in \mathbb{R}^{T \times M}$, is
\begin{equation*}
\begin{split}
\bm{Σ}_{\bm{A}} \func{ \vecOp }{ \bm{Z} } = & \, 
\bigl( \expec{ \bm{α} }^{-1} \otimes \bm{K}_{\bm{A}} \bigr)
\bigl( \bm{U}_{\bm{P} \bm{α}} \otimes \bm{U}_{\bm{K}} \bigr) \,\, \ldots \\
& \qquad \ldots \,\, \bigl( \bm{\Lambda}_{\bm{P} \bm{α}} \otimes \bm{\Lambda}_{\bm{K}} + \bm{I}_{MT} \bigr)^{-1} 
\func{ \vecOp }{ \bm{U}_{\bm{K}}^{\transpose} \bm{Z} \bm{U}_{\bm{P} \bm{α}}^{-\transpose} } \\
\end{split}
\end{equation*}

As the eigenvalue matrix is diagonal, we can simplify this further---intuitively, we should be able to collapse the multiplication of a vector by a diagonal matrix into something more computationally convenient.
For notational purposes, let $\bm{L} \in \mathbb{R}^{M \times T}$ be a rearrangement of this eigenvector matrix, such that 
\begin{equation*}
\bm{L}(m,t) = \frac{ 1 }{ \bm{\Lambda}_{\bm{P} \bm{α}}(m) \bm{\Lambda}_{\bm{K}}(t) + 1 } \\
\end{equation*}
Using $\circ$ to represent the Hadamard product, we can continue simplifying the equation above as
\begin{equation*}
\begin{split}
%Add L
\bm{Σ}_{\bm{A}} \func{ \vecOp }{ \bm{Z} } = & \, 
\bigl( \expec{ \bm{α} }^{-1} \otimes \bm{K}_{\bm{A}} \bigr)
\bigl( \bm{U}_{\bm{P} \bm{α}} \otimes \bm{U}_{\bm{K}} \bigr) 
\vecOp \Bigl( \bm{L}^{\transpose} \circ ( \bm{U}_{\bm{K}}^{\transpose} \bm{Z} \bm{U}_{\bm{P} \bm{α}}^{-\transpose} ) \Bigr) \\
% Chomp Kronecker matrix to the left
= & \, \bigl( \expec{ \bm{α} }^{-1} \otimes \bm{K}_{\bm{A}} \bigr)
\vecOp \Bigl( \bm{U}_{\bm{K}} \bigl( \bm{L}^{\transpose} \circ ( \bm{U}_{\bm{K}}^{\transpose} \bm{Z} \bm{U}_{\bm{P} \bm{α}}^{-\transpose} ) \bigr) \bm{U}_{\bm{P} \bm{α}}^{\transpose} \Bigr) \\
% And again
= & \, \vecOp \Bigl( \bm{U}_{\bm{K}} \bm{\Lambda}_{\bm{K}} \bigl( \bm{L}^{\transpose} \circ ( \bm{U}_{\bm{K}}^{\transpose} \bm{Z} \bm{U}_{\bm{P} \bm{α}}^{-\transpose} ) \bigr) \bm{U}_{\bm{P} \bm{α}}^{\transpose} \expec{ \bm{α} }^{-1} \Bigr) \\
\end{split}
\end{equation*}

This can be further simplified with a tweak to the eigenvector matrix, such that 
\begin{equation*}
\bm{L}_{\bm{K}}(m,t) = \frac{ \bm{\Lambda}_{\bm{K}}(t) }{ \bm{\Lambda}_{\bm{P} \bm{α}}(m) \bm{\Lambda}_{\bm{K}}(t) + 1 } \\
\end{equation*}

Finally, we can substitute the correct terms in from \Ref{Equation}{A:posterior} to give the calculation for the posterior mean.
\begin{equation}
\begin{split}
\func{ \vecOp }{ \bm{M}_{\bm{A}}^{\transpose} } = & \, \vecOp \Bigl( \bm{U}_{\bm{K}} \bigl( \bm{L}_{\bm{K}}^{\transpose} \circ ( \bm{U}_{\bm{K}}^{\transpose} \bm{D}^{\transpose} \expec{ \bm{P} } \expec{ ψ } \bm{U}_{\bm{P} \bm{α}}^{-\transpose} ) \bigr) \bm{U}_{\bm{P} \bm{α}}^{\transpose} \expec{ \bm{α} }^{-1} \Bigr) \\
 %Remove vec and transpose
\bm{M}_{\bm{A}} = & \, \expec{ \bm{α} }^{-1} \bm{U}_{\bm{P} \bm{α}} \bigl( \bm{L}_{\bm{K}} \circ 
( \bm{U}_{\bm{P} \bm{α}}^{-1} \expec{ ψ } \expec{ \bm{P}^{\transpose} } \bm{D} \bm{U}_{\bm{K}} ) \bigr)
 \bm{U}_{\bm{K}}^{\transpose}
 \end{split}
\label{A:postM_eig}
\end{equation}

While this looks daunting, note how it is in essence a standard eigendecomposition.
We project $\expec{ ψ } \expec{ \bm{P}^{\transpose} } \bm{D}$ through the two eigenvalue matrices, modify the eigenvectors using $\bm{L}_{\bm{K}}$, and then project back via the eigenvectors (with some adjustment via $\bigl( \expec{ ψ } \expec{ \bm{P}^{\transpose} \bm{P} } \bigr)^{-1}$).



\subsubsection{Blockwise trace}
It turns out that the contribution of the posterior covariance to $\expec{ \bm{A} \bm{A}^{\transpose} }$ can be formulated as the trace of different blocks of $\bm{Σ}_{\bm{A}}$.%, each of size $T \times T$.
If we denote the $i$,$j$th block of $\bm{Σ}_{\bm{A}}$ as $\bm{Σ}_{\bm{A}}^{[i,j]} \in \mathbb{R}^{T \times T}$, then it turns out we can simplify the expression for the expectation of the $i$th mode's time course when multiplied by that of the $j$th mode is:
\begin{equation}
\begin{split}
\expec{ \bm{A} \bm{A}^{\transpose} } (i,j) & = \sum_{t=1}^{T} \bm{M}_{\bm{A}}(i,t) \bm{M}_{\bm{A}}(j,t) 
+ \sum_{t=1}^{T} \bm{Σ}_{\bm{A}} \bigl( (i-1)T+t, (j-1)T+t \bigr) \\
 & = \sum_{t=1}^{T} \bm{M}_{\bm{A}}(i,t) \bm{M}_{\bm{A}}(j,t) +  \func{ \trace }{ \bm{Σ}_{\bm{A}}^{[i,j]} }
\end{split}
\label{AAt_form}
\end{equation}

The expression for blockwise matrix multiplication has a similar form to the standard expression in terms of indices, so
\begin{equation*}
\bm{Z} = \bm{X} \bm{Y} \qquad 
\bm{Z}(i,j) = \sum_{k} \bm{X}(i,k) \bm{Y}(k,j) \qquad
\bm{Z}^{[i,j]} = \sum_{k} \bm{X}^{[i,k]} \bm{Y}^{[k,j]}
\end{equation*}
provided the blocks are sized appropriately.
Similarly, there is obviously a profound relationship between these blockwise respresentations and the Kronecker product.
For $\bm{X} \in \mathbb{R}^{M \times M}$ and $\bm{Y} \in \mathbb{R}^{N \times N}$, then $ \bm{Z} = \bm{X} \otimes \bm{Y} $ implies $ \bm{Z}^{[i,j]} \in \mathbb{R}^{N \times N} = \bm{X}(i,j) \bm{Y} $.

We can use \Ref{Equation}{eigenSigma} to get an explicit formula for each of the blocks of $\bm{Σ}_{\bm{A}}$:
\begin{equation*}
\begin{split}
%Express in block form
\bm{Σ}_{\bm{A}}^{[i,j]} = & \, \sum_{k1} \sum_{k2} \sum_{k3} \bigl( \expec{ \bm{α} }^{-1} \otimes \bm{K}_{\bm{A}} \bigr)^{[i,k1]}
\bigl( \bm{U}_{\bm{P} \bm{α}} \otimes \bm{U}_{\bm{K}} \bigr)^{[k1,k2]} \,\, \ldots \\
& \qquad \ldots \,\, \Bigl( \bigl( \bm{\Lambda}_{\bm{P} \bm{α}} \otimes \bm{\Lambda}_{\bm{K}} + \bm{I}_{MT} \bigr)^{-1} \Bigr)^{[k2,k3]}
 \bigl( \bm{U}_{\bm{P} \bm{α}}^{-1} \otimes \bm{U}_{\bm{K}}^{\transpose} \bigr)^{[k3,j]} \\
 %Collapse diagonal
 = & \, \sum_{k1} \sum_{k2} \bigl( \expec{ \bm{α} }^{-1} \otimes \bm{K}_{\bm{A}} \bigr)^{[i,k1]}
\bigl( \bm{U}_{\bm{P} \bm{α}} \otimes \bm{U}_{\bm{K}} \bigr)^{[k1,k2]} \,\, \ldots \\
& \qquad \ldots \,\, \Bigl( \bigl( \bm{\Lambda}_{\bm{P} \bm{α}} \otimes \bm{\Lambda}_{\bm{K}} + \bm{I}_{MT} \bigr)^{-1} \Bigr)^{[k2,k2]}
 \bigl( \bm{U}_{\bm{P} \bm{α}}^{-1} \otimes \bm{U}_{\bm{K}}^{\transpose} \bigr)^{[k2,j]} \\
 %Use Kronecker structure
 = & \, \sum_{k1} \sum_{k2} \Bigl( \expec{ \bm{α} }^{-1}(i,k1) \Bigr)
\Bigl( \bm{U}_{\bm{P} \bm{α}}(k1,k2) \Bigr) \Bigl( \bm{U}_{\bm{P} \bm{α}}^{-1}(k2,j) \Bigr) \,\, \ldots \\
& \qquad \ldots \,\, \bm{U}_{\bm{K}} \bm{\Lambda}_{\bm{K}} \Bigl( \bigl( \bm{\Lambda}_{\bm{P} \bm{α}} \otimes \bm{\Lambda}_{\bm{K}} + \bm{I}_{MT} \bigr)^{-1} \Bigr)^{[k2,k2]}
  \bm{U}_{\bm{K}}^{\transpose} \\
\end{split}
\end{equation*}

Taking the trace over the blocks of $\bm{Σ}_{\bm{A}}$ allows us to simplify the expression in the above equation.
This gives:
\begin{equation*}
\begin{split}
%Take trace
 \trace \bigl( \bm{Σ}_{\bm{A}}^{[i,j]}  \bigr)  = & \, \sum_{k1} \sum_{k2} \Bigl( \expec{ \bm{α} }^{-1}(i,k1) \Bigr)
\Bigl( \bm{U}_{\bm{P} \bm{α}}(k1,k2) \Bigr) \Bigl( \bm{U}_{\bm{P} \bm{α}}^{-1}(k2,j) \Bigr) \,\, \ldots \\
& \qquad \ldots \,\, \trace \Bigl( \bm{U}_{\bm{K}} \bm{\Lambda}_{\bm{K}} \Bigl( \bigl( \bm{\Lambda}_{\bm{P} \bm{α}} \otimes \bm{\Lambda}_{\bm{K}} + \bm{I}_{MT} \bigr)^{-1} \Bigr)^{[k2,k2]} \bm{U}_{\bm{K}}^{\transpose} \Bigr) \\
% Remove U_K
= & \, \sum_{k1} \sum_{k2} \Bigl( \expec{ \bm{α} }^{-1}(i,k1) \Bigr)
\Bigl( \bm{U}_{\bm{P} \bm{α}}(k1,k2) \Bigr) \Bigl( \bm{U}_{\bm{P} \bm{α}}^{-1}(k2,j) \Bigr) \,\, \ldots \\
& \qquad \ldots \,\, \trace \Bigl( \bm{\Lambda}_{\bm{K}} \Bigl( \bigl( \bm{\Lambda}_{\bm{P} \bm{α}} \otimes \bm{\Lambda}_{\bm{K}} + \bm{I}_{MT} \bigr)^{-1} \Bigr)^{[k2,k2]} \Bigr) \\
\end{split}
\end{equation*}

%Use L again to simplify diagonal terms and express as full matrix multiplication
At this point we can use this form to calculate each of the elements of $\expec{ \bm{A} \bm{A}^{\transpose} }$.
However, we can turn the above back into a matrix multiplication, and calculate all of $\expec{ \bm{A} \bm{A}^{\transpose} }$ in one go.
To do this, form the diagonal matrix $\widehat{\bm{L}}_{\bm{K}} \in \mathbb{R}^{M \times M}$, where $\widehat{\bm{L}}_{\bm{K}}(m,m) = \sum_{t=1}^{T} \bm{L}_{\bm{K}}(m,t)$.
We can now substitute the formula above into \Ref{Equation}{AAt_form} to give:
\begin{equation}
\expec{ \bm{A} \bm{A}^{\transpose} } = \bm{M}_{\bm{A}} \bm{M}_{\bm{A}}^{\transpose} 
+ \expec{ \bm{α} }^{-1}
\bm{U}_{\bm{P} \bm{α}} \, \widehat{\bm{L}}_{\bm{K}} \, \bm{U}_{\bm{P} \bm{α}}^{-1}
\label{AAt_calculation}
\end{equation}



\subsubsection{Extension to blockwise trace with matrix multiplication}
It is relatively straightforward to extend the above result to $\expec{ \bm{A} \bm{K}_{\bm{A}}^{-1} \bm{A}^{\transpose} }$.
It can be shown that the terms that involve $\bm{Σ}_{\bm{A}}$ that we require take the form $\trace \bigl( \bm{K}_{\bm{A}}^{-1} \bm{Σ}_{\bm{A}}^{[i,j]}  \bigr)$.
Note that if we multiply the expression for $\bm{Σ}_{\bm{A}}^{[i,j]}$ in the section above by $\bm{K}_{\bm{A}}^{-1}$ then the eigenvectors cancel, which means we simply revert to $\bm{L}$ rather than $\bm{L}_{\bm{K}}$.
Then, defining $\widehat{\bm{L}}$ in an analogous manner to $\widehat{\bm{L}}_{\bm{K}}$, the extension to \Ref{Equation}{AAt_calculation} is trivial, yielding:
\begin{equation}
\expec{ \bm{A} \bm{K}_{\bm{A}}^{-1} \bm{A}^{\transpose} } = \bm{M}_{\bm{A}} \bm{K}_{\bm{A}}^{-1} \bm{M}_{\bm{A}}^{\transpose} 
+ \expec{ \bm{α} }^{-1}
\bm{U}_{\bm{P} \bm{α}} \, \widehat{\bm{L}} \, \bm{U}_{\bm{P} \bm{α}}^{-1}
\label{AiKAt_calculation}
\end{equation}

Finally, rather than calculating $\bm{M}_{\bm{A}} \bm{K}_{\bm{A}}^{-1} \bm{M}_{\bm{A}}^{\transpose}$ directly, note that \Ref{Equation}{A:postM_eig} gives an expression for $\bm{M}_{\bm{A}}$ involving terms from the eigendecomposition of $\bm{K}_{\bm{A}}$.
Some of these will cancel, allowing calculations to proceed without explicitly forming $\bm{K}_{\bm{A}}^{-1}$.



\subsubsection{Determinant}
This is straightforward to calculate using the expression for $\bm{Σ}_{\bm{A}}$ in \Ref{Equation}{eigenSigma}.
There are a few properties of the determinant we need in order to proceed.
Firstly, $ | \bm{X} \bm{Y} | = | \bm{X} | | \bm{Y} | $.
Secondly, $ | \bm{X}^{-1} | = | \bm{X} |^{-1} $.
Thirdly, for $\bm{X} \in \mathbb{R}^{M \times M}$ and $\bm{Y} \in \mathbb{R}^{N \times N}$, $ | \bm{X} \otimes \bm{Y} | = | \bm{X} |^{N} | \bm{Y} |^{M} $.
Combining the above gives:
\begin{equation}
\begin{split}
\bigl| \bm{Σ}_{\bm{A}} \bigr| = & \, \bigl| \expec{ \bm{α} }^{-1} \otimes \bm{K}_{\bm{A}} \bigr|
\bigl| \bm{U}_{\bm{P} \bm{α}} \otimes \bm{U}_{\bm{K}} \bigr|
\bigl| \bm{\Lambda}_{\bm{P} \bm{α}} \otimes \bm{\Lambda}_{\bm{K}} + \bm{I}_{MT} \bigr|^{-1}
\bigl| \bm{U}_{\bm{P} \bm{α}}^{-1} \otimes \bm{U}_{\bm{K}}^{\transpose} \bigr| \\
%
= & \, \bigl| \expec{ \bm{α} } \bigr|^{-T} \bigl| \bm{K}_{\bm{A}} \bigr|^{M}
\bigl| \bm{\Lambda}_{\bm{P} \bm{α}} \otimes \bm{\Lambda}_{\bm{K}}^{-1} + \bm{I}_{MT} \bigr|^{-1}
\end{split}
\label{detSigma}
\end{equation}

The free energy requires the log of the determinant, which follows straightforwardly from \Ref{Equation}{detSigma}.
Note that we can reuse the matrices of eigenvalues, $\bm{L}$ and $\bm{L}_{\bm{K}}$, from previous calculations.
\begin{equation}
\begin{split}
\ln \Bigl( \bigl| \bm{Σ}_{\bm{A}} \bigr| \Bigr) = & \, - T \ln \Bigl( \bigl| \expec{ \bm{α} } \bigr| \Bigr) + M \ln \Bigl( \bigl| \bm{K}_{\bm{A}} \bigr| \Bigr) 
+ \ln \Bigl( \bigl| \bm{\Lambda}_{\bm{P} \bm{α}} \otimes \bm{\Lambda}_{\bm{K}} + \bm{I}_{MT} \bigr|^{-1}  \Bigr) \\
% Use L from posterior mean
= & \, - T \ln \Bigl( \bigl| \expec{ \bm{α} } \bigr| \Bigr) + M \ln \Bigl( \bigl| \bm{K}_{\bm{A}} \bigr| \Bigr) 
+ \sum_{t=1}^{T} \sum_{m=1}^{M} \ln \bigl( \bm{L}(m,t) \bigr) \\
% Use L from posterior mean
= & \, - T \ln \Bigl( \bigl| \expec{ \bm{α} } \bigr| \Bigr) 
+ \sum_{t=1}^{T} \sum_{m=1}^{M} \ln \bigl( \bm{L}_{\bm{K}}(m,t) \bigr)
\end{split}
\label{logDetSigma}
\end{equation}

% }}}
%------------------------------------------------------------------------------

% }}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography {{{

\newrefcontext[sorting=nyt]
\printbibliography

% }}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
