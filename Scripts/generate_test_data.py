#!/usr/bin/env python
# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2019
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# generate_test_data.py

###############################################################################

import argparse
parser = argparse.ArgumentParser(
        description="Generate a test data set for PROFUMO.")

parser.add_argument("output_dir")
parser.add_argument(
        "--data_type", choices=['cifti', 'nifti', 'txt'], default='nifti')

args = parser.parse_args()

###############################################################################

import os, os.path as op
import sys

import json
import numpy
import nibabel
import random

args.output_dir = op.realpath(op.expanduser(args.output_dir))
os.makedirs(args.output_dir, exist_ok=True)

###############################################################################
print("-" * 80)
print()

print("Generating test data...")

n_subjects   = 10
n_runs       = 2
n_modes      = 5
n_voxels     = 100
n_timepoints = 50

signal = numpy.random.rand(n_voxels, n_modes) ** 2 \
        @ numpy.random.rand(n_modes, n_timepoints) ** 2

data_locs = {}
for s in range(n_subjects):
    subject = 'sub-{:02d}'.format(s)
    data_locs[subject] = {}
    for r in range(n_runs):
        run = 'run-{:02d}'.format(r)
        
        data = signal + numpy.random.randn(n_voxels, n_timepoints)
        filename = op.join(args.output_dir, subject+'_'+run)
        if args.data_type == 'cifti':
            filename = filename + '.dtseries.nii'
            bma = nibabel.cifti2.BrainModelAxis.from_mask(
                    numpy.ones([n_voxels]))
            sa = nibabel.cifti2.SeriesAxis(0.0, 1.0, n_timepoints)
            img = nibabel.cifti2.Cifti2Image(data.T, [sa, bma])
            img.nifti_header.set_intent(3002, name='ConnDenseSeries')
            nibabel.save(img, filename)
        elif args.data_type == 'nifti':
            filename = filename + '.nii.gz'
            img = nibabel.nifti1.Nifti1Image(
                    data[None,None,:,:], affine=numpy.eye(4))
            nibabel.save(img, filename)
        elif args.data_type == 'txt':
            filename = filename + '.txt'
            numpy.savetxt(filename, data, fmt='%.5e')
        data_locs[subject][run] = filename

data_filename = op.join(args.output_dir, 'DataLocations.json')
with open(data_filename, 'w') as f:
    json.dump(data_locs, f, sort_keys=True, indent=4)

if args.data_type == 'cifti':
    reference_filename = op.join(args.output_dir, 'Reference.dscalar.nii')
    bma = nibabel.cifti2.BrainModelAxis.from_mask(
            numpy.ones([n_voxels]))
    sa = nibabel.cifti2.ScalarAxis(['ref'])
    img = nibabel.cifti2.Cifti2Image(numpy.ones([1, n_voxels]), [sa, bma])
    img.nifti_header.set_intent(3006, name='ConnDenseScalar')
    nibabel.save(img, reference_filename)
elif args.data_type == 'nifti':
    reference_filename = op.join(args.output_dir, 'Reference.nii.gz')
    img = nibabel.nifti1.Nifti1Image(
            numpy.ones([1, 1, n_voxels]), affine=numpy.eye(4))
    nibabel.save(img, reference_filename)

# Generate some test files for postprocessing
modes_filename = op.join(args.output_dir, 'Modes.csv')
numpy.savetxt(
        modes_filename,
        random.sample(range(n_modes), n_modes), fmt='%d', delimiter=',')
signs_filename = op.join(args.output_dir, 'Signs.csv')
numpy.savetxt(
        signs_filename,
        random.choices([-1, 1], k=n_modes), fmt='%d', delimiter=',')
runs_filename = op.join(args.output_dir, 'Runs.json')
runs = {subject: random.choices(list(runs.keys()), k=1)
        for subject, runs in data_locs.items()}
with open(runs_filename, 'w') as f:
    json.dump(runs, f, sort_keys=True, indent=4)

print("Done.")
print()

###############################################################################

analysis_dir = op.join(args.output_dir, 'Analysis.pfm')
results_dir = op.join(args.output_dir, 'Results.ppp')

print("You can now run PROFUMO using e.g.:")
print(">>> PROFUMO {} {:d} {}".format(
        data_filename, n_modes, analysis_dir))
if args.data_type in ['nifti', 'cifti']:
    print(">>> postprocess_results.py {} {} {}".format(
            analysis_dir, results_dir, reference_filename))
    print(">>> postprocess_results.py --web-report -m {} -s {} -r {} {} {} {}".format(
            modes_filename, signs_filename, runs_filename,
            analysis_dir, results_dir, reference_filename))

print()

# https://stackoverflow.com/a/44989219
# Compile with `-O1 -g`
# valgrind \
#     --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose \
#     <command> > valgrind_output.txt 2>&1

###############################################################################
print("-" * 80)
