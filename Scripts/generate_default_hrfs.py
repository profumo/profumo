#!/usr/bin/env python
# -*- coding: utf-8 -*-

# PROFUMO
# Framework for variational Bayesian inference of modes from fMRI data

# - Sam Harrison 2017
# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

# generate_default_hrfs.py

################################################################################
print("-" * 80)
print("Generating HRFs...")
print()

import os, os.path
import sys

import numpy, numpy.fft
import scipy, scipy.stats

import matplotlib as mpl
import matplotlib.pyplot as plt

################################################################################
# FLOBS

flobs_file = os.path.expandvars(os.path.join(
    '$FSLDIR', 'etc', 'default_flobs.flobs', 'hrfbasisfns.txt'))
flobs_basis = numpy.loadtxt(flobs_file)

# FLOBS defaults
dt = 0.05
flobs_t = numpy.linspace(0, dt * (flobs_basis.shape[0] - 1), flobs_basis.shape[0])

# Extract HRF (first basis component)
flobs_hrf = flobs_basis[:,0]
flobs_hrf = flobs_hrf / numpy.max(numpy.abs(flobs_hrf))

# Plot basis
plt.figure()
plt.plot(flobs_t, flobs_basis)
plt.xlabel("Time (s)"); plt.ylabel("BOLD response")
plt.title("FLOBS basis set")

plt.show()

# Save!
HRF = numpy.stack([flobs_t, flobs_hrf], axis=1)
numpy.savetxt('../HRFs/FLOBS.phrf', HRF)

################################################################################
# Double gamma HRF

# FLOBS defaults, with longer time
dt = 0.05; n_dt = 700
dg_t = numpy.linspace(0, dt * (n_dt - 1), n_dt)

# SPM default parameters
# See e.g. https://github.com/spm/spm12/blob/master/spm_hrf.m
dg_hrf = scipy.stats.gamma.pdf(dg_t, 6.0, scale=1.0) \
         - scipy.stats.gamma.pdf(dg_t, 16.0, scale=1.0) / 6.0
dg_hrf = dg_hrf / numpy.max(numpy.abs(dg_hrf))

# Save!
HRF = numpy.stack([dg_t, dg_hrf], axis=1)
numpy.savetxt('../HRFs/DoubleGamma.phrf', HRF)

################################################################################
# Plot together

plt.figure()
plt.plot([0.0, max(flobs_t[-1], dg_t[-1])], [0.0, 0.0], color=[0.7,]*3)
plt.plot(flobs_t, flobs_hrf, label="FLOBS")
plt.plot(dg_t, dg_hrf, label="Double Gamma")
plt.xlabel("Time (s)"); plt.ylabel("BOLD response"); plt.legend()
plt.title("Default HRFs")

# Frequency responses
n_fft = 4098
f = numpy.fft.rfftfreq(n_fft, d=dt)
flobs_fft = numpy.abs(numpy.fft.rfft(flobs_hrf, n=4098))
dg_fft = numpy.abs(numpy.fft.rfft(dg_hrf, n=4098))

plt.figure()
plt.plot([0.0, f[-1]], [0.0, 0.0], color=[0.7,]*3)
plt.plot(f, flobs_fft, label="FLOBS")
plt.plot(f, dg_fft, label="Double Gamma")
plt.xlim([-0.025, 0.525])
plt.xlabel("Frequency (Hz)"); plt.ylabel("BOLD response"); plt.legend()
plt.title("Frequency responses")

plt.figure()
# Clip the data rather than `plt.xlim` to get ylim correct too
plt.semilogy(f[f < 2.0], flobs_fft[f < 2.0], label="FLOBS")
plt.semilogy(f[f < 2.0], dg_fft[f < 2.0], label="Double Gamma")
plt.xlabel("Frequency (Hz)"); plt.ylabel("BOLD response"); plt.legend()
plt.title("Frequency responses")

plt.show()

print("Done.")
print()

################################################################################
print("-" * 80)
