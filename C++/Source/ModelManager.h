// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Class that manages the whole inference process: loads the data, sets up the 
// cohort, manages the updates etc

#ifndef MODEL_MANAGER_H
#define MODEL_MANAGER_H

#include <memory>
#include <vector>
#include <string>
#include <cmath>

#include <boost/optional.hpp>

#include "CohortModelling/Cohort.h"
#include "CohortModelling/GroupModel.h"
#include "DataLoader.h"

namespace PROFUMO
{
    
    class ModelManager
    {
    public:
        // Define the potential models
        //----------------------------------------------------------------------
        enum class SpatialModel {
            MODES,
            PARCELLATION
        };
        
        // Extra spatial parameters
        struct SpatialParameters {
        public:
            boost::optional<arma::fmat> fixedGroupMeans;
            boost::optional<arma::fmat> fixedGroupPrecisions;
            boost::optional<arma::fmat> fixedGroupMemberships;
            
            boost::optional<std::vector<arma::uvec>> spatialNeighbours;
        };
        //----------------------------------------------------------------------
        enum class WeightModel {
            CONSTANT,
            GROUP,
            RECTIFIED_MULTIVARIATE_NORMAL,
            RECTIFIED_SPIKE_SLAB
        };
        //----------------------------------------------------------------------
        enum class TimeCourseModel {
            MULTIVARIATE_NORMAL,
            CLEAN_HRF,
            NOISY_HRF
        };
        //----------------------------------------------------------------------
        enum class TemporalPrecisionModel {
            CONSTANT,
            GROUP_PRECISION_MATRIX,
            CONSTANT_GROUP,
            SUBJECT_PRECISION_MATRICES,
            RUN_PRECISION_MATRICES,
            CATEGORISED_RUN_PRECISION_MATRICES
        };
        //----------------------------------------------------------------------
        enum class NoiseModel {
            INDEPENDENT_RUNS
        };
        //----------------------------------------------------------------------
        
        
        // Options for the constructor
        //----------------------------------------------------------------------
        struct HRFOptions {
        public:
            float TR;
            std::string file;
        };
        //----------------------------------------------------------------------
        struct Options {
        public:
            boost::optional<unsigned int> lowRank;
            DataLoaders::Options dataLoader;
            float dofCorrectionFactor = 1.0f;
            boost::optional<HRFOptions> hrf;
            unsigned int multiStartIterations = 5;
            bool globalInit = false;
            boost::optional<arma::fmat> initialMaps;
        };
        //----------------------------------------------------------------------
        
        
        // Constructor
        ModelManager( const std::string dataLocationsFile, const unsigned int M, const SpatialModel spatialModel, const SpatialParameters& spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const Options& options, const std::string preprocessingDirectory );
        
        // Updates the model
        void update(const unsigned int iterations, const std::string intermediatesDir, const bool normaliseTimeCourses=false);
        
        // Functions to dynamically switch models
        void switchSpatialModel(const SpatialModel spatialModel, const SpatialParameters& spatialParameters);
        void switchWeightModel(const WeightModel weightModel);
        void switchTemporalModel(const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel);
        void switchNoiseModel(const NoiseModel noiseModel);
        
        // Reset the model, with continuity coming from the current group maps
        void reinitialiseModel(const SpatialModel spatialModel, const SpatialParameters& spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel);
        
        // Save everything
        void save(const std::string directory) const;
        
        // Get F
        double getFreeEnergy() const;
        
    private:
        // The cohort itself
        std::unique_ptr<CohortModelling::Cohort> cohort_;
        
        // Key sizes
        unsigned int S_;        // Subjects
        //unsigned int R_;        // Runs
        const unsigned int M_;  // Modes       [FIXED]
        unsigned int V_;        // Voxels      [FIXED] - all scans are in the same space
        //arma::uvec T_;          // Time points [VARIABLE]
        boost::optional<HRFOptions> hrf_;
        
        // Prior strengths
        const float N_PRIOR_SUBJECTS = 5.0f;    // Prior is equivalent to observing this many subjects
        const float P_PRIOR_SPARSITY = 0.067f;  // Proportion of voxels non-zero weights per mode
        
        // Spatial DOF correction
        const float dofCorrectionFactor_;
        
        // Helper function to put Cohort together - useful as we have different types of data
        // Uses simple constant spatial model based on initialMaps
        template<class D>
        void initialiseCohort(const std::map<SubjectID, std::map<RunID, D>> subjectData, const arma::fmat initialMaps, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel );
        
        // Helper functions to generate models
        std::shared_ptr<CohortModelling::GroupSpatialModel> getSpatialModel(const SpatialModel spatialModel, const SpatialParameters& spatialParameters) const;
        std::shared_ptr<CohortModelling::GroupWeightModel> getWeightModel(const WeightModel weightModel) const;
        std::shared_ptr<CohortModelling::GroupTemporalPrecisionModel> getTemporalPrecisionModel(const TemporalPrecisionModel temporalPrecisionModel) const;
        std::shared_ptr<CohortModelling::GroupTemporalModel> getTemporalModel(const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel) const;
        std::shared_ptr<CohortModelling::GroupNoiseModel> getNoiseModel(const NoiseModel noiseModel) const;
        
        // Get a set of maps to intialise the full algorithm with 
        // Based on multiple decompositions for reliability
        arma::fmat generateInitialMaps(const SpatialBasis& spatialBasis, const SpatialModel spatialModel, const unsigned int multiStartIterations, const bool globalInit, const std::string outputDirectory);
        
        // Saves the group maps to file to examine convergence post hoc
        void saveIntermediates(const std::string intermediatesDir, const unsigned int iteration, const unsigned int maxIteration);
    };
    
}
#endif
