// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines the interface to a matrix factorisation (MF) model. It combines the
// data, spatial maps, time course and noise models. Given these, it provides
// the expectations required for updating any of the components, and can return
// the free energy of the model (i.e. the likelihood of the data).

#ifndef MF_MODEL_H
#define MF_MODEL_H

#include "Module.h"
#include "MFVariableModels.h"

namespace PROFUMO
{
    
    class MFModel :
        protected MFModel_PInterface,
        protected MFModel_HInterface,
        protected MFModel_AInterface,
        protected MFModel_PsiInterface
    {
    public:
        // Constructor
        MFModel(MFPModel* P, MFHModel* H, MFAModel* A, MFPsiModel* Psi, const float dofCorrectionFactor=1.0f);
        
        // Returns likelihood of data w.r.t. posterior
        virtual double getLogLikelihood() const = 0;
        
        // Functions allowing dynamic model switching
        void setPModel(MFPModel* P);
        void setHModel(MFHModel* H);
        void setAModel(MFAModel* A);
        void setPsiModel(MFPsiModel* Psi);
        
        void setTimeCourseNormalisation(const bool normalisation);
        
        virtual ~MFModel() = default;
        
    protected:
        // Pointers to parents (just to make calls to parents easier)
        // Spatial maps
        Module<MFModels::P::P2C>* P_;
        // Component weightings
        Module<MFModels::H::P2C>* H_;
        // Time courses
        Module<MFModels::A::P2C>* A_;
        // Noise
        Module<MFModels::Psi::P2C>* Psi_;
        
        // Spatial DOF correction
        const float dofCorrectionFactor_;
        
        // Whether to normalise time course variance
        bool normaliseTimeCourses_ = false;
        void normaliseTimeCourseExpectations(MFModels::A::C2P& AD) const;
    };
    
}
#endif
