// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "IndependentGamma.h"

namespace MatPrecs = PROFUMO::Modules::MatrixPrecisions;
namespace VBMatPrecs = PROFUMO::VBModules::MatrixPrecisions;

////////////////////////////////////////////////////////////////////////////////

VBMatPrecs::IndependentGamma::IndependentGamma(const Parameters& prior)
: nRows_(prior.a.n_rows), nCols_(prior.a.n_cols), nElem_(prior.a.n_elem)
{
    // Generate all the posteriors
    posteriors_.reserve(nElem_);
    for (unsigned int i = 0; i < nElem_; ++i) {
        Gamma::Parameters elemPrior;
        elemPrior.a = prior.a[i];
        elemPrior.b = prior.b[i];
        posteriors_.emplace_back(elemPrior);
    }
    
    // Intialise expectations
    expectations_.X    = arma::zeros<arma::fmat>(nRows_,nCols_);
    expectations_.logX = arma::zeros<arma::fmat>(nRows_,nCols_);
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBMatPrecs::IndependentGamma::update()
{
    // Collect the data from all the children
    const MatPrecs::C2P D = collectExpectations();
    
    // Pass to the gamma posteriors
    #pragma omp parallel for schedule(dynamic,512) if(nElem_ > 2048)
    for (unsigned int i = 0; i < nElem_; ++i) {
        Gamma::DataExpectations d;
        d.n  = D.N[i];
        d.d2 = D.D2[i];
        
        posteriors_[i].update(d);
    }
    
    // Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double VBMatPrecs::IndependentGamma::getKL() const
{
    double KL = 0.0;
    #pragma omp parallel for reduction(+:KL) schedule(dynamic,512) if(nElem_ > 2048)
    for (unsigned int i = 0; i < nElem_; ++i) {
        KL += posteriors_[i].getKL();
    }
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void VBMatPrecs::IndependentGamma::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Means
    fileName = directory + "Means.hdf5";
    expectations_.X.save(fileName, arma::hdf5_binary);
    
    // E[ log(X) ]
    fileName = directory + "Logs.hdf5";
    expectations_.logX.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MatPrecs::C2P VBMatPrecs::IndependentGamma::collectExpectations() const
{
    // Group-level model so parallelise collection
    
    // Intialise expectations
    MatPrecs::C2P D;
    D.N  = arma::zeros<arma::fmat>(nRows_,nCols_);
    D.D2 = arma::zeros<arma::fmat>(nRows_,nCols_);
    
    // Switch based on number of children
    if (childModules_.size() >= 100) {
        // If we have a lot of children, such that we can expect each thread 
        // to be used lots of times, recreate a reduction block.
        // Each thread has a set of local expectations, and these are all added 
        // together at the end. This parallelises both the expectation 
        // gathering and the accumulation into one set of expectations. 
        
        #pragma omp parallel
        {
        // Give each thread a local set of expectations
        MatPrecs::C2P D_local;
        D_local.N  = arma::zeros<arma::fmat>(nRows_,nCols_);
        D_local.D2 = arma::zeros<arma::fmat>(nRows_,nCols_);
        
        // Loop over all children, collecting expectations
        for (listType::const_iterator child = childModules_.begin(); child != childModules_.end(); ++child) {
            #pragma omp single nowait
            {
            const MatPrecs::C2P d = (*child)->getExpectations();
            D_local.N  += d.N;
            D_local.D2 += d.D2;
            }
        }
        
        // Now collect all local expectations
        #pragma omp critical
        {
        D.N  += D_local.N;
        D.D2 += D_local.D2;
        }
        }
    }
    else {
        // If we don't have many children then the overhead of assigning each 
        // thread its own expectations is proably too high (accumulation is 
        // relatively cheap compared to instantiation). This therefore is a 
        // more conventional method for parallelisation where the expectation 
        // gathering is sped up, but the accumulation takes the same amount 
        // of time.  
        
        #pragma omp parallel
        {
        for (listType::const_iterator child = childModules_.begin(); child != childModules_.end(); ++child) {
            #pragma omp single nowait
            {
            const MatPrecs::C2P d = (*child)->getExpectations();
            #pragma omp critical
            {
            D.N  += d.N;
            D.D2 += d.D2;
            }
            }
        }
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBMatPrecs::IndependentGamma::updateExpectations()
{
    // Reset expectations
    expectations_.X.zeros();
    expectations_.logX.zeros();
    
    // And collect from posteriors
    #pragma omp parallel for schedule(dynamic,512) if(nElem_ > 2048)
    for (unsigned int i = 0; i < nElem_; ++i) {
        const Gamma::Expectations e = posteriors_[i].getExpectations();
        
        expectations_.X[i]    = e.x;
        expectations_.logX[i] = e.log_x;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
