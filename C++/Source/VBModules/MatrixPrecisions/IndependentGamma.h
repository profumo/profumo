// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a matrix of independent gamma distributions. This is a thin
// wrapper around VBDistributions::Gamma that deals with expectation
// gathering and caching.

#ifndef VB_MODULES_MATRIX_PRECISIONS_INDEPENDENT_GAMMA_H
#define VB_MODULES_MATRIX_PRECISIONS_INDEPENDENT_GAMMA_H

#include <armadillo>
#include <string>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "VBDistributions/Gamma.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace MatrixPrecisions
        {
            
            class IndependentGamma :
                public Modules::MatrixPrecision_VBPosterior,
                protected CachedModule<Modules::MatrixPrecisions::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    arma::fmat a;  // Shape
                    arma::fmat b;  // Rate
                };
                //--------------------------------------------------------------
                
                IndependentGamma(const Parameters& prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            private:
                // Storage for posteriors
                // Use value semantics - want a nice simple memory layout (and
                // each class is self contained) to try and help caching
                // https://stackoverflow.com/a/71264
                typedef VBDistributions::Gamma Gamma;
                std::vector<Gamma> posteriors_;
                const unsigned int nRows_, nCols_, nElem_;
                
                // Helper functions to collect expectations / set caches
                Modules::MatrixPrecisions::C2P collectExpectations() const;
                void updateExpectations();
            };
            
        }
    }
}
#endif
