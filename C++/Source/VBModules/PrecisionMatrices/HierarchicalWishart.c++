// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "HierarchicalWishart.h"

#include <cmath>

#include "Utilities/DataIO.h"
#include "Utilities/MiscMaths.h"
namespace miscmaths = PROFUMO::Utilities::MiscMaths;
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace Mods = PROFUMO::Modules;
namespace PrecMats = PROFUMO::Modules::PrecisionMatrices;
namespace VBPrecMats = PROFUMO::VBModules::PrecisionMatrices;

// Resolve calls to internally cached expectations
typedef PROFUMO::CachedModule<PrecMats::C2P> C2PCache;
typedef PROFUMO::CachedModule<PrecMats::P2C> P2CCache;

////////////////////////////////////////////////////////////////////////////////

VBPrecMats::HierarchicalWishart::HierarchicalWishart(Mods::PrecisionMatrix_Parent* B, const Parameters prior)
: Mods::PrecisionMatrix_Child(B), prior_(prior), size_(prior.size)
{
    // Set initial params to prior
    posterior_.a = prior_.a;
    posterior_.B = parentModule_->getExpectations().X;
    
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::HierarchicalWishart::update()
{
    // Collect expectations from all linked models
    const PrecMats::C2P D = collectExpectations();
    
    // Update posterior parameters
    posterior_.a = prior_.a + D.N;
    posterior_.B = parentModule_->getExpectations().X + D.XXt;
    
    // Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double VBPrecMats::HierarchicalWishart::getKL() const
{
    // Get expectations from parent
    const PrecMats::P2C prior_B = parentModule_->getExpectations();
    
    // KL divergence between prior and posterior
    double KL = 0.5 * prior_.a * (linalg::log_det_sympd(posterior_.B) - prior_B.logDetX);
    KL += 0.5 * posterior_.a * (arma::trace(prior_B.X * linalg::inv_sympd(posterior_.B)) - size_);
    KL += 0.5 * (posterior_.a - prior_.a) * miscmaths::mdigamma(posterior_.a / 2.0, size_);
    KL -= miscmaths::mlgamma(posterior_.a / 2.0, size_);
    KL += miscmaths::mlgamma(prior_.a / 2.0, size_);
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::HierarchicalWishart::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Mean
    fileName = directory + "Mean.hdf5";
    P2CCache::expectations_.X.save(fileName, arma::hdf5_binary);
    
    // logDet
    fileName = directory + "logDeterminant.txt";
    Utilities::save(fileName, P2CCache::expectations_.logDetX);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

PrecMats::C2P VBPrecMats::HierarchicalWishart::collectExpectations() const
{
    // Can be either group or subject level, and super quick anyway, so not
    // worth parallelising as yet
    
    PrecMats::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.N = 0.0f;
        D.XXt = arma::zeros<arma::fmat>(size_,size_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator child = childModules_.begin();
        D = (*child)->getExpectations();
        
        // Add other models if necessary
        for (++child; child != childModules_.end(); ++child) {
            const PrecMats::C2P d = (*child)->getExpectations();
            D.N += d.N;
            D.XXt += d.XXt;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::HierarchicalWishart::updateExpectations()
{
    // Expectations as a parent
    arma::fmat& X = P2CCache::expectations_.X;
    float& logDetX = P2CCache::expectations_.logDetX;
    
    // Mean
    X = posterior_.a * linalg::inv_sympd(posterior_.B);
    
    // logDet
    logDetX  = miscmaths::mdigamma(posterior_.a / 2.0f, size_);
    logDetX -= linalg::log_det_sympd(posterior_.B);
    logDetX += size_ * std::log(2.0f);
    
    
    // Expectations as a child
    arma::fmat& XXt = C2PCache::expectations_.XXt;
    float& N = C2PCache::expectations_.N;
    
    XXt = X;
    N = prior_.a;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
/*
import math, numpy, matplotlib, matplotlib.pyplot as plt

# Use gamma distributions for this example, but trivial to extend to Wishart
# x ~ N(0, alpha^-1)
# alpha ~ G(ak, beta)
# beta ~ G(a, b)

N = 5      # Observations
xxt = 2.0  # Sum of squares of observations
K = 3      # Classes / subjects / components etc
# Priors
ak = 2.0   # Equivalent to 'ak' observations
a = 2.0    # Equivalent to 'a' classes
b = 1.0    # Prior prec ≈ ak / (a / b) ≈ (ak / a) * b

# Update rules
print("Update rules: alpha, beta")
alpha = beta = 5.0
print(alpha, beta)
for n in range(10):
    alpha = (ak + N) / (beta + xxt)
    beta = (a + K*ak) / (b + K*alpha)
    print(alpha, beta)
print()

print("Observation std: {:.3f}".format(math.sqrt(xxt / N)))
print("Prior std: {:.3f}".format(math.sqrt(a / (ak * b))))
print("Inferred std: {:.3f}".format(math.sqrt(1.0 / alpha)))


# We can also solve these equations exactly, which gives
def solve(N, xxt, K, ak, a, b):
    # We get a quadratic expression for alpha
    a1 = K * xxt
    b1 = a - K * N + b * xxt
    c1 = - b * (ak + N)
    alpha = (-b1 + numpy.sqrt(b1**2 - 4 * a1 * c1)) / (2 * a1)
    
    # And then the standard update rule for beta
    beta = (a + K*ak) / (b + K*alpha)
    
    return alpha, beta

alpha, beta = solve(N, xxt, K, ak, a, b)
print(alpha, beta)
print("Exact std: {:.3f}".format(math.sqrt(1.0 / alpha)))


# Note that the update rules also represent the natural gradient of the system
A, B = numpy.meshgrid(numpy.linspace(0.0,5.0,20), numpy.linspace(0.0,5.0,20))
dA = (ak + N) / (B + xxt) - A
dB = (a + K*ak) / (b + K*A) - B
plt.quiver(A,B,dA,dB)
plt.xlabel("alpha"); plt.ylabel("beta"); plt.title("Natural gradient")
plt.show()
*/
////////////////////////////////////////////////////////////////////////////////
