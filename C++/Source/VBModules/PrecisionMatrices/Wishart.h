// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Wishart distribution for a precision matrix

#ifndef VB_MODULES_PRECISION_MATRICES_WISHART_H
#define VB_MODULES_PRECISION_MATRICES_WISHART_H

#include <armadillo>
#include <string>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"


namespace PROFUMO
{
    namespace VBModules
    {
        namespace PrecisionMatrices
        {
            
            class Wishart :
                public Modules::PrecisionMatrix_VBPosterior,
                protected CachedModule<Modules::PrecisionMatrices::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    float a;     // Shape
                    arma::fmat B;  // Rate
                };
                //--------------------------------------------------------------
                Wishart(const Parameters prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            private:
                const Parameters prior_;
                Parameters posterior_;
                const unsigned int size_; // size_ x size_ precision matrix
                
                // Helper functions to collect expectations / set caches
                Modules::PrecisionMatrices::C2P collectExpectations() const;
                void updateExpectations();
            };
            
        }
    }
}
#endif
