// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Precision matrix model - just the identity scaled by a gamma distribution.

#ifndef VB_MODULES_PRECISION_MATRICES_SHARED_GAMMA_PRECISION_H
#define VB_MODULES_PRECISION_MATRICES_SHARED_GAMMA_PRECISION_H

#include <string>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"

#include "VBDistributions/Gamma.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace PrecisionMatrices
        {
            
            class SharedGammaPrecision :
                public Modules::PrecisionMatrix_VBPosterior,
                protected CachedModule<Modules::PrecisionMatrices::P2C>
            {
            public:
                //-------------------------------------------------------------
                struct Parameters :
                    public VBDistributions::Gamma::Parameters
                {
                public:
                    unsigned int size;
                };
                //-------------------------------------------------------------
                
                SharedGammaPrecision(const Parameters prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            private:
                VBDistributions::Gamma posterior_;
                const unsigned int size_; // size_ x size_ precision matrix
                
                // Helper functions to collect expectations / set caches
                Modules::PrecisionMatrices::C2P collectExpectations() const;
                void updateExpectations();
            };
            
        }
    }
}
#endif
