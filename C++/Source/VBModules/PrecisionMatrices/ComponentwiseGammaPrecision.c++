// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "ComponentwiseGammaPrecision.h"

#include "Utilities/DataIO.h"

namespace VBDists = PROFUMO::VBDistributions;
namespace PrecMats = PROFUMO::Modules::PrecisionMatrices;
namespace VBPrecMats = PROFUMO::VBModules::PrecisionMatrices;

////////////////////////////////////////////////////////////////////////////////

VBPrecMats::ComponentwiseGammaPrecision::ComponentwiseGammaPrecision(const Parameters prior)
: size_(prior.a.n_elem)
{
    // Generate all the posteriors
    posteriors_.reserve(size_);
    for (unsigned int i = 0; i < size_; ++i) {
        Gamma::Parameters elemPrior;
        elemPrior.a = prior.a[i];
        elemPrior.b = prior.b[i];
        posteriors_.emplace_back(elemPrior);
    }
    
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::ComponentwiseGammaPrecision::update()
{
    // Collect expectations from all linked models
    const PrecMats::C2P D = collectExpectations();
    
    // Pass to the gamma posteriors
    for (unsigned int i = 0; i < size_; ++i) {
        Gamma::DataExpectations d;
        d.n = D.N; d.d2 = D.XXt(i,i);
        posteriors_[i].update(d);
    }
    
    // Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double VBPrecMats::ComponentwiseGammaPrecision::getKL() const
{
    double KL = 0.0;
    for (unsigned int i = 0; i < size_; ++i) {
        KL += posteriors_[i].getKL();
    }
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::ComponentwiseGammaPrecision::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Mean
    fileName = directory + "Mean.hdf5";
    expectations_.X.save(fileName, arma::hdf5_binary);
    
    // logDet
    fileName = directory + "logDeterminant.txt";
    Utilities::save(fileName, expectations_.logDetX);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

PrecMats::C2P VBPrecMats::ComponentwiseGammaPrecision::collectExpectations() const
{
    // Can be either group or subject level, and super quick anyway, so not
    // worth parallelising as yet
    
    PrecMats::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.N = 0.0f;
        D.XXt = arma::zeros<arma::fmat>(size_,size_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator child = childModules_.begin();
        D = (*child)->getExpectations();
        
        // Add other models if necessary
        for (++child; child != childModules_.end(); ++child) {
            const PrecMats::C2P d = (*child)->getExpectations();
            D.N += d.N;
            D.XXt += d.XXt;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::ComponentwiseGammaPrecision::updateExpectations()
{
    expectations_.X = arma::zeros<arma::fmat>(size_, size_);
    expectations_.logDetX = 0.0f;
    
    for (unsigned int i = 0; i < size_; ++i) {
        const Gamma::Expectations e = posteriors_[i].getExpectations();
        
        expectations_.X(i,i) = e.x;
        expectations_.logDetX += e.log_x;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
