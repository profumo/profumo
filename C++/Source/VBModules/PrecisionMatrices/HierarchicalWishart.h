// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Wishart distribution for a precision matrix, with a hyperprior on B

#ifndef VB_MODULES_PRECISION_MATRICES_HIERARCHICAL_WISHART_H
#define VB_MODULES_PRECISION_MATRICES_HIERARCHICAL_WISHART_H

#include <armadillo>
#include <string>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "VBModules/PrecisionMatrices/Wishart.h"


namespace PROFUMO
{
    namespace VBModules
    {
        namespace PrecisionMatrices
        {
            
            class HierarchicalWishart :
                public Modules::PrecisionMatrix_VBPosterior,
                protected CachedModule<Modules::PrecisionMatrices::P2C>,
                public Modules::PrecisionMatrix_Child,
                protected CachedModule<Modules::PrecisionMatrices::C2P>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    float a;     // Shape
                    unsigned int size;
                };
                //--------------------------------------------------------------
                HierarchicalWishart(Modules::PrecisionMatrix_Parent* B, const Parameters prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            private:
                const Parameters prior_;
                Wishart::Parameters posterior_;
                const unsigned int size_; // size_ x size_ precision matrix
                
                // Helper functions to collect expectations / set caches
                Modules::PrecisionMatrices::C2P collectExpectations() const;
                void updateExpectations();
            };
            
        }
    }
}
#endif
