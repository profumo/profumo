// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "SharedGammaPrecision.h"

#include <armadillo>

#include "Utilities/DataIO.h"

namespace VBDists = PROFUMO::VBDistributions;
namespace PrecMats = PROFUMO::Modules::PrecisionMatrices;
namespace VBPrecMats = PROFUMO::VBModules::PrecisionMatrices;

////////////////////////////////////////////////////////////////////////////////

VBPrecMats::SharedGammaPrecision::SharedGammaPrecision(const Parameters prior)
: posterior_(prior), size_(prior.size)
{
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::SharedGammaPrecision::update()
{
    // Collect expectations from all linked models
    const PrecMats::C2P D = collectExpectations();
    
    // Pass to gamma posterior
    VBDists::DataExpectations::Gamma d;
    d.n = size_ * D.N; d.d2 = arma::trace(D.XXt);
    posterior_.update(d);
    
    // Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double VBPrecMats::SharedGammaPrecision::getKL() const
{
    return posterior_.getKL();
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::SharedGammaPrecision::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Mean
    fileName = directory + "Mean.hdf5";
    expectations_.X.save(fileName, arma::hdf5_binary);
    
    // logDet
    fileName = directory + "logDeterminant.txt";
    Utilities::save(fileName, expectations_.logDetX);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

PrecMats::C2P VBPrecMats::SharedGammaPrecision::collectExpectations() const
{
    // Can be either group or subject level, and super quick anyway, so not
    // worth parallelising as yet
    
    PrecMats::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.N = 0.0f;
        D.XXt = arma::zeros<arma::fmat>(size_,size_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator child = childModules_.begin();
        D = (*child)->getExpectations();
        
        // Add other models if necessary
        for (++child; child != childModules_.end(); ++child) {
            const PrecMats::C2P d = (*child)->getExpectations();
            D.N += d.N;
            D.XXt += d.XXt;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::SharedGammaPrecision::updateExpectations()
{
    // Get expectations from posterior
    const VBDists::Expectations::Gamma e = posterior_.getExpectations();
    
    // And transform to required form
    expectations_.X = e.x * arma::eye<arma::fmat>(size_,size_);
    expectations_.logDetX = e.log_x * size_;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
