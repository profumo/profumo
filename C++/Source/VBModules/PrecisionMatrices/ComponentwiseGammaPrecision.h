// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A precision matrix, where each element along the diagonal is an independent 
// gamma distribution. Off-diagonal elements are zero.

#ifndef VB_MODULES_PRECISION_MATRICES_COMPONENTWISE_GAMMA_PRECISION_H
#define VB_MODULES_PRECISION_MATRICES_COMPONENTWISE_GAMMA_PRECISION_H

#include <armadillo>
#include <string>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"

#include "VBDistributions/Gamma.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace PrecisionMatrices
        {
            
            class ComponentwiseGammaPrecision :
                public Modules::PrecisionMatrix_VBPosterior,
                protected CachedModule<Modules::PrecisionMatrices::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    arma::fvec a;  // Shape
                    arma::fvec b;  // Rate
                };
                //--------------------------------------------------------------
                
                ComponentwiseGammaPrecision(const Parameters prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            private:
                typedef VBDistributions::Gamma Gamma;
                std::vector<Gamma> posteriors_;
                const unsigned int size_; // size_ x size_ precision matrix
                
                // Helper functions to collect expectations / set caches
                Modules::PrecisionMatrices::C2P collectExpectations() const;
                void updateExpectations();
            };
            
        }
    }
}
#endif
