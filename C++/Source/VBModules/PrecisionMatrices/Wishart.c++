// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Wishart.h"

#include <cmath>

#include "Utilities/DataIO.h"
#include "Utilities/MiscMaths.h"
namespace miscmaths = PROFUMO::Utilities::MiscMaths;
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace PrecMats = PROFUMO::Modules::PrecisionMatrices;
namespace VBPrecMats = PROFUMO::VBModules::PrecisionMatrices;

////////////////////////////////////////////////////////////////////////////////

VBPrecMats::Wishart::Wishart(const Parameters prior)
: prior_(prior), size_(prior.B.n_rows)
{
    // Set initial params to prior
    posterior_ = prior_;
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::Wishart::update()
{
    // Collect expectations from all linked models
    const PrecMats::C2P D = collectExpectations();
    
    // Update posterior parameters
    posterior_.a = prior_.a + D.N;
    posterior_.B = prior_.B + D.XXt;
    
    // Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double VBPrecMats::Wishart::getKL() const
{
    // KL divergence between prior and posterior
    double KL = 0.5 * prior_.a * (linalg::log_det_sympd(posterior_.B) - linalg::log_det_sympd(prior_.B));
    KL += 0.5 * posterior_.a * (arma::trace(prior_.B * linalg::inv_sympd(posterior_.B)) - size_);
    KL += 0.5 * (posterior_.a - prior_.a) * miscmaths::mdigamma(posterior_.a / 2.0, size_);
    KL -= miscmaths::mlgamma(posterior_.a / 2.0, size_);
    KL += miscmaths::mlgamma(prior_.a / 2.0, size_);
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::Wishart::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Mean
    fileName = directory + "Mean.hdf5";
    expectations_.X.save(fileName, arma::hdf5_binary);
    
    // logDet
    fileName = directory + "logDeterminant.txt";
    Utilities::save(fileName, expectations_.logDetX);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

PrecMats::C2P VBPrecMats::Wishart::collectExpectations() const
{
    // Can be either group or subject level, and super quick anyway, so not
    // worth parallelising as yet
    
    PrecMats::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.N = 0.0f;
        D.XXt = arma::zeros<arma::fmat>(size_,size_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator child = childModules_.begin();
        D = (*child)->getExpectations();
        
        // Add other models if necessary
        for (++child; child != childModules_.end(); ++child) {
            const PrecMats::C2P d = (*child)->getExpectations();
            D.N += d.N;
            D.XXt += d.XXt;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBPrecMats::Wishart::updateExpectations()
{
    expectations_.X = posterior_.a * linalg::inv_sympd(posterior_.B);
    
    expectations_.logDetX  = miscmaths::mdigamma(posterior_.a / 2.0f, size_);
    expectations_.logDetX -= linalg::log_det_sympd(posterior_.B);
    expectations_.logDetX += size_ * std::log(2.0f);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
