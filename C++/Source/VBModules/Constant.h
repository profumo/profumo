// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A generic VBModule that just holds a set of constant expectations.

#ifndef VB_MODULES_CONSTANT_H
#define VB_MODULES_CONSTANT_H

#include <string>

#include "Module.h"
#include "Posterior.h"

namespace PROFUMO
{
    namespace VBModules
    {
        ////////////////////////////////////////////////////////////////////////
        
        template <class C2P, class P2C>
        class Constant :
            public VBPosterior<C2P, P2C>,
            protected CachedModule<P2C>
        {
        public:
            // Construct from a supplied set of expectations
            Constant(const P2C expectations);
            
            // Required functionality
            void update();
            double getKL() const;
            void save(const std::string directory) const;
        };
    
        ////////////////////////////////////////////////////////////////////////
        // Constructor

        template <class C2P, class P2C>
        Constant<C2P,P2C>::Constant(const P2C expectations)
        {
            CachedModule<P2C>::expectations_ = expectations;
            return;
        }
    
        ////////////////////////////////////////////////////////////////////////

        template <class C2P, class P2C>
        void Constant<C2P,P2C>::update()
        {
            return;
        }
    
        ////////////////////////////////////////////////////////////////////////

        template <class C2P, class P2C>
        double Constant<C2P,P2C>::getKL() const
        {
            // Prior = posterior, so KL divergence between the two is zero
            return 0.0;
        }
    
        ////////////////////////////////////////////////////////////////////////

        template <class C2P, class P2C>
        void Constant<C2P,P2C>::save(const std::string /*directory*/) const
        {
            // No generic way of saving expectations so can't do anything here
            return;
        }
    
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
