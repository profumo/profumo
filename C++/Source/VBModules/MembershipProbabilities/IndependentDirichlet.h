// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Manages an arbitrary matrix of independent Dirichlet distributions, useful
// for modelling membership probabilities in mixture models. This is a thin
// wrapper around VBDistributions::Dirichlet that deals with expectation
// gathering and caching.

#ifndef VB_MODULES_MEMBERSHIP_PROBABILITIES_INDEPENDENT_DIRICHLET_H
#define VB_MODULES_MEMBERSHIP_PROBABILITIES_INDEPENDENT_DIRICHLET_H

#include <array>
#include <armadillo>
#include <string>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "VBDistributions/Dirichlet.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace MembershipProbabilities
        {
            
            template<unsigned int nClasses>
            class IndependentDirichlet :
                public Modules::MembershipProbability_VBPosterior,
                protected CachedModule<Modules::MembershipProbabilities::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters
                {
                public:
                    std::array<arma::fmat, nClasses> counts;
                };
                //--------------------------------------------------------------
                
                IndependentDirichlet(const Parameters& prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            private:
                // Storage for posteriors
                // Use value semantics - want a nice simple memory layout (and
                // each class is self contained) to try and help caching
                // https://stackoverflow.com/a/71264
                typedef VBDistributions::Dirichlet<nClasses> Dirichlet;
                std::vector<Dirichlet> posteriors_;
                const unsigned int nRows_, nCols_, nElem_;
                
                // Helper functions to collect expectations / set caches
                Modules::MembershipProbabilities::C2P collectExpectations() const;
                void updateExpectations();
            };
            
        }
    }
}

// Template implementation
#include "IndependentDirichlet.t++"

#endif
