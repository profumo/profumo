// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "DynamicDirichlet.h"

#include <iomanip>
#include <sys/stat.h>

#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/special_functions/digamma.hpp>
namespace bmath = boost::math;

namespace MemberProbs = PROFUMO::Modules::MembershipProbabilities;
namespace VBMemberProbs = PROFUMO::VBModules::MembershipProbabilities;

////////////////////////////////////////////////////////////////////////////////

VBMemberProbs::DynamicDirichlet::DynamicDirichlet(const Parameters& prior)
: prior_(prior), posterior_(prior_), nClasses_(prior.counts.size())
{
    // Initialise expectations
    expectations_.p    = std::vector<arma::fmat>(nClasses_, arma::zeros<arma::fmat>(arma::size(prior_.counts[0])));
    expectations_.logP = std::vector<arma::fmat>(nClasses_, arma::zeros<arma::fmat>(arma::size(prior_.counts[0])));
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBMemberProbs::DynamicDirichlet::update()
{
    // Collect the data from all the children
    const Modules::MembershipProbabilities::C2P D = collectExpectations();
    
    // Update the Dirichlet posteriors
    for (unsigned int c = 0; c < nClasses_; ++c) {
        posterior_.counts[c] = prior_.counts[c] + D.counts[c];
    }
    
    //Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double VBMemberProbs::DynamicDirichlet::getKL() const
{
    // Cache some useful results
    arma::fmat sumPrior = prior_.counts[0];
    arma::fmat sumPosterior = posterior_.counts[0];
    for (unsigned int c = 1; c < nClasses_; ++c) {
        sumPrior += prior_.counts[c];
        sumPosterior += posterior_.counts[c];
    }
    
    // Calculate KL
    double KL = 0.0;
    #pragma omp parallel for reduction(+:KL) schedule(dynamic,512) if(sumPrior.n_elem > 2048)
    for (unsigned int i = 0; i < sumPrior.n_elem; ++i) {
        const float& spr = sumPrior[i];
        const float& spo = sumPosterior[i];
        const float dspo = bmath::digamma(spo);
        
        KL += bmath::lgamma(spo) - bmath::lgamma(spr);
        
        for (unsigned int c = 0; c < nClasses_; ++c) {
            const float& pr = prior_.counts[c][i];
            const float& po = posterior_.counts[c][i];
            
            KL += bmath::lgamma(pr) - bmath::lgamma(po)
                  + (po - pr) * (bmath::digamma(po) - dspo);
        }
    }
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void VBMemberProbs::DynamicDirichlet::save(const std::string directory) const
{
    // Save cached expectations for each component in turn
    const unsigned int padWidth = ((unsigned int) std::floor(std::log10(nClasses_))) + 1;
    for (unsigned int c = 0; c < nClasses_; ++c) {
        
        //Get zero padded class number
        std::ostringstream classNumber;
        classNumber << std::setw( padWidth ) << std::setfill( '0' ) << c+1;
        
        // Make directory
        const std::string classDir = directory + "Class_" + classNumber.str() + "/";
        mkdir( classDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        
        // Save!
        std::string fileName;
        
        // P
        fileName = classDir + "Probabilities.hdf5";
        expectations_.p[c].save(fileName, arma::hdf5_binary);
        
        // logP
        fileName = classDir + "LogProbabilities.hdf5";
        expectations_.logP[c].save(fileName, arma::hdf5_binary);
        
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MemberProbs::C2P VBMemberProbs::DynamicDirichlet::collectExpectations() const
{
    // Group-level model so parallelise collection
    
    // Intialise expectations
    MemberProbs::C2P D;
    D.counts = std::vector<arma::fmat>(nClasses_, arma::zeros<arma::fmat>(arma::size(prior_.counts[0])));
    
    // Switch based on number of children
    if (childModules_.size() >= 100) {
        // If we have a lot of children, such that we can expect each thread 
        // to be used lots of times, recreate a reduction block.
        // Each thread has a set of local expectations, and these are all added 
        // together at the end. This parallelises both the expectation 
        // gathering and the accumulation into one set of expectations. 
        
        #pragma omp parallel
        {
        // Give each thread a local set of expectations
        MemberProbs::C2P D_local;
        D_local.counts = std::vector<arma::fmat>(nClasses_, arma::zeros<arma::fmat>(arma::size(prior_.counts[0])));
        
        // Loop over all children, collecting expectations
        for (listType::const_iterator child = childModules_.begin(); child != childModules_.end(); ++child) {
            #pragma omp single nowait
            {
            const MemberProbs::C2P d = (*child)->getExpectations();
            for (unsigned int c = 0; c < nClasses_; ++c) {
                D_local.counts[c] += d.counts[c];
            }
            }
        }
        
        // Now collect all local expectations
        #pragma omp critical
        {
        for (unsigned int c = 0; c < nClasses_; ++c) {
            D.counts[c] += D_local.counts[c];
        }
        }
        }
    }
    else {
        // If we don't have many children then the overhead of assigning each 
        // thread its own expectations is proably too high (accumulation is 
        // relatively cheap compared to instantiation). This therefore is a 
        // more conventional method for parallelisation where the expectation 
        // gathering is sped up, but the accumulation takes the same amount 
        // of time.  
        
        #pragma omp parallel
        {
        for (listType::const_iterator child = childModules_.begin(); child != childModules_.end(); ++child) {
            #pragma omp single nowait
            {
            const MemberProbs::C2P d = (*child)->getExpectations();
            #pragma omp critical
            {
            for (unsigned int c = 0; c < nClasses_; ++c) {
                D.counts[c] += d.counts[c];
            }
            }
            }
        }
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBMemberProbs::DynamicDirichlet::updateExpectations()
{
    // Reset expectations
    for (unsigned int c = 0; c < nClasses_; ++c) {
        expectations_.p[c].zeros();
        expectations_.logP[c].zeros();
    }
    
    // Cache some useful results
    arma::fmat sumCounts = posterior_.counts[0];
    for (unsigned int c = 1; c < nClasses_; ++c) {
        sumCounts += posterior_.counts[c];
    }
    
    arma::fmat digammaSumCounts = sumCounts;
    #pragma omp parallel for schedule(dynamic,512) if(digammaSumCounts.n_elem > 2048)
    for (unsigned int i = 0; i < digammaSumCounts.n_elem; ++i) {
        float& dsc = digammaSumCounts[i];
        dsc = bmath::digamma(dsc);
    }
    
    // Probabilities
    for (unsigned int c = 0; c < nClasses_; ++c) {
        expectations_.p[c] = posterior_.counts[c] / sumCounts;
    }
    
    // log(Probabilities)
    // digamma(counts) - digamma(sum(counts))
    for (unsigned int c = 0; c < nClasses_; ++c) {
        arma::fmat digammaCounts = posterior_.counts[c];
        #pragma omp parallel for schedule(dynamic,512) if(digammaCounts.n_elem > 2048)
        for (unsigned int i = 0; i < digammaCounts.n_elem; ++i) {
            float& dc = digammaCounts[i];
            dc = bmath::digamma(dc);
        }
        expectations_.logP[c] = digammaCounts - digammaSumCounts;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
