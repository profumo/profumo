// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines arbitrary matrix of independent Dirichlet distributions, useful
// for modelling membership probabilities in mixture models. As opposed to
// `IndependentDirichlet`, this does not rely on the VBDistributions
// implementation as the size there needs to be known at compile time.
// Rather, it defines the distributions in terms of Armadillo matrices such
// that the number of classes can be set at runtime.

#ifndef VB_MODULES_MEMBERSHIP_PROBABILITIES_DYNAMIC_DIRICHLET_H
#define VB_MODULES_MEMBERSHIP_PROBABILITIES_DYNAMIC_DIRICHLET_H

#include <armadillo>
#include <string>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "VBDistributions/Dirichlet.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace MembershipProbabilities
        {
            
            class DynamicDirichlet :
                public Modules::MembershipProbability_VBPosterior,
                protected CachedModule<Modules::MembershipProbabilities::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters
                {
                public:
                    std::vector<arma::fmat> counts;
                };
                //--------------------------------------------------------------
                
                DynamicDirichlet(const Parameters& prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            private:
                // Storage for posteriors
                const Parameters prior_;
                Parameters posterior_;
                
                const unsigned int nClasses_;
                
                // Helper functions to collect expectations / set caches
                Modules::MembershipProbabilities::C2P collectExpectations() const;
                void updateExpectations();
            };
            
        }
    }
}

#endif
