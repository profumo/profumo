// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a matrix of independent Gaussian distributions. This is a thin
// wrapper around VBDistributions::Gaussian that deals with expectation
// gathering and caching.

#ifndef VB_MODULES_MATRIX_MEANS_INDEPENDENT_GAUSSIAN_H
#define VB_MODULES_MATRIX_MEANS_INDEPENDENT_GAUSSIAN_H

#include <armadillo>
#include <string>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "VBDistributions/Gaussian.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace MatrixMeans
        {
            
            class IndependentGaussian :
                public Modules::MatrixMean_VBPosterior,
                protected CachedModule<Modules::MatrixMeans::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    arma::fmat mu;
                    arma::fmat sigma2;
                    bool randomInitialisation = true;
                };
                //--------------------------------------------------------------
                
                IndependentGaussian(const Parameters& prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            private:
                // Storage for posteriors
                // Use value semantics - want a nice simple memory layout (and
                // each class is self contained) to try and help caching
                // https://stackoverflow.com/a/71264
                typedef VBDistributions::Gaussian Gaussian;
                std::vector<Gaussian> posteriors_;
                const unsigned int nRows_, nCols_, nElem_;
                
                // Helper functions to collect expectations / set caches
                Modules::MatrixMeans::C2P collectExpectations() const;
                void updateExpectations();
            };
            
        }
    }
}
#endif
