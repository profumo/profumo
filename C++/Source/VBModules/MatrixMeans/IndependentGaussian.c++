// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "IndependentGaussian.h"

namespace MatMeans = PROFUMO::Modules::MatrixMeans;
namespace VBMatMeans = PROFUMO::VBModules::MatrixMeans;

////////////////////////////////////////////////////////////////////////////////

VBMatMeans::IndependentGaussian::IndependentGaussian(const Parameters& prior)
: nRows_(prior.mu.n_rows), nCols_(prior.mu.n_cols), nElem_(prior.mu.n_elem)
{
    // Generate all the posteriors
    posteriors_.reserve(nElem_);
    for (unsigned int i = 0; i < nElem_; ++i) {
        Gaussian::Parameters elemPrior;
        elemPrior.mu     = prior.mu[i];
        elemPrior.sigma2 = prior.sigma2[i];
        posteriors_.emplace_back(elemPrior, prior.randomInitialisation);
    }
    
    // Intialise expectations
    expectations_.X  = arma::zeros<arma::fmat>(nRows_,nCols_);
    expectations_.X2 = arma::zeros<arma::fmat>(nRows_,nCols_);
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBMatMeans::IndependentGaussian::update()
{
    // Collect the data from all the children
    const MatMeans::C2P D = collectExpectations();
    
    // And collect from posteriors
    #pragma omp parallel for schedule(dynamic,512) if(nElem_ > 2048)
    for (unsigned int i = 0; i < nElem_; ++i) {
        Gaussian::DataExpectations d;
        d.psi   = D.Psi[i];
        d.psi_d = D.PsiD[i];
        
        posteriors_[i].update(d);
    }
    
    // Gather expectations
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double VBMatMeans::IndependentGaussian::getKL() const
{
    double KL = 0.0;
    #pragma omp parallel for reduction(+:KL) schedule(dynamic,512) if(nElem_ > 2048)
    for (unsigned int i = 0; i < nElem_; ++i) {
        KL += posteriors_[i].getKL();
    }
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void VBMatMeans::IndependentGaussian::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // Means
    fileName = directory + "Means.hdf5";
    expectations_.X.save(fileName, arma::hdf5_binary);
    
    // Variances
    fileName = directory + "Variances.hdf5";
    expectations_.X2.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MatMeans::C2P VBMatMeans::IndependentGaussian::collectExpectations() const
{
    // Group-level model so parallelise collection
    
    // Intialise expectations
    MatMeans::C2P D;
    D.Psi  = arma::zeros<arma::fmat>(nRows_,nCols_);
    D.PsiD = arma::zeros<arma::fmat>(nRows_,nCols_);
    
    // Switch based on number of children
    if (childModules_.size() >= 100) {
        // If we have a lot of children, such that we can expect each thread 
        // to be used lots of times, recreate a reduction block.
        // Each thread has a set of local expectations, and these are all added 
        // together at the end. This parallelises both the expectation 
        // gathering and the accumulation into one set of expectations. 
        
        #pragma omp parallel
        {
        // Give each thread a local set of expectations
        MatMeans::C2P D_local;
        D_local.Psi  = arma::zeros<arma::fmat>(nRows_,nCols_);
        D_local.PsiD = arma::zeros<arma::fmat>(nRows_,nCols_);
        
        // Loop over all children, collecting expectations
        for (listType::const_iterator child = childModules_.begin(); child != childModules_.end(); ++child) {
            #pragma omp single nowait
            {
            const MatMeans::C2P d = (*child)->getExpectations();
            D_local.Psi  += d.Psi;
            D_local.PsiD += d.PsiD;
            }
        }
        
        // Now collect all local expectations
        #pragma omp critical
        {
        D.Psi  += D_local.Psi;
        D.PsiD += D_local.PsiD;
        }
        }
    }
    else {
        // If we don't have many children then the overhead of assigning each 
        // thread its own expectations is proably too high (accumulation is 
        // relatively cheap compared to instantiation). This therefore is a 
        // more conventional method for parallelisation where the expectation 
        // gathering is sped up, but the accumulation takes the same amount 
        // of time.  
        
        #pragma omp parallel
        {
        for (listType::const_iterator child = childModules_.begin(); child != childModules_.end(); ++child) {
            #pragma omp single nowait
            {
            const MatMeans::C2P d = (*child)->getExpectations();
            #pragma omp critical
            {
            D.Psi  += d.Psi;
            D.PsiD += d.PsiD;
            }
            }
        }
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void VBMatMeans::IndependentGaussian::updateExpectations()
{
    // Reset expectations
    expectations_.X.zeros();
    expectations_.X2.zeros();
    
    // And collect from posteriors
    #pragma omp parallel for schedule(dynamic,512) if(nElem_ > 2048)
    for (unsigned int i = 0; i < nElem_; ++i) {
        const Gaussian::Expectations e = posteriors_[i].getExpectations();
        
        expectations_.X[i]  = e.x;
        expectations_.X2[i] = e.x2;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
