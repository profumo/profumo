// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Manages an arbitrary matrix of independent Gaussian mixture models. This is
// a thin wrapper around VBDistributions::GaussianMixtureModel that deals with
// expectation gathering and caching.

#ifndef VB_MODULES_MATRIX_MEANS_INDEPENDENT_MIXTURE_MODEL_H
#define VB_MODULES_MATRIX_MEANS_INDEPENDENT_MIXTURE_MODEL_H

#include <armadillo>
#include <string>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "VBDistributions/GaussianMixtureModel.h"

namespace PROFUMO
{
    namespace VBModules
    {
        namespace MatrixMeans
        {
            
            template<unsigned int nDelta, unsigned int nGaussian>
            class IndependentMixtureModel :
                public Modules::MatrixMean_VBPosterior,
                protected CachedModule<Modules::MatrixMeans::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct GaussianParameters
                {
                public:
                    arma::fmat mu;
                    arma::fmat sigma2;
                    arma::fmat p;
                };
                //--------------------------------------------------------------
                struct DeltaParameters
                {
                public:
                    arma::fmat mu;
                    arma::fmat p;
                };
                //--------------------------------------------------------------
                struct Parameters
                {
                public:
                    std::array<DeltaParameters, nDelta> deltas;
                    std::array<GaussianParameters, nGaussian> gaussians;
                    bool randomInitialisation = true;
                };
                //--------------------------------------------------------------
                
                IndependentMixtureModel(const Parameters& prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            private:
                // Storage for posteriors
                // Use value semantics - want a nice simple memory layout (and
                // each class is self contained) to try and help caching
                // https://stackoverflow.com/a/71264
                typedef VBDistributions::GaussianMixtureModel<nDelta, nGaussian> GMM;
                std::vector<GMM> posteriors_;
                const unsigned int nRows_, nCols_, nElem_;
                
                // Helper functions to collect expectations / set caches
                Modules::MatrixMeans::C2P collectExpectations() const;
                void updateExpectations();
            };
            
        }
    }
}

// Template implementation
#include "IndependentMixtureModel.t++"

#endif
