// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Gaussian.h"

#include <cmath>
#include <random>

#include "Utilities/MiscMaths.h"
#include "Utilities/Random.h"

namespace miscmaths = PROFUMO::Utilities::MiscMaths;
namespace VBDists = PROFUMO::VBDistributions;

////////////////////////////////////////////////////////////////////////////////

VBDists::Gaussian::Gaussian(const Parameters& prior, const bool randomInitialisation)
: prior_(prior), posterior_(prior)
{
    // Randomly draw mean from prior, if requested
    if (randomInitialisation) {
        auto generator = Utilities::get_random_engine();
        std::normal_distribution<float> priorDistribution(prior_.mu, std::sqrt(prior_.sigma2));
        posterior_.mu = priorDistribution(generator);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Gaussian::update(const DataExpectations D)
{
    // sigma
    posterior_.sigma2 = 1.0f / ( D.psi + (1.0f / prior_.sigma2) );
    
    // mu
    posterior_.mu = posterior_.sigma2 * ( D.psi_d + (prior_.mu / prior_.sigma2) );
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

VBDists::Gaussian::Expectations VBDists::Gaussian::getExpectations() const
{
    Expectations E;
    
    E.x = posterior_.mu;
    E.x2 = miscmaths::square(posterior_.mu) + posterior_.sigma2;
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

double VBDists::Gaussian::getKL() const
{
    // Convert params to double to begin with
    const double prior_sigma2 = prior_.sigma2, post_sigma2 = posterior_.sigma2;
    const double prior_mu = prior_.mu, post_mu = posterior_.mu;
    
    const double KL =
        0.5 * (
            std::log(prior_sigma2) - std::log(post_sigma2)
            + (
                miscmaths::square(post_mu - prior_mu)
                + post_sigma2
            ) / prior_sigma2
            - 1.0
        );
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
