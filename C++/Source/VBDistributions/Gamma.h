// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Update rules for a set of independent gamma distributions
// See documentation for the update rules

#ifndef VB_DISTRIBUTIONS_GAMMA_H
#define VB_DISTRIBUTIONS_GAMMA_H

#include "VBDistribution.h"


namespace PROFUMO
{
    namespace VBDistributions
    {
        ////////////////////////////////////////////////////////////////////////
        
        namespace DataExpectations
        {
            struct Gamma
            {
            public:
                float n;  // Number of observations
                float d2; // Sum of squares of observations
            };
        }
        
        namespace Expectations
        {
            struct Gamma
            {
            public:
                float x;
                float log_x;
            };
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        class Gamma :
            public virtual VBDistribution<DataExpectations::Gamma, Expectations::Gamma>
        {
        public:
            //------------------------------------------------------------------
            struct Parameters
            {
            public:
                float a;  // Shape
                float b;  // Rate
            };
            //------------------------------------------------------------------
            
            // Reference to allow polymorphism
            Gamma(const Parameters& prior);
            
            void update(const DataExpectations D);
            
            Expectations getExpectations() const;
            
            double getKL() const;
            
        protected:
            const Parameters prior_;
            Parameters posterior_;
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
