// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Update rules for a Dirichlet distribution

#ifndef VB_DISTRIBUTIONS_DIRICHLET_H
#define VB_DISTRIBUTIONS_DIRICHLET_H

#include <array>

#include "VBDistribution.h"


namespace PROFUMO
{
    namespace VBDistributions
    {
        ////////////////////////////////////////////////////////////////////////
        
        namespace DataExpectations
        {
            template<unsigned int nClasses>
            struct Dirichlet
            {
            public:
                std::array<float, nClasses> counts;
            };
        }
        
        namespace Expectations
        {
            template<unsigned int nClasses>
            struct Dirichlet
            {
            public:
                std::array<float, nClasses> p;
                std::array<float, nClasses> log_p;
            };
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        // Note that all storage is managed by the class and is on the stack,
        // so this should only really be used as part of e.g. a std::vector
        // where the classes themselves will end up on the heap.
        
        template<unsigned int nClasses>
        class Dirichlet :
            public virtual VBDistribution<DataExpectations::Dirichlet<nClasses>, Expectations::Dirichlet<nClasses>>
        {
        public:
            static_assert(nClasses > 0, "Dirirchlet distribution must have at least one class!");
            //------------------------------------------------------------------
            struct Parameters
            {
            public:
                std::array<float, nClasses> counts;
            };
            //------------------------------------------------------------------
            // Bring base class typedefs into scope
            // https://stackoverflow.com/a/13405829
            using DataExpectations = typename VBDistribution<VBDistributions::DataExpectations::Dirichlet<nClasses>, VBDistributions::Expectations::Dirichlet<nClasses>>::DataExpectations;
            using Expectations = typename VBDistribution<VBDistributions::DataExpectations::Dirichlet<nClasses>, VBDistributions::Expectations::Dirichlet<nClasses>>::Expectations;
            
            // Reference to allow polymorphism
            Dirichlet(const Parameters& prior);
            
            void update(const DataExpectations D);
            
            Expectations getExpectations() const;
            
            double getKL() const;
            
        protected:
            const Parameters prior_;
            Parameters posterior_;
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}

// Template implementation
#include "Dirichlet.t++"

#endif
