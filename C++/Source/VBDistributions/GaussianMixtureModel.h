// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Update rules for a set of independent Gaussian mixture models
// See documentation for the update rules

#ifndef VB_DISTRIBUTIONS_GAUSSIAN_MIXTURE_MODEL_H
#define VB_DISTRIBUTIONS_GAUSSIAN_MIXTURE_MODEL_H

#include <array>
#include <utility>

#include "VBDistribution.h"
#include "VBDistributions/Gaussian.h"
#include "Utilities/MiscMaths.h"

////////////////////////////////////////////////////////////////////////////////
// Definition of a couple of mixture model distributions
////////////////////////////////////////////////////////////////////////////////

namespace PROFUMO
{
    namespace VBDistributions
    {
        ////////////////////////////////////////////////////////////////////////
        
        class GaussianMMD :
            public MixtureModelDistribution<DataExpectations::Gaussian, Expectations::Gaussian>,
            public Gaussian
        {
        public:
            GaussianMMD(const Parameters& prior, const bool randomInitialisation = false);
            float getEvidence() const;
        };
        
        ////////////////////////////////////////////////////////////////////
        
        class DeltaMMD :
            public MixtureModelDistribution<DataExpectations::Gaussian, Expectations::Gaussian>
        {
        public:
            //--------------------------------------------------------------
            struct Parameters
            {
            public:
                float mu;
            };
            //--------------------------------------------------------------
            
            DeltaMMD(const Parameters& prior);
            void update(const DataExpectations D);
            Expectations getExpectations() const;
            double getKL() const;
            float getEvidence() const;
            
        protected:
            // Prior is always equal to the posterior (infinite precision), so
            // don't keep separate prior/posterior structs hanging around
            const Parameters parameters_;
            float evidence_;
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}

////////////////////////////////////////////////////////////////////////////////
// Definition of the mixture model itself
////////////////////////////////////////////////////////////////////////////////

namespace PROFUMO
{
    namespace VBDistributions
    {
        ////////////////////////////////////////////////////////////////////////
        
        template<unsigned int nDelta, unsigned int nGaussian>
        class GaussianMixtureModel :
            public virtual VBDistribution<DataExpectations::Gaussian, Expectations::Gaussian>
        {
        public:
            static_assert(nDelta + nGaussian > 0, "Gaussian mixture model must have at least one component!");
            //------------------------------------------------------------------
            struct DeltaParameters :
                public DeltaMMD::Parameters
            {
            public:
                float p;
            };
            //------------------------------------------------------------------
            struct GaussianParameters :
                public GaussianMMD::Parameters
            {
            public:
                float p;
            };
            //------------------------------------------------------------------
            // Bring base class typedefs into scope
            // https://stackoverflow.com/a/13405829
            using DataExpectations = typename VBDistribution<VBDistributions::DataExpectations::Gaussian, VBDistributions::Expectations::Gaussian>::DataExpectations;
            using Expectations = typename VBDistribution<VBDistributions::DataExpectations::Gaussian, VBDistributions::Expectations::Gaussian>::Expectations;

            GaussianMixtureModel(
                    const std::array<DeltaParameters, nDelta>& deltaPriors,
                    const std::array<GaussianParameters, nGaussian>& gaussianPriors,
                    const bool randomInitialisation = false);
            
            void update(const DataExpectations D);
            
            Expectations getExpectations() const;
            
            double getKL() const;
            
        protected:
            typedef MixtureModelComponent<DeltaMMD> DeltaComponent;
            std::array<DeltaComponent, nDelta> deltas_;
            
            typedef MixtureModelComponent<GaussianMMD> GaussianComponent;
            std::array<GaussianComponent, nGaussian> gaussians_;
            
            // Components don't have default constructors
            // Use variadic template magic to initialise
            template<std::size_t... Is>
            std::array<DeltaComponent, sizeof...(Is)> makeDeltas(
                    const std::array<DeltaParameters, nDelta>& priors,
                    std::index_sequence<Is...>)
            {
                return { DeltaComponent(DeltaMMD(std::get<Is>(priors)), std::get<Is>(priors).p)... };
            }
            
            template<std::size_t... Is>
            std::array<GaussianComponent, sizeof...(Is)> makeGaussians(
                    const std::array<GaussianParameters, nGaussian>& priors,
                    const bool randomInitialisation,
                    std::index_sequence<Is...>)
            {
                return { GaussianComponent(GaussianMMD(std::get<Is>(priors), randomInitialisation), std::get<Is>(priors).p)... };
            }
            // https://kevinushey.github.io/blog/2016/01/27/introduction-to-c++-variadic-templates/
            // https://www.murrayc.com/permalink/2015/12/05/modern-c-variadic-template-parameters-and-tuples/
            // https://stackoverflow.com/a/46899787
            // https://stackoverflow.com/a/37897057
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}

////////////////////////////////////////////////////////////////////////////////

// Template implementation of the mixture model
#include "GaussianMixtureModel.t++"

////////////////////////////////////////////////////////////////////////////////
#endif
