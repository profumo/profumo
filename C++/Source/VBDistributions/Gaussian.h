// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Update rules for a set of independent Gaussian distributions
// See documentation for the update rules

#ifndef VB_DISTRIBUTIONS_GAUSSIAN_H
#define VB_DISTRIBUTIONS_GAUSSIAN_H

#include "VBDistribution.h"


namespace PROFUMO
{
    namespace VBDistributions
    {
        ////////////////////////////////////////////////////////////////////////
        
        namespace DataExpectations
        {
            struct Gaussian
            {
            public:
                float psi;   //Precision of observations
                float psi_d; //Sum of observations, weighted by their precisions
            };
        }
        
        namespace Expectations
        {
            struct Gaussian
            {
            public:
                float x;  // Mean
                float x2; // Second moment (non-centred)
            };
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        class Gaussian :
            public virtual VBDistribution<DataExpectations::Gaussian, Expectations::Gaussian>
        {
        public:
            //------------------------------------------------------------------
            struct Parameters
            {
            public:
                float mu;
                float sigma2;
            };
            //------------------------------------------------------------------
            
            // Reference to allow polymorphism
            Gaussian(const Parameters& prior, const bool randomInitialisation = false);
            
            void update(const DataExpectations D);
            
            Expectations getExpectations() const;
            
            double getKL() const;
            
        protected:
            const Parameters prior_;
            Parameters posterior_;
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
