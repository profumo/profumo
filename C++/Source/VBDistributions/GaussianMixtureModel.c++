// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GaussianMixtureModel.h"

#include <cmath>
#include <random>

#include "Utilities/MiscMaths.h"
namespace miscmaths = PROFUMO::Utilities::MiscMaths;

namespace VBDists = PROFUMO::VBDistributions;

////////////////////////////////////////////////////////////////////////////////
// Mixture Model Component: Gaussian
////////////////////////////////////////////////////////////////////////////////

VBDists::GaussianMMD::GaussianMMD(const Parameters& prior, const bool randomInitialisation)
: Gaussian(prior, randomInitialisation)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

float VBDists::GaussianMMD::getEvidence() const
{
    // Calculate!
    const float evidence =
        0.5f * (
            std::log(posterior_.sigma2)
            - std::log(prior_.sigma2)
            + miscmaths::square(posterior_.mu) / posterior_.sigma2
            - miscmaths::square(prior_.mu) / prior_.sigma2
        );
    
    return evidence;
}

////////////////////////////////////////////////////////////////////////////////
// Mixture Model Component: Delta
////////////////////////////////////////////////////////////////////////////////

VBDists::DeltaMMD::DeltaMMD(const Parameters& prior)
: parameters_(prior)
{
    // Initialise cache - no data yet 
    evidence_ = 0.0f;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::DeltaMMD::update(const DataExpectations D)
{
    // Posterior always equals prior (infinite precision)
    // So just record the evidence
    evidence_ = D.psi_d * parameters_.mu - 0.5f * D.psi * miscmaths::square(parameters_.mu);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

VBDists::DeltaMMD::Expectations VBDists::DeltaMMD::getExpectations() const
{
    Expectations E;
    
    E.x  = parameters_.mu;
    E.x2 = miscmaths::square(parameters_.mu);
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

float VBDists::DeltaMMD::getEvidence() const
{
    return evidence_;
}

////////////////////////////////////////////////////////////////////////////////

double VBDists::DeltaMMD::getKL() const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
