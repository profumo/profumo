// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Gamma.h"

#include <cmath>

#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/special_functions/digamma.hpp>
namespace bmath = boost::math;

namespace VBDists = PROFUMO::VBDistributions;

////////////////////////////////////////////////////////////////////////////////

VBDists::Gamma::Gamma(const Parameters& prior)
: prior_(prior), posterior_(prior_)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

void VBDists::Gamma::update(const DataExpectations D)
{
    posterior_.a = prior_.a + D.n;
    
    posterior_.b = prior_.b + D.d2;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

VBDists::Gamma::Expectations VBDists::Gamma::getExpectations() const
{
    Expectations E;
    
    E.x = posterior_.a / posterior_.b;
    
    E.log_x =
        bmath::digamma(posterior_.a / 2.0f)
        - std::log(posterior_.b / 2.0f);
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

double VBDists::Gamma::getKL() const
{
    // Convert to double to begin with - for the noise models in particular
    // these values can be very large!
    const double prior_a = prior_.a, post_a = posterior_.a;
    const double prior_b = prior_.b, post_b = posterior_.b;
    
    const double KL =
        0.5 * (
            prior_a * (std::log(post_b) - std::log(prior_b))
            - post_a * (post_b - prior_b) / post_b
            + (post_a - prior_a) * bmath::digamma(post_a / 2.0)
        )
        - bmath::lgamma(post_a / 2.0)
        + bmath::lgamma(prior_a / 2.0);
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
