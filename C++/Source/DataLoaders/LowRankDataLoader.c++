// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "LowRankDataLoader.h"

#include <algorithm>  // std::min

#include "Utilities/LinearAlgebra.h"
#include "Utilities/RandomSVD.h"

namespace linalg = PROFUMO::Utilities::LinearAlgebra;

////////////////////////////////////////////////////////////////////////////////

PROFUMO::DataLoaders::LowRankDataTransformer::LowRankDataTransformer(const unsigned int N)
: N_(N)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

std::map<PROFUMO::RunID, PROFUMO::DataTypes::LowRankData> PROFUMO::DataLoaders::LowRankDataTransformer::transformSubjectData(const std::map<PROFUMO::RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices) const
{
    // Get a consistent ordering of RunIDs
    std::vector<RunID> runIDs;
    for (const auto& run : subjectDataMatrices) {
        runIDs.push_back(run.first);
    }
    
    // Calculate SVD over all data for this subject
    std::vector<const arma::fmat*> D;
    unsigned int n_rows = 0, n_cols = 0;
    for (RunID runID : runIDs) {
        // Get data
        const arma::fmat* d = subjectDataMatrices.at(runID).get();
        D.push_back(d);
        // Record sizes
        n_rows = d->n_rows; n_cols += d->n_cols;
    }
    // Make sure basis isn't bigger than the data...
    const unsigned int N = std::min({N_, n_rows, n_cols});
    const ConcatenatedSVD svd = Utilities::computeRandomConcatenatedSVD(D, N);
    
    // Now transform into the low rank form needed for our MFModels
    // U * diagmat(s) * V.t() = Pd * Ad
    
    // First, extract the shared spatial basis
    // Split the variance such that P and A have similar scalings
    std::shared_ptr<arma::fmat> Pd = std::make_shared<arma::fmat>();
    *Pd = svd.U * arma::diagmat(arma::sqrt( svd.s ));
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    Pd->elem( arma::find(arma::abs(*Pd) < 1.0e-10f) ).zeros();
    //**************************************************************************
    
    // Now populate the subject specific data
    std::map<RunID, DataTypes::LowRankData> subjectLRData;
    for (unsigned int r = 0; r < subjectDataMatrices.size(); ++r) {
        const RunID runID = runIDs[r];
        
        // Generate Ad
        std::shared_ptr<arma::fmat> Ad = std::make_shared<arma::fmat>();
        *Ad = arma::diagmat(arma::sqrt( svd.s )) * svd.V[r].t();
        //**********************************************************************
        // Seems to help matrix multiplications if matrices don't have wacky values
        Ad->elem( arma::find(arma::abs(*Ad) < 1.0e-10f) ).zeros();
        //**********************************************************************
        
        // Key stats
        const float TrDtD = linalg::trace_XtX( *subjectDataMatrices.at(runID) );
        
        // Combine into correct class
        DataTypes::LowRankData LRD(Pd, Ad, TrDtD);
        
        // And store
        subjectLRData.emplace(runID, LRD);
    }
    
    return subjectLRData;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::DataLoaders::LowRankDataLoader::LowRankDataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, const unsigned int N, const Options options)
: DataLoader<PROFUMO::DataTypes::LowRankData>( dataLocationsFile, outputDirectory, M, std::make_unique<LowRankDataTransformer>(N), options )
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SpatialBasis PROFUMO::DataLoaders::LowRankDataLoader::computeBasis(const unsigned int K) const
{
    typedef PROFUMO::DataTypes::LowRankData LRData;
    
    // Loop over the subject data map, recording subject-specific spatial bases
    std::vector<arma::fmat> spatialBases;
    // Get for each subject
    for (const auto& subject : DataLoader<LRData>::dataStore_) {
        // If there is some data, add it to the list
        // Just add one: all runs share the same spatial basis, and the 
        // scaling is appropriate for using one entry
        if ( ! subject.second.empty() ) {
            // HOLY SHIT WE NEED C++17 STRUCTURED BINDINGS HERE!!
            arma::fmat subjectBasis = *(subject.second.begin()->second.Pd);
            // Don't use too many components - if we have LowRankData we are in
            // the memory reduction game
            if (subjectBasis.n_cols > 2 * K) {
                subjectBasis = subjectBasis.head_cols(2 * K);
            }
            // Unweighted basis is orthonormal, so this is the scaling by sqrt(s)
            const arma::frowvec scaling =
                arma::sqrt(linalg::sum(arma::square(subjectBasis), 0));
            // Apply the scaling again to get U * s
            // See Python code for validation of taking the SVD over U * s
            subjectBasis.each_row() %= scaling;
            // And store
            spatialBases.push_back(subjectBasis);
        }
    }
    
    // Compute the random SVD of all these subject specific bases
    std::vector<const arma::fmat*> svdBases;
    unsigned int n_rows = 0, n_cols = 0;
    for (const auto& subjectBasis : spatialBases) {
        svdBases.push_back( &subjectBasis );
        // Record sizes
        n_rows = subjectBasis.n_rows; n_cols += subjectBasis.n_cols;
    }
    // Make sure basis isn't bigger than the data...
    const unsigned int k = std::min({K, n_rows, n_cols});
    const ConcatenatedSVD svd = Utilities::computeRandomConcatenatedSVD(svdBases, k);
    
    // Collate results
    SpatialBasis basis;
    basis.U = svd.U;
    basis.s = svd.s;
    basis.S = dataStore_.size();
    basis.T = n_cols;
    
    return basis;
}

////////////////////////////////////////////////////////////////////////////////

// Python code for checking scalings of concatenated SVD
/*
import numpy
import matplotlib, matplotlib.pyplot as plt
import random

# No of datasets
K = 10
# Data sizes
M = 200; N = [1000,2000]
# True data rank
R = 20
# Utilised rank
r = 50

# Generate data and do sub-SVDs
D = []; SVD = []
for k in range(K):
    n = random.randint(*N)
    d = numpy.random.randn(M,R) @ numpy.random.randn(R,n) + numpy.random.randn(M,n)
    
    D.append(d)
    SVD.append( numpy.linalg.svd(d, full_matrices=False) )

# Full SVD
[U,S,V] = numpy.linalg.svd(numpy.concatenate(D, axis=1), full_matrices=False)

# Rank r concatenated SVDs
us = numpy.concatenate([svd[0][:,:r] @ numpy.diag(svd[1][:r]) for svd in SVD], axis=1)
#us = numpy.concatenate([svd[0][:,:r] @ numpy.diag(svd[1][:r]**0.5) for svd in SVD], axis=1)
[u,s,v] = numpy.linalg.svd(us, full_matrices=False)

plt.figure()
plt.plot(S[:r], label="Full")
plt.plot(s[:r], label="Concat")
plt.plot(S[:r] - s[:r], label="Diff")
plt.legend()

plt.figure(); plt.imshow(numpy.corrcoef(U[:,:r].T,u[:,:r].T))

plt.show()
*/

// Python code for checking influence of noise on spatial basis
/*
import numpy
import matplotlib, matplotlib.pyplot as plt
import random

# Generate data
M = 2000; N = 1000; R = 200
d = numpy.random.randn(M,R) @ numpy.random.randn(R,N) + numpy.random.randn(M,N)
# And a noisy version
D = d + 0.5 * numpy.sqrt(R) * numpy.random.randn(M,N)

plt.figure(); plt.imshow(d); plt.colorbar(); plt.title("Clean data")
plt.figure(); plt.imshow(D); plt.colorbar(); plt.title("Noisy data")

[u,s,v] = numpy.linalg.svd(d, full_matrices=False)
[U,S,V] = numpy.linalg.svd(D, full_matrices=False)

plt.figure()
plt.plot(S, label="Noisy")
plt.plot(s, label="Clean")
plt.plot(S - s, label="Diff")
plt.legend()

plt.figure(); plt.imshow(numpy.corrcoef(U.T,u.T)); plt.colorbar(); plt.title("Noisy U; Clean U")

# Form U * s
US = (U @ numpy.diag(S))[:,:R]
us = (u @ numpy.diag(s))[:,:R]
# Predict US with us (i.e. gets rid of sign flip issues)
US_us = us @ numpy.linalg.pinv(us) @ US


plt.figure(); plt.imshow(numpy.corrcoef(numpy.concatenate([US,us,US_us], axis=1).T)); plt.colorbar(); plt.title("Noisy U*S; Clean U*S; Clean U*S (sign flipped)")

plt.figure(); plt.imshow(US - US_us); plt.colorbar(); plt.title("Noisy U*S - clean U*S")
plt.figure(); plt.plot(numpy.std(US - US_us, axis=0)); plt.title("std(Noisy U*S - clean U*S)")
*/
