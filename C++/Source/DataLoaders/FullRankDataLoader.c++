// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "FullRankDataLoader.h"

#include <algorithm>  // std::min

#include "Utilities/RandomSVD.h"

////////////////////////////////////////////////////////////////////////////////

std::map<PROFUMO::RunID, PROFUMO::DataTypes::FullRankData> PROFUMO::DataLoaders::FullRankDataTransformer::transformSubjectData(const std::map<PROFUMO::RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices) const
{
    std::map<PROFUMO::RunID, PROFUMO::DataTypes::FullRankData> subjectData;
    
    for (const auto& run : subjectDataMatrices) {
        subjectData.emplace( run.first, DataTypes::FullRankData(run.second) );
    }
    
    return subjectData;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::DataLoaders::FullRankDataLoader::FullRankDataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, const Options options)
: DataLoader<PROFUMO::DataTypes::FullRankData>( dataLocationsFile, outputDirectory, M, std::make_unique<FullRankDataTransformer>(), options )
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SpatialBasis PROFUMO::DataLoaders::FullRankDataLoader::computeBasis(const unsigned int K) const
{
    typedef PROFUMO::DataTypes::FullRankData FRData;
    
    // Loop over the subject data map, recording subject-specific spatial bases
    std::vector<const arma::fmat*> data;
    unsigned int n_rows = 0, n_cols = 0;
    // Get for each subject
    for (const auto& subject : DataLoader<FRData>::dataStore_) {
        // And record all runs
        for (const auto& run : subject.second) {
            const arma::fmat* d = run.second.D.get();
            data.push_back(d);
            // Record sizes
            n_rows = d->n_rows; n_cols += d->n_cols;
        }
    }
    // Make sure basis isn't bigger than the data...
    const unsigned int k = std::min({K, n_rows, n_cols});
    
    // Compute the random SVD of all these subject specific bases
    const ConcatenatedSVD svd = Utilities::computeRandomConcatenatedSVD(data, k);
    
    // Collate results
    SpatialBasis basis;
    basis.U = svd.U;
    basis.s = svd.s;
    basis.S = dataStore_.size();
    basis.T = n_cols;
    
    return basis;
}

////////////////////////////////////////////////////////////////////////////////
