// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Implementation of DataLoader for FullRankData.

#ifndef DATA_LOADERS_FULL_RANK_DATA_LOADER_H
#define DATA_LOADERS_FULL_RANK_DATA_LOADER_H

#include "DataLoader.h"
#include "DataTypes.h"

namespace PROFUMO
{
    namespace DataLoaders
    {
        ////////////////////////////////////////////////////////////////////////
        
        class FullRankDataTransformer :
            public DataTransformer<DataTypes::FullRankData>
        {
        public:
            // Converts a set of data matrices into FullRankData
            virtual std::map<RunID, DataTypes::FullRankData> transformSubjectData(const std::map<RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices) const;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        class FullRankDataLoader :
            public DataLoader<DataTypes::FullRankData>
        {
        public:
            // Constructor: just loads all the data
            FullRankDataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, const Options options = {});
            
        private:
            // Computes the spatial basis for all the data
            virtual SpatialBasis computeBasis(const unsigned int K) const;
        };
    
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
