// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Implementation of DataLoader for LowRankData.

#ifndef DATA_LOADERS_LOW_RANK_DATA_LOADER_H
#define DATA_LOADERS_LOW_RANK_DATA_LOADER_H

#include "DataLoader.h"
#include "DataTypes.h"

namespace PROFUMO
{
    namespace DataLoaders
    {
        ////////////////////////////////////////////////////////////////////////
        
        class LowRankDataTransformer :
            public DataTransformer<DataTypes::LowRankData>
        {
        public:
            // Constructor
            LowRankDataTransformer(const unsigned int N);
            
            // Turns a set of data matrices into LowRankData
            virtual std::map<RunID, DataTypes::LowRankData> transformSubjectData(const std::map<RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices) const;
            
        protected:
            // Rank of the approximation
            unsigned int N_;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        class LowRankDataLoader :
            public DataLoader<DataTypes::LowRankData>
        {
        public:
            // Constructor: loads all the data and reduces to rank N
            LowRankDataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, const unsigned int N, const Options options = {});
            
        private:
            // Computes the spatial basis for all the data
            virtual SpatialBasis computeBasis(const unsigned int K) const;
        };
    
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
