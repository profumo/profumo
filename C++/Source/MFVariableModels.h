// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Interface and expectations needed for the component parts of the matrix
// factorisation model

#ifndef MF_VARIABLE_MODELS_H
#define MF_VARIABLE_MODELS_H

#include <vector>
#include <armadillo>

#include "Module.h"
#include "Posterior.h"

namespace PROFUMO
{
    ////////////////////////////////////////////////////////////////////////////
    // Expectations
    
    namespace MFModels
    {
        ////////////////////////////////////////////////////////////////////////
        
        // Spatial maps
        namespace P
        {
            // What a model for P, the spatial maps, needs to return
            struct P2C
            {
            public:
                arma::fmat P;
                arma::fmat PtP;
            };
            
            // The expectations needed to update the VB posterior
            struct C2P
            {
            public:
                arma::fmat psiAAt;
                arma::fmat psiDAt;
            };
        }
        
        // Define parents
        typedef Module_Parent<P::C2P, P::P2C> P_Parent;
        typedef Posterior<P::C2P, P::P2C> P_Posterior;
        typedef VBPosterior<P::C2P, P::P2C> P_VBPosterior;
        
        // Define children
        typedef Module_Child<P::C2P, P::P2C> P_Child;
        
        class P_JointChild : public P_Child
        {
        public:
            P_JointChild(P_Parent* parentModule)
            : P_Child(parentModule)
            {
                return;
            }
            
            P::C2P getExpectations() const
            {
                return getPData();
            }
            
        protected:
            virtual P::C2P getPData() const = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Component weightings
        namespace H
        {
            struct P2C
            {
            public:
                arma::fvec h;
                arma::fmat hht;
            };
            
            struct C2P
            {
            public:
                arma::fmat psiPtPoAAt;
                arma::fvec diag_psiPtDAt;
            };
        }
        
        // Define parents
        typedef Module_Parent<H::C2P, H::P2C> H_Parent;
        typedef Posterior<H::C2P, H::P2C> H_Posterior;
        typedef VBPosterior<H::C2P, H::P2C> H_VBPosterior;
        
        // Define children
        typedef Module_Child<H::C2P, H::P2C> H_Child;
        
        class H_JointChild : public H_Child
        {
        public:
            H_JointChild(H_Parent* parentModule)
            : H_Child(parentModule)
            {
                return;
            }
            
            H::C2P getExpectations() const
            {
                return getHData();
            }
            
        protected:
            virtual H::C2P getHData() const = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Time courses
        namespace A
        {
            struct P2C
            {
            public:
                arma::fmat A;
                arma::fmat AAt;
            };
            
            struct C2P
            {
            public:
                arma::fmat psiPtP;
                arma::fmat psiPtD;
            };
        }
        
        // Define parents
        typedef Module_Parent<A::C2P, A::P2C> A_Parent;
        typedef Posterior<A::C2P, A::P2C> A_Posterior;
        typedef VBPosterior<A::C2P, A::P2C> A_VBPosterior;
        
        // Define children
        typedef Module_Child<A::C2P, A::P2C> A_Child;
        
        class A_JointChild : public A_Child
        {
        public:
            A_JointChild(A_Parent* parentModule)
            : A_Child(parentModule)
            {
                return;
            }
            
            A::C2P getExpectations() const
            {
                return getAData();
            }
            
        protected:
            virtual A::C2P getAData() const = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Noise
        namespace Psi
        {
            struct P2C
            {
            public:
                float psi;
                float logPsi;
            };
            
            struct C2P
            {
            public:
                float TrDtD;
                float N;
            };
        }
        
        // Define parents
        typedef Module_Parent<Psi::C2P, Psi::P2C> Psi_Parent;
        typedef Posterior<Psi::C2P, Psi::P2C> Psi_Posterior;
        typedef VBPosterior<Psi::C2P, Psi::P2C> Psi_VBPosterior;
        
        // Define children
        typedef Module_Child<Psi::C2P, Psi::P2C> Psi_Child;
        
        class Psi_JointChild : public Psi_Child
        {
        public:
            Psi_JointChild(Psi_Parent* parentModule)
            : Psi_Child(parentModule)
            {
                return;
            }
            
            Psi::C2P getExpectations() const
            {
                return getPsiData();
            }
            
        protected:
            virtual Psi::C2P getPsiData() const = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Some useful typedefs
    
    typedef MFModels::P_Parent MFPModel;
    typedef MFModels::P_JointChild MFModel_PInterface;
    
    typedef MFModels::H_Parent MFHModel;
    typedef MFModels::H_JointChild MFModel_HInterface;
    
    typedef MFModels::A_Parent MFAModel;
    typedef MFModels::A_JointChild MFModel_AInterface;
    
    typedef MFModels::Psi_Parent MFPsiModel;
    typedef MFModels::Psi_JointChild MFModel_PsiInterface;
    
    ////////////////////////////////////////////////////////////////////////////
}
#endif
