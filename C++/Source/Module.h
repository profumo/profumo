// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Minimal class structure that allows us to construct graphical models

#ifndef MODULE_H
#define MODULE_H

#include <vector>

namespace PROFUMO
{
    ////////////////////////////////////////////////////////////////////////////
    
    // Abstract class that just defines that a module has to return expectations
    //
    // We can use this to define reciprocal parent/child relationships between
    // different modules, and from there can construct whole graphical models.
    // Obviously, this message passing framework already makes some assumptions
    // about how we are going to solve the model, but they should not be too
    // restrictive. For example, this structure allows modules to represent
    // data, VB posteriors, posteriors inferred via sampling, constants etc
    
    template<class E>
    class Module
    {
    public:
        virtual E getExpectations() const = 0;
        
        virtual ~Module() = default;  // See end of file
    };
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Abstract class that defines a parent in a graphical model. This can
    // have an arbitrary number of children (c.f. multiple observations from a
    // distribution).
    
    // Expectations:
    // C2P: Child -> Parent
    // P2C: Parent -> Child
    
    template<class C2P, class P2C>
    class Module_Parent :
        public virtual Module<P2C>
    {
    public:
        void addChildModule(Module<C2P>* childModule);
        
    protected:
        typedef std::vector<Module<C2P>*> listType;
        listType childModules_;
    };
    
    
    template<class C2P, class P2C>
    void Module_Parent<C2P, P2C>::addChildModule(Module<C2P>* childModule)
    {
        #pragma omp critical
        {
        childModules_.push_back(childModule);
        }
        
        return;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Abstract class that defines a child in a graphical model. This has one
    // parent of this type, though can have multiple different parents via
    // multiple inheritance (c.f. mean and variance hyperpriors)
    
    // Expectations:
    // C2P: Child -> Parent
    // P2C: Parent -> Child
    
    template<class C2P, class P2C>
    class Module_Child :
        public virtual Module<C2P>
    {
    public:
        Module_Child(Module_Parent<C2P, P2C>* parentModule);
        
        void setParentModule(Module_Parent<C2P, P2C>* parentModule);
        
    protected:
        Module<P2C>* parentModule_;
    };
    
    
    // Has to be constructed with the parent present
    template<class C2P, class P2C>
    Module_Child<C2P, P2C>::Module_Child(Module_Parent<C2P, P2C>* parentModule)
    {
        setParentModule(parentModule);
        
        return;
    }
    
    // Code to set parent distribution (can be changed dynamically if required)
    template<class C2P, class P2C>
    void Module_Child<C2P, P2C>::setParentModule(Module_Parent<C2P, P2C>* parentModule)
    {
        // Record parent
        parentModule_ = parentModule;
        // And set up link
        parentModule->addChildModule(this);
        
        return;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Abstract class that just defines a module where there is an internal
    // cache for the expectations
    
    template<class E>
    class CachedModule :
        public virtual Module<E>
    {
    public:
        E getExpectations() const;
        
    protected:
        E expectations_;
    };
    
    
    template<class E>
    E CachedModule<E>::getExpectations() const
    {
        return expectations_;
    }
    
    ////////////////////////////////////////////////////////////////////////////
}
#endif

/*
### Default destructors

If we exclusively deal with `shared_ptr`, then they do all the magic of calling
the destructor of the original class (i.e. not any base class the pointer is
later cast to). However, `unique_ptr` is closer to a raw pointer and does not
do this wizardry:
<https://stackoverflow.com/a/22127258>

Given this, one approach would be just to use `shared_ptr` everywhere (e.g.
as suggested here: <https://stackoverflow.com/a/22861890>). However, this
feels somewhat restrictive.

The alternative is to stick with the classic virtual destructor pattern . The
key advice is '[A base class destructor should be either public and virtual, or
protected and nonvirtual](http://www.gotw.ca/publications/mill18.htm)'.

Things that make this easier:
 + `default`
 + We don't need to change anything in the derived classes, as this propagates
   up the chain:  <https://stackoverflow.com/a/41000041>,
   <https://stackoverflow.com/a/2198400>

Finally, note that this removes the default move constructors. This is
probably what we want <https://stackoverflow.com/a/26040064>, especially if
the base classes are abstract <https://stackoverflow.com/a/29289980>.
*/
