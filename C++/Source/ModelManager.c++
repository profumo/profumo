// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "ModelManager.h"

#include <sys/stat.h>
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <cmath>

#include <spdlog/spdlog.h>

#include "DataTypes.h"
#include "DataLoaders/FullRankDataLoader.h"
#include "DataLoaders/LowRankDataLoader.h"

#include "CohortModelling/GroupSpatialModels/GroupMaps.h"
#include "CohortModelling/GroupSpatialModels/DGMM.h"
#include "CohortModelling/GroupSpatialModels/AdditiveGaussian.h"
#include "CohortModelling/GroupSpatialModels/Parcellation.h"
#include "CohortModelling/GroupWeightModels/GroupWeights.h"
#include "CohortModelling/GroupWeightModels/IndependentWeights.h"
#include "CohortModelling/GroupWeightModels/ParameterisedWeights.h"
#include "CohortModelling/GroupTemporalModels/MultivariateNormal.h"
#include "CohortModelling/GroupTemporalModels/CleanHRF.h"
#include "CohortModelling/GroupTemporalModels/NoisyHRF.h"
#include "CohortModelling/GroupTemporalPrecisionModels/GroupPrecisionMatrix.h"
#include "CohortModelling/GroupTemporalPrecisionModels/SubjectPrecisionMatrices.h"
#include "CohortModelling/GroupTemporalPrecisionModels/RunPrecisionMatrices.h"
#include "CohortModelling/GroupTemporalPrecisionModels/CategorisedRunPrecisionMatrices.h"
#include "CohortModelling/GroupNoiseModels/IndependentRuns.h"

#include "MFModels/P/DGMM.h"
#include "MFModels/P/AdditiveGaussian.h"
#include "MFModels/P/Parcellation.h"
#include "MFModels/P/PottsParcellation.h"
#include "MFModels/A/MultivariateNormal.h"
#include "MFModels/A/KroneckerHRF.h"
#include "MFModels/A/AdditiveMultivariateNormal.h"
#include "MFModels/H/MultivariateNormal.h"
#include "MFModels/H/SpikeSlab.h"
#include "MFModels/Psi/GammaPrecision.h"

#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/PrecisionMatrices/Wishart.h"
#include "VBModules/PrecisionMatrices/HierarchicalWishart.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"
#include "VBModules/MatrixMeans/IndependentGaussian.h"
#include "VBModules/MatrixMeans/IndependentMixtureModel.h"
#include "VBModules/MatrixPrecisions/IndependentGamma.h"
#include "VBModules/MembershipProbabilities/IndependentDirichlet.h"
#include "VBModules/MembershipProbabilities/DynamicDirichlet.h"

#include "Utilities/DataNormalisation.h"
#include "Utilities/LinearAlgebra.h"
#include "Utilities/MatrixManipulations.h"
#include "Utilities/MiscMaths.h"
#include "Utilities/Timer.h"

namespace DN = PROFUMO::Utilities::DataNormalisation;
namespace linalg = PROFUMO::Utilities::LinearAlgebra;
namespace MM = PROFUMO::Utilities::MatrixManipulations;
namespace miscmaths = PROFUMO::Utilities::MiscMaths;

////////////////////////////////////////////////////////////////////////////////
// Convenience functions
namespace PROFUMO
{
    // Do a VB decomposition of a matrix (essentially a Bayesian spatial ICA if a spatial basis is fed in)
    arma::fmat decomposeMatrix(const arma::fmat& data, const unsigned int rank, const PROFUMO::ModelManager::SpatialModel spatialModel, const unsigned int iterations, const arma::fmat& initialMaps); // = arma::fmat());

    // Generate a randomised version of a basis, to initialise a decomposition
    arma::fmat generateRandomInitialisaion(const arma::fmat& basis, const unsigned int rank);
}
////////////////////////////////////////////////////////////////////////////////

PROFUMO::ModelManager::ModelManager( const std::string dataLocationsFile, const unsigned int M, const SpatialModel spatialModel, const SpatialParameters& spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel, const Options& options, const std::string preprocessingDirectory )
: M_(M), hrf_(options.hrf), dofCorrectionFactor_(options.dofCorrectionFactor)
{
    Utilities::Timer timer;
    spdlog::info("Building the model...");
    
    ////////////////////////////////////////////////////////////////////////////
    // Load the data
    
    // Storage for whichever data type we need - note that these aren't
    // polymorphic as they are templated on different DataTypes
    std::shared_ptr<DataLoaders::LowRankDataLoader>  LRDLoader = nullptr;
    std::shared_ptr<DataLoaders::FullRankDataLoader> FRDLoader = nullptr;
    
    // Load either full or reduced data
    if (options.lowRank) {
        LRDLoader = std::make_shared<DataLoaders::LowRankDataLoader>(
                dataLocationsFile, preprocessingDirectory, M_, *options.lowRank, options.dataLoader);
        S_ = LRDLoader->getS(); V_ = LRDLoader->getV();
    } else {
        FRDLoader = std::make_shared<DataLoaders::FullRankDataLoader>(
                dataLocationsFile, preprocessingDirectory, M_, options.dataLoader);
        S_ = FRDLoader->getS(); V_ = FRDLoader->getV();
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Get an initial set of spatial maps
    
    arma::fmat initialMaps;
    if (options.initialMaps) {
        initialMaps = (*options.initialMaps);
    }
    else if (spatialParameters.fixedGroupMeans && spatialParameters.fixedGroupMemberships) {
        initialMaps = (*spatialParameters.fixedGroupMeans) % (*spatialParameters.fixedGroupMemberships);
    }
    else if (spatialParameters.fixedGroupMeans) {
        initialMaps = (*spatialParameters.fixedGroupMeans);
    }
    else if (spatialParameters.fixedGroupMemberships) {
        initialMaps = (*spatialParameters.fixedGroupMemberships);
    }
    else {
        ////////////////////////////////////////////////////////////////////////
        // Get the spatial basis
        
        SpatialBasis spatialBasis;
        if (options.lowRank) {
            spatialBasis = LRDLoader->computeSpatialBasis( 2 * M_ );
        } else {
            spatialBasis = FRDLoader->computeSpatialBasis( 2 * M_ );
        }
        // Flip the column signs
        Utilities::MatrixManipulations::flipColumnSigns( spatialBasis.U );
        
        // Save!
        spatialBasis.U.save(preprocessingDirectory + "NormalisedSpatialBasis.hdf5", arma::hdf5_binary);
        {
            const arma::fmat weightedBasis = (
                    spatialBasis.U.each_row() % spatialBasis.s.t());
            weightedBasis.save(preprocessingDirectory + "SpatialBasis.hdf5", arma::hdf5_binary);
        }
        
        ////////////////////////////////////////////////////////////////////////
        // Decompose the spatial basis
        
        // We are aiming to get an approaximation of D = P*H*A + E
        // We have D = U*S*Vt available to us
        // This performs a simple Bayesian ICA, i.e. D = X*Y + E
        // We perform the ICA on U*S - somewhat surprisingly, adding noise to 
        // the data seems to induce constant variance noise on U*S (see simple 
        // code at the bottom of LowRankDataLoader.c++), which means that this 
        // is the matrix to decompose.
        // Therefore, U*S = X*Y + E; P ≈ X, H*A ≈ Y*Vt
        
        // We can always check the residuals post-hoc too to satisfy ourselves 
        // that the above holds, e.g.
        // SB = PROFUMO.loadHDF5(pfmDir + "/Preprocessing/SpatialBasis.hdf5")
        // IM = PROFUMO.loadHDF5(pfmDir + "/Preprocessing/InitialMaps.hdf5")
        // residuals = SB - IM @ numpy.linalg.pinv(IM) @ SB
        // PFMplots.plotMaps(SB, "Spatial basis"); PFMplots.plotMaps(IM, "Initial maps"); PFMplots.plotMaps(residuals, "Residuals")
        
        // Make directory for output
        const std::string decompositionDir = preprocessingDirectory + "BasisDecomposition/";
        mkdir( decompositionDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        
        // Do the decompositions
        initialMaps = generateInitialMaps(spatialBasis, spatialModel, options.multiStartIterations, options.globalInit, decompositionDir);
        
        ////////////////////////////////////////////////////////////////////////
    }
    
    // Save!
    initialMaps.save(preprocessingDirectory + "InitialMaps.hdf5", arma::hdf5_binary);
    
    ////////////////////////////////////////////////////////////////////////////
    // And make the Cohort!
    
    if (options.lowRank) {
        initialiseCohort(LRDLoader->getData(), initialMaps, weightModel, timeCourseModel, temporalPrecisionModel, noiseModel);
    } else {
        initialiseCohort(FRDLoader->getData(), initialMaps, weightModel, timeCourseModel, temporalPrecisionModel, noiseModel);
    }
    
    spdlog::info("Finished building simplified model. Time elapsed: {:s}", timer.getTimeElapsed());
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Switch to the requested spatial model
    switchSpatialModel(spatialModel, spatialParameters);
    
    ////////////////////////////////////////////////////////////////////////////
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::update(const unsigned int iterations, const std::string intermediatesDir, const bool normaliseTimeCourses)
{
    Utilities::Timer timer;
    spdlog::info("Updating model...");
    
    double F = getFreeEnergy();
    spdlog::info("Free energy before updates: {:.6e}", F);
    
    // Save initial maps
    saveIntermediates(intermediatesDir, 0, iterations);
    
    // Update model for as long as requested
    cohort_->setTimeCourseNormalisation(normaliseTimeCourses);
    for (unsigned int iteration = 1; iteration <= iterations; ++iteration) {
        spdlog::debug(std::string(20, '-'));
        spdlog::debug("Iteration: {:d} of {:d}", iteration, iterations);
        
        cohort_->updateSpatialModel();
        
        cohort_->updateWeightModel();
        
        cohort_->updateTemporalModel();
        
        cohort_->updateWeightModel();
        
        if ((iteration <= 25) || (iteration % 10 == 0)) {
            cohort_->updateNoiseModel();
        }
        
        // Free energy
        if (iteration % 5 == 0) {
            const double oldF = F;
            F = getFreeEnergy();
            spdlog::info("Free energy: {:.6e} (Δ: {:.2e})", F, F - oldF);
        }
        
        // Save intermediates
        if ((iteration <= 25) || (iteration % 10 == 0)) {
            saveIntermediates(intermediatesDir, iteration, iterations);
        }
    }
    cohort_->setTimeCourseNormalisation(false);
    spdlog::debug(std::string(20, '-'));
    
    F = getFreeEnergy();
    spdlog::info("Free energy after updates: {:.6e}", F);
    spdlog::info("Model updates finished. Time elapsed: {:s}\n", timer.getTimeElapsed());
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::switchSpatialModel(const SpatialModel spatialModel, const SpatialParameters& spatialParameters)
{
    Utilities::Timer timer;
    spdlog::info("Switching the spatial model...");
    
    cohort_->setSpatialModel( getSpatialModel(spatialModel, spatialParameters) );
    spdlog::debug("Model switched.");
    
    spdlog::debug("Initial updates...");
    for (unsigned int z = 0; z < 12; ++z) {
        spdlog::debug(std::string(20, '-'));
        spdlog::debug("Iteration: {:d}", z+1);
        // Update new model
        cohort_->updateSpatialModel();
        // And dependent parts, when established
        if (z >= 3) {
            cohort_->updateWeightModel();
            cohort_->updateNoiseModel();
        }
    }
    spdlog::debug(std::string(20, '-'));
    
    spdlog::info("New spatial model initialised. Time elapsed: {:s}\n", timer.getTimeElapsed());
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::switchWeightModel(const WeightModel weightModel)
{
    Utilities::Timer timer;
    spdlog::info("Switching the weight model...");
    
    cohort_->setWeightModel( getWeightModel(weightModel) );
    spdlog::debug("Model switched.");
    
    spdlog::debug("Initial updates...");
    for (unsigned int z = 0; z < 3; ++z) {
        spdlog::debug(std::string(20, '-'));
        spdlog::debug("Iteration: {:d}", z+1);
        // Update new model
        cohort_->updateWeightModel();
    }
    spdlog::debug(std::string(20, '-'));
    
    spdlog::info("New weight model initialised. Time elapsed: {:s}\n", timer.getTimeElapsed());
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::switchTemporalModel(const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel)
{
    Utilities::Timer timer;
    spdlog::info("Switching the temporal model...");
    
    cohort_->setTemporalModel( getTemporalModel(timeCourseModel, temporalPrecisionModel) );
    spdlog::debug("Model switched.");
    
    spdlog::debug("Initial updates...");
    for (unsigned int z = 0; z < 12; ++z) {
        spdlog::debug(std::string(20, '-'));
        spdlog::debug("Iteration: {:d}", z+1);
        // Update new model
        cohort_->updateTemporalModel();
        // And dependent parts, when established
        if (z >= 3) {
            cohort_->updateWeightModel();
            cohort_->updateNoiseModel();
        }
    }
    spdlog::debug(std::string(20, '-'));
    
    spdlog::info("New temporal model initialised. Time elapsed: {:s}\n", timer.getTimeElapsed());
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::switchNoiseModel(const NoiseModel noiseModel)
{
    Utilities::Timer timer;
    spdlog::info("Switching the noise model...");
    
    cohort_->setNoiseModel( getNoiseModel(noiseModel) );
    spdlog::debug("Model switched.");
    
    spdlog::debug("Initial updates...");
    for (unsigned int z = 0; z < 3; ++z) {
        spdlog::debug(std::string(20, '-'));
        spdlog::debug("Iteration: {:d}", z+1);
        // Update new model
        cohort_->updateNoiseModel();
    }
    spdlog::debug(std::string(20, '-'));
    
    spdlog::info("New noise model initialised. Time elapsed: {:s}\n", timer.getTimeElapsed());
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::reinitialiseModel(const SpatialModel spatialModel, const SpatialParameters& spatialParameters, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel)
{
    Utilities::Timer timer;
    
    ////////////////////////////////////////////////////////////////////////////
    
    spdlog::info("Reinitialising model...");
    
    // First, make a constant spatial model based on the group maps
    MFModels::P::P2C Ebasis;
    Ebasis.P = cohort_->getGroupMaps(); Utilities::MatrixManipulations::flipColumnSigns( Ebasis.P );
    Ebasis.PtP = Ebasis.P.t() * Ebasis.P + 0.05f * V_ * arma::eye<arma::fmat>(M_, M_);
    std::shared_ptr<MFModels::P_VBPosterior> Pbasis = std::make_shared<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(Ebasis);
    std::shared_ptr<CohortModelling::GroupSpatialModel> gSM = std::make_shared<CohortModelling::GroupSpatialModels::GroupMaps>(Pbasis);
    
    // Change to this model
    cohort_->setSpatialModel( gSM );
    
    // And update the temporal / weight models too
    cohort_->setWeightModel( getWeightModel(weightModel) );
    cohort_->setTemporalModel( getTemporalModel(timeCourseModel, temporalPrecisionModel) );
    
    spdlog::debug("Models reset (with simplified spatial model).");
    
    ////////////////////////////////////////////////////////////////////////////
    
    spdlog::debug("Initial updates...");
    
    // The group maps are fixed for these, and we also hold the time course 
    // variances fixed so the weights are in the right ballpark
    cohort_->setTimeCourseNormalisation(true);
    for (unsigned int z = 0; z < 5; ++z) {
        spdlog::debug(std::string(20, '-'));
        spdlog::debug("Iteration: {:d}", z+1);
        cohort_->updateTemporalModel();
        cohort_->updateWeightModel();
        if (z >= 3) {
            cohort_->updateNoiseModel();
        }
    }
    cohort_->setTimeCourseNormalisation(false);
    
    // And now some updates to let the time course variances stabilise based 
    // on their priors
    for (unsigned int z = 0; z < 10; ++z) {
        spdlog::debug(std::string(20, '-'));
        spdlog::debug("Iteration: {:d}", z+1);
        cohort_->updateTemporalModel();
        cohort_->updateWeightModel();
        cohort_->updateNoiseModel();
    }
    spdlog::debug(std::string(20, '-'));
    
    spdlog::info("Simplified model reinitialised. Time elapsed: {:s}", timer.getTimeElapsed());
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Switch to the requested spatial model
    switchSpatialModel(spatialModel, spatialParameters);
    
    ////////////////////////////////////////////////////////////////////////////
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::save(const std::string directory) const
{
    Utilities::Timer timer;
    spdlog::info("Saving model...");
    
    cohort_->save(directory);
    spdlog::info("Model saved: {:s}", directory);
    spdlog::info("Time elapsed: {:s}\n", timer.getTimeElapsed());
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::ModelManager::getFreeEnergy() const
{
    Utilities::Timer timer;
    
    const double F = cohort_->getFreeEnergy();
    
    // Lower level than normal - this is often embedded within other functions
    spdlog::trace("Free energy calculated. Time elapsed: {:s}", timer.getTimeElapsed());
    
    return F;
}

////////////////////////////////////////////////////////////////////////////////
// Private helper functions
////////////////////////////////////////////////////////////////////////////////
// Make a Cohort, given the various models (templated for different types of data)
// Uses simple constant spatial model based on initialMaps

template<class D>
void PROFUMO::ModelManager::initialiseCohort(const std::map<SubjectID, std::map<RunID, D>> subjectData, const arma::fmat initialMaps, const WeightModel weightModel, const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel, const NoiseModel noiseModel)
{
    ////////////////////////////////////////////////////////////////////////////
    // Make the group models
    
    // First, make a constant spatial model
    MFModels::P::P2C Ebasis;
    Ebasis.P = initialMaps;
    Ebasis.PtP = Ebasis.P.t() * Ebasis.P + 0.05f * V_ * arma::eye<arma::fmat>(M_, M_);
    std::shared_ptr<MFModels::P_VBPosterior> Pbasis = std::make_shared<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(Ebasis);
    std::shared_ptr<CohortModelling::GroupSpatialModel> gSM = std::make_shared<CohortModelling::GroupSpatialModels::GroupMaps>(Pbasis);
    
    // And the rest of the models
    std::shared_ptr<CohortModelling::GroupWeightModel> gWM = getWeightModel(weightModel);
    std::shared_ptr<CohortModelling::GroupTemporalModel> gTM = getTemporalModel(timeCourseModel, temporalPrecisionModel);
    std::shared_ptr<CohortModelling::GroupNoiseModel> gNM = getNoiseModel(noiseModel);
    
    ////////////////////////////////////////////////////////////////////////////
    // And make the Cohort!
    
    // Put all subject information together
    std::vector<SubjectInformation> subjectInformation;
    for (const auto& subject : subjectData) {
        SubjectInformation subjectInfo;
        subjectInfo.subjectID = subject.first;
        for (const auto& run : subject.second) {
            RunInformation runInfo;
            runInfo.runID = run.first;
            //runInfo.V = run.second.V;
            runInfo.T = run.second.T;
            //runInfo.M = M_;
            subjectInfo.runs.push_back(runInfo);
        }
        subjectInformation.push_back(subjectInfo);
    }
    
    // And then the cohort!
    cohort_ = std::make_unique<CohortModelling::Cohort>(
        subjectInformation, subjectData, gSM, gWM, gTM, gNM, dofCorrectionFactor_
    );
    
    ////////////////////////////////////////////////////////////////////////////
    
    spdlog::debug("Initial updates...");
    
    // The group maps are fixed for these, and we also hold the time course 
    // variances fixed so the weights are in the right ballpark
    cohort_->setTimeCourseNormalisation(true);
    for (unsigned int z = 0; z < 5; ++z) {
        spdlog::debug(std::string(20, '-'));
        spdlog::debug("Iteration: {:d}", z+1);
        cohort_->updateTemporalModel();
        cohort_->updateWeightModel();
        cohort_->updateNoiseModel();
    }
    cohort_->setTimeCourseNormalisation(false);
    
    // And now some updates to let the time course variances stabilise based 
    // on their priors
    for (unsigned int z = 0; z < 10; ++z) {
        spdlog::debug(std::string(20, '-'));
        spdlog::debug("Iteration: {:d}", z+1);
        cohort_->updateTemporalModel();
        cohort_->updateWeightModel();
        cohort_->updateNoiseModel();
    }
    spdlog::debug(std::string(20, '-'));
    
    ////////////////////////////////////////////////////////////////////////////
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<PROFUMO::CohortModelling::GroupSpatialModel> PROFUMO::ModelManager::getSpatialModel(const SpatialModel spatialModel, const SpatialParameters& spatialParameters) const
{
    // Store for the new model type
    std::shared_ptr<CohortModelling::GroupSpatialModel> groupSpatialModel;
    
    // Look at the specified model class and make appropriately
    switch (spatialModel) {
        ////////////////////////////////////////////////////////////////////////
        
        // The overall scale of the maps is tricky. The data is normalised 
        // such that E[p^2] ≈ 1.0, with the weights set to account for the 
        // spatial sparsity.
        
        // However, there is a balance between the weights adjusting 
        // the scaling of the data to fit either the scale of the null, or 
        // the scale of the group means - both of which are fixed.
        // Increasing the width of the null will tend to push the group 
        // means to higher values, which can be problematic if the group 
        // prior is not sufficiently flexible (hole-y PFMs?).
        // ** Be careful how you adjust each one! **
        
        // Here, we have gone for quite lax distributions for the signal 
        // scaling, as the null will tend to dominate (as it will be present 
        // in the majority of voxels).
        
        case SpatialModel::MODES : {
            //------------------------------------------------------------------
            
            // Signal means
            std::shared_ptr<Modules::MatrixMean_VBPosterior> signalMeans;
            if (!spatialParameters.fixedGroupMeans) {
                // Spatial means are less sparse than memberships
                // Still want to allow strong means in areas that don't appear in every subject
                const float p = 1.5f * P_PRIOR_SPARSITY;
                typedef VBModules::MatrixMeans::IndependentMixtureModel<1,2> IndependentMixtureModel;
                IndependentMixtureModel::Parameters meanPrior;
                meanPrior.randomInitialisation = false;
                // Positive Gaussian
                meanPrior.gaussians[0].mu = 1.0f * arma::ones<arma::fmat>(V_,M_);
                meanPrior.gaussians[0].sigma2 = std::pow(0.75f, 2) * arma::ones<arma::fmat>(V_,M_);
                meanPrior.gaussians[0].p = 0.8f * p * arma::ones<arma::fmat>(V_,M_);
                // Negative Gaussian
                /*VBDistributions::GaussianMixtureModel::GaussianParameters negGaussianPrior;
                negGaussianPrior.parameters.mu = -1.0f; negGaussianPrior.parameters.sigma2 = std::pow(0.75f, 2f); negGaussianPrior.probability = 0.010f;*/
                // High variance 'slab'
                meanPrior.gaussians[1].mu = 1.0f * arma::ones<arma::fmat>(V_,M_);
                meanPrior.gaussians[1].sigma2 = std::pow(2.0f, 2) * arma::ones<arma::fmat>(V_,M_);
                meanPrior.gaussians[1].p = 0.2f * p * arma::ones<arma::fmat>(V_,M_);
                // And a spike at zero
                meanPrior.deltas[0].mu = arma::zeros<arma::fmat>(V_,M_);
                meanPrior.deltas[0].p = (1.0f - p) * arma::ones<arma::fmat>(V_,M_);
                // And put together
                signalMeans = std::make_shared<IndependentMixtureModel>(meanPrior);
            }
            else {
                // Constant
                Modules::MatrixMeans::P2C Emeans;
                Emeans.X = (*spatialParameters.fixedGroupMeans); Emeans.X2 = arma::square( Emeans.X );
                signalMeans = std::make_shared<VBModules::Constant<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>>(Emeans);
            }
            
            //------------------------------------------------------------------
            
            // Signal precisions
            std::shared_ptr<Modules::MatrixPrecision_VBPosterior> signalPrecisions;
            if (!spatialParameters.fixedGroupPrecisions) {
                VBModules::MatrixPrecisions::IndependentGamma::Parameters precisionPrior;
                // Make sure this has a larger standard deviation than the null distribution!
                // Precision = a/b = 2.0 -> std = 0.7(ish)
                precisionPrior.a = N_PRIOR_SUBJECTS * arma::ones<arma::fmat>(V_,M_);
                precisionPrior.b = 0.5f * N_PRIOR_SUBJECTS * arma::ones<arma::fmat>(V_,M_);
                signalPrecisions = std::make_shared<VBModules::MatrixPrecisions::IndependentGamma>(precisionPrior);
            }
            else {
                // Constant
                Modules::MatrixPrecisions::P2C Eprecs;
                Eprecs.X = (*spatialParameters.fixedGroupPrecisions); Eprecs.logX = arma::log(Eprecs.X);
                signalPrecisions = std::make_shared<VBModules::Constant<Modules::MatrixPrecisions::C2P, Modules::MatrixPrecisions::P2C>>(Eprecs);
            }
            
            //------------------------------------------------------------------
            
            // Noise precisions
            std::shared_ptr<Modules::MatrixPrecision_VBPosterior> noisePrecisions;
            
            VBModules::MatrixPrecisions::IndependentGamma::Parameters precisionPrior;
            // Precision = a/b = 10.0 -> std = 0.3(ish)
            precisionPrior.a = N_PRIOR_SUBJECTS * arma::ones<arma::fmat>(V_,1);
            precisionPrior.b = 0.1f * N_PRIOR_SUBJECTS * arma::ones<arma::fmat>(V_,1);
            
            noisePrecisions = std::make_shared<VBModules::MatrixPrecisions::IndependentGamma>(precisionPrior);
            
            //------------------------------------------------------------------
            
            // Membership Probs
            std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships;
            if (!spatialParameters.fixedGroupMemberships) {
                // Dirichlet
                const unsigned int nClasses = 2;
                typedef VBModules::MembershipProbabilities::IndependentDirichlet<nClasses> IndependentDirichlet;
                IndependentDirichlet::Parameters membershipPrior;
                // pPos = counts[1] / (counts[0] + counts[1]) = P_PRIOR_SPARSITY
                membershipPrior.counts[0] = N_PRIOR_SUBJECTS * (1.0f - P_PRIOR_SPARSITY) * arma::ones<arma::fmat>(V_,M_);
                membershipPrior.counts[1] = N_PRIOR_SUBJECTS * P_PRIOR_SPARSITY * arma::ones<arma::fmat>(V_,M_);
                memberships = std::make_shared<IndependentDirichlet>(membershipPrior);
            }
            else {
                // Constant
                Modules::MembershipProbabilities::P2C Eprobs;
                const arma::fmat probs = *spatialParameters.fixedGroupMemberships;
                Eprobs.p = {1.0f - probs, probs}; Eprobs.logP = {arma::log(1.0f - probs), arma::log(probs)};
                memberships = std::make_shared<VBModules::Constant<Modules::MembershipProbabilities::C2P, Modules::MembershipProbabilities::P2C>>(Eprobs);
            }
            
            //------------------------------------------------------------------
            
            // Put it together!
            MFModels::P::DGMM::Parameters prior; prior.V = V_; prior.M = M_;
            groupSpatialModel = std::make_shared<CohortModelling::GroupSpatialModels::DGMM>(signalMeans, signalPrecisions, noisePrecisions, memberships, prior, dofCorrectionFactor_);
            
            //------------------------------------------------------------------
            break;
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        case SpatialModel::PARCELLATION : {
            //------------------------------------------------------------------
            
            // Means
            std::shared_ptr<Modules::MatrixMean_VBPosterior> groupMeans;
            if (!spatialParameters.fixedGroupMeans) {
                // Gaussian
                VBModules::MatrixMeans::IndependentGaussian::Parameters meanPrior;
                meanPrior.randomInitialisation = false;
                meanPrior.mu = 0.5f * arma::ones<arma::fmat>(V_,M_);
                meanPrior.sigma2 = 1.0f * arma::ones<arma::fmat>(V_,M_);
                groupMeans = std::make_shared<VBModules::MatrixMeans::IndependentGaussian>(meanPrior);
            }
            else {
                // Constant
                Modules::MatrixMeans::P2C Emeans;
                Emeans.X = (*spatialParameters.fixedGroupMeans); Emeans.X2 = arma::square( Emeans.X );
                groupMeans = std::make_shared<VBModules::Constant<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>>(Emeans);
            }
            
            //------------------------------------------------------------------
            
            // Precisions
            std::shared_ptr<Modules::MatrixPrecision_VBPosterior> groupPrecisions;
            if (!spatialParameters.fixedGroupPrecisions) {
                VBModules::MatrixPrecisions::IndependentGamma::Parameters precisionPrior;
                // Precision = a/b = 2.0 -> std = 0.7(ish)
                precisionPrior.a = N_PRIOR_SUBJECTS * arma::ones<arma::fmat>(V_,M_);
                precisionPrior.b = 0.5f * N_PRIOR_SUBJECTS * arma::ones<arma::fmat>(V_,M_);
                groupPrecisions = std::make_shared<VBModules::MatrixPrecisions::IndependentGamma>(precisionPrior);
            }
            else {
                // Constant
                Modules::MatrixPrecisions::P2C Eprecs;
                Eprecs.X = (*spatialParameters.fixedGroupPrecisions); Eprecs.logX = arma::log(Eprecs.X);
                groupPrecisions = std::make_shared<VBModules::Constant<Modules::MatrixPrecisions::C2P, Modules::MatrixPrecisions::P2C>>(Eprecs);
            }
            
            //------------------------------------------------------------------
            
            // Membership Probs
            std::shared_ptr<Modules::MembershipProbability_VBPosterior> groupMemberships;
            if (!spatialParameters.fixedGroupMemberships) {
                // Dirichlet
                VBModules::MembershipProbabilities::DynamicDirichlet::Parameters membershipPrior;
                membershipPrior.counts = std::vector<arma::fmat>(M_, 0.1f * arma::ones<arma::fmat>(V_,1));
                groupMemberships = std::make_shared<VBModules::MembershipProbabilities::DynamicDirichlet>(membershipPrior);
            }
            else {
                // Constant
                Modules::MembershipProbabilities::P2C Eprobs;
                const arma::fmat probs = *spatialParameters.fixedGroupMemberships;
                for (unsigned int m = 0; m < M_; ++m) {
                    Eprobs.p.push_back( probs.col(m) );
                    Eprobs.logP.push_back( arma::trunc_log( probs.col(m) ) );
                }
                groupMemberships = std::make_shared<VBModules::Constant<Modules::MembershipProbabilities::C2P, Modules::MembershipProbabilities::P2C>>(Eprobs);
            }
            
            //------------------------------------------------------------------
            
            // Put it together!
            if (spatialParameters.spatialNeighbours) {
                MFModels::P::PottsParcellation::Parameters pottsPrior;
                pottsPrior.V = V_; pottsPrior.M = M_; pottsPrior.beta = 100.0f;
                pottsPrior.couplings = std::make_shared<std::vector<arma::uvec>>(*spatialParameters.spatialNeighbours);
                typedef CohortModelling::GroupSpatialModels::Parcellation<MFModels::P::PottsParcellation> ParcellationModel;
                groupSpatialModel = std::make_shared<ParcellationModel>(groupMeans, groupPrecisions, groupMemberships, pottsPrior, dofCorrectionFactor_);
            }
            else {
                MFModels::P::Parcellation::Parameters PPrior;
                PPrior.V = V_; PPrior.M = M_;
                typedef CohortModelling::GroupSpatialModels::Parcellation<MFModels::P::Parcellation> ParcellationModel;
                groupSpatialModel = std::make_shared<ParcellationModel>(groupMeans, groupPrecisions, groupMemberships, PPrior, dofCorrectionFactor_);
            }
            
            //------------------------------------------------------------------
            break;
        }
        
        ////////////////////////////////////////////////////////////////////////
        
        default :
            throw std::invalid_argument("ModelManager: bad SpatialModel identifier.");
            
        ////////////////////////////////////////////////////////////////////////
    }
    
    return groupSpatialModel;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<PROFUMO::CohortModelling::GroupWeightModel> PROFUMO::ModelManager::getWeightModel(const WeightModel weightModel) const
{
    // Store for the new model type
    std::shared_ptr<CohortModelling::GroupWeightModel> groupWeightModel;
    
    // The data normalisation assumes E[d^2] = M, which relates to 
    // E[p^2] * E[a^2] = 1.0 (see DataLoader.h). We are after unit variance 
    // time courses, so that holds. Similarly, even though the group maps 
    // are sparse, the subject maps normally exhibit a fair bit of variance 
    // around the group, so E[h^2] = 1.0 is in the right ball-park.
    
    // Look at the specified model class and make appropriately
    switch (weightModel) {
        case WeightModel::CONSTANT : {
            // Constant set of weights (no uncertainty either)
            MFModels::H::P2C EH; EH.h = arma::ones<arma::fvec>(M_); EH.hht = arma::ones<arma::fmat>(M_, M_);
            std::shared_ptr<MFModels::H_VBPosterior> groupWeights = std::make_shared<VBModules::Constant<MFModels::H::C2P, MFModels::H::P2C>>(EH);
            // And make a group model based around those
            groupWeightModel = std::make_shared<CohortModelling::GroupWeightModels::GroupWeights>(groupWeights);
            break;
        }
        
        case WeightModel::GROUP : {
            // Spike-slab weights (MVN models require covariance)
            MFModels::H::SpikeSlab::Parameters HPrior;
            HPrior.M = M_; HPrior.mu = 1.0f; HPrior.sigma2 = std::pow(0.5f, 2); HPrior.p = 0.95f; //HPrior.rectifyWeights = true;
            std::shared_ptr<MFModels::H_VBPosterior> groupWeights = std::make_shared<MFModels::H::SpikeSlab>(HPrior);
            // And make a group model based around those
            groupWeightModel = std::make_shared<CohortModelling::GroupWeightModels::GroupWeights>(groupWeights);
            break;
        }
        
        case WeightModel::RECTIFIED_MULTIVARIATE_NORMAL : {
            // Group-level means
            VBModules::MatrixMeans::IndependentGaussian::Parameters muHPrior;
            muHPrior.mu = 1.0f * arma::ones<arma::fvec>(M_);
            muHPrior.sigma2 = std::pow(0.1f, 2) * arma::ones<arma::fvec>(M_);
            muHPrior.randomInitialisation = false;
            std::shared_ptr<Modules::MatrixMean_VBPosterior> muH = std::make_shared<VBModules::MatrixMeans::IndependentGaussian>(muHPrior);
            // Make the group-level precision matrix (i.e. the correlations between the weights)
            VBModules::PrecisionMatrices::Wishart::Parameters alphaHPrior;
            alphaHPrior.a = float(M_); alphaHPrior.B = std::pow(0.1f, 2) * float(M_) * arma::eye<arma::fmat>(M_, M_);
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> alphaH = std::make_shared<VBModules::PrecisionMatrices::Wishart>(alphaHPrior);
            // And make a group model based around those
            typedef CohortModelling::GroupWeightModels::ParameterisedWeights<MFModels::H::MultivariateNormal> WeightModel;
            MFModels::H::MultivariateNormal::Parameters HPrior;
            HPrior.M = M_; HPrior.rectifyWeights = true;
            HPrior.clipWeights = true;
            HPrior.clipRange_min = 1.0f / 5.0f; HPrior.clipRange_max = 5.0f;
            groupWeightModel = std::make_shared<WeightModel>(muH, alphaH, HPrior);
            break;
        }
        
        case WeightModel::RECTIFIED_SPIKE_SLAB : {
            // This then has 95% of the weights being non-zero a priori
            typedef CohortModelling::GroupWeightModels::IndependentWeights<MFModels::H::SpikeSlab> WeightModel;
            MFModels::H::SpikeSlab::Parameters HPrior;
            HPrior.M = M_; HPrior.mu = 1.0f; HPrior.sigma2 = std::pow(0.5f, 2); HPrior.p = 0.95f; HPrior.rectifyWeights = true;
            groupWeightModel = std::make_shared<WeightModel>(HPrior);
            break;
        }
        
        default :
            throw std::invalid_argument("ModelManager: bad WeightModel identifier.");
    }
    
    return groupWeightModel;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<PROFUMO::CohortModelling::GroupTemporalPrecisionModel> PROFUMO::ModelManager::getTemporalPrecisionModel(const TemporalPrecisionModel temporalPrecisionModel) const
{
    // Store for the new model type
    std::shared_ptr<CohortModelling::GroupTemporalPrecisionModel> groupTemporalPrecisionModel;
    
    // Useful results
    // Group correlation matrix (we expect some positive correlations)
    const arma::fmat gCM = 0.9f * arma::eye<arma::fmat>(M_, M_) + 0.1f * arma::ones<arma::fmat>(M_, M_);
    // Gives the following precision matrix
    const arma::fmat gPM = linalg::inv_sympd(gCM);
    
    // For the full hierarchy, the following relationships are useful ways of modifying the priors
    // group ~ W(a,B); subjectPM ~ W(a,group); target precision gPM
    // E[group]     = a * B^-1
    // E[subjectPM] = a_s * (E[group])^-1
    //              = (a_s / a) * B
    // Wishart requires that a_s, a > M;
    // To strengthen the group prior, use:
    // a_s = M; a = c * M; B = c * gPM; c >= 1.0
    // To strengthen the subject prior, use:
    // a_s = k * M; a = k * M; B = gPM; k >= 1.0
    // See Python code at the bottom of the file for a demonstration of this
    
    // Look at the specified model class and make appropriately
    switch (temporalPrecisionModel) {
        // Constant group-level precision matrix, fixed across subjects/runs
        case TemporalPrecisionModel::CONSTANT : {
            // Simple constant prec-mat
            Modules::PrecisionMatrices::P2C CE;
            CE.X = gPM; CE.logDetX = linalg::log_det_sympd(CE.X);
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> groupPrecision = std::make_shared<VBModules::Constant<Modules::PrecisionMatrices::C2P, Modules::PrecisionMatrices::P2C>>(CE);
            // And make a group model based around those
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix>(groupPrecision);
            break;
        }
        
        // Inferred group-level precision matrix, fixed across subjects/runs
        case TemporalPrecisionModel::GROUP_PRECISION_MATRIX : {
            // Make the group-level precision matrix
            VBModules::PrecisionMatrices::Wishart::Parameters prior;
            const float c = std::max(N_PRIOR_SUBJECTS, 1.0f);
            prior.a = c * float(M_); prior.B = c * float(M_) * gCM; // E[X] = a * B^-1
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> groupPrecision = std::make_shared<VBModules::PrecisionMatrices::Wishart>(prior);
            // And make a group model based around those
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix>(groupPrecision);
            break;
        }
        
        // Constant group-level hyperprior, subject-specific prec-mats
        case TemporalPrecisionModel::CONSTANT_GROUP : {
            // Make the constant group-level hyperprior
            // E[subject] = a_s * (E[group])^-1
            Modules::PrecisionMatrices::P2C CE;
            const float k = 5.0f;
            CE.X = k * float(M_) * gCM; CE.logDetX = linalg::log_det_sympd(CE.X);
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> groupPrecision = std::make_shared<VBModules::Constant<Modules::PrecisionMatrices::C2P, Modules::PrecisionMatrices::P2C>>(CE);
            // And combine with the subject prior
            VBModules::PrecisionMatrices::HierarchicalWishart::Parameters subjectPrior;
            subjectPrior.a = k * float(M_); subjectPrior.size = M_;
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices>(groupPrecision, subjectPrior);
            break;
        }
        
        // Subject-specific precision matrices
        case TemporalPrecisionModel::SUBJECT_PRECISION_MATRICES : {
            // Make the group-level hyperprior
            VBModules::PrecisionMatrices::Wishart::Parameters groupPrior;
            const float c = std::max(N_PRIOR_SUBJECTS, 1.0f); const float k = 1.0f;
            groupPrior.a = k * c * float(M_); groupPrior.B = c * gPM;
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> groupPrecision = std::make_shared<VBModules::PrecisionMatrices::Wishart>(groupPrior);
            // And make a group model based around those
            VBModules::PrecisionMatrices::HierarchicalWishart::Parameters subjectPrior;
            subjectPrior.a = k * float(M_); subjectPrior.size = M_;
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices>(groupPrecision, subjectPrior);
            break;
        }
        
        // Run-specific precision matrices
        case TemporalPrecisionModel::RUN_PRECISION_MATRICES : {
            // Make the group-level precision matrix
            VBModules::PrecisionMatrices::Wishart::Parameters groupPrior;
            const float c = std::max(N_PRIOR_SUBJECTS, 1.0f); const float k = 1.0f;
            groupPrior.a = k * c * float(M_); groupPrior.B = c * gPM;
            std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> groupPrecision = std::make_shared<VBModules::PrecisionMatrices::Wishart>(groupPrior);
            // And make a group model based around those
            VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior;
            runPrior.a = k * float(M_); runPrior.size = M_;
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices>(groupPrecision, runPrior);
            break;
        }
        
        // Run-specific precision matrices, different hyperpriors for different categories
        case TemporalPrecisionModel::CATEGORISED_RUN_PRECISION_MATRICES : {
            // Make the priors
            VBModules::PrecisionMatrices::Wishart::Parameters groupPrior;
            const float c = std::max(N_PRIOR_SUBJECTS, 1.0f); const float k = 1.0f;
            groupPrior.a = k * c * float(M_); groupPrior.B = c * gPM;
            VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior;
            runPrior.a = k * float(M_); runPrior.size = M_;
            // And make a group model based around those
            groupTemporalPrecisionModel = std::make_shared<CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices>(groupPrior, runPrior);
            break;
        }
        
        default :
            throw std::invalid_argument("ModelManager: bad TemporalPrecisionModel identifier.");
    }
    
    return groupTemporalPrecisionModel;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<PROFUMO::CohortModelling::GroupTemporalModel> PROFUMO::ModelManager::getTemporalModel(const TimeCourseModel timeCourseModel, const TemporalPrecisionModel temporalPrecisionModel) const
{
    // Store for the new model type
    std::shared_ptr<CohortModelling::GroupTemporalModel> groupTemporalModel;
    
    // Look at the specified model class and make appropriately
    switch (timeCourseModel) {
        case TimeCourseModel::MULTIVARIATE_NORMAL : {
            groupTemporalModel = std::make_shared<CohortModelling::GroupTemporalModels::MultivariateNormal>(getTemporalPrecisionModel(temporalPrecisionModel), M_);
            break;
        }
        
        case TimeCourseModel::CLEAN_HRF : {
            if (!hrf_) {
                throw std::invalid_argument("ModelManager: invalid TemporalModel identifier, no HRF parameters specified.");
            }
            const float priorRelaxation = 0.05f; // Relaxation of 'true' HRF covariance
            const float posteriorRelaxation = 0.05f; // Relaxation of deconvolution specifically
            groupTemporalModel = std::make_shared<CohortModelling::GroupTemporalModels::CleanHRF>(getTemporalPrecisionModel(temporalPrecisionModel), M_, hrf_->TR, hrf_->file, priorRelaxation, posteriorRelaxation);
            break;
        }
        
        case TimeCourseModel::NOISY_HRF : {
            if (!hrf_) {
                throw std::invalid_argument("ModelManager: invalid TemporalModel identifier, no HRF parameters specified.");
            }
            // Make the noise prior
            VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters noisePrior;
            // Precision = a/b = 2.5 -> std = 0.6(ish)
            noisePrior.a = 5.0f * arma::ones<arma::fvec>(M_);
            noisePrior.b = 2.0f * arma::ones<arma::fvec>(M_);
            // See Python code at the bottom of the file for a demonstration of gamma precision hyperpriors
            
            // And make a group model based around those
            const float priorRelaxation = 0.01f; // Relaxation of 'true' HRF covariance
            const float posteriorRelaxation = 0.05f; // Relaxation of deconvolution specifically
            groupTemporalModel = std::make_shared<CohortModelling::GroupTemporalModels::NoisyHRF>(getTemporalPrecisionModel(temporalPrecisionModel), M_, hrf_->TR, hrf_->file, noisePrior, priorRelaxation, posteriorRelaxation);
            break;
        }
        
        default :
            throw std::invalid_argument("ModelManager: bad TemporalModel identifier.");
    }
    
    return groupTemporalModel;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<PROFUMO::CohortModelling::GroupNoiseModel> PROFUMO::ModelManager::getNoiseModel(const NoiseModel noiseModel) const
{
    // Store for the new model type
    std::shared_ptr<CohortModelling::GroupNoiseModel> groupNoiseModel;
    
    // Look at the specified model class and make appropriately
    switch (noiseModel) {
        case NoiseModel::INDEPENDENT_RUNS : {
            MFModels::Psi::GammaPrecision::Parameters prior;
            prior.a = 0.2f; prior.b = 1.0f;
            groupNoiseModel = std::make_shared<CohortModelling::GroupNoiseModels::IndependentRuns>(prior);
            break;
        }
        
        default :
            throw std::invalid_argument("ModelManager: bad NoiseModel identifier.");
    }
    
    return groupNoiseModel;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::ModelManager::generateInitialMaps(const SpatialBasis& spatialBasis, const SpatialModel spatialModel, const unsigned int multiStartIterations, const bool globalInit, const std::string outputDirectory)
{
    Utilities::Timer timer;
    spdlog::debug("Generating spatial initialisation...");
    
    // We use three different initialisation strategies. Starting with the
    // simplest, these are:
    
    // ** `multiStartIterations == 0` **
    // This is a straightforward randomisation of the spatial basis, but with
    // no attempt to form likely modes. However, the common subspace across
    // subjects is a good place to start.
    
    // ** `multiStartIterations == 1` **
    // Here, we run a (Bayesian version of) ICA on the group spatial basis.
    // This gives a plausible candidate set of modes, though for the reasons
    // discussed below is likely to be overconfident.
    
    // ** `multiStartIterations > 1` **
    // Here, we decompose the group basis, but try and match the SNR to
    // something more like what we see in individual subjects. The fundamental
    // question is what are we hoping these initial maps actually approximate?
    //   We are going to use these maps to intitialise all the subject maps,
    // so we really want them to represent a typical set of *subject* maps.
    // The issue is that the group basis is 'easy', in that the noise is far
    // lower than for a typical subject. Therefore, we can add noise back in,
    // adjusting for the fact we have averaged over `S_` subjects. The
    // noisy basis this generates represents an 'ideal' subject, in that it
    // includes the basis from the group and perfectly white Gaussian noise.
    //   However, this isn't a magic bullet: it makes the initialisation
    // unreliable (it also doesn't capture the blurring over subjects that is
    // key to the group as well as the full model, but that is hard to avoid).
    // Therefore, we perform multiple decompositions and average over them to
    // recover a consensus initialisation.
    
    // Initialise and record basis
    SpatialBasis adjustedBasis(spatialBasis);
    arma::fmat(spatialBasis.U.each_row() % spatialBasis.s.t()).save(
                outputDirectory + "SpatialBasis.hdf5", arma::hdf5_binary);
    
    // Correct singular values and approximate the noise-level for a single
    // subject. This is based on the Marchenko-Pastur distribution and is
    // described in some detail in `Documentation/SVD/SVD_Calibration.md`.
    // 1) Correct for number of subjects
    // While we could choose any overall scale, setting to be comparable to an
    // individual subject helps with some sanity checks
    adjustedBasis.s /= std::sqrt((float) adjustedBasis.S);
    // 2) Calculate max singular value of the noise (averaging over a few)
    const float s_max =
        std::sqrt(linalg::mean(arma::square(adjustedBasis.s.tail(3))));
    // 3) Transform this to the equivalent unstructured noise level
    const unsigned int M = adjustedBasis.U.n_rows;
    const unsigned int N = adjustedBasis.T;
    const float noiseStd = s_max / (std::sqrt(M) + std::sqrt(N));
    // 4) Correct the signal singular values
    adjustedBasis.s = arma::sqrt(arma::abs(  // Just in case...
            arma::square(adjustedBasis.s)
            - miscmaths::square(noiseStd) * (M + N)
        ));
    
    // Make the basis, removing global signal if requested
    arma::fvec globalSignal = arma::ones<arma::fvec>(adjustedBasis.U.n_rows);
    arma::frowvec beta = arma::zeros<arma::frowvec>(adjustedBasis.U.n_cols);
    if (globalInit) {
        const arma::fmat basis = (adjustedBasis.U.each_row() % adjustedBasis.s.t());
        beta = linalg::pinv(globalSignal) * basis;
        globalSignal = basis * linalg::pinv(beta);
    }
    const arma::fmat basis =
        (adjustedBasis.U.each_row() % adjustedBasis.s.t())
        - globalSignal * beta;
    
    // Save!
    basis.save(outputDirectory + "AdjustedBasis.hdf5", arma::hdf5_binary);
    
    // Do the decompositions!
    arma::fmat finalMaps;
    //--------------------------------------------------------------------------
    if (multiStartIterations == 0) {
        // Just return a randomised version of the  basis if no iterations
        // requested
        finalMaps = basis;
        MM::randomiseColumns(finalMaps, M_);
        // Get the scale in the right ballpark
        DN::normaliseColumns(finalMaps);
    }
    //--------------------------------------------------------------------------
    else if (multiStartIterations == 1) {
        // If we actually requested a decomposition, fit the model to the group
        const arma::fmat initialMaps = generateRandomInitialisaion(basis, M_);
        finalMaps = decomposeMatrix(basis, M_, spatialModel, 250, initialMaps);
    }
    //--------------------------------------------------------------------------
    else { // multiStartIterations > 1
        const unsigned int padWidth = ((unsigned int) std::floor(std::log10(std::max(multiStartIterations, 1u)))) + 1u;
        
        
        // Set dimensionality
        // If we are doing multiple runs, add a few extra components
        // Then select the most reproducible at the end
        //const unsigned int rank = M_ + std::min(10u, (unsigned int) std::round(1.25f * M_));
        const unsigned int rank = M_;
        
        
        // Do decompositions
        // Can't parallelise because randomisation is not thread safe
        std::vector<arma::fmat> decompositions(multiStartIterations, arma::fmat());
        for (unsigned int i = 0; i < multiStartIterations; ++i) {
            spdlog::debug("Preliminary decomposition: {:d} of {:d}", i+1, multiStartIterations);
            // Put new basis together: add noise to approximate a single
            // subject (observation noise and uncertainty on basis.s)
            const arma::fmat subjectBasis =
                (basis.each_row() % (0.8f + 0.4f * arma::randu<arma::frowvec>(basis.n_cols)))
                + noiseStd * arma::randn<arma::fmat>(arma::size(basis));
            // Decompose!
            const arma::fmat initialMaps = generateRandomInitialisaion(basis, rank);
            const arma::fmat decomposition = decomposeMatrix(subjectBasis, rank, spatialModel, 150, initialMaps);
            decompositions[i] = decomposition;
            // And save
            std::ostringstream iterNumber;
            iterNumber << std::setw( padWidth ) << std::setfill( '0' ) << i + 1;
            std::string fileName = outputDirectory + "DecomposedSpatialBasis_" + iterNumber.str() + ".hdf5";
            decomposition.save(fileName, arma::hdf5_binary);
        }
        
        
        // Now pair everything up!
        arma::fmat templateMaps = decompositions[0]; // Start with the first decomposition (they're all random anyway)
        arma::urowvec N = arma::ones<arma::urowvec>(rank);
        std::vector<arma::uvec> indices(multiStartIterations, arma::zeros<arma::uvec>(rank));
        indices[0] = arma::regspace<arma::uvec>(0,rank-1);
        
        // Critical threshold: any similarities less than this will be added 
        // as separate components rather than being averaged
        const float threshold = 0.5f;
        // In practice, we actually don't want this to be too high, otherwise 
        // we end up with what are essentially duplicates in the template
        
        // Make the template
        for (unsigned int i = 1; i < multiStartIterations; ++i) {
            const arma::fmat& decomposition = decompositions[i];
            // Get pairing
            const MM::Pairing pairing = MM::pairMatrixColumns(templateMaps, decomposition);
            const arma::fvec signs = arma::sign(pairing.scores); // Why no std::sign? :-(
            
            // Add to template
            for (unsigned int j = 0; j < rank; ++j) {
                const unsigned int jT = pairing.inds1[j]; // template
                const unsigned int jD = pairing.inds2[j]; // decomposition
                
                if (std::abs(pairing.scores[j]) > threshold) {
                    // Add to template if a good match
                    templateMaps.col(jT) += signs[j] * decomposition.col(jD);
                    N[jT] += 1;
                    // Record where the map went
                    indices[i][jD] = jT;
                }
                else {
                    // Otherwise, append to the end of the template
                    templateMaps = arma::join_rows(templateMaps, decomposition.col(jD));
                    N = arma::join_rows(N, arma::ones<arma::urowvec>(1));
                    // Record where the map went
                    indices[i][jD] = templateMaps.n_cols - 1;
                }
            }
        }
        // Normalise by number of maps
        templateMaps.each_row() /= arma::conv_to<arma::frowvec>::from(N);
        
        
        // Score each map by it's consistency
        arma::frowvec scores = arma::zeros<arma::frowvec>(templateMaps.n_cols);
        for (unsigned int i = 0; i < multiStartIterations; ++i) {
            scores.elem(indices[i]) += arma::abs(arma::diagvec(
                MM::calculateCosineSimilarity(templateMaps.cols(indices[i]), decompositions[i])
                )) - threshold;
            // Subtracting threshold increases emphasis on similarity (as 
            // opposed to number present)
            // e.g. threshold = 0.5
            // scores = 0.6,0.55,0.65 => 1.8 or 0.3
            // scores = 0.7,0.8 => 1.5 or 0.5
        }
        // Don't give maps credit for being the only one in the template...
        scores.elem(arma::find(N == 1)).zeros();
        
        
        // Reorder so the top scoring maps are first
        const arma::uvec sortOrder = arma::sort_index(scores, "descend");
        templateMaps = templateMaps.cols(sortOrder);
        scores = scores.cols(sortOrder);
        
        
        // Save all the key template files
        templateMaps.save(outputDirectory + "TemplateMaps.hdf5", arma::hdf5_binary);
        scores.save(outputDirectory + "Scores.hdf5", arma::hdf5_binary);
        for (unsigned int i = 0; i < multiStartIterations; ++i) {
            arma::fmat reorderedBasis = arma::zeros<arma::fmat>(arma::size(templateMaps));
            reorderedBasis.cols( indices[i] ) = decompositions[i];
            reorderedBasis = reorderedBasis.cols(sortOrder);
            
            std::ostringstream iterNumber;
            iterNumber << std::setw( padWidth ) << std::setfill( '0' ) << i + 1;
            std::string fileName = outputDirectory + "DecomposedSpatialBasis_" + iterNumber.str() + "_Reordered.hdf5";
            reorderedBasis.save(fileName, arma::hdf5_binary);
        }
        
        finalMaps = templateMaps.head_cols(M_);
        // Intuitively, we might want to do something like dual regression from
        // the noise-free basis to clean the template, but it isn't numerically
        // stable...
        
        /*
        // Generate the initialisation for the final decomposition
        arma::fmat initialMaps = templateMaps.head_cols(M_);
        // Make columns have a positive skew
        MM::flipColumnSigns( initialMaps );
        initialMaps.save(outputDirectory + "InitialMaps.hdf5", arma::hdf5_binary);
        
        
        // And fit the final model! Start with the template maps, and refine 
        // so that it is valid (i.e. the template generation hasn't adjusted 
        // the maps away from the data) and gives it a chance to run for longer
        spdlog::debug("Final decomposition");
        finalMaps = decomposeMatrix(
                basis + 0.33f * noiseStd * arma::randn<arma::fmat>(arma::size(basis)),
                M_, spatialModel, 250, initialMaps);
        */
    }
    //--------------------------------------------------------------------------
    
    // Make columns have a positive skew
    MM::flipColumnSigns( finalMaps );
    // And add global component
    if (globalInit) {
        finalMaps.tail_cols(1) = globalSignal;
    }
    
    // And save
    finalMaps.save(outputDirectory + "FinalMaps.hdf5", arma::hdf5_binary);
    
    spdlog::debug("Initial maps generated. Time elapsed: {:s}", timer.getTimeElapsed());
    
    return finalMaps;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::ModelManager::saveIntermediates(const std::string intermediatesDir, const unsigned int iteration, const unsigned int maxIteration)
{
    // Get file name (with zero-padded iter number)
    const unsigned int padWidth = ((unsigned int) std::floor(std::log10(maxIteration))) + 1;
    std::ostringstream iterNumber;
    iterNumber << std::setw( padWidth ) << std::setfill( '0' ) << iteration;
    
    // Save!
    arma::fmat groupMaps = cohort_->getGroupMaps();
    std::string fileName = intermediatesDir + "GroupMaps_" + iterNumber.str() + ".hdf5";
    groupMaps.save(fileName, arma::hdf5_binary);
    
    arma::fmat groupPrecMat = cohort_->getGroupPrecisionMatrix();
    fileName = intermediatesDir + "GroupPrecisionMatrix_" + iterNumber.str() + ".hdf5";
    groupPrecMat.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Separately-declared convenience functions
////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::decomposeMatrix(const arma::fmat& data, const unsigned int rank, const PROFUMO::ModelManager::SpatialModel spatialModel, const unsigned int iterations, const arma::fmat& initialMaps)
{
    // Select the type of decomposition we want to do
    MM::FactorisationType factorisationType;
    switch (spatialModel) {
        case ModelManager::SpatialModel::MODES : {
            factorisationType = MM::FactorisationType::MODES;
            break;
        }
        
        case ModelManager::SpatialModel::PARCELLATION : {
            factorisationType = MM::FactorisationType::PARCELS;
            break;
        }
        
        default :
            throw std::invalid_argument("ModelManager: bad SpatialModel identifier.");
    }
    
    // Decompose!
    const MM::Decomposition decomposition = MM::calculateVBMatrixFactorisation(
            DataTypes::FullRankData(data), rank, factorisationType, initialMaps, iterations);
    arma::fmat finalMaps = decomposition.P;
    
    // Make columns have a positive skew
    MM::flipColumnSigns( finalMaps );
    
    // Make sure things haven't been eliminated...
    const arma::frowvec rms = arma::sqrt(linalg::mean(arma::square(finalMaps), 0));
    for (unsigned int m = 0; m < finalMaps.n_cols; ++m) {
        if (rms(m) < 0.05f) {
            spdlog::warn("Replacing eliminated map (RMS: {:.2e})", rms(m));
            finalMaps.col(m) = arma::square(arma::randu<arma::fvec>(finalMaps.n_rows))
                               + 0.25f * arma::randn<arma::fvec>(finalMaps.n_rows);
        }
    }
    
    // Normalise columns
    //DN::normaliseColumns( finalMaps );
    // Best not to - scale should hopefully be pretty good already
    
    return finalMaps;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::generateRandomInitialisaion(const arma::fmat& basis, const unsigned int rank)
{
    // Start with a random rotation of the basis, appropriately normalised
    arma::fmat randomisedBasis = basis;
    MM::randomiseColumns(randomisedBasis, rank);
    // Could orthogonalise here, but generated maps don't look as nice
    
    // Get the scale in the right ballpark
    DN::normaliseColumns(randomisedBasis);
    
    // Make columns have a positive skew
    MM::flipColumnSigns(randomisedBasis);
    
    return randomisedBasis;
}

////////////////////////////////////////////////////////////////////////////////

// Python for examples of null standard deviation for DGMM null
/*
import math, scipy.stats

vals = [0.8, 0.9, 0.95, 0.99]
mu = 0; sigmas = [0.2, 0.3, 0.4, 0.5, 0.6]
#mu = 0; sigmas = numpy.sqrt(numpy.linspace(0.1, 0.25, 7))

for sigma in sigmas:
    print(sigma, sigma**2)
    for val in vals:
        print("{:.2f}: {:f}".format(val, scipy.stats.norm.ppf(val, mu, sigma)))
    print()
*/

// Python for examples of signal in group spike-slab
/*
import scipy.stats

x = numpy.linspace(-4.0, 4.0, 1001)
y = numpy.zeros((1001, 3))
z = numpy.zeros((500, 3))

i = 0
for mu,sigma in [[0.5, 1.0], [0.75, 1.5], [1.0, 2.0]]:
    y[:,i] = scipy.stats.norm.pdf(x, mu, sigma)
    z[:,i] = y[501:,i] / y[499::-1,i]
    i += 1

plt.figure(); plt.plot([0,0], [0,1.1*numpy.max(y)], color=[0.7 ,]*3); plt.plot(x,y)
plt.figure(); plt.plot(x[501:], z); plt.ylabel("p(+x) / p(-x)")
*/

// Python for group TGMM
/*
import scipy.stats

x = numpy.linspace(-5,5,500)
#params = [[0.65, 0.0, 0.5], [0.25,2.5,1.4], [0.1,-2.5,1.4]] # prob, mean, std
params = [[0.850, 0.0, 0.05], [0.125,1.0,0.75], [0.025,1.0,2.0]] # prob, mean, std

plt.figure()
cumPDF = numpy.zeros((500,))
for p,m,s in params:
    pdf = p * scipy.stats.norm.pdf(x,loc=m,scale=s)
    cumPDF = cumPDF + pdf
    plt.plot(x,pdf)
plt.plot(x,cumPDF)
*/

// Python for visualisation of inverse gamma priors
/*
import scipy.stats

x = numpy.linspace(1.0e-3, 5.0, 500)
z = x ** -2
a = 5.0; b = 5.0
y = scipy.stats.gamma.pdf(z, a=a, scale=1.0/b)

plt.figure(); plt.plot(x,y); plt.xlabel("Standard deviation")
plt.figure(); plt.plot(z,y); plt.xlim([0.0,10.0]); plt.xlabel("Precision")
*/


// Python for visualisation of Wishart priors
/*
import scipy.stats

def randWishart(a, B):
    return scipy.stats.wishart.rvs(a, numpy.linalg.inv(B))

M = 25; nSamples = 1000
gCM = 0.7 * (0.9 * numpy.eye(M) + 0.1 * numpy.ones((M,M)))
gPM = numpy.linalg.inv(gCM)
#print(gPM[:3,:3])

# Hyperpriors (c=group, k=subject)
c = 5.0; k = 2.0;
a_g = c * k * M;
B_g = c * gPM;
a_s = k * float(M);

diagObs = numpy.zeros((nSamples, M))
offObs = numpy.zeros((nSamples, int(M*(M-1)/2)))

diagInds = numpy.diag_indices(M)
offInds = numpy.triu_indices(M,1)

for i in range(nSamples):
    groupW = randWishart(a_g, B_g)
    subjectW = randWishart(a_s, groupW)
    diagObs[i,:] = subjectW[diagInds]
    offObs[i,:] = subjectW[offInds]

print((a_s / a_g) * B_g[:2,:2])
print(numpy.mean(diagObs), numpy.mean(offObs))

plt.figure(); h = plt.hist(diagObs.flatten(), bins=50); plt.plot([gPM[0,0],gPM[0,0]], [0,1.1*numpy.max(h[0])])
plt.figure(); h = plt.hist(offObs.flatten(), bins=50); plt.plot([gPM[0,1],gPM[0,1]], [0,1.1*numpy.max(h[0])])

for i in range(5):
    w = randWishart(a_s, randWishart(a_g, B_g))
    plt.figure(); plt.imshow(w, interpolation='none', vmin=-2.0, vmax = 2.0); plt.colorbar()
*/
