// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "KroneckerHRF.h"

#include "Utilities/DataIO.h"
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace MFAModels = PROFUMO::MFModels::A;
namespace Mods = PROFUMO::Modules;

// Resolve references to parents
typedef Mods::PrecisionMatrix_Child PrecMatModel;

// Resolve calls to internally cached expectations
typedef PROFUMO::CachedModule<MFAModels::P2C> ACache;
typedef PROFUMO::CachedModule<Mods::PrecisionMatrices::C2P> PrecMatCache;

////////////////////////////////////////////////////////////////////////////////

MFAModels::KroneckerHRF::KroneckerHRF(Mods::PrecisionMatrix_Parent* precisionMatrix, const Parameters prior)
: Mods::PrecisionMatrix_Child(precisionMatrix), prior_(prior), M_(prior.M), T_(prior.T)
{
    // Get a few key expectations from alpha
    const arma::fmat iAlpha = linalg::inv_sympd( PrecMatModel::parentModule_->getExpectations().X );
    const float logDetAlpha = PrecMatModel::parentModule_->getExpectations().logDetX;
    
    // Set expectations - initialise to the prior
    ACache::expectations_.A = arma::zeros<arma::fmat>(M_,T_);
    ACache::expectations_.AAt = arma::trace( *prior_.K ) * iAlpha;
    
    PrecMatCache::expectations_.N = T_;
    PrecMatCache::expectations_.XXt = T_ * iAlpha;
    
    logDetSigma_ =
        M_ * linalg::accu( arma::log(*prior_.L_K) )
        - T_ * logDetAlpha;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFAModels::KroneckerHRF::update()
{
    // Collect the data from all the MFModels
    const A::C2P D = collectExpectations();
    
    
    // Now collect expectations from the precision matrix hyperprior
    // Just need the mean
    const arma::fmat alpha = PrecMatModel::parentModule_->getExpectations().X;
    
    
    // Invert psiPtP and get eigendecomposition of psiPtP * inv(alpha)
    // This is pretty tricky because psiPtP * iAlpha is not symmetric
    // See e.g. "Applied Numerical Linear Algebra", James W. Demmel, 1997
    // Chapter 4: Nonsymmetric Eigenvalue Problems
    // http://www.math.washington.edu/~morrow/498_13/498.html
    // However, as alpha and psiPtP are symmetric we can make progress
    // using the symmetric generalised eigenvalue problem
    // http://www.netlib.org/lapack/lug/node54.html
    arma::fmat iAlpha; arma::fvec L_PA; arma::fmat U_PA, iU_PA; float logDetAlpha;
    {
        // Calculate the (robust) eigendecomposition of alpha
        arma::fvec L_A; arma::fmat U_A;
        linalg::eig_sympd(L_A, U_A, alpha);
        
        // Form inverse
        iAlpha = U_A * arma::diagmat(1.0f / L_A) * U_A.t();
        
        // Cache determinant for KL calculations
        logDetAlpha = linalg::accu( arma::log(L_A) );
        
        
        // Now solve the generalised eigenvalue problem
        // Aim: A * B^-1 = U_AB * L * U_AB^-1
        // A * u = l * B * u  -->  B^-1 * A = U * L * U^-1  -->  A * B^-1 = U^-t * L * U^t
        // Because A, B are symmetric, we can also do e.g. B = Z * Zt
        // A * u = l * B * u  -->  (iZ * A * iZt) * (Zt * u) = l * (Zt * u)  -->  iZAiZt = ZtU * L * ZtU^-1
        // Which is just a classic symmetric eigenvalue problem
        arma::fmat Z = U_A * arma::diagmat(arma::pow(L_A, 0.5));
        arma::fmat iZ = arma::diagmat(arma::pow(L_A, -0.5)) * U_A.t();
        arma::fvec L_Z; arma::fmat U_Z;
        linalg::eig_sympd(L_Z, U_Z, iZ * D.psiPtP * iZ.t());
        
        // And rearrange
        L_PA = L_Z;
        // U_Z = (Zt * U); U_AB = U^-t  -->  U_AB = iZt * U_Z
        U_PA = Z * U_Z;
        iU_PA = U_Z.t() * iZ;
    }
    
    
    // Collect and combine eigenvalues of Sigma_A
    const arma::fmat Lk = arma::repmat(prior_.L_K->t(),M_,1) / (L_PA * prior_.L_K->t() + 1.0f);
    // And the modified version for AiKAt
    // REGULARISED INVERSE: SEE EXPLANATION AT END OF FILE
    const float& r = prior_.relaxation;
    const arma::fvec rL_K = (1.0f - r) * (*prior_.L_K) + r * arma::trace(*prior_.K) / T_;
    const arma::fmat L = 1.0f / (L_PA * rL_K.t() + 1.0f);
    
    
    // Now calculate everything!
    // See update rules document
    arma::fmat& Ma    = ACache::expectations_.A;
    arma::fmat& AAt   = ACache::expectations_.AAt;
    arma::fmat& AiKAt = PrecMatCache::expectations_.XXt;
    
    // MFModel Expectations
    // Posterior mean
    const arma::fmat MaUk = iAlpha * U_PA * ( Lk % (iU_PA * D.psiPtD * (*prior_.U_K)) );
    Ma = MaUk * prior_.U_K->t();
    
    // AAt
    AAt = Ma * Ma.t()
        + iAlpha * U_PA * arma::diagmat(linalg::sum(Lk, 1)) * iU_PA;
    
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    Ma.elem( arma::find(arma::abs(Ma) < 1.0e-10f) ).zeros();
    AAt.elem( arma::find(arma::abs(AAt) < 1.0e-10f) ).zeros();
    //**************************************************************************
    
    // Covariance expectations
    // AiKAt
    AiKAt = MaUk * arma::diagmat(1.0f / rL_K) * MaUk.t()
        + iAlpha * U_PA * arma::diagmat(linalg::sum(L, 1)) * iU_PA;
    
    // N
    PrecMatCache::expectations_.N = T_;
    
    // Record determinant of sigma for KL calculations
    logDetSigma_ = linalg::accu( arma::log(Lk) ) - T_ * logDetAlpha;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double MFAModels::KroneckerHRF::getKL() const
{
    double KL = 0;
    
    // Get precision matrix hyperprior
    const Mods::PrecisionMatrices::P2C alpha = PrecMatModel::parentModule_->getExpectations();
    
    // Difference between prior and posterior determinants
    KL += 0.5 * M_ * linalg::accu( arma::log(*prior_.L_K) );
    KL -= 0.5 * T_ * alpha.logDetX;
    KL -= 0.5 * logDetSigma_;
    
    // And likelihood type terms
    float Tr_alphaAiKAt = arma::trace( alpha.X * PrecMatCache::expectations_.XXt );
    
    KL += 0.5 * Tr_alphaAiKAt;
    KL -= 0.5 * M_ * T_;
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void MFAModels::KroneckerHRF::save(const std::string directory) const
{
    // Just save cached expectations (full posterior covariance would be daft)
    std::string fileName;
    
    // A
    fileName = directory + "Means.hdf5";
    ACache::expectations_.A.save(fileName, arma::hdf5_binary);
    
    // AAt
    fileName = directory + "InnerProduct.hdf5";
    ACache::expectations_.AAt.save(fileName, arma::hdf5_binary);
    
    // AiKAt
    fileName = directory + "InnerProduct_InverseCovariance.hdf5";
    PrecMatCache::expectations_.XXt.save(fileName, arma::hdf5_binary);
    
    // N
    fileName = directory + "Samples.txt";
    Utilities::save(fileName, PrecMatCache::expectations_.N);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFAModels::C2P MFAModels::KroneckerHRF::collectExpectations() const
{
    // Subject-level model so not parallelised
    A::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.psiPtP = arma::zeros<arma::fmat>(M_,M_);
        D.psiPtD = arma::zeros<arma::fmat>(M_,T_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator mfModel = childModules_.begin();
        D = (*mfModel)->getExpectations();
        
        // Add other models if necessary
        for (++mfModel; mfModel != childModules_.end(); ++mfModel) {
            const A::C2P d = (*mfModel)->getExpectations();
            D.psiPtP += d.psiPtP;
            D.psiPtD += d.psiPtD;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////
// REGULARISED INVERSE

// For most HRF type models, there is negligible power admitted at high 
// frequencies, as expected. However, this can cause problems on real data, 
// where the presence of noise results in some power at high frequencies. This 
// tends to result in a large amplification of the noise when trying to invert 
// the model.

// Specifically, this tends to give a large mismatch between E[AAt] and 
// E[AiKAt], and this can result in the netmats not being consistent with e.g. 
// the variance modelling as done by the weights (i.e. h).

// Using the noisy HRF model (i.e. using this model in tandem with 
// AdditiveMultivariateNormal) alleviates this to some extent, and we might 
// expect that relaxing the HRF model (e.g. K = K + 0.01 * I) would help too.

// However, we take a different approach here. We still want a set of time 
// courses that are as clean as possible (i.e. AAt conforms to the model 
// specified by K), but we accept that this may lead to a noise amplification.
// In order to alleviate this, we relax the inversion of K (as used to calculate 
// E[AiKAt]) only. We do this by adding a small amount on to the eigenvalues 
// of K, which is equivalent to saying iK = inv(K + 0.025 * I).

// Heuristically, this seems to result in "clean" time courses, while at the 
// same time reducing the effect of higher frequencies on the netmats.

////////////////////////////////////////////////////////////////////////////////
/*
import numpy, numpy.linalg
import scipy, scipy.stats
import matplotlib, matplotlib.pyplot as plt

hrf = numpy.loadtxt('HRFs/DoubleGamma.phrf')
dt = 1.0; T = 500; t = numpy.linspace(0.0, dt * (T - 1), T)
hrf = numpy.interp(t, hrf[:,0], hrf[:,1])
plt.figure(); plt.plot(t, hrf); plt.xlim(0.0, 30.0)

m = numpy.zeros([T,])
K = scipy.linalg.toeplitz(numpy.correlate(hrf, hrf, 'full')[T-1:])
K = K / numpy.max(K)
#L = numpy.linalg.cholesky(K)
plt.figure(); plt.imshow(K, vmax=1.0, vmin=-1.0); plt.colorbar()

plt.figure(); plt.plot(numpy.random.multivariate_normal(
        m, K, [3,]).T + 5.0 * numpy.arange(3))
plt.figure(); plt.plot(numpy.random.multivariate_normal(
        m, K + 0.01 * numpy.eye(*K.shape), [3,]).T + 5.0 * numpy.arange(3))

R = numpy.random.multivariate_normal(m, K + 0.01 * numpy.eye(*K.shape))
plt.figure(); plt.plot(scipy.stats.mstats.zscore(R))

L = numpy.linalg.cholesky(K)
decorr = scipy.linalg.cho_solve((L, True), R)
plt.plot(scipy.stats.mstats.zscore(decorr) - 5.0)
pred = numpy.convolve(decorr, hrf)[:T]
plt.plot(scipy.stats.mstats.zscore(pred) - 5.0, 'k')

L = numpy.linalg.cholesky(K + 0.025 * numpy.eye(*K.shape))
decorr = scipy.linalg.cho_solve((L, True), R)
plt.plot(scipy.stats.mstats.zscore(decorr) - 10.0)
pred = numpy.convolve(decorr, hrf)[:T]
plt.plot(scipy.stats.mstats.zscore(pred) - 10.0, 'k')
*/
////////////////////////////////////////////////////////////////////////////////
