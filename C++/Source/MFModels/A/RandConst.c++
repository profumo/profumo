// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "RandConst.h"

#include <armadillo>

////////////////////////////////////////////////////////////////////////////////

PROFUMO::MFModels::A::RandConst::RandConst(const Parameters prior)
{
    const arma::fmat A = arma::randn<arma::fmat>(prior.M, prior.T);
    
    expectations_.A = A;
    expectations_.AAt = A * A.t() + prior.T * arma::eye<arma::fmat>(prior.M, prior.M);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModels::A::RandConst::update()
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::MFModels::A::RandConst::getKL() const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModels::A::RandConst::save(std::string directory) const
{
    std::string fileName;
    
    // A
    fileName = directory + "Means.hdf5";
    expectations_.A.save(fileName, arma::hdf5_binary);
    
    // AAt
    fileName = directory + "InnerProduct.hdf5";
    expectations_.AAt.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////cons
