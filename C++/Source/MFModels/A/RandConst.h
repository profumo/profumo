// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A test MFAModel - just generates a set of random time courses and keeps them 
// constant.

#ifndef MF_MODELS_A_RAND_CONST_H
#define MF_MODELS_A_RAND_CONST_H

#include <string>

#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace A
        {
            
            class RandConst :
                public A_VBPosterior,
                protected CachedModule<A::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    unsigned int M, T; // Sizes
                };
                //--------------------------------------------------------------
                
                RandConst(const Parameters prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
            };
            
        }
    }
}
#endif
