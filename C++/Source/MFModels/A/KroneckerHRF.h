// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A model for fMRI time courses. Includes a prior on the covariance between 
// time points (i.e. based on the autocorrelation of the HRF), as well as a 
// hyperprior on the between-mode temporal precision matrix (i.e. netmat).

#ifndef MF_MODELS_A_KRONECKER_HRF_H
#define MF_MODELS_A_KRONECKER_HRF_H

#include <string>
#include <memory>
#include <vector>
#include <armadillo>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "MFModel.h"
#include "MFVariableModels.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace A
        {
            ////////////////////////////////////////////////////////////////////
            
            class KroneckerHRF :
                public A_VBPosterior,
                public Modules::PrecisionMatrix_Child,
                protected CachedModule<A::P2C>,
                protected CachedModule<Modules::PrecisionMatrices::C2P>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    // HRF
                    std::shared_ptr<arma::fvec> hrf
                        = std::make_shared<arma::fvec>();
                    // Covariance
                    std::shared_ptr<arma::fmat> K
                        = std::make_shared<arma::fmat>();
                    // Eigendecomposition
                    std::shared_ptr<arma::fvec> L_K
                        = std::make_shared<arma::fvec>();
                    std::shared_ptr<arma::fmat> U_K
                        = std::make_shared<arma::fmat>();
                    
                    // Sizes
                    unsigned int M; // Number of modes
                    unsigned int T; // Number of time points
                    float TR;
                    
                    // Relaxes/regularises the deconvolution
                    // Between 0.0 and 1.0
                    float relaxation = 0.0f;
                };
                //--------------------------------------------------------------
                
                KroneckerHRF(Modules::PrecisionMatrix_Parent* precisionMatrix, const Parameters prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            protected:
                // Prior
                Parameters prior_;
                
                // Posterior
                float logDetSigma_;
                
                // Matrix sizes
                const unsigned int M_; // Number of modes
                const unsigned int T_; // Number of time points
                
                // Helper function to collect expectations from the MFModels
                A::C2P collectExpectations() const;
            };
            
            ////////////////////////////////////////////////////////////////////
        }
    }
}
#endif
