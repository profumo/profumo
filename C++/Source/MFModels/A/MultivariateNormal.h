// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Very simple time course model - just a multivariate normal with a hyperprior 
// on the between mode covariance structure.

#ifndef MF_MODELS_A_MULTIVARIATE_NORMAL_H
#define MF_MODELS_A_MULTIVARIATE_NORMAL_H

#include <string>
#include <armadillo>

#include "Module.h"
#include "Posterior.h"
#include "ModuleList.h"
#include "MFModel.h"
#include "MFVariableModels.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace A
        {
            
            class MultivariateNormal :
                public A_VBPosterior,
                public Modules::PrecisionMatrix_Child,
                protected CachedModule<A::P2C>,
                protected CachedModule<Modules::PrecisionMatrices::C2P>
            {
            public:
                //--------------------------------------------------------------
                struct Parameters {
                public:
                    unsigned int M, T; // Sizes
                };
                //--------------------------------------------------------------
                
                MultivariateNormal(Modules::PrecisionMatrix_Parent* precisionMatrix, const Parameters prior);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            protected:
                // Posterior
                arma::fmat Sigma_;
                
                // Matrix sizes
                const unsigned int M_; // Number of modes
                const unsigned int T_; // Number of time points
                
                // Helper function to collect expectations from the MFModels
                A::C2P collectExpectations() const;
            };
            
        }
    }
}
#endif
