// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A noise model for an MF model - in this case a single gamma precision. As
// such just a wrapper for the Gamma VBDistribution, that deals with collecting
// the data expectations from multiple MFModels.

#ifndef MF_MODELS_PSI_GAMMA_PRECISION_H
#define MF_MODELS_PSI_GAMMA_PRECISION_H

#include <string>

#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"
#include "VBDistributions/Gamma.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace Psi
        {
            
            class GammaPrecision :
                public Psi_VBPosterior
            {
            public:
                //--------------------------------------------------------------
                typedef VBDistributions::Gamma::Parameters Parameters;
                //--------------------------------------------------------------
                
                GammaPrecision(const Parameters prior);
                
                void update();
                
                Psi::P2C getExpectations() const;
                
                void save(const std::string directory) const;
                
                double getKL() const;
                
            private:
                VBDistributions::Gamma posterior_;
                
                // Helper function to collect expectations from the MFModels
                Psi::C2P collectExpectations() const;
            };
            
        }
    }
}
#endif
