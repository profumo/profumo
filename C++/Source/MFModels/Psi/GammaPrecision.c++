// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GammaPrecision.h"

#include "Utilities/DataIO.h"

namespace VBDists = PROFUMO::VBDistributions;
namespace MFPsiModels = PROFUMO::MFModels::Psi;

////////////////////////////////////////////////////////////////////////////////

MFPsiModels::GammaPrecision::GammaPrecision(const Parameters prior)
: posterior_(prior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFPsiModels::GammaPrecision::update()
{
    // Collect the data from all the MFModels
    const Psi::C2P D = collectExpectations();
    
    // Pass stats to actual posterior
    VBDists::Gamma::DataExpectations d;
    d.n = D.N;
    d.d2 = D.TrDtD;
    posterior_.update(d);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double MFPsiModels::GammaPrecision::getKL() const
{
    return posterior_.getKL();
}

////////////////////////////////////////////////////////////////////////////////

void MFPsiModels::GammaPrecision::save(const std::string directory) const
{
    // Just save expectations
    std::string fileName;
    const VBDists::Gamma::Expectations expectations = posterior_.getExpectations();
    
    // Means
    fileName = directory + "Precision.txt";
    Utilities::save(fileName, expectations.x);
    
    // E[ log(X) ]
    fileName = directory + "LogPrecision.txt";
    Utilities::save(fileName, expectations.log_x);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFPsiModels::P2C MFPsiModels::GammaPrecision::getExpectations() const
{
    // Get expectations from posterior
    const VBDists::Gamma::Expectations e = posterior_.getExpectations();
    
    // And transform to required form
    Psi::P2C E;
    E.psi = e.x;
    E.logPsi = e.log_x;

    return E;
}

////////////////////////////////////////////////////////////////////////////////

MFPsiModels::C2P MFPsiModels::GammaPrecision::collectExpectations() const
{
    // Subject-level model so not parallelised
    Psi::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.TrDtD = 0.0f;
        D.N = 0.0f;
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator mfModel = childModules_.begin();
        D = (*mfModel)->getExpectations();
        
        // Add other models if necessary
        for (++mfModel; mfModel != childModules_.end(); ++mfModel) {
            const Psi::C2P d = (*mfModel)->getExpectations();
            D.TrDtD += d.TrDtD;
            D.N += d.N;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////
