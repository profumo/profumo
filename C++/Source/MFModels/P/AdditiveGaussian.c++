// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "AdditiveGaussian.h"

#include <cmath>

#include "Utilities/DataIO.h"
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace MFPModels = PROFUMO::MFModels::P;
namespace Mods = PROFUMO::Modules;
namespace MatMeans = Mods::MatrixMeans;
namespace VBDists = PROFUMO::VBDistributions;

// Resolve references to parents
typedef Mods::MatrixMean_Child MeanModel;

// Resolve calls to internally cached expectations
typedef PROFUMO::CachedModule<MFPModels::P2C> PCache;
typedef PROFUMO::CachedModule<Mods::MatrixMeans::C2P> MeanCache;

////////////////////////////////////////////////////////////////////////////////

MFPModels::AdditiveGaussian::AdditiveGaussian(Mods::MatrixMean_Parent* means, const Parameters prior, const float dofCorrectionFactor)
: Mods::MatrixMean_Child(means), precision_(prior.precisionPrior), V_(prior.V), M_(prior.M), dofCorrectionFactor_(dofCorrectionFactor)
{
    // Initialise posteriors from parents
    const float precision = precision_.getExpectations().x;
    
    Sigma_ = (1.0f / std::sqrt(precision)) * arma::eye<arma::fmat>(M_,M_);
    
    Mu_ = MeanModel::parentModule_->getExpectations().X;
         // + (1.0f / std::sqrt(precision)) * arma::randn<arma::fmat>(V_,M_);
    
    // Make everything ship shape
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::AdditiveGaussian::update()
{
    // Collect the data from all the MFModels
    const P::C2P D = collectExpectations();
    
    // Get references to hyperpriors
    const MatMeans::P2C means = MeanModel::parentModule_->getExpectations();
    const VBDists::Gamma::Expectations precision = precision_.getExpectations();
    
    ////////////////////////////////////////////////////////////////////////////
    // Maps
    
    // Posterior covariance
    Sigma_ = linalg::inv_sympd( precision.x * arma::eye<arma::fmat>(M_,M_) + D.psiAAt );
    
    // Posterior mean
    Mu_ = ( precision.x * means.X + D.psiDAt ) * Sigma_;
    
    ////////////////////////////////////////////////////////////////////////////
    // Precision
    
    VBDists::Gamma::DataExpectations precD;
    
    precD.n = dofCorrectionFactor_ * V_ * M_;
    
    precD.d2 = dofCorrectionFactor_ * linalg::sum(
            linalg::sum(arma::square(Mu_) - (2.0f * Mu_ % means.X) + means.X2, 0) + V_ * Sigma_.diag().t(), 1
        ).at(0);
    
    precision_.update(precD);
    
    ////////////////////////////////////////////////////////////////////////////
    
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double MFPModels::AdditiveGaussian::getKL() const
{
    // Get hyperpriors
    const MatMeans::P2C means = MeanModel::parentModule_->getExpectations();
    const VBDists::Gamma::Expectations precision = precision_.getExpectations();
    
    double KL = 0.0;
    
    // Contribution from Gaussian
    KL += - 0.5 * V_ * M_ * precision.log_x;
    KL += - 0.5 * V_ * std::real(arma::log_det(Sigma_));
    KL += 0.5 * precision.x * linalg::sum(
            linalg::sum(arma::square(Mu_) - (2.0 * Mu_ % means.X) + means.X2, 0) + V_ * Sigma_.diag().t(), 1
        ).at(0);
    KL += - 0.5 * V_ * M_;
    
    // Correct for voxels not being independent
    KL *= dofCorrectionFactor_;
    
    // And from precision
    KL += precision_.getKL();
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::AdditiveGaussian::save(const std::string directory) const
{
    // Just save posterior params
    std::string fileName;
    
    // Mu
    fileName = directory + "Means.hdf5";
    Mu_.save(fileName, arma::hdf5_binary);
    
    // Sigma
    fileName = directory + "Covariance.hdf5";
    Sigma_.save(fileName, arma::hdf5_binary);
    
    // Precision - just save expectations
    const VBDists::Gamma::Expectations expectations = precision_.getExpectations();
    
    // Means
    fileName = directory + "Precision.txt";
    Utilities::save(fileName, expectations.x);
    
    // E[ log(X) ]
    fileName = directory + "LogPrecision.txt";
    Utilities::save(fileName, expectations.log_x);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFPModels::C2P MFPModels::AdditiveGaussian::collectExpectations() const
{
    // Subject-level model so not parallelised
    P::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.psiAAt = arma::zeros<arma::fmat>(M_,M_);
        D.psiDAt = arma::zeros<arma::fmat>(V_,M_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator mfModel = childModules_.begin();
        D = (*mfModel)->getExpectations();
        
        // Add other models if necessary
        for (++mfModel; mfModel != childModules_.end(); ++mfModel) {
            const P::C2P d = (*mfModel)->getExpectations();
            D.psiAAt += d.psiAAt;
            D.psiDAt += d.psiDAt;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void MFPModels::AdditiveGaussian::updateExpectations()
{
    // Get references to cached expectations
    arma::fmat& P_ = PCache::expectations_.P;
    arma::fmat& PtP_ = PCache::expectations_.PtP;
    arma::fmat& Psi_ = MeanCache::expectations_.Psi;
    arma::fmat& PsiD_ = MeanCache::expectations_.PsiD;
    
    // Set expectations
    
    // MFPModel
    P_ = Mu_;
    
    PtP_ = P_.t() * P_ + V_ * Sigma_;
    
    // MatrixMeans
    Psi_ = precision_.getExpectations().x * arma::ones<arma::fmat>(V_,M_);
    PsiD_ = Psi_ % Mu_;
    
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    P_.elem( arma::find(arma::abs(P_) < 1.0e-10f) ).zeros();
    PtP_.elem( arma::find(arma::abs(PtP_) < 1.0e-10f) ).zeros();
    //**************************************************************************
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
