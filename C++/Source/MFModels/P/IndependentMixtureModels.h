// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A set of spatial maps, where each weight is drawn from an independent 
// Gaussian mixture model.

#ifndef MF_MODELS_P_INDEPENDENT_MIXTURE_MODELS_H
#define MF_MODELS_P_INDEPENDENT_MIXTURE_MODELS_H

#include <armadillo>
#include <array>
#include <string>
#include <vector>

#include "Module.h"
#include "Posterior.h"
#include "MFModel.h"
#include "MFVariableModels.h"
#include "VBDistributions/GaussianMixtureModel.h"

namespace PROFUMO
{
    namespace MFModels
    {
        namespace P
        {
            
            template<unsigned int nDelta, unsigned int nGaussian>
            class IndependentMixtureModels :
                public P_VBPosterior,
                protected CachedModule<P::P2C>
            {
            public:
                //--------------------------------------------------------------
                struct GaussianParameters
                {
                public:
                    arma::fmat mu;
                    arma::fmat sigma2;
                    arma::fmat p;
                };
                //--------------------------------------------------------------
                struct DeltaParameters
                {
                public:
                    arma::fmat mu;
                    arma::fmat p;
                };
                //--------------------------------------------------------------
                struct Parameters
                {
                public:
                    std::array<DeltaParameters, nDelta> deltas;
                    std::array<GaussianParameters, nGaussian> gaussians;
                    bool randomInitialisation = true;
                };
                //--------------------------------------------------------------
                IndependentMixtureModels(const Parameters prior, const float dofCorrectionFactor=1.0f);
                
                void update();
                
                double getKL() const;
                
                void save(const std::string directory) const;
                
            private:
                // Helper functions to collect expectations / set caches
                P::C2P collectExpectations() const;
                void updateExpectations();
                
                // Storage for posteriors
                // Use value semantics - want a nice simple memory layout (and
                // each class is self contained) to try and help caching
                // https://stackoverflow.com/a/71264
                typedef VBDistributions::GaussianMixtureModel<nDelta, nGaussian> GMM;
                std::vector<GMM> posteriors_;
                
                // Sizes
                const unsigned int V_; // Number of voxels
                const unsigned int M_; // Number of modes
                
                // Spatial DOF correction
                const float dofCorrectionFactor_;
            };
            
        }
    }
}

// Template implementation
#include "IndependentMixtureModels.t++"

#endif
