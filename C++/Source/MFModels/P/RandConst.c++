// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "RandConst.h"

#include <armadillo>

////////////////////////////////////////////////////////////////////////////////

PROFUMO::MFModels::P::RandConst::RandConst(const Parameters prior, const float /*dofCorrectionFactor*/)
{
    const arma::fmat P = arma::randn<arma::fmat>(prior.V, prior.M);
    
    expectations_.P = P;
    expectations_.PtP = P.t() * P + prior.V * arma::eye<arma::fmat>(prior.M, prior.M);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModels::P::RandConst::update()
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::MFModels::P::RandConst::getKL() const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::MFModels::P::RandConst::save(std::string directory) const
{
    std::string fileName;
    
    // P
    fileName = directory + "Means.hdf5";
    expectations_.P.save(fileName, arma::hdf5_binary);
    
    // PtP
    fileName = directory + "InnerProduct.hdf5";
    expectations_.PtP.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
