// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "SpikeSlab.h"

#include <armadillo>
#include <vector>
#include <cmath>

namespace MFHModels = PROFUMO::MFModels::H;

namespace VBDists = PROFUMO::VBDistributions;

////////////////////////////////////////////////////////////////////////////////

MFHModels::SpikeSlab::SpikeSlab(const Parameters prior)
: M_(prior.M), prior_(prior)
{
    // Put all the parameters to make a spike-slab together
    std::array<GMM::DeltaParameters, 1> deltaPriors;
    deltaPriors[0].mu = 0.0f;
    deltaPriors[0].p  = 1.0f - prior.p;
    std::array<GMM::GaussianParameters, 1> gaussianPriors;
    gaussianPriors[0].mu     = prior.mu;
    gaussianPriors[0].sigma2 = prior.sigma2;
    gaussianPriors[0].p      = prior.p;
    
    // Make the posteriors!
    posteriors_.reserve(M_);
    const bool randomInitialisation = false;
    for (unsigned int m = 0; m < M_; ++m) {
        posteriors_.emplace_back(deltaPriors, gaussianPriors, randomInitialisation);
    }
    
    // Initialise expectations to unity
    expectations_.h = arma::ones<arma::fvec>(M_);
    // And a bit of uncertainty for the variance
    expectations_.hht = 
        expectations_.h * expectations_.h.t()
            + 0.1f * arma::eye<arma::fmat>(M_, M_);
    
    // Make everything ship-shape (just sets expectations to posteriors)
    // But don't do this if it would set the weights to zero...
    if (prior.mu > 0.0f) {
        updateExpectations();
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFHModels::SpikeSlab::update()
{
    // Collect the data from all the MFModels
    const H::C2P D = collectExpectations();
    
    // Transform and pass to posteriors
    for (unsigned int m = 0; m < M_; ++m) {
            // Calculate conditionals
            VBDists::DataExpectations::Gaussian d;
            d.psi = D.psiPtPoAAt(m,m);
            
            // Need to regress out the contributions from the other weights for the mean
            expectations_.h(m) = 0.0f;
            d.psi_d = D.diag_psiPtDAt(m)
                      - arma::as_scalar(expectations_.h.t() * D.psiPtPoAAt.col(m));
            
            // Rectify
            if (prior_.rectifyWeights) {
                d.psi_d = std::abs( d.psi_d );
            }
            
            // Update posterior
            posteriors_[m].update(d);
            
            // Update expectations
            expectations_.h(m) = posteriors_[m].getExpectations().x;
    }
    
    // Make everything ship-shape
    updateExpectations();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double MFHModels::SpikeSlab::getKL() const
{
    // Collect from posteriors
    double KL = 0.0;
    for (unsigned int m = 0; m < M_; ++m) {
        KL += posteriors_[m].getKL();
    }
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void MFHModels::SpikeSlab::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // h
    fileName = directory + "Means.hdf5";
    expectations_.h.save(fileName, arma::hdf5_binary);
    
    // hht
    fileName = directory + "OuterProduct.hdf5";
    expectations_.hht.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFHModels::C2P MFHModels::SpikeSlab::collectExpectations() const
{
    
    // Initialise expectations
    H::C2P D;
    D.psiPtPoAAt = arma::zeros<arma::fmat>(M_,M_);
    D.diag_psiPtDAt = arma::zeros<arma::fvec>(M_);
    
    // Parallelise over iterator
    #pragma omp parallel
    for (listType::const_iterator mfModel = childModules_.begin(); mfModel != childModules_.end(); ++mfModel) {
        #pragma omp single nowait
        {
        const H::C2P d = (*mfModel)->getExpectations();
        #pragma omp critical
        {
        D.psiPtPoAAt += d.psiPtPoAAt;
        D.diag_psiPtDAt += d.diag_psiPtDAt;
        }
    }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

void MFHModels::SpikeSlab::updateExpectations()
{
    // Get references to cached expectations
    arma::fvec& h_ = expectations_.h;
    arma::fmat& hht_ = expectations_.hht;
    
    // Collect from posteriors
    arma::fvec h2 = arma::zeros<arma::fvec>(M_);
    for (unsigned int m = 0; m < M_; ++m) {
        const VBDists::Expectations::Gaussian e = posteriors_[m].getExpectations();
        
        h_(m) = e.x;
        h2(m) = e.x2;
    }
    
    // Form E[h * ht]
    hht_ = h_ * h_.t();
    hht_.diag() = h2;
    
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    h_.elem( arma::find(arma::abs(h_) < 1.0e-10f) ).zeros();
    hht_.elem( arma::find(arma::abs(hht_) < 1.0e-10f) ).zeros();
    //**************************************************************************
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
