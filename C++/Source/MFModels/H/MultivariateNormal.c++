// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "MultivariateNormal.h"

#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

namespace MFHModels = PROFUMO::MFModels::H;
namespace Mods = PROFUMO::Modules;
namespace MatMeans = Mods::MatrixMeans;
namespace PrecMats = Mods::PrecisionMatrices;

// Resolve references to parents
typedef Mods::MatrixMean_Child MeanModel;
typedef Mods::PrecisionMatrix_Child PrecMatModel;

// Resolve calls to internally cached expectations
typedef PROFUMO::CachedModule<MFHModels::P2C> HCache;

////////////////////////////////////////////////////////////////////////////////

MFHModels::MultivariateNormal::MultivariateNormal(Mods::MatrixMean_Parent* means, Mods::PrecisionMatrix_Parent* precisionMatrix, const Parameters prior)
: Mods::MatrixMean_JointChild(means), Mods::PrecisionMatrix_JointChild(precisionMatrix), M_(prior.M), prior_(prior)
{
    // Initialise posterior
    H::P2C& E = HCache::expectations_;
    E.h = MeanModel::parentModule_->getExpectations().X;
    // Set weights to 1.0 if prior is zero mean so components have a chance
    E.h.elem( arma::find(arma::abs(E.h) < 1.0e-10f) ).ones();
    // And a bit of uncertainty for the variance
    E.hht = E.h * E.h.t() + 0.1f * arma::eye<arma::fmat>(M_, M_);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MFHModels::MultivariateNormal::update()
{
    // Collect the data from all the MFModels
    const H::C2P D = collectExpectations();
    
    // And collect expectations from hyperpriors (just need the means)
    const arma::fmat pMu = MeanModel::parentModule_->getExpectations().X;
    const arma::fmat alpha = PrecMatModel::parentModule_->getExpectations().X;
    
    // Updates!
    // Calculate covariance
    Sigma_ = linalg::inv_sympd( alpha + D.psiPtPoAAt );
    // Calculate mean
    arma::fvec mu = Sigma_ * (D.diag_psiPtDAt + alpha * pMu);
    
    // Rectify
    if (prior_.rectifyWeights) {
        const arma::fvec signs = arma::sign(mu);
        mu %= signs;
        Sigma_ %= signs * signs.t();
    }
    
    // Clip
    if (prior_.clipWeights) {
        // Min
        const arma::fvec min_mu = prior_.clipRange_min * pMu;
        const arma::uvec min_inds = arma::find(arma::abs(mu) < arma::abs(min_mu));
        mu(min_inds) = min_mu(min_inds);
        // Max
        const arma::fvec max_mu = prior_.clipRange_max * pMu;
        const arma::uvec max_inds = arma::find(arma::abs(mu) > arma::abs(max_mu));
        mu(max_inds) = max_mu(max_inds);
    }
    
    // Update expectations
    H::P2C& E = HCache::expectations_;
    E.h = mu;
    E.hht = mu * mu.t() + Sigma_;
    //**************************************************************************
    // Seems to help matrix multiplications if matrices don't have wacky values
    E.h.elem( arma::find(arma::abs(E.h) < 1.0e-10f) ).zeros();
    E.hht.elem( arma::find(arma::abs(E.hht) < 1.0e-10f) ).zeros();
    //**************************************************************************
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double MFHModels::MultivariateNormal::getKL() const
{
    // Get precision matrix hyperprior
    const PrecMats::P2C alpha = PrecMatModel::parentModule_->getExpectations();
    
    // Calculate KL divergence
    double KL = 0.0;
    
    // Prior and posterior determinants
    KL -= 0.5 * alpha.logDetX;
    KL -= 0.5 * std::real(arma::log_det(Sigma_));
    
    // Likelihood type terms
    KL += 0.5 * arma::trace( alpha.X * getPrecisionMatrices().XXt );
    KL -= 0.5 * M_;
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////

void MFHModels::MultivariateNormal::save(const std::string directory) const
{
    // Just save cached expectations
    std::string fileName;
    
    // h
    fileName = directory + "Means.hdf5";
    HCache::expectations_.h.save(fileName, arma::hdf5_binary);
    
    // hht
    fileName = directory + "OuterProduct.hdf5";
    HCache::expectations_.hht.save(fileName, arma::hdf5_binary);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

MFHModels::C2P MFHModels::MultivariateNormal::collectExpectations() const
{
    // Subject-level model so not parallelised
    H::C2P D;
    if (childModules_.empty()) {
        // All zero if no children
        D.psiPtPoAAt = arma::zeros<arma::fmat>(M_,M_);
        D.diag_psiPtDAt = arma::zeros<arma::fvec>(M_);
    }
    else {
        // Initialise store from first MF model
        listType::const_iterator mfModel = childModules_.begin();
        D = (*mfModel)->getExpectations();
        
        // Add other models if necessary
        for (++mfModel; mfModel != childModules_.end(); ++mfModel) {
            const H::C2P d = (*mfModel)->getExpectations();
            D.psiPtPoAAt += d.psiPtPoAAt;
            D.diag_psiPtDAt += d.diag_psiPtDAt;
        }
    }
    
    return D;
}

////////////////////////////////////////////////////////////////////////////////

MatMeans::C2P MFHModels::MultivariateNormal::getMatrixMeans() const
{
    const arma::fmat alpha = PrecMatModel::parentModule_->getExpectations().X;
    
    MatMeans::C2P E;
    
    // APPROXIMATION!!
    // Note that the posterior over the means assumes that the different 
    // elements are independent, which they're not...
    // See Python code at the end of the file.
    E.Psi = arma::diagvec(alpha);
    E.PsiD = arma::diagvec(alpha) % HCache::expectations_.h;
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////

PrecMats::C2P MFHModels::MultivariateNormal::getPrecisionMatrices() const
{
    // Get necessary expectations from hyperpriors
    // Mismatched independence assumption means we need to do a bit of rearrangement
    const MatMeans::P2C means = MeanModel::parentModule_->getExpectations();
    const arma::fvec pMu = means.X;
    arma::fmat pMuMut = means.X * means.X.t(); pMuMut.diag() = means.X2;
    const H::P2C& EH = HCache::expectations_;
    
    PrecMats::C2P E;
    
    E.N = 1;
    E.XXt = EH.hht - (EH.h * pMu.t()) - (pMu * EH.h.t()) + pMuMut;
    
    return E;
}

////////////////////////////////////////////////////////////////////////////////
/*
import numpy, numpy.linalg

N = 50

for a in numpy.linspace(0.75,1.25,9):
    print(a)
    print()
    
    # Generate covariance
    alpha = a * numpy.eye(N) + (1.0 - a) * numpy.ones([N,N])
    mu = numpy.ones([N,])
    print(alpha[:,:2][:2,:])
    print()
    if numpy.min(numpy.linalg.eig(alpha)[0]) < 0.0:
        continue
    
    # True updates
    Sigma = numpy.linalg.inv(alpha + numpy.eye(N))
    M = Sigma @ (alpha @ mu)
    
    # Independence assumption
    sigma = 1.0 / (1.0 + alpha.diagonal())
    m = sigma * (alpha @ mu)
    # Alternative formulation
    m2 = sigma * (alpha.diagonal() * mu)
    
    print(Sigma[:,:2][:2,:], Sigma.diagonal()[:2], sigma[:2])
    print()
    print(M[:2],m[:2],m2[:2])
    
    print()
    print('*'*10)
    print()
*/
////////////////////////////////////////////////////////////////////////////////
