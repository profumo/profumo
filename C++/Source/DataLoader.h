// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Class that loads the full set of subject data.

#ifndef DATA_LOADER_H
#define DATA_LOADER_H

#include <vector>
#include <map>
#include <armadillo>
#include <memory>
#include <string>
#include <sstream>    // std::ostringstream
#include <algorithm>  // std::min
#include <sys/stat.h>
#include <stdexcept>

#include <boost/optional.hpp>
#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>

#include "Utilities/DataIO.h"
#include "Utilities/DataNormalisation.h"
#include "Utilities/Timer.h"

#include "SubjectModelling/Run.h"
#include "SubjectModelling/Subject.h"

using json = nlohmann::json;

namespace PROFUMO
{
    ////////////////////////////////////////////////////////////////////////////
    
    // Class which contains a method that transforms the data
    // Need this as a separate class because we can't call virtual functions 
    // in the constructor
    // See https://isocpp.org/wiki/faq/strange-inheritance#calling-virtuals-from-ctor-idiom
    template <class D>
    class DataTransformer
    {
    public:
        // Turns a set of data matrices into the requested form
        virtual std::map<RunID, D> transformSubjectData(const std::map<RunID, std::shared_ptr<const arma::fmat>>& subjectDataMatrices) const = 0;
        
        virtual ~DataTransformer() = default;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    
    namespace DataLoaders
    {
        // Define here rather than nested as normal so doesn't depend on the
        // template parameter of DataLoader...
        // I.e. otherwise it would be DataLoader<T>::Options
        struct Options {
        public:
            boost::optional<arma::uvec> maskInds;
            bool normaliseData = true;
            #ifdef _OPENMP
                bool paralleliseLoading = true;
            #endif
        };
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Spatial basis: Spatial singular vectors of potentially multiple data
    struct SpatialBasis
    {
    public:
        // SVD
        arma::fmat U;
        arma::fvec s;
        
        // Metadata: total number of subjects and timepoints
        unsigned int S=0, T=0;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    
    template <class D>
    class DataLoader
    {
    public:
        // Constructor: loads all the data
        DataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, std::unique_ptr<DataTransformer<D>> dataTransformer, const DataLoaders::Options options = {});
        // https://stackoverflow.com/a/38131963
        
        // Get the data!
        std::map<SubjectID, std::map<RunID, D>> getData() const;
        
        // Computes the spatial basis for all the data (i.e. U * diag(s) from an SVD)
        SpatialBasis computeSpatialBasis(const unsigned int K) const;
        
        // Get key info
        unsigned int getS() const {return S_;}
        unsigned int getV() const {return V_;}
        arma::uvec getT() const {return T_;}
        
        virtual ~DataLoader() = default;
        
    protected:
        // Converts data matrices to new representation
        const std::unique_ptr<DataTransformer<D>> dataTransformer_;
        
        // Data store
        const std::string dataLocationsFile_;
        std::map<SubjectID, std::map<RunID, D>> dataStore_;
        
        // Key sizes
        unsigned int S_, V_ = 0; // Subjects, voxels
        arma::uvec T_; // Time points (can be different across runs)
        
        // Size of subspace for noise correction
        const unsigned int M_;
        
        // Mask
        const boost::optional<arma::uvec> maskInds_;
        
        // Loads the subject information from a file
        std::map<SubjectID, std::map<RunID, std::string>> loadDataLocations();
        
        // Loads data, records key info, masks etc
        arma::fmat loadDataMatrix(const std::string dataLocation);
        
        // Few basic pre-processing steps
        void preprocessDataMatrix(arma::fmat& data, const bool normaliseData, const std::string outputBaseName);
        
        // Needs overriding with the basis computation
        virtual SpatialBasis computeBasis(const unsigned int K) const = 0;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    
    template <class D>
    DataLoader<D>::DataLoader(const std::string dataLocationsFile, const std::string outputDirectory, const unsigned int M, std::unique_ptr<DataTransformer<D>> dataTransformer, const DataLoaders::Options options)
    : dataTransformer_(std::move(dataTransformer)), dataLocationsFile_(dataLocationsFile), M_(M), maskInds_(options.maskInds)
    {
        Utilities::Timer timer;
        spdlog::debug("Loading data...");
        
        // Get IDs and data locations from spec file
        const std::map<SubjectID, std::map<RunID, std::string>> dataLocations
            = loadDataLocations();
        
        // Collect SubjectIDs (easier for parallelising than a map)
        std::vector<SubjectID> subjectList;
        for (const auto& subject : dataLocations) {
            subjectList.emplace_back(subject.first);
        }
        
        // Loop over subjects
        #pragma omp parallel for schedule(dynamic) if(options.paralleliseLoading)
        for (unsigned int s = 0; s < subjectList.size(); ++s) {
            const SubjectID subjectID = subjectList[s];
            
            // Make directory for preprocessing output
            const std::string subjectDirectory = outputDirectory + subjectID + "/";
            mkdir( subjectDirectory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
            
            // Container for raw subject data
            std::map<RunID, std::shared_ptr<const arma::fmat>> subjectDataMatrices;
            
            // Load each run
            for (const auto& run : dataLocations.at(subjectID)) {
                const RunID runID = run.first;
                const std::string dataLocation = run.second;
                
                // Get base filename for everything we save
                const std::string baseName = subjectDirectory + runID;
                
                // Save the data location
                Utilities::save(baseName + "_DataLocation.txt", dataLocation);
                
                // Get the data and preprocess
                arma::fmat runDataMatrix = loadDataMatrix(dataLocation);
                preprocessDataMatrix( runDataMatrix, options.normaliseData, baseName );
                
                // Store
                subjectDataMatrices.emplace(runID, std::make_shared<arma::fmat>(std::move(runDataMatrix)));
            }
            
            // Turn into the form we want
            const std::map<RunID, D> subjectData = dataTransformer_->transformSubjectData( subjectDataMatrices );
            
            // Add to the map
            #pragma omp critical(addToMap)
            {
            dataStore_.emplace(subjectID, subjectData);
            spdlog::debug(
                    "Subject \"{:s}\" loaded [{:d} / {:d}].",
                    subjectID, dataStore_.size(), subjectList.size());
            
            if (dataStore_.size() % 50 == 0) {
                spdlog::info("{:d} subjects loaded.", dataStore_.size());
            }
            }
        }
        
        // Set key sizes
        S_ = dataStore_.size();
        unsigned int R_ = 0;
        for (const auto& subject : dataStore_) {
            R_ += subject.second.size();
        }
        T_ = arma::sort(T_);
        
        // If we are usng a mask, update the number of voxels to reflect this
        if (maskInds_) {
            V_ = maskInds_->n_elem;
        }
        
        // And print a summary
        std::ostringstream summary;
        summary << "------------" << std::endl;
        summary << "Data summary" << std::endl;
        summary << "------------" << std::endl;
        summary << "Subjects:   " << S_ << std::endl;
        summary << "Runs:       " << R_ << std::endl;
        summary << "Voxels:     " << V_ << std::endl;
        summary << "Timepoints: ";
        for (unsigned int i = 0; i < T_.size(); ++i) {
            summary << T_[i];
            if (i != T_.size() - 1) {summary << ", ";}
        }
        summary << std::endl;
        summary << "------------";
        spdlog::debug(summary.str());
        
        spdlog::debug("Data loaded. Time elapsed: {:s}", timer.getTimeElapsed());
        
        return;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Get the data!
    
    template <class D>
    std::map<SubjectID, std::map<RunID, D>> DataLoader<D>::getData() const
    {
        return dataStore_;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    template <class D>
    SpatialBasis DataLoader<D>::computeSpatialBasis(const unsigned int K) const
    {
        Utilities::Timer timer;
        spdlog::debug("Computing spatial basis...");
        
        const SpatialBasis basis = computeBasis(K);
        
        spdlog::debug("Basis computed. Time elapsed: {:s}", timer.getTimeElapsed());
        
        return basis;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Get data locations from spec file
    
    template <class D>
    std::map<SubjectID, std::map<RunID, std::string>> DataLoader<D>::loadDataLocations()
    {
        std::map<SubjectID, std::map<RunID, std::string>> dataLocationStore;
        
        try {
            // Parse file into JSON object
            std::ifstream dataLocationsFile; dataLocationsFile.open(dataLocationsFile_);
            const json dataLocations = json::parse(dataLocationsFile);
            dataLocationsFile.close();
            
            // Check we've actually got an object we can work with...
            if ( !dataLocations.is_object() || dataLocations.empty() ) {
                throw std::invalid_argument("Could not find base JSON object in data locations file! Note the file cannot be set up as an array (i.e. it must use { } instead of [ ]).");
            }
            
            // Loop over subjects
            for (json::const_iterator subject = dataLocations.begin(); subject != dataLocations.end(); ++subject) {
                const SubjectID subjectID = subject.key();
                // Sanity checks
                if ( !subject.value().is_object() || subject.value().empty() ) {
                    std::ostringstream message; message << "Could not find run locations for subject \"" << subjectID << "\"! Note the runs cannot be set up as an array (i.e. they must use { } instead of [ ]).";
                    throw std::invalid_argument( message.str() );
                }
                // Duplicate keys - I think the parser silently ignores these for now...
                else if (dataLocationStore.count(subjectID) != 0) {
                    std::ostringstream message; message << "Found two entries for subject \"" << subjectID << "\"!";
                    throw std::invalid_argument( message.str() );
                }
                
                // And then process the runs
                std::map<RunID, std::string> subjectLocations;
                for (json::const_iterator run = subject.value().begin(); run != subject.value().end(); ++run) {
                    const RunID runID = run.key();
                    // More sanity checks
                    if ( !run.value().is_string() ) {
                        std::ostringstream message; message << "Could not find valid file name for run \"" << runID << "\" in subject \"" << subjectID << "\"!";
                        throw std::invalid_argument( message.str() );
                    }
                    else if (subjectLocations.count(runID) != 0) {
                        std::ostringstream message; message << "Found two entries for run \"" << runID << "\" in subject \"" << subjectID << "\"!";
                        throw std::invalid_argument( message.str() );
                    }
                    // Yay!
                    const std::string fileName = run.value();
                    subjectLocations.emplace(runID, fileName);
                }
                // Record everything
                dataLocationStore.emplace(subjectID, subjectLocations);
            }
        }
        catch (const std::invalid_argument& error) {
            std::ostringstream message; message << "DataLoader: Error while parsing file \"" << dataLocationsFile_ << "\".\n    " << error.what();
            throw std::invalid_argument( message.str() );
        }
        
        return dataLocationStore;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Pre-process data
    
    template <class D>
    void DataLoader<D>::preprocessDataMatrix(arma::fmat& data, const bool normaliseData, const std::string outputBaseName)
    {
        namespace DN = PROFUMO::Utilities::DataNormalisation;
        
        // Demean
        arma::fvec means = DN::demeanRows(data);
        #pragma omp critical(HDF5)
        {
        means.save(outputBaseName + "_Means.hdf5", arma::hdf5_binary);
        }
        
        // Normalise variances, if requested
        if (normaliseData) {
            // Sanity check the rank of the internal subspace
            const unsigned int dataRank = std::min({data.n_rows, data.n_cols});
            const unsigned int M = std::min({M_, (dataRank+1) / 2});  // +1 to ensure this can't be zero
            
            // Generative model assumes noise variance is the same in all voxels
            // And we probably want each scan to have a roughly similar amount of signal
            arma::fvec norm = arma::ones<arma::fvec>(data.n_rows);
            norm *= DN::normaliseGlobally(data);     // Just gets things in the right ballpark
            norm %= DN::normaliseRows(data, 1.0e-5f); // If the voxelwise std is less than 1.0e-5, safer to ignore (probably e.g. mask issues)
            norm %= DN::normaliseNoiseSubspaceRows(data, M, 1.0e-5f); // Likewise (things are v. unlikely to be pure signal)
            norm *= DN::normaliseSignalSubspaceGlobally(data, M);
            // Finally, apply a correction for the number of modes.
            data *= std::sqrt((float) M); norm *= std::sqrt((float) M);
            // The penultimate step sets the subspace of the top M SVD 
            // components to  be unit variance. However, the generative model 
            // is D = P * A, so D_ij = sum_m P_im A_km. Therefore, assuming 
            // independence between P and A, the elementwise relationship is:
            //   E[d^2] = M * E[p^2] * E[a^2]
            // If, for example, we have p,a ~ N(0,1) then E[d^2] = M. In 
            // essence, by applying this correction, we set each modes subspace 
            // to be unit variance, rather than the combination thereof.
            
            // Save normalisation to file
            #pragma omp critical(HDF5)
            {
            norm.save(outputBaseName + "_Normalisation.hdf5", arma::hdf5_binary);
            }
        }
        
        //**********************************************************************
        // Seems to help matrix multiplications if matrices don't have wacky values
        data.elem( arma::find(arma::abs(data) < 1.0e-10f) ).zeros();
        //**********************************************************************
        
        // Warn on NaN: we can reasonably replace with zero after demeaning
        if (data.has_nan() || data.has_inf()) {
            spdlog::warn("NaN / inf detected in data after preprocessing. Replacing.");
            data.elem( arma::find_nonfinite(data) ).zeros();
        }
        
        return;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    template <class D>
    arma::fmat DataLoader<D>::loadDataMatrix(const std::string dataLocation)
    {
        // Get the data!
        const arma::fmat dataMatrix = Utilities::loadDataMatrix( dataLocation );
        spdlog::trace("Raw data loaded from \"{:s}\".\nSize [{:d}, {:d}].", dataLocation, dataMatrix.n_rows, dataMatrix.n_cols);
        
        if (dataMatrix.has_nan() || dataMatrix.has_inf()) {
            std::stringstream errorMessage;
            errorMessage << "DataLoader: NaN / inf detected in raw data!\n";
            errorMessage << "    File: \"" << dataLocation << "\"\n";
            throw std::runtime_error( errorMessage.str() );
        }
        
        // Check size
        #pragma omp critical(checkSize)
        {
        // Check voxels
        if ( V_ != 0 ) {
            if ( dataMatrix.n_rows != V_ ) {
                std::stringstream errorMessage;
                errorMessage << "DataLoader: All data must have the same number of voxels!\n";
                errorMessage << "    New: " << dataMatrix.n_rows << "; Old: " << V_ << "\n";
                errorMessage << "    File: \"" << dataLocation << "\"\n";
                throw std::runtime_error( errorMessage.str() );
            }
        }
        else { V_ = dataMatrix.n_rows; }
        
        // And time points
        if ( not arma::any(T_ == dataMatrix.n_cols) ) {
            T_ = arma::join_vert(T_, arma::uvec{dataMatrix.n_cols});
        }
        }
        
        // Mask, if required
        if (maskInds_) {
            try {
                // Have to return directly as dataMatrix is const
                return dataMatrix.rows(*maskInds_);
            }
            catch (const std::logic_error& /* error */) {
                std::stringstream errorMessage;
                errorMessage << "DataLoader: Mask and image sizes incompatible!\n";
                errorMessage << "    Mask: " << maskInds_->n_elem << "; Image: " << dataMatrix.n_rows << "\n";
                errorMessage << "    File: \"" << dataLocation << "\"\n";
                throw std::runtime_error( errorMessage.str() );
            }
        }
        else {
            return dataMatrix;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
}
#endif

// Python to establish variance normalisation thresholds
/*
mask = nibabel.load(maskFile)
data = nibabel.load(dataFile)

data = data.get_data()[mask.get_data() != 0.0]
data = (data.T - numpy.mean(data, axis=1)).T
PFMplots.plotMaps(data, "Raw data")

sD = numpy.std(data, axis=1)
sD[sD < 1.0e-10] = 1.0

plt.figure(); plt.plot(sD, '.')
plt.figure(); plt.plot(numpy.log10(sD), '.')
plt.figure(); plt.hist(numpy.log10(sD), bins=250)
*/

// Python to look at norms post-hoc
/*
pfms = PROFUMO.PFMs(resultsDir)
norms = pfms.loadDataNormalisations()
norms = [norms[s][r] for s in norms for r in norms[s]]
norms = numpy.vstack(norms).T
plt.figure(); plt.hist(numpy.log10(norms.flatten()), bins=250)
*/
