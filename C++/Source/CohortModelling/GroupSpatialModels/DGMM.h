// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a group-level spatial model based around the DGMM subject map model.

#ifndef COHORT_MODELLING_GROUP_SPATIAL_MODELS_DGMM_H
#define COHORT_MODELLING_GROUP_SPATIAL_MODELS_DGMM_H

#include <memory>
#include <string>
#include <armadillo>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "ModuleList.h"
#include "MFModels/P/DGMM.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupSpatialModels
        {
            
            class DGMM :
                public GroupSpatialModel
            {
            public:
                DGMM(std::shared_ptr<Modules::MatrixMean_VBPosterior> signalMeans, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> signalPrecisions, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> noisePrecisions, std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships, const MFModels::P::DGMM::Parameters prior, const float dofCorrectionFactor);
                
                virtual SubjectModelling::Subject::SpatialModel getSubjectModel(const SubjectInformation subjectInformation);
                
                virtual arma::fmat getGroupMaps() const;
                
            private:
                virtual void updateModel();
                
                virtual void saveModel(const std::string directory) const;
                
                virtual double getModelKL() const;
                
                // Hyperpriors
                std::shared_ptr<Modules::MatrixMean_VBPosterior> signalMeans_;
                std::shared_ptr<Modules::MatrixPrecision_VBPosterior> signalPrecisions_;
                std::shared_ptr<Modules::MatrixPrecision_VBPosterior> noisePrecisions_;
                std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships_;
                
                // Parameters
                const MFModels::P::DGMM::Parameters prior_;
                const float dofCorrectionFactor_;
            };
            
        }
    }
}
#endif
