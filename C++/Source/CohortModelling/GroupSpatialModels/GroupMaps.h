// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a generic class that contains a group level set of spatial maps that 
// are consistent across a set of subjects.

#ifndef COHORT_MODELLING_GROUP_SPATIAL_MODELS_GROUP_MAPS_H
#define COHORT_MODELLING_GROUP_SPATIAL_MODELS_GROUP_MAPS_H

#include <memory>
#include <string>
#include <armadillo>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupSpatialModels
        {
            
            class GroupMaps :
                public GroupSpatialModel
            {
            public:
                GroupMaps(std::shared_ptr<SubjectModelling::P_Post> groupMaps);
                
                virtual SubjectModelling::Subject::SpatialModel getSubjectModel(const SubjectInformation subjectInformation);
                
                virtual arma::fmat getGroupMaps() const;
                
            private:
                std::shared_ptr<SubjectModelling::P_Post> groupMaps_;
                
                virtual void updateModel();
                
                virtual void saveModel(const std::string directory) const;
                
                virtual double getModelKL() const;
            };
            
        }
    }
}
#endif
