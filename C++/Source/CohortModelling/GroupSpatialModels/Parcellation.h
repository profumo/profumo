// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a group-level spatial model based around subject-level parcellations.

#ifndef COHORT_MODELLING_GROUP_SPATIAL_MODELS_PARCELLATION_H
#define COHORT_MODELLING_GROUP_SPATIAL_MODELS_PARCELLATION_H

#include <memory>
#include <string>
#include <sys/stat.h>
#include <armadillo>

#include <spdlog/spdlog.h>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "ModuleList.h"

#include "Utilities/Timer.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupSpatialModels
        {
            
            template<class Implementation>
            class Parcellation :
                public GroupSpatialModel
            {
            public:
                Parcellation(std::shared_ptr<Modules::MatrixMean_VBPosterior> means, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> precisions, std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships, const typename Implementation::Parameters prior, const float dofCorrectionFactor);
                
                virtual SubjectModelling::Subject::SpatialModel getSubjectModel(const SubjectInformation subjectInformation);
                
                virtual arma::fmat getGroupMaps() const;
                
            private:
                virtual void updateModel();
                
                virtual void saveModel(const std::string directory) const;
                
                virtual double getModelKL() const;
                
                // Hyperpriors
                std::shared_ptr<Modules::MatrixMean_VBPosterior> means_;
                std::shared_ptr<Modules::MatrixPrecision_VBPosterior> precisions_;
                std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships_;
                
                // Parameters
                const typename Implementation::Parameters prior_;
                const float dofCorrectionFactor_;
            };
            
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::Parcellation(std::shared_ptr<Modules::MatrixMean_VBPosterior> means, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> precisions, std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships, const typename Implementation::Parameters prior, const float dofCorrectionFactor)
: means_(means), precisions_(precisions), memberships_(memberships), prior_(prior), dofCorrectionFactor_(dofCorrectionFactor)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::SubjectModelling::Subject::SpatialModel PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::getSubjectModel(const SubjectInformation)
{
    SubjectModelling::Subject::SpatialModel subjectModel;
    subjectModel.model = std::make_shared<Implementation>(means_.get(), precisions_.get(), memberships_.get(), prior_, dofCorrectionFactor_);
    subjectModel.isInternalModel = true;
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::getGroupMaps() const
{
    // Rescale memberships (useful if prior is relatively strong)
    Modules::MembershipProbabilities::P2C pi = memberships_->getExpectations();
    arma::fmat memberships = arma::zeros<arma::fmat>(prior_.V, prior_.M);
    for (unsigned int m = 0; m < prior_.M; ++m) {
        memberships.col(m) = pi.p[m];
    }
    memberships -= memberships.min();
    memberships /= memberships.max();
    
    // And combine with means
    arma::fmat means = means_->getExpectations().X;
    
    return means % memberships;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::updateModel()
{
    Utilities::Timer timer;
    
    means_->update();
    spdlog::trace("Group map means: {:s}", timer.getTimeElapsed());
    timer.reset();
    
    precisions_->update();
    spdlog::trace("Group map precisions: {:s}", timer.getTimeElapsed());
    timer.reset();
    
    memberships_->update();
    spdlog::trace("Group map memberships: {:s}", timer.getTimeElapsed());
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::saveModel(const std::string directory) const
{
    // Voxelwise means
    const std::string meansDir = directory + "Means.post" + "/";
    mkdir( meansDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    means_->save(meansDir);
    
    // Voxelwise precisions
    const std::string precisionsDir = directory + "Precisions.post" + "/";
    mkdir( precisionsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    precisions_->save(precisionsDir);
    
    // Voxelwise memberships
    const std::string membershipsDir = directory + "Memberships.post" + "/";
    mkdir( membershipsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    memberships_->save(membershipsDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
double PROFUMO::CohortModelling::GroupSpatialModels::Parcellation<Implementation>::getModelKL() const
{
    double KL = 0.0;
    
    // Have to correct for D.O.F.!
    KL += dofCorrectionFactor_ * means_->getKL();
    KL += dofCorrectionFactor_ * precisions_->getKL();
    KL += dofCorrectionFactor_ * memberships_->getKL();
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
#endif
