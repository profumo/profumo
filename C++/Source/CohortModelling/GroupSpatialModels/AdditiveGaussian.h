// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a group-level spatial model based around the AdditiveGaussian 
// subject map model.

#ifndef COHORT_MODELLING_GROUP_SPATIAL_MODELS_ADDITIVE_GAUSSIAN_H
#define COHORT_MODELLING_GROUP_SPATIAL_MODELS_ADDITIVE_GAUSSIAN_H

#include <memory>
#include <string>
#include <armadillo>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "ModuleList.h"
#include "MFModels/P/AdditiveGaussian.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupSpatialModels
        {
            
            class AdditiveGaussian :
                public GroupSpatialModel
            {
            public:
                AdditiveGaussian(std::shared_ptr<Modules::MatrixMean_VBPosterior> groupMeans, const MFModels::P::AdditiveGaussian::Parameters prior, const float dofCorrectionFactor);
                
                virtual SubjectModelling::Subject::SpatialModel getSubjectModel(const SubjectInformation subjectInformation);
                
                virtual arma::fmat getGroupMaps() const;
                
            private:
                virtual void updateModel();
                
                virtual void saveModel(const std::string directory) const;
                
                virtual double getModelKL() const;
                
                // Hyperpriors
                std::shared_ptr<Modules::MatrixMean_VBPosterior> groupMeans_;
                
                // Parameters
                const MFModels::P::AdditiveGaussian::Parameters prior_;
                const float dofCorrectionFactor_;
            };
            
        }
    }
}
#endif
