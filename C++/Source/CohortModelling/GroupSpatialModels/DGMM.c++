// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "DGMM.h"

#include <sys/stat.h>

#include <spdlog/spdlog.h>

#include "Utilities/Timer.h"

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupSpatialModels::DGMM::DGMM(std::shared_ptr<Modules::MatrixMean_VBPosterior> signalMeans, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> signalPrecisions, std::shared_ptr<Modules::MatrixPrecision_VBPosterior> noisePrecisions, std::shared_ptr<Modules::MembershipProbability_VBPosterior> memberships, const MFModels::P::DGMM::Parameters prior, const float dofCorrectionFactor)
: signalMeans_(signalMeans), signalPrecisions_(signalPrecisions), noisePrecisions_(noisePrecisions), memberships_(memberships), prior_(prior), dofCorrectionFactor_(dofCorrectionFactor)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::SpatialModel PROFUMO::CohortModelling::GroupSpatialModels::DGMM::getSubjectModel(const SubjectInformation)
{
    SubjectModelling::Subject::SpatialModel subjectModel;
    subjectModel.model = std::make_shared<MFModels::P::DGMM>(signalMeans_.get(), signalPrecisions_.get(), noisePrecisions_.get(), memberships_.get(), prior_, dofCorrectionFactor_);
    subjectModel.isInternalModel = true;
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::DGMM::getGroupMaps() const
{
    // Rescale memberships (useful if prior is relatively strong)
    arma::fmat memberships = memberships_->getExpectations().p[1];
    memberships -= memberships.min();
    memberships /= memberships.max();
    
    // And combine with means
    arma::fmat means = signalMeans_->getExpectations().X;
    
    return means % memberships;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::DGMM::updateModel()
{
    Utilities::Timer timer;
    
    signalMeans_->update();
    spdlog::trace("Group map signal means: {:s}", timer.getTimeElapsed());
    timer.reset();
    
    signalPrecisions_->update();
    spdlog::trace("Group map signal precisions: {:s}", timer.getTimeElapsed());
    timer.reset();
    
    noisePrecisions_->update();
    spdlog::trace("Group map noise precisions: {:s}", timer.getTimeElapsed());
    timer.reset();
    
    memberships_->update();
    spdlog::trace("Group map memberships: {:s}", timer.getTimeElapsed());
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::DGMM::saveModel(const std::string directory) const
{
    // Voxelwise means
    const std::string meansDir = directory + "SignalMeans.post" + "/";
    mkdir( meansDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    signalMeans_->save(meansDir);
    
    // Voxelwise precisions
    const std::string precisionsDir = directory + "SignalPrecisions.post" + "/";
    mkdir( precisionsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    signalPrecisions_->save(precisionsDir);
    
    // Noise
    const std::string noiseDir = directory + "NoisePrecisions.post" + "/";
    mkdir( noiseDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    noisePrecisions_->save(noiseDir);
    
    // Voxelwise memberships
    const std::string membershipsDir = directory + "Memberships.post" + "/";
    mkdir( membershipsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    memberships_->save(membershipsDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupSpatialModels::DGMM::getModelKL() const
{
    double KL = 0.0;
    
    // Have to correct for D.O.F.!
    KL += dofCorrectionFactor_ * signalMeans_->getKL();
    KL += dofCorrectionFactor_ * signalPrecisions_->getKL();
    KL += dofCorrectionFactor_ * noisePrecisions_->getKL();
    KL += dofCorrectionFactor_ * memberships_->getKL();
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
