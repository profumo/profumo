// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GroupMaps.h"

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::GroupMaps(std::shared_ptr<SubjectModelling::P_Post> groupMaps)
: groupMaps_(groupMaps)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::SpatialModel PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::getSubjectModel(const SubjectInformation)
{
    SubjectModelling::Subject::SpatialModel subjectModel;
    subjectModel.model = groupMaps_;
    subjectModel.isInternalModel = false;
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::getGroupMaps() const
{
    return groupMaps_->getExpectations().P;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::updateModel()
{
    groupMaps_->update();
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::saveModel(const std::string directory) const
{
    groupMaps_->save(directory);
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupSpatialModels::GroupMaps::getModelKL() const
{
    return groupMaps_->getKL();
}

////////////////////////////////////////////////////////////////////////////////
