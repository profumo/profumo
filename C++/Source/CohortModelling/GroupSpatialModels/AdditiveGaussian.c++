// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "AdditiveGaussian.h"

#include <sys/stat.h>

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::AdditiveGaussian(std::shared_ptr<Modules::MatrixMean_VBPosterior> groupMeans, const MFModels::P::AdditiveGaussian::Parameters prior, const float dofCorrectionFactor)
: groupMeans_(groupMeans), prior_(prior), dofCorrectionFactor_(dofCorrectionFactor)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::SpatialModel PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::getSubjectModel(const SubjectInformation)
{
    SubjectModelling::Subject::SpatialModel subjectModel;
    subjectModel.model = std::make_shared<MFModels::P::AdditiveGaussian>(groupMeans_.get(), prior_, dofCorrectionFactor_);
    subjectModel.isInternalModel = true;
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::getGroupMaps() const
{
    return groupMeans_->getExpectations().X;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::updateModel()
{
    groupMeans_->update();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::saveModel(const std::string directory) const
{
    // Voxelwise means
    const std::string meansDir = directory + "Means.post" + "/";
    mkdir( meansDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    groupMeans_->save(meansDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupSpatialModels::AdditiveGaussian::getModelKL() const
{
    // Have to correct for D.O.F.!
    return dofCorrectionFactor_ * groupMeans_->getKL();
}

////////////////////////////////////////////////////////////////////////////////
