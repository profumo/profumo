// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a group-level time course model, where there is a set of 'clean' 
// HRF-based time courses corrupted by additive noise.

#ifndef COHORT_MODELLING_GROUP_TEMPORAL_MODELS_NOISY_HRF_H
#define COHORT_MODELLING_GROUP_TEMPORAL_MODELS_NOISY_HRF_H

#include <memory>
#include <string>
#include <map>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "MFModels/A/AdditiveMultivariateNormal.h"
#include "MFModels/A/KroneckerHRF.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupTemporalModels
        {
            
            class NoisyHRF :
                public GroupTemporalModel
            {
            public:
                // Constructor
                NoisyHRF(std::shared_ptr<GroupTemporalPrecisionModel> temporalPrecisionModel, const unsigned int M, const float TR, const std::string hrfFile, const VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters noisePrior, const float priorRelaxation=0.0f, const float posteriorRelaxation=0.0f);
                
                virtual SubjectModelling::Subject::TemporalModel getSubjectModel(const SubjectInformation subjectInformation);
                
            private:
                virtual void updateTimeCourseModel();
                
                virtual void saveTimeCourseModel(const std::string directory) const;
                
                virtual double getTimeCourseModelKL() const;
                
                
                // Parameters, hyperpriors etc
                const unsigned int M_;
                const float TR_, priorRelaxation_, posteriorRelaxation_;
                const std::string hrfFile_;
                std::map<unsigned int, MFModels::A::KroneckerHRF::Parameters> hrfPriors_;
                const VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters noisePrior_;
            };
            
        }
    }
}
#endif
