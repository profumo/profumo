// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "NoisyHRF.h"

#include <armadillo>

#include "Utilities/HRFModelling.h"

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::NoisyHRF(std::shared_ptr<GroupTemporalPrecisionModel> temporalPrecisionModel, const unsigned int M, const float TR, const std::string hrfFile, const VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters noisePrior, const float priorRelaxation, const float posteriorRelaxation)
: GroupTemporalModel(temporalPrecisionModel), M_(M), TR_(TR), priorRelaxation_(priorRelaxation), posteriorRelaxation_(posteriorRelaxation), hrfFile_(hrfFile), noisePrior_(noisePrior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::getSubjectModel(const SubjectInformation subjectInformation)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel subjectModel;
    // Get the precision matrix model
    subjectModel.precisions = precisionModel_->getSubjectModel(subjectInformation);
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        const RunID runID = runInformation.runID;
        const unsigned int T = runInformation.T;
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::A_Post> runModel;
        // Check to see if we have made an HRF prior before
        if (hrfPriors_.count(T) == 0) {
            hrfPriors_.emplace(T, Utilities::generateHRFPrior(hrfFile_, T, TR_, M_, priorRelaxation_, posteriorRelaxation_));
        }
        // Make the clean HRF-based TCs
        Modules::PrecisionMatrix_Parent* cleanAlpha = subjectModel.precisions.runLevel.models.at(runID).model.get();
        std::unique_ptr<MFModels::A_VBPosterior> cleanA = std::make_unique<MFModels::A::KroneckerHRF>(cleanAlpha, hrfPriors_.at(T));
        // Noise precision
        std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> noiseAlpha = std::make_unique<VBModules::PrecisionMatrices::ComponentwiseGammaPrecision>(noisePrior_);
        // And put together
        MFModels::A::AdditiveMultivariateNormal::Parameters APrior; APrior.M = M_; APrior.T = T;
        runModel.model = std::make_shared<MFModels::A::AdditiveMultivariateNormal>(std::move(cleanA), std::move(noiseAlpha), APrior);
        runModel.isInternalModel = true;
        // And record
        subjectModel.timeCourses.models.emplace(runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::updateTimeCourseModel()
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::saveTimeCourseModel(const std::string directory) const
{
    // Save HRF
    for (const auto& hrfPrior : hrfPriors_) {
        const unsigned int T = hrfPrior.first;
        hrfPrior.second.hrf->save(directory + "HRF_T" + std::to_string(T) + ".hdf5", arma::hdf5_binary);
        hrfPrior.second.K->save(directory + "HRFCovariance_T" + std::to_string(T) + ".hdf5", arma::hdf5_binary);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupTemporalModels::NoisyHRF::getTimeCourseModelKL() const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
