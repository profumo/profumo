// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "MultivariateNormal.h"

#include <armadillo>

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::MultivariateNormal(std::shared_ptr<GroupTemporalPrecisionModel> temporalPrecisionModel, const unsigned int M)
: GroupTemporalModel(temporalPrecisionModel), M_(M)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::getSubjectModel(const SubjectInformation subjectInformation)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel subjectModel;
    // Get the precision matrix model
    subjectModel.precisions = precisionModel_->getSubjectModel(subjectInformation);
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        const RunID runID = runInformation.runID;
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::A_Post> runModel;
        MFModels::A::MultivariateNormal::Parameters prior;
        prior.M = M_; prior.T = runInformation.T;
        auto runPrecision = subjectModel.precisions.runLevel.models.at(runID).model.get();
        runModel.model = std::make_shared<MFModels::A::MultivariateNormal>(runPrecision, prior);
        runModel.isInternalModel = true;
        // And record
        subjectModel.timeCourses.models.emplace(runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::updateTimeCourseModel()
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::saveTimeCourseModel(const std::string) const
{;
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupTemporalModels::MultivariateNormal::getTimeCourseModelKL() const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
