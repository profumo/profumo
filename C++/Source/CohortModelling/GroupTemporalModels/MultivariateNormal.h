// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a group-level time course model based on an MVN.

#ifndef COHORT_MODELLING_GROUP_TEMPORAL_MODELS_MULTIVARIATE_NORMAL_H
#define COHORT_MODELLING_GROUP_TEMPORAL_MODELS_MULTIVARIATE_NORMAL_H

#include <memory>
#include <string>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "MFModels/A/MultivariateNormal.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupTemporalModels
        {
            
            class MultivariateNormal :
                public GroupTemporalModel
            {
            public:
                // Constructor
                MultivariateNormal(std::shared_ptr<GroupTemporalPrecisionModel> temporalPrecisionModel, const unsigned int M);
                
                virtual SubjectModelling::Subject::TemporalModel getSubjectModel(const SubjectInformation subjectInformation);
                
            private:
                virtual void updateTimeCourseModel();
                
                virtual void saveTimeCourseModel(const std::string directory) const;
                
                virtual double getTimeCourseModelKL() const;
                
                // Size
                const unsigned int M_;
            };
            
        }
    }
}
#endif
