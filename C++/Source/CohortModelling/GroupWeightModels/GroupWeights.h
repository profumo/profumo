// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a generic class that contains a group level set of weights that 
// are consistent across a set of subjects.

#ifndef COHORT_MODELLING_GROUP_WEIGHT_MODELS_GROUP_WEIGHTS_H
#define COHORT_MODELLING_GROUP_WEIGHT_MODELS_GROUP_WEIGHTS_H

#include <memory>
#include <string>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupWeightModels
        {
            
            class GroupWeights :
                public GroupWeightModel
            {
            public:
                GroupWeights(std::shared_ptr<SubjectModelling::H_Post> groupWeights);
                
                virtual SubjectModelling::Subject::WeightModel getSubjectModel(const SubjectInformation subjectInformation);
                
            private:
                std::shared_ptr<SubjectModelling::H_Post> groupWeights_;
                
                virtual void updateModel();
                
                virtual void saveModel(const std::string directory) const;
                
                virtual double getModelKL() const;
            };
            
        }
    }
}
#endif
