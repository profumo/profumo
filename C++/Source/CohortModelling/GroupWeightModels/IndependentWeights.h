// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2017
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a generic class that makes a set of weights with a consistent 
// correlation structure across a set of subjects.

#ifndef COHORT_MODELLING_GROUP_WEIGHT_MODELS_INDEPENDENT_WEIGHTS_H
#define COHORT_MODELLING_GROUP_WEIGHT_MODELS_INDEPENDENT_WEIGHTS_H

#include <memory>
#include <string>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "ModuleList.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupWeightModels
        {
            
            template<class Implementation>
            class IndependentWeights :
                public GroupWeightModel
            {
            public:
                IndependentWeights(const typename Implementation::Parameters prior);
                
                virtual SubjectModelling::Subject::WeightModel getSubjectModel(const SubjectInformation subjectInformation);
                
            private:
                // Prior
                const typename Implementation::Parameters prior_;
                
                virtual void updateModel();
                
                virtual void saveModel(const std::string directory) const;
                
                virtual double getModelKL() const;
            };
            
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::IndependentWeights(const typename Implementation::Parameters prior)
: prior_(prior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::SubjectModelling::Subject::WeightModel PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::getSubjectModel(const SubjectInformation subjectInformation)
{
    // Make the subject model
    SubjectModelling::Subject::WeightModel subjectModel;
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::H_Post> runModel;
        runModel.model = std::make_shared<Implementation>(prior_);
        runModel.isInternalModel = true;
        // And record
        subjectModel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::updateModel()
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::saveModel(const std::string /*directory*/) const
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
double PROFUMO::CohortModelling::GroupWeightModels::IndependentWeights<Implementation>::getModelKL() const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
#endif
