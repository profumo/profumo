// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a generic class that makes a set of weights with a consistent 
// set of means and correlation structure across a set of subjects.

#ifndef COHORT_MODELLING_GROUP_WEIGHT_MODELS_PARAMETERISED_WEIGHTS_H
#define COHORT_MODELLING_GROUP_WEIGHT_MODELS_PARAMETERISED_WEIGHTS_H

#include <memory>
#include <string>
#include <sys/stat.h>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "ModuleList.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupWeightModels
        {
            
            template<class Implementation>
            class ParameterisedWeights :
                public GroupWeightModel
            {
            public:
                ParameterisedWeights(std::shared_ptr<Modules::MatrixMean_VBPosterior> means, std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> precisionMatrix, const typename Implementation::Parameters prior);
                
                virtual SubjectModelling::Subject::WeightModel getSubjectModel(const SubjectInformation subjectInformation);
                
            private:
                // Group means
                std::shared_ptr<Modules::MatrixMean_VBPosterior> means_;
                // Group precision matrix - captures the between weight correlations
                std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> precisionMatrix_;
                
                // Prior
                const typename Implementation::Parameters prior_;
                
                virtual void updateModel();
                
                virtual void saveModel(const std::string directory) const;
                
                virtual double getModelKL() const;
            };
            
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::ParameterisedWeights(std::shared_ptr<Modules::MatrixMean_VBPosterior> means, std::shared_ptr<Modules::PrecisionMatrix_VBPosterior> precisionMatrix, const typename Implementation::Parameters prior)
: means_(means), precisionMatrix_(precisionMatrix), prior_(prior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
PROFUMO::SubjectModelling::Subject::WeightModel PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::getSubjectModel(const SubjectInformation subjectInformation)
{
    // Make the subject model
    SubjectModelling::Subject::WeightModel subjectModel;
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::H_Post> runModel;
        runModel.model = std::make_shared<Implementation>(means_.get(), precisionMatrix_.get(), prior_);
        runModel.isInternalModel = true;
        // And record
        subjectModel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::updateModel()
{
    means_->update();
    precisionMatrix_->update();
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
void PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::saveModel(const std::string directory) const
{
    const std::string meansDir = directory + "Means.post" + "/";
    mkdir( meansDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    means_->save(meansDir);
    
    const std::string precMatDir = directory + "PrecisionMatrix.post" + "/";
    mkdir( precMatDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    precisionMatrix_->save(precMatDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class Implementation>
double PROFUMO::CohortModelling::GroupWeightModels::ParameterisedWeights<Implementation>::getModelKL() const
{
    return means_->getKL() + precisionMatrix_->getKL();
}

////////////////////////////////////////////////////////////////////////////////
#endif
