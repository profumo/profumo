// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GroupWeights.h"

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::GroupWeights(std::shared_ptr<SubjectModelling::H_Post> groupWeights)
: groupWeights_(groupWeights)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::WeightModel PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::getSubjectModel(const SubjectInformation subjectInformation)
{
    // Make the subject model
    SubjectModelling::Subject::WeightModel subjectModel;
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::H_Post> runModel;
        runModel.model = groupWeights_;
        runModel.isInternalModel = false;
        // And record
        subjectModel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::updateModel()
{
    groupWeights_->update();
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::saveModel(const std::string directory) const
{
    groupWeights_->save(directory);
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupWeightModels::GroupWeights::getModelKL() const
{
    return groupWeights_->getKL();
}

////////////////////////////////////////////////////////////////////////////////
