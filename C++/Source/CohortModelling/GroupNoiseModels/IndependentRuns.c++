// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "IndependentRuns.h"

#include <memory>

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::IndependentRuns(const MFModels::Psi::GammaPrecision::Parameters prior)
: prior_(prior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::NoiseModel PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::getSubjectModel(const SubjectInformation subjectInformation)
{
    // Make the subject model
    SubjectModelling::Subject::NoiseModel subjectModel;
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Psi_Post> runModel;
        runModel.model = std::make_shared<MFModels::Psi::GammaPrecision>(prior_);
        runModel.isInternalModel = true;
        // And record
        subjectModel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::updateModel()
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::saveModel(const std::string) const
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupNoiseModels::IndependentRuns::getModelKL() const
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
