// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Generates noise precisions, independent across runs.

#ifndef COHORT_MODELLING_GROUP_NOISE_MODELS_INDEPENDENT_RUNS_H
#define COHORT_MODELLING_GROUP_NOISE_MODELS_INDEPENDENT_RUNS_H

#include <string>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "MFModels/Psi/GammaPrecision.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupNoiseModels
        {
            
            class IndependentRuns :
                public GroupNoiseModel
            {
            public:
                IndependentRuns(const MFModels::Psi::GammaPrecision::Parameters prior);
                
                virtual SubjectModelling::Subject::NoiseModel getSubjectModel(const SubjectInformation subjectInformation);
                
            private:
                const MFModels::Psi::GammaPrecision::Parameters prior_;
                
                virtual void updateModel();
                
                virtual void saveModel(const std::string directory) const;
                
                virtual double getModelKL() const;
            };
            
        }
    }
}
#endif
