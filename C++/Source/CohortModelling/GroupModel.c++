// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GroupModel.h"

#include <sys/stat.h>

////////////////////////////////////////////////////////////////////////////////
// GroupTemporalModel
////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalModel::GroupTemporalModel(std::shared_ptr<GroupTemporalPrecisionModel> precisionModel)
: precisionModel_(precisionModel)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalModel::getGroupPrecisionMatrix() const
{
    return precisionModel_->getGroupPrecisionMatrix();
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModel::updateModel()
{
    // Time courses
    updateTimeCourseModel();
    // Then precision
    precisionModel_->update();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalModel::saveModel(const std::string directory) const
{
    // Time courses
    saveTimeCourseModel(directory);
    
    // Then precision
    // Make directory
    const std::string precisionDir = directory + "PrecisionMatrixModel" + "/";
    mkdir( precisionDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    // Save
    precisionModel_->save(precisionDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupTemporalModel::getModelKL() const
{
    double KL = 0.0;
    
    // Time courses
    KL += getTimeCourseModelKL();
    // Then precision
    KL += precisionModel_->getKL();
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
