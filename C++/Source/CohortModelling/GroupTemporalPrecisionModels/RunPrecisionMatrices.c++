// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "RunPrecisionMatrices.h"

#include "Utilities/DataIO.h"
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::RunPrecisionMatrices(std::shared_ptr<SubjectModelling::Alpha_Post> groupPrecision, const VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior)
: groupPrecision_(groupPrecision), runPrior_(runPrior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel::Precisions PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::getSubjectModel(const SubjectInformation subjectInformation)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel::Precisions subjectModel;
    // No subject level precision matrix
    subjectModel.subjectLevel.model = nullptr;
    subjectModel.subjectLevel.isInternalModel = false;
    // Make the independent run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Alpha_Post> runModel;
        runModel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(groupPrecision_.get(), runPrior_);
        runModel.isInternalModel = true;
        // And record
        subjectModel.runLevel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::getGroupPrecisionMatrix() const
{
    // Transform hyperprior on rate matrix to precision matrix
    const arma::fmat X = groupPrecision_->getExpectations().X;
    return runPrior_.a * linalg::inv_sympd(X);
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::updateModel()
{
    groupPrecision_->update();
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::saveModel(const std::string directory) const
{
    // Group posterior
    groupPrecision_->save(directory);
    
    // And save prior (needed to calculate run expectations)
    const std::string fileName = directory + "PriorDegreesOfFreedom.txt";
    Utilities::save(fileName, runPrior_.a);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupTemporalPrecisionModels::RunPrecisionMatrices::getModelKL() const
{
    return groupPrecision_->getKL();
}

////////////////////////////////////////////////////////////////////////////////
