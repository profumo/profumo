// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "CategorisedRunPrecisionMatrices.h"

#include <sys/stat.h>

#include "Utilities/DataIO.h"
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::CategorisedRunPrecisionMatrices(const VBModules::PrecisionMatrices::Wishart::Parameters groupPrior, const VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior)
: groupPrior_(groupPrior), runPrior_(runPrior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel::Precisions PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::getSubjectModel(const SubjectInformation subjectInformation)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel::Precisions subjectModel;
    // No subject level precision matrix
    subjectModel.subjectLevel.model = nullptr;
    subjectModel.subjectLevel.isInternalModel = false;
    // Either make, or assign from pool of, run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        const RunID runID = runInformation.runID;
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Alpha_Post> runModel;
        // See if we have this in the pool already, and if not make it
        if (groupPrecisions_.count(runID) == 0) {
            groupPrecisions_.emplace(runID, std::make_shared<VBModules::PrecisionMatrices::Wishart>(groupPrior_));
        }
        // Make the run level model
        runModel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(groupPrecisions_.at(runID).get(), runPrior_);
        runModel.isInternalModel = true;
        // And record
        subjectModel.runLevel.models.emplace(runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::getGroupPrecisionMatrix() const
{
    // Use the mean precision matrix
    // Initialise with precision matrix from first category
    auto gPM = groupPrecisions_.cbegin();
    arma::fmat X = gPM->second->getExpectations().X;
    // Add other models if necessary
    for (++gPM; gPM != groupPrecisions_.cend(); ++gPM) {
        X += gPM->second->getExpectations().X;
    }
    // And normalise
    X /= groupPrecisions_.size();
    
    // Transform hyperprior on rate matrix to precision matrix
    return runPrior_.a * linalg::inv_sympd(X);
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::updateModel()
{
    for (auto& groupPrecision : groupPrecisions_) {
        groupPrecision.second->update();
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::saveModel(const std::string directory) const
{
    // Group posteriors
    for (const auto& groupPrecision : groupPrecisions_) {
        RunID runID = groupPrecision.first;
        const std::string runDir = directory + runID + "/";
        mkdir( runDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        groupPrecision.second->save(runDir);
    }
    
    // And save prior (needed to calculate run expectations)
    const std::string fileName = directory + "PriorDegreesOfFreedom.txt";
    Utilities::save(fileName, runPrior_.a);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupTemporalPrecisionModels::CategorisedRunPrecisionMatrices::getModelKL() const
{
    double KL = 0.0;
    
    for (const auto& groupPrecision : groupPrecisions_) {
        KL += groupPrecision.second->getKL();
    }
    
    return KL;
}

////////////////////////////////////////////////////////////////////////////////
