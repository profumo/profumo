// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a model where there is a single group-level precision matrix. 

#ifndef COHORT_MODELLING_GROUP_TEMPORAL_PRECISION_MODELS_GROUP_PRECISION_MATRIX_H
#define COHORT_MODELLING_GROUP_TEMPORAL_PRECISION_MODELS_GROUP_PRECISION_MATRIX_H

#include <memory>
#include <string>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupTemporalPrecisionModels
        {
            
            class GroupPrecisionMatrix :
                public GroupTemporalPrecisionModel
            {
            public:
                GroupPrecisionMatrix(std::shared_ptr<SubjectModelling::Alpha_Post> groupPrecision);
                
                virtual SubjectModelling::Subject::TemporalModel::Precisions getSubjectModel(const SubjectInformation subjectInformation);
                
                virtual arma::fmat getGroupPrecisionMatrix() const;
                
            private:
                std::shared_ptr<SubjectModelling::Alpha_Post> groupPrecision_;
                
                virtual void updateModel();
                
                virtual void saveModel(const std::string directory) const;
                
                virtual double getModelKL() const;
            };
            
        }
    }
}
#endif
