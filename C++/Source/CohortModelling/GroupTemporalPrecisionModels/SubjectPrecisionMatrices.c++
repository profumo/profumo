// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "SubjectPrecisionMatrices.h"

#include "Utilities/DataIO.h"
#include "Utilities/LinearAlgebra.h"
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::SubjectPrecisionMatrices(std::shared_ptr<SubjectModelling::Alpha_Post> groupPrecision, const VBModules::PrecisionMatrices::HierarchicalWishart::Parameters subjectPrior)
: groupPrecision_(groupPrecision), subjectPrior_(subjectPrior)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel::Precisions PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::getSubjectModel(const SubjectInformation subjectInformation)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel::Precisions subjectModel;
    // Make subject level precision matrix
    subjectModel.subjectLevel.model = std::make_shared<VBModules::PrecisionMatrices::HierarchicalWishart>(groupPrecision_.get(), subjectPrior_);
    subjectModel.subjectLevel.isInternalModel = true;
    // And just copy into run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Alpha_Post> runModel;
        runModel.model = subjectModel.subjectLevel.model;
        runModel.isInternalModel = false;
        // And record
        subjectModel.runLevel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::getGroupPrecisionMatrix() const
{
    // Transform hyperprior on rate matrix to precision matrix
    const arma::fmat X = groupPrecision_->getExpectations().X;
    return subjectPrior_.a * linalg::inv_sympd(X);
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::updateModel()
{
    groupPrecision_->update();
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::saveModel(const std::string directory) const
{
    // Group posterior
    groupPrecision_->save(directory);
    
    // And save prior (needed to calculate subject expectations)
    const std::string fileName = directory + "PriorDegreesOfFreedom.txt";
    Utilities::save(fileName, subjectPrior_.a);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupTemporalPrecisionModels::SubjectPrecisionMatrices::getModelKL() const
{
    return groupPrecision_->getKL();
}

////////////////////////////////////////////////////////////////////////////////
