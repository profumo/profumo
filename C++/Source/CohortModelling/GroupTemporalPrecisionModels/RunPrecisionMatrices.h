// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a model where there is a precision matrix created for every run.

#ifndef COHORT_MODELLING_GROUP_TEMPORAL_PRECISION_MODELS_RUN_PRECISION_MATRICES_H
#define COHORT_MODELLING_GROUP_TEMPORAL_PRECISION_MODELS_RUN_PRECISION_MATRICES_H

#include <memory>
#include <string>

#include "SubjectModelling/Subject.h"
#include "CohortModelling/GroupModel.h"

#include "VBModules/PrecisionMatrices/HierarchicalWishart.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        namespace GroupTemporalPrecisionModels
        {
            
            class RunPrecisionMatrices :
                public GroupTemporalPrecisionModel
            {
            public:
                RunPrecisionMatrices(std::shared_ptr<SubjectModelling::Alpha_Post> groupPrecision, const VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior);
                
                virtual SubjectModelling::Subject::TemporalModel::Precisions getSubjectModel(const SubjectInformation subjectInformation);
                
                virtual arma::fmat getGroupPrecisionMatrix() const;
                
            private:
                std::shared_ptr<SubjectModelling::Alpha_Post> groupPrecision_;
                const VBModules::PrecisionMatrices::HierarchicalWishart::Parameters runPrior_;
                
                virtual void updateModel();
                
                virtual void saveModel(const std::string directory) const;
                
                virtual double getModelKL() const;
            };
            
        }
    }
}
#endif
