// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "GroupPrecisionMatrix.h"

////////////////////////////////////////////////////////////////////////////////

PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::GroupPrecisionMatrix(std::shared_ptr<SubjectModelling::Alpha_Post> groupPrecision)
: groupPrecision_(groupPrecision)
{
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::SubjectModelling::Subject::TemporalModel::Precisions PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::getSubjectModel(const SubjectInformation subjectInformation)
{
    // Make the subject model
    SubjectModelling::Subject::TemporalModel::Precisions subjectModel;
    // Nothing at the subject level
    subjectModel.subjectLevel.model = nullptr;
    subjectModel.subjectLevel.isInternalModel = false;
    // Populate with run models
    for (const RunInformation& runInformation : subjectInformation.runs) {
        // Make a Run model
        SubjectModelling::Run::Model<SubjectModelling::Alpha_Post> runModel;
        runModel.model = groupPrecision_;
        runModel.isInternalModel = false;
        // And record
        subjectModel.runLevel.models.emplace(runInformation.runID, runModel);
    }
    
    return subjectModel;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::getGroupPrecisionMatrix() const
{
    return groupPrecision_->getExpectations().X;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::updateModel()
{
    groupPrecision_->update();
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::saveModel(const std::string directory) const
{
    groupPrecision_->save(directory);
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::GroupTemporalPrecisionModels::GroupPrecisionMatrix::getModelKL() const
{
    return groupPrecision_->getKL();
}

////////////////////////////////////////////////////////////////////////////////
