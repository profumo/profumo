// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a Cohort: a class that contains the group level parameters and a set 
// of subjects.

#ifndef COHORT_MODELLING_COHORT_H
#define COHORT_MODELLING_COHORT_H

#include <memory>
#include <vector>
#include <string>
#include <armadillo>

#include <spdlog/spdlog.h>

#include "SubjectModelling/Run.h"
#include "SubjectModelling/Subject.h"

#include "CohortModelling/GroupModel.h"

#include "Utilities/Timer.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
            
        class Cohort
        {
        public:
            // Constructor templated by data type
            template<class D>
            Cohort(const std::vector<SubjectInformation>   subjectInformation,
                   std::map<SubjectID, std::map<RunID, D>> subjectData,
                   std::shared_ptr<GroupSpatialModel>  groupSpatialModel, 
                   std::shared_ptr<GroupWeightModel>   groupWeightModel, 
                   std::shared_ptr<GroupTemporalModel> groupTemporalModel, 
                   std::shared_ptr<GroupNoiseModel>    groupNoiseModel,
                   const float dofCorrectionFactor=1.0f);
            
            // Update the models!
            void update();
            void updateSpatialModel();
            void updateWeightModel();
            void updateTemporalModel();
            void updateNoiseModel();
            
            // Functions to dynamically switch models
            void setSpatialModel(std::shared_ptr<GroupSpatialModel>   newGroupSpatialModel);
            void setWeightModel(std::shared_ptr<GroupWeightModel>     newGroupWeightModel);
            void setTemporalModel(std::shared_ptr<GroupTemporalModel> newGroupTemporalModel);
            void setNoiseModel(std::shared_ptr<GroupNoiseModel>       newGroupNoiseModel);
            
            // Save everything to file
            void save(const std::string directory) const;
            
            // Get F
            double getFreeEnergy() const;
            
            // Get current group parameters
            arma::fmat getGroupMaps() const;
            arma::fmat getGroupPrecisionMatrix() const;
            
            // Whether to normalise time course variance
            void setTimeCourseNormalisation(const bool normalisation);
            
        private:
            // List of subjects
            std::vector<std::unique_ptr<SubjectModelling::Subject>> subjects_;
            const unsigned int S_;
            
            // Store for group models
            std::shared_ptr<GroupSpatialModel>  groupSpatialModel_;
            std::shared_ptr<GroupWeightModel>   groupWeightModel_;
            std::shared_ptr<GroupTemporalModel> groupTemporalModel_;
            std::shared_ptr<GroupNoiseModel>    groupNoiseModel_;
        };
        
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

// Constructor templated by data type
template<class D>
PROFUMO::CohortModelling::Cohort::Cohort(
                   const std::vector<SubjectInformation>   subjectInformation,
                   std::map<SubjectID, std::map<RunID, D>> subjectData,
                   std::shared_ptr<GroupSpatialModel>  groupSpatialModel, 
                   std::shared_ptr<GroupWeightModel>   groupWeightModel, 
                   std::shared_ptr<GroupTemporalModel> groupTemporalModel, 
                   std::shared_ptr<GroupNoiseModel>    groupNoiseModel,
                   const float dofCorrectionFactor)
: S_(subjectInformation.size()), groupSpatialModel_(groupSpatialModel), groupWeightModel_(groupWeightModel), groupTemporalModel_(groupTemporalModel), groupNoiseModel_(groupNoiseModel)
{
    Utilities::Timer timer;
    spdlog::debug("Constructing cohort...");
        
    // Make the subjects!
    for (const SubjectInformation subject : subjectInformation) {
        
        // Make the models
        auto sSM = groupSpatialModel_->getSubjectModel(subject);
        auto sWM = groupWeightModel_->getSubjectModel(subject);
        auto sTM = groupTemporalModel_->getSubjectModel(subject);
        auto sNM = groupNoiseModel_->getSubjectModel(subject);
        
        // And then the subject
        subjects_.push_back(
                std::make_unique<SubjectModelling::Subject>(
                    subject, subjectData.at(subject.subjectID),
                    sSM, sWM, sTM, sNM, dofCorrectionFactor)
                );
    }
    
    spdlog::debug("Cohort constructed. Time elapsed: {:s}", timer.getTimeElapsed());
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
#endif
