// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a generic class that contains the group level parameters for a set 
// of subjects.

#ifndef COHORT_MODELLING_GROUP_MODEL_H
#define COHORT_MODELLING_GROUP_MODEL_H

#include <memory>
#include <string>
#include <armadillo>

#include "SubjectModelling/Subject.h"

namespace PROFUMO
{
    namespace CohortModelling
    {
        ////////////////////////////////////////////////////////////////////////
        
        template<class SubjectModel>
        class GroupModel
        {
        public:
            virtual SubjectModel getSubjectModel(const SubjectInformation subjectInformation) = 0;
            
            void update();
            
            void save(const std::string directory) const;
            
            double getKL() const;
            
            virtual ~GroupModel() = default;
            
        private:
            virtual void updateModel() = 0;
            
            virtual void saveModel(const std::string directory) const = 0;
            
            virtual double getModelKL() const = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Spatial model
        class GroupSpatialModel :
            public GroupModel<SubjectModelling::Subject::SpatialModel>
        {
        public:
            virtual arma::fmat getGroupMaps() const = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Weight model
        typedef GroupModel<SubjectModelling::Subject::WeightModel> GroupWeightModel;
        
        ////////////////////////////////////////////////////////////////////////
        
        // Precision matrix model
        class GroupTemporalPrecisionModel:
            public GroupModel<SubjectModelling::Subject::TemporalModel::Precisions>
        {
        public:
            virtual arma::fmat getGroupPrecisionMatrix() const = 0;
        };
        
        
        // Full temporal model
        class GroupTemporalModel :
            public GroupModel<SubjectModelling::Subject::TemporalModel>
        {
        public:
            GroupTemporalModel(std::shared_ptr<GroupTemporalPrecisionModel> precisionModel);
            
            arma::fmat getGroupPrecisionMatrix() const;
            
        protected:
            std::shared_ptr<GroupTemporalPrecisionModel> precisionModel_;
            
        private:
            // Override group functions to deal with precision
            void updateModel() final;
            
            void saveModel(const std::string directory) const final;
            
            double getModelKL() const final;
            
            // And declare functions that let time course model play
            virtual void updateTimeCourseModel() = 0;
            
            virtual void saveTimeCourseModel(const std::string directory) const = 0;
            
            virtual double getTimeCourseModelKL() const = 0;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Noise model
        typedef GroupModel<SubjectModelling::Subject::NoiseModel> GroupNoiseModel;
        
        ////////////////////////////////////////////////////////////////////////
    }
}

////////////////////////////////////////////////////////////////////////////////
//                    Implementation of template functions                    //
////////////////////////////////////////////////////////////////////////////////

template<class SubjectModel>
void PROFUMO::CohortModelling::GroupModel<SubjectModel>::update()
{
    // Logging?
    updateModel();
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class SubjectModel>
void PROFUMO::CohortModelling::GroupModel<SubjectModel>::save(const std::string directory) const
{
    // Logging?
    saveModel(directory);
    return;
}

////////////////////////////////////////////////////////////////////////////////

template<class SubjectModel>
double PROFUMO::CohortModelling::GroupModel<SubjectModel>::getKL() const
{
    return getModelKL();
}

////////////////////////////////////////////////////////////////////////////////
#endif
