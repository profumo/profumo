// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Cohort.h"

#include <sys/stat.h>

////////////////////////////////////////////////////////////////////////////////
// Updates

void PROFUMO::CohortModelling::Cohort::update()
{
    updateSpatialModel();
    updateWeightModel();
    updateTemporalModel();
    updateNoiseModel();
    return;
}

void PROFUMO::CohortModelling::Cohort::updateSpatialModel()
{
    Utilities::Timer timer;
    
    // Update subjects
    //for (auto& subject : subjects_) {
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int s = 0; s < S_; ++s) {
        subjects_[s]->updateSpatialModel();
    }
    spdlog::debug("Subject spatial models: {:s}", timer.getTimeElapsed());
    timer.reset();
    
    // Update group model
    groupSpatialModel_->update();
    spdlog::debug("Group spatial model: {:s}", timer.getTimeElapsed());
    
    return;
}

void PROFUMO::CohortModelling::Cohort::updateWeightModel()
{
    Utilities::Timer timer;
    
    // Update subjects
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int s = 0; s < S_; ++s) {
        subjects_[s]->updateWeightModel();
    }
    spdlog::debug("Subject weight models: {:s}", timer.getTimeElapsed());
    timer.reset();
    
    // Update group model
    groupWeightModel_->update();
    spdlog::debug("Group weight model: {:s}", timer.getTimeElapsed());
    
    return;
}

void PROFUMO::CohortModelling::Cohort::updateTemporalModel()
{
    Utilities::Timer timer;
    
    // Update subjects
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int s = 0; s < S_; ++s) {
        subjects_[s]->updateTemporalModel();
    }
    spdlog::debug("Subject temporal models: {:s}", timer.getTimeElapsed());
    timer.reset();
    
    // Update group model
    groupTemporalModel_->update();
    spdlog::debug("Group temporal model: {:s}", timer.getTimeElapsed());
    
    return;
}

void PROFUMO::CohortModelling::Cohort::updateNoiseModel()
{
    Utilities::Timer timer;
    
    // Update subjects
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int s = 0; s < S_; ++s) {
        subjects_[s]->updateNoiseModel();
    }
    spdlog::debug("Subject noise models: {:s}", timer.getTimeElapsed());
    timer.reset();
    
    // Update group model
    groupNoiseModel_->update();
    spdlog::debug("Group noise model: {:s}", timer.getTimeElapsed());
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Model switching

void PROFUMO::CohortModelling::Cohort::setSpatialModel(std::shared_ptr<GroupSpatialModel> newGroupSpatialModel)
{
    // Change the Subjects
    for (auto& subject : subjects_) {
        // Make the models
        auto sSM = newGroupSpatialModel->getSubjectModel( subject->getSubjectInformation() );
        // And then update the subject
        subject->setSpatialModel(sSM);
    }
    
    // Update the internal model
    groupSpatialModel_ = newGroupSpatialModel;
    
    return;
}

void PROFUMO::CohortModelling::Cohort::setWeightModel(std::shared_ptr<GroupWeightModel> newGroupWeightModel)
{
    // Change the Subjects
    for (auto& subject : subjects_) {
        // Make the models
        auto sWM = newGroupWeightModel->getSubjectModel( subject->getSubjectInformation() );
        // And then update the subject
        subject->setWeightModel(sWM);
    }
    
    // Update the internal model
    groupWeightModel_ = newGroupWeightModel;
    
    return;
}

void PROFUMO::CohortModelling::Cohort::setTemporalModel(std::shared_ptr<GroupTemporalModel> newGroupTemporalModel)
{
    // Change the Subjects
    for (auto& subject : subjects_) {
        // Make the models
        auto sTM = newGroupTemporalModel->getSubjectModel( subject->getSubjectInformation() );
        // And then update the subject
        subject->setTemporalModel(sTM);
    }
    
    // Update the internal model
    groupTemporalModel_ = newGroupTemporalModel;
    
    return;
}

void PROFUMO::CohortModelling::Cohort::setNoiseModel(std::shared_ptr<GroupNoiseModel> newGroupNoiseModel)
{
    // Change the Subjects
    for (auto& subject : subjects_) {
        // Make the models
        auto sNM = newGroupNoiseModel->getSubjectModel( subject->getSubjectInformation() );
        // And then update the subject
        subject->setNoiseModel(sNM);
    }
    
    // Update the internal model
    groupNoiseModel_ = newGroupNoiseModel;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Save

void PROFUMO::CohortModelling::Cohort::save(const std::string directory) const
{
    // Save Subjects
    // Make subjects directory
    const std::string subjectsDir = directory + "Subjects" + "/";
    mkdir( subjectsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    // And then save subjects in that
    for (const auto& subject : subjects_) {
        // Make directory
        const std::string subjectDir = subjectsDir + subject->getSubjectID() + "/";
        mkdir( subjectDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
        // Save subject
        subject->save(subjectDir);
    }
    
    // And group models
    const std::string GSMDir = directory + "GroupSpatialModel" + "/";
    mkdir( GSMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    groupSpatialModel_->save(GSMDir);
    
    const std::string GWMDir = directory + "GroupWeightModel" + "/";
    mkdir( GWMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    groupWeightModel_->save(GWMDir);
    
    const std::string GTMDir = directory + "GroupTemporalModel" + "/";
    mkdir( GTMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    groupTemporalModel_->save(GTMDir);
    
    const std::string GNMDir = directory + "GroupNoiseModel" + "/";
    mkdir( GNMDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    groupNoiseModel_->save(GNMDir);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

double PROFUMO::CohortModelling::Cohort::getFreeEnergy() const
{
    double F = 0.0;
    
    // Collect from Subjects
    #pragma omp parallel for schedule(dynamic) reduction(+:F)
    for (unsigned int s = 0; s < S_; ++s) {
        F += subjects_[s]->getFreeEnergy();
    }
    
    // Then adjust with group models
    F -= groupSpatialModel_->getKL();
    F -= groupWeightModel_->getKL();
    F -= groupTemporalModel_->getKL();
    F -= groupNoiseModel_->getKL();
    
    return F;
}

////////////////////////////////////////////////////////////////////////////////

arma::fmat PROFUMO::CohortModelling::Cohort::getGroupMaps() const
{
    return groupSpatialModel_->getGroupMaps();
}

arma::fmat PROFUMO::CohortModelling::Cohort::getGroupPrecisionMatrix() const
{
    return groupTemporalModel_->getGroupPrecisionMatrix();
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::CohortModelling::Cohort::setTimeCourseNormalisation(const bool normalisation)
{
    for (auto& subject : subjects_) {
        subject->setTimeCourseNormalisation(normalisation);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
