// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// A few useful linear algebra routines, mostly based around computing 
// inverses / decompositions safely

#ifndef UTILITIES_LINEAR_ALGEBRA
#define UTILITIES_LINEAR_ALGEBRA

#include <armadillo>
#include <algorithm>
#include <limits>
#include <stdexcept>

namespace PROFUMO
{
    namespace Utilities
    {
        namespace LinearAlgebra
        {
            ////////////////////////////////////////////////////////////////////
            
            const float EPSILON = std::numeric_limits<float>::epsilon();
            
            ////////////////////////////////////////////////////////////////////
            // Numerical stable version of `arma::accu / sum / mean`
            
            template<class fmat_like>
            inline float accu(const fmat_like& X)
            {
                // Summation via matrix multiplication for stability (see CodingConventions.md)
                // arma intermediates may not provide `n_rows / n_cols` directly
                const auto size = arma::size(X);
                
                // Don't use `arma::as_scalar()` to prevent premature optimisation
                return arma::fmat(
                        arma::ones<arma::frowvec>(size.n_rows)
                        * X
                        * arma::ones<arma::fvec>(size.n_cols)
                    ).at(0);
            }
            
            // N.B. Doesn't automatically detect and sum over vectors!
            template<class fmat_like>
            inline arma::fmat sum(const fmat_like& X, const unsigned int dim)
            {
                // Summation via matrix multiplication for stability (see CodingConventions.md)
                // arma intermediates may not provide `n_rows / n_cols` directly
                const auto size = arma::size(X);
                
                if (dim == 0) {
                    return arma::ones<arma::frowvec>(size.n_rows) * X;
                }
                else if (dim == 1) {
                    return X * arma::ones<arma::fvec>(size.n_cols);
                }
                else {
                    throw std::invalid_argument("Utilities::LinearAlgebra::sum(arma::fmat X, uint dim): 'dim' must be 0 or 1.");
                }
            }
            
            template<class fmat_like>
            inline float mean(const fmat_like& X)
            {
                const auto size = arma::size(X);
                
                return LinearAlgebra::accu(X) / (size.n_rows * size.n_cols);
            }
            
            template<class fmat_like>
            inline arma::fmat mean(const fmat_like& X, const unsigned int dim)
            {
                const auto size = arma::size(X);
                
                return LinearAlgebra::sum(X, dim) / size[dim];
            }
            
            ////////////////////////////////////////////////////////////////////
            // Numerical stable trace of `Xt * X` (i.e. sum of squares)
            
            template<class fmat_like>
            inline float trace_XtX(const fmat_like& X)
            {
                // Computationally cheaper than calculating directly, and this
                // implementation falls back on numerically stable BLAS
                // routines (see `CodingConventions.md`).
                // https://en.wikipedia.org/wiki/Trace_(linear_algebra)#Trace_of_a_product
                
                return LinearAlgebra::accu(X % X);  // ADL ambiguity resolution
            }
            
            ////////////////////////////////////////////////////////////////////
            // A robust way of computing the SVD of a matrix
            
            // If a tolerance is supplied, then all singular values less than 
            // tolerance * max(eigenvalues) * max(n_rows, n_cols) are set to that value
            
            static inline void svd_econ(arma::fmat& U, arma::fvec& s, arma::fmat& V, const arma::fmat& X, const float tolerance = EPSILON)
            {
                // Calculate the SVD
                // X = U * arma::diagmat(s) * V.t();
                bool success = 
                    arma::svd_econ(U, s, V, X);
                
                // If this has gone wrong, panic!
                if (success == false) {
                    // Storage for new version of X
                    arma::fmat rX = X; rX.elem( arma::find_nonfinite(rX) ).zeros();
                    float r = 10.0f * std::numeric_limits<float>::epsilon();
                    while (success == false) {
                        // Add some random noise and try again
                        rX += r * arma::abs(rX).max() * arma::randn<arma::fmat>(arma::size(X));
                        r *= 10.0f;
                        success = arma::svd_econ(U, s, V, rX);
                    }
                }
                
                // Check singular values are above tolerance, and reset those that aren't
                const float minSingVal = tolerance * arma::max(s) * std::max(X.n_rows, X.n_cols);
                s( arma::find(s < minSingVal) ).fill( minSingVal );
                
                return;
            }
            
            ////////////////////////////////////////////////////////////////////
            // A robust way of computing the (pseudo)inverse of a matrix using 
            // the SVD
            
            // To make this robust, small singular values are rounded up to
            // tolerance * max(eigenvalues) * max(n_rows, n_cols)
            
            static inline arma::fmat pinv(const arma::fmat& X, const float tolerance = EPSILON)
            {
                // Calculate robust SVD
                arma::fmat U, V; arma::fvec s;
                svd_econ(U, s, V, X, tolerance);
                
                // Invert!
                return V * arma::diagmat(1.0f / s) * U.t();
            }
            
            // Cheeky alias to give inv(X): pinv(X) will give the inverse for 
            // a (non-symmetric) square matrix X
            static inline arma::fmat inv(const arma::fmat& X, const float tolerance = EPSILON)
            {
                return pinv(X, tolerance);
            }
            
            ////////////////////////////////////////////////////////////////////
            
            
            
            ////////////////////////////////////////////////////////////////////
            // A robust way of computing the eigendecomposition of a symmetric 
            // (semi) positive-definite matrix
            
            // If a tolerance is supplied, then all eigenvalues less than 
            // tolerance * max(eigenvalues) * max(n_rows, n_cols) are set to that value
            
            static inline void eig_sympd(arma::fvec& eigenvalues, arma::fmat& eigenvectors, const arma::fmat& X, const float tolerance = EPSILON)
            {
                // Calculate the eigenvalues / eigenvectors
                // X = U * arma::diagmat(l) * U.t();
                bool success = 
                    arma::eig_sym(eigenvalues, eigenvectors, X);
                
                // If this has gone wrong, panic!
                if (success == false) {
                    // Storage for new version of X
                    arma::fmat rX = X; rX.elem( arma::find_nonfinite(rX) ).zeros();
                    float r = 10.0f * std::numeric_limits<float>::epsilon();
                    while (success == false) {
                        // Add some random noise and try again
                        const arma::fmat R = arma::randn<arma::fmat>(arma::size(X));
                        rX += r * arma::abs(rX).max() * (R * R.t());
                        r *= 10.0f;
                        success = arma::eig_sym(eigenvalues, eigenvectors, rX);
                    }
                }
                
                // Check eigenvalues are above tolerance, and reset those that aren't
                const float minEig = tolerance * arma::max(eigenvalues) * std::max(X.n_rows, X.n_cols);
                eigenvalues( arma::find(eigenvalues < minEig) ).fill( minEig );
                
                return;
            }
            
            // Cheeky modification of the above
            // Performs an eigendecomposition after normalising the main diagonal
            // Therefore *NOT* a true eigendecomposition, but can be useful 
            // provided you keep track of the parts properly... ;-p
            static inline void norm_eig_sympd(arma::fvec& eigenvalues, arma::fmat& eigenvectors, arma::fvec& norm, const arma::fmat& X, const float tolerance = EPSILON)
            {
                norm = arma::sqrt(arma::diagvec(X));
                const float minNorm = arma::max(norm) * std::numeric_limits<float>::epsilon();
                norm.elem( arma::find(norm < minNorm) ).fill( minNorm );
                norm.elem( arma::find_nonfinite(norm) ).ones();
                
                eig_sympd(eigenvalues, eigenvectors, arma::diagmat(1.0f / norm) * X * arma::diagmat(1.0f / norm), tolerance);
                
                return;
            }
            
            ////////////////////////////////////////////////////////////////////
            // A robust way of inverting a symmetric positive-definite matrix 
            // using the eigendecomposition
            
            // To make this robust, small eigenvalues are rounded up to
            // tolerance * max(eigenvalues) * max(n_rows, n_cols)
            
            static inline arma::fmat inv_sympd(const arma::fmat& X, const float tolerance = EPSILON)
            {
                // Calculate robust eigendecomposition
                arma::fvec l; arma::fmat U;
                eig_sympd(l, U, X, tolerance);
                
                // Invert!
                return U * arma::diagmat(1.0f / l) * U.t();
            }
            
            ////////////////////////////////////////////////////////////////////
            // A robust way of getting the log determinant of a symmetric 
            // positive-definite matrix using the eigendecomposition
            
            // To make this robust, small eigenvalues are rounded up to
            // tolerance * max(eigenvalues) * max(n_rows, n_cols)
            
            static inline float log_det_sympd(const arma::fmat& X, const float tolerance = EPSILON)
            {
                // Calculate robust eigendecomposition
                arma::fvec l; arma::fmat U;
                eig_sympd(l, U, X, tolerance);
                
                // log(det(X)) = sum(log(eigenvalues))
                return arma::sum(arma::log(l));
            }
            
            ////////////////////////////////////////////////////////////////////
            // Method to find the 'closest' orthogonal matrix to a given
            // matrix. Closest is measured by the Frobenius norm.
            
            // There are two ways to calculate this:
            // 1) Y = X (Xt * X)^-1/2
            // See e.g. Hyvärinen et al., "Independent Component Analysis",
            // John Wiley & Sons, 2001 [pp. 142]
            // 2) U,s,V = svd(X); Y = U * Vt
            // See e.g. Colclough et al., "A symmetric multivariate leakage
            // correction for MEG connectomes", NeuroImage, 2015
            // DOI: 10.1016/j.neuroimage.2015.03.071
            
            static inline arma::fmat closest_orth(
                    const arma::fmat& X,
                    const float tolerance = EPSILON)
            {
                // Calculate robust SVD of X
                arma::fmat U, V; arma::fvec s;
                svd_econ(U, s, V, X, tolerance);
                
                // And return an orthogonal version of X
                return U * V.t();
            }
            
            ////////////////////////////////////////////////////////////////////
            // Method to find the 'closest' symmetric positive-definite matrix 
            // to a given matrix. Closest is measured by the Frobenius norm.
            
            // Method from:
            // N. J. Higham, "Computing a nearest symmetric positive semidefinite matrix", Linear Algebra and its Applications, 1988
            // DOI: 10.1016/0024-3795(88)90223-6
            
            // For a MATLAB implementation see:
            // https://gitlab.com/giles-colclough/OHBA-HIPPO/blob/0.5/+HIPPO/+external/nearestSPD.m
            
            static inline arma::fmat closest_sympd(const arma::fmat& X, const float tolerance = EPSILON)
            {
                if (!X.is_square()) { throw std::invalid_argument("Utilities::LinearAlgebra::closest_sympd(): Matrix must be square!"); }
                
                // Method: X_sympd = (Y + Z)/2, where Y=(X + Xt)/2 and Z is the 
                // symmetric polar factor of Y
                
                // Make a symmetric version of X
                const arma::fmat Y = (X + X.t()) / 2.0f;
                
                // Calculate robust SVD of Y
                arma::fmat U, V; arma::fvec s;
                svd_econ(U, s, V, Y, tolerance);
                
                // Polar factor
                // https://en.wikipedia.org/wiki/Polar_decomposition#Matrix_polar_decomposition
                const arma::fmat Z = V * arma::diagmat(s) * V.t();
                
                // And make X_sympd
                return (Y + Z) / 2.0f;
            }
            
            ////////////////////////////////////////////////////////////////////
        }
    }
}
#endif
