// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "RandomSVD.h"

#include <stdexcept>
#include <numeric>     // std::accumulate

////////////////////////////////////////////////////////////////////////////////

// Find an orthogonal version fo a matrix via the QR decomposition
static inline arma::fmat orthogonalise(const arma::fmat& X)
{
    arma::fmat Q,R;
    
    if (X.n_rows >= X.n_cols) {
        arma::qr_econ(Q, R, X);
    }
    else {
        // This ensures Q is the same size as X
        arma::qr_econ(Q, R, X.t()); Q = Q.t();
    }
    
    return Q;
}

////////////////////////////////////////////////////////////////////////////////

// Compute a randomised version of the SVD
// Algorithm from Halko, Martinson & Tropp, SIAM Review 53(2) pp.217-288, 2011
// DOI: 10.1137/090771806
// Parameters:
// D: Data
// K: Target number of components to estimate
// k: Number of extra components during algorithm to improve accuracy
// q: Rounds of power iteration to perform
PROFUMO::SVD PROFUMO::Utilities::computeRandomSVD(const arma::fmat& D, const unsigned int K, const unsigned int k, const unsigned int q)
{
    // Get key sizes
    const unsigned int M = D.n_rows, N = D.n_cols;
    
    // We will compute things slightly differently, depending on whether M > N
    const bool transposeD = (M < N);
    // This ensures two things:
    //  - The random basis is small, so we don't spend too long generating it
    //  - The real SVD (in the random subspace) is small
    
    ////////////////////////////////////////////////////////////////////////////
    // Find an orthonormal Y that best approximates the range of D
    // Algorithm 4.4: Randomized Subspace Iteration
    // X = randn(N,K); Y = (D * Dt)^q * D * X; Y = orth(Y)
    // 1) M > N: X = randn(N,K);  Y =  D * (Dt * D)^q * X; Y = orth(Y)
    // 2) N > M: X = randn(M,K); Y = Dt * (D * Dt)^q * X; Y = orth(Y)
    // This means the random generation of X, and the iterative application of 
    // DtD / DDt, is carried out in the low dimensional configuration.
    
    // Generate random basis as initialisation
    arma::fmat Y;
    if ( ! transposeD ) {
        Y = arma::randn<arma::fmat>(N, K+k);
    }
    else {
        Y = arma::randn<arma::fmat>(M, K+k);
    }
    
    // Few rounds of power iteration
    // Compute Y(n+1) = (D * Dt) * Y(n)
    // Orthogonalise after each round for numerical stability
    // They recommend orthogonalising after every multiplication with D, but 
    // in practice doing it after each D*Dt seems to be stable too
    for (unsigned int p = 0; p < q; ++p) {
        
        if ( ! transposeD ) {
            Y = D.t() * (D * Y);
        }
        else {
            Y = D * (D.t() * Y);
        }
        
        Y = orthogonalise(Y);
        
    }
    
    // Do final multiplication with D
    if ( ! transposeD ) {
        Y = D * Y;
    }
    else {
        Y = D.t() * Y;
    }
    
    // Do the final orthogonalisation
    // This is relatively expensive, as Y is 'big', but it gives us a cheap SVD 
    // in the next step
    Y = orthogonalise(Y);
    
    ////////////////////////////////////////////////////////////////////////////
    // Transform to a regular SVD
    // Algorithm 5.1: Direct SVD
    // 1) D ≈ Y * Yt * D; U,S,V = svd(Yt * D); U = Y * U
    // 2) D ≈ D * Y * Yt; U,S,V = svd(D * Y); Vt = Vt * Y
    // Replace D by Dt in (1) to get (2)
    
    // Compute real SVD
    SVD svd;
    if ( ! transposeD ) {
        arma::svd_econ(svd.U, svd.s, svd.V, Y.t() * D);
    }
    else {
        arma::svd_econ(svd.U, svd.s, svd.V, D * Y);
    }
    
    // Turn into full SVD by modifying U/V with Y
    if ( ! transposeD ) {
        svd.U = Y * svd.U;
    }
    else {
        svd.V = Y * svd.V;
    }
    
    // Sort singular values and take the top K components
    arma::uvec indices = sort_index(svd.s, "descend"); // Belt & braces
    indices = indices.head(K);
    
    // Sort the SVD itself
    svd.U = svd.U.cols(indices);
    svd.s = svd.s.elem(indices);
    svd.V = svd.V.cols(indices);
    
    return svd;
}

////////////////////////////////////////////////////////////////////////////////

// Compute the random SVD on multiple data matrices
// Algorithm from Halko, Martinson & Tropp, SIAM Review 53(2) pp.217-288, 2011
// DOI: 10.1137/090771806
// Data will be concatenated in the second dimension
// e.g. D[0] is (M x N1), D[1] is (M x N2) etc
// Parameters:
// D: Data
// K: Target number of components to estimate
// k: Number of extra components during algorithm to improve accuracy
// q: Rounds of power iteration to perform
PROFUMO::ConcatenatedSVD PROFUMO::Utilities::computeRandomConcatenatedSVD(const std::vector<const arma::fmat*>& D, const unsigned int K, const unsigned int k, const unsigned int q)
{
    // Useful matrix concatenation identities
    // https://en.wikipedia.org/wiki/Block_matrix#Block_matrix_multiplication
    // D = [D0 D1 ... Di ... ]
    // X * D = [X*D0 ... X*Di .. ]
    // D * X = sum( Di * Xi )
    // D * Dt = sum( Di * Dit )
    
    // Make sure we were given some data!
    if (D.empty()) {
        throw std::invalid_argument("Utilities::computeRandomConcatenatedSVD(): No data provided!");
    }
    
    // Get key sizes
    const unsigned int M = D[0]->n_rows;
    const unsigned int nD = D.size();
    
    std::vector<unsigned int> n(nD,0), start(nD,0), end(nD,0);
    for (unsigned int i = 0, s = 0; i < nD; ++i) {
        n[i] = D[i]->n_cols;
        start[i] = s;
        s += n[i];
        end[i] = s - 1;
        
        // Check that the sizes match!
        if (D[i]->n_rows != M) {
            throw std::invalid_argument("Utilities::computeRandomConcatenatedSVD(): Matrices have different numbers of rows!");
        }
    }
    const unsigned int N = std::accumulate(n.begin(), n.end(), 0);
    
    // We will compute things slightly differently, depending on whether M > N
    const bool transposeD = (M < N);
    // This ensures two things:
    //  - The random basis is small, so we don't spend too long generating it
    //  - The real SVD (in the random subspace) is small
    
    ////////////////////////////////////////////////////////////////////////////
    // Find an orthonormal Y that best approximates the range of D
    // Algorithm 4.4: Randomized Subspace Iteration
    // 1) M > N: X = randn(N,K); Y = (D * Dt)^q * D * X; Y = orth(Y)
    // 2) N > M: X = randn(M,K); Y = D * (D * Dt)^q * X; Y = orth(Y)
    // Replace D by Dt in (1) to get (2)
    
    // Generate random basis as initialisation
    arma::fmat Y;
    if ( ! transposeD ) {
        // (1) Do first pass multiplication with D too
        Y = arma::zeros<arma::fmat>(M, K+k);
        #pragma omp parallel for schedule(dynamic)
        for (unsigned int i = 0; i < nD; ++i) {
            const arma::fmat y = (*D[i]) * arma::randn<arma::fmat>(n[i], K+k);
            #pragma omp critical
            {
            Y += y;
            }
        }
    }
    else {
        Y = arma::randn<arma::fmat>(M, K+k);
    }
    
    // Few rounds of power iteration
    // Compute Y(n+1) = (D * Dt) * Y(n)
    // Orthogonalise after each round for numerical stability
    // They recommend orthogonalising after every multiplication with D, but 
    // in practice doing it after each D*Dt seems to be stable too
    for (unsigned int p = 0; p < q; ++p) {
        
        // arma::fmat X = D.t() * Y;
        arma::fmat X = arma::zeros<arma::fmat>(N, K+k);
        #pragma omp parallel for schedule(dynamic)
        for (unsigned int i = 0; i < nD; ++i) {
            X.rows(start[i], end[i]) = D[i]->t() * Y;
        }
        if ( ! transposeD ) {
            X = orthogonalise(X);
        }
        
        // Y = D * X;
        Y = arma::zeros<arma::fmat>(M, K+k);
        #pragma omp parallel for schedule(dynamic)
        for (unsigned int i = 0; i < nD; ++i) {
            const arma::fmat y = (*D[i]) * X.rows(start[i], end[i]);
            #pragma omp critical
            {
            Y += y;
            }
        }
        if ( transposeD ) {
            Y = orthogonalise(Y);
        }
        
        // Note that we can't take the same approach as the single matrix 
        // version here. Because we have split up D into D[i] we can't compute 
        // Dt * D in one pass: DtD is NxN, which is e.g. [D1t*D1 D1t*D2; D2t*D1 D2t*D2]
        // Therefore, given that we have to do two passes anyway, this approach 
        // is preferred
        
    }
    
    // (2) Do final multiplication with D
    if ( transposeD ) {
        //Y = D.t() * Y;
        arma::fmat X = arma::zeros<arma::fmat>(N, K+k);
        #pragma omp parallel for schedule(dynamic)
        for (unsigned int i = 0; i < nD; ++i) {
            X.rows(start[i], end[i]) = D[i]->t() * Y;
        }
        Y = X;
    }
    
    // Do the final orthogonalisation
    // This is relatively expensive, as Y is 'big', but it gives us a cheap SVD 
    // in the next step
    Y = orthogonalise(Y);
    
    ////////////////////////////////////////////////////////////////////////////
    // Transform to a regular SVD
    // Algorithm 5.1: Direct SVD
    // 1) D ≈ Y * Yt * D; U,S,V = svd(Yt * D); U = Y * U
    // 2) D ≈ D * Y * Yt; U,S,V = svd(D * Y); Vt = Vt * Y
    // Replace D by Dt in (1) to get (2)
    
    // Compute real SVD
    SVD svd;
    if ( ! transposeD ) {
        //arma::fmat X = Y.t() * D; svd(X);
        arma::fmat X = arma::zeros<arma::fmat>(K+k, N);
        #pragma omp parallel for schedule(dynamic)
        for (unsigned int i = 0; i < nD; ++i) {
            X.cols(start[i], end[i]) = Y.t() * (*D[i]);
        }
        arma::svd_econ(svd.U, svd.s, svd.V, X);
    }
    else {
        //arma::fmat X = D * Y; svd(X);
        arma::fmat X = arma::zeros<arma::fmat>(M, K+k);
        #pragma omp parallel for schedule(dynamic)
        for (unsigned int i = 0; i < nD; ++i) {
            const arma::fmat x = (*D[i]) * Y.rows(start[i], end[i]);
            #pragma omp critical
            {
            X += x;
            }
        }
        arma::svd_econ(svd.U, svd.s, svd.V, X);
    }
    
    // Sort singular values and take the top K components
    arma::uvec indices = sort_index(svd.s, "descend"); // Belt & braces
    indices = indices.head(K);
    
    // And form the concatenated SVD
    ConcatenatedSVD cSVD;
    // Combine svd.U with Y to form full basis, then sort
    if ( ! transposeD ) {
        cSVD.U = Y * svd.U;
    }
    else {
        cSVD.U = svd.U;
    }
    cSVD.U = cSVD.U.cols(indices);
    // Sort singular values
    cSVD.s = svd.s.elem(indices);
    // Split V up and sort
    cSVD.V = std::vector<arma::fmat>(nD, arma::fmat());
    #pragma omp parallel for schedule(dynamic)
    for (unsigned int i = 0; i < nD; ++i) {
        // Split V into chunks corresponding to original data
        arma::fmat V;
        if ( ! transposeD ) {
            V = svd.V.rows(start[i], end[i]);
        }
        else {
            V = Y.rows(start[i], end[i]) * svd.V;
        }
        // Then sort and save
        cSVD.V[i] = V.cols(indices);
    }
    
    return cSVD;
}

////////////////////////////////////////////////////////////////////////////////
