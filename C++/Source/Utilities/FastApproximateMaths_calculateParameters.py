#!/usr/bin/env python
# -*- coding: utf-8 -*-

# A separate copy of the licence is also included within the repository.
# SHBASECOPYRIGHT

################################################################################
# Import

import math
import numpy
import scipy, scipy.optimize
import random

import matplotlib as mpl
import matplotlib.pyplot as plt

################################################################################
print("*" * 80)
print("Generating function...")
print()

print("1.0 / log(2.0): {:.10f}:".format(1.0 / math.log(2.0)))

# Generate function to approximate
x = numpy.linspace(0.0, 1.0, 10001); x = x[:-1]
y = numpy.power(2.0, x)

plt.figure()
plt.plot(x, y)
plt.show()

################################################################################
print("*" * 80)
print("Fitting polynomial...")
print()

# Fit 3rd order polynomial
pPol = numpy.polyfit(x, y, 3)

# Calculate values & error
yPol = numpy.polyval(pPol, x)
ePol = numpy.mean( (y - yPol)**2 )

for p in pPol:
    print("{:.10f}".format(p))
print(ePol)

################################################################################
print("*" * 80)
print("Fitting Padé approximant (order: 1,1)...")
print()

# Function to evaluate approximation
def pade11val(p, x):
    return (p[0] + p[1] * x) / (1.0 + p[2] * x)

# Error to minimise
def pade11error(p, x, y):
    return numpy.mean( numpy.abs(y - pade11val(p, x)) )

# Minimise!
keepRes = scipy.optimize.minimize(pade11error, numpy.random.randn(3), args=(x,y))
# Find good set of parameters
for i in range(250):
    
    res = scipy.optimize.minimize(pade11error, numpy.random.randn(3), args=(x,y))
    print("{:.3e}".format(res.fun))
    
    if res.fun < keepRes.fun:
        keepRes = res

# Then refine in the vicinity of the best guess
for i in range(750):
    
    res = scipy.optimize.minimize(pade11error, keepRes.x + 0.01 * numpy.random.randn(3), args=(x,y))
    print("{:.3e}".format(res.fun))
    
    if res.fun < keepRes.fun:
        keepRes = res

# Save params
pPade11 = keepRes.x
yPade11 = pade11val(pPade11, x)
ePade11 = numpy.mean( (y - yPade11)**2 )

print()
for p in pPade11:
    print("{:.10f}".format(p))
print(ePade11)

################################################################################
print("*" * 80)
print("Fitting Padé approximant (order: 2,2)...")
print()

# Function to evaluate approximation
def pade22val(p, x):
    return (p[0] + p[1] * x + p[2] * x**2) / (1.0 + p[3] * x + p[4] * x**2)

# Error to minimise
def pade22error(p, x, y):
    #return numpy.mean( (y - pade22val(p, x))**2 )
    return numpy.mean( numpy.abs(y - pade22val(p, x)) )
    #return math.sqrt(numpy.mean( (y - pade22val(p, x))**2 ))

# Minimise!
keepRes = scipy.optimize.minimize(pade22error, numpy.random.randn(5), args=(x,y))
# Find good set of parameters
for i in range(250):
    
    res = scipy.optimize.minimize(pade22error, numpy.random.randn(5), args=(x,y))
    print("{:.3e}".format(res.fun))
    
    if res.fun < keepRes.fun:
        keepRes = res

# Then refine in the vicinity of the best guess
for i in range(1750):
    
    res = scipy.optimize.minimize(pade22error, keepRes.x + 0.01 * numpy.random.randn(5), args=(x,y))
    print("{:.3e}".format(res.fun))
    
    if res.fun < keepRes.fun:
        keepRes = res

# Save params
pPade22 = keepRes.x
yPade22 = pade22val(pPade22, x)
ePade22 = numpy.mean( (y - yPade22)**2 )

print()
for p in pPade22:
    print("{:.10f}".format(p))
print(ePade22)

################################################################################
print("*" * 80)
print("Plotting...")
print()

plt.figure()
plt.hold(True)
plt.plot(x, numpy.zeros(x.size), '-', color=3*[0.7,])
plt.plot(x, y - yPol, 'b-', label="Pol")
plt.plot(x, y - yPade11, 'g-', label="Pade11")
plt.plot(x, y - yPade22, 'r-', label="Pade22")

plt.legend()
plt.show()

################################################################################
