// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "MatrixManipulations.h"

#include <algorithm>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <cmath>
#include <ios>      // std::scientific
#include <iomanip>  // std::setprecision

#include <spdlog/spdlog.h>

#include "MFModel.h"
#include "MFVariableModels.h"
#include "MFModels/FullRankMFModel.h"
#include "MFModels/P/IndependentMixtureModels.h"
#include "MFModels/P/Parcellation.h"
#include "MFModels/A/MultivariateNormal.h"
#include "MFModels/Psi/GammaPrecision.h"

#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"

#include "Utilities/DataNormalisation.h"
#include "Utilities/LinearAlgebra.h"
#include "Utilities/Timer.h"

namespace DN = PROFUMO::Utilities::DataNormalisation;
namespace linalg = PROFUMO::Utilities::LinearAlgebra;
namespace MM = PROFUMO::Utilities::MatrixManipulations;

////////////////////////////////////////////////////////////////////////////////

// Apply a rotation via an orthogonal matrix
// Can return more or less columns than in X
void MM::randomiseColumns(arma::fmat& X, const unsigned int n_cols)
{
    // Generate a random orthogonal matrix
    // R. M. Heiberger, "Algorithm AS 127: Generation of Random Orthogonal Matrices", Journal of the Royal Statistical Society. Series C (Applied Statistics), 1978
    // DOI: 10.2307/2346957
    const unsigned int n = std::max(n_cols, (unsigned int) X.n_cols);
    arma::fmat Q, R, Z;
    Z = arma::randn<arma::fmat>(n, n);
    arma::qr(Q, R, Z);
    // Shed extra rows/columns
    Q = Q.submat(0, 0, X.n_cols-1, n_cols-1);
    
    // And apply to the columns of X
    X *= Q;
    
    return;
}

void MM::randomiseColumns(arma::fmat& X) {
    randomiseColumns(X, X.n_cols);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MM::flipColumnMeans(arma::fmat& X)
{
    X.each_row() %= arma::sign( linalg::mean(X, 0) );
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MM::flipColumnSkews(arma::fmat& X)
{
    // Calculate skewness
    // https://en.wikipedia.org/wiki/Skewness#Pearson.27s_moment_coefficient_of_skewness
    // Just do that here: we should probably do it in MiscMaths.h, but then 
    // there's the issue of making it general across vectors / rows / columns, 
    // and it seems overkill to create a new Armadillo operation.
    const arma::frowvec E_X  = linalg::mean(X, 0);
    const arma::frowvec E_X2 = linalg::mean(arma::square(X), 0);
    const arma::frowvec E_X3 = linalg::mean(arma::pow(X, 3), 0);
    const arma::frowvec skews =
        E_X3 - 3.0f * E_X * E_X2 + 2.0f * arma::pow(E_X, 3);
    // Can ignore divisor as we are only interested in the sign
    
    X.each_row() %= arma::sign( skews );
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void MM::flipColumnSigns(arma::fmat& X)
{
    // Essentially work with the non-central third moment (i.e. we are
    // interested in the shape of the distribution around zero, as that is
    // more useful than the observed mean for sparse maps)
    const arma::frowvec skews = linalg::mean(arma::pow(X, 3), 0);
    // Can ignore divisor as we are only interested in the sign
    
    X.each_row() %= arma::sign( skews );
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Cosine similarity
// https://en.wikipedia.org/wiki/Cosine_similarity

arma::fmat MM::calculateCosineSimilarity(const arma::fmat& X)
{
    return calculateCosineSimilarity(X, X);
}

arma::fmat MM::calculateCosineSimilarity(const arma::fmat& X, const arma::fmat& Y)
{
    // Check for size issues
    if (X.n_rows != Y.n_rows) {
        throw std::invalid_argument("Utilities::MiscMaths::calculateCosineSimilarity(X,Y): X & Y must have the same number of rows.");
    }
    
    // Calculate scores and component magnitudes
    const arma::fmat XY = X.t() * Y;
    arma::frowvec x = linalg::sum(X % X, 0); x.elem( arma::find(x < 1.0e-10f) ).ones(); x = arma::sqrt(x);
    arma::frowvec y = linalg::sum(Y % Y, 0); y.elem( arma::find(y < 1.0e-10f) ).ones(); y = arma::sqrt(y);
    
    // Combine to form similarity
    arma::fmat similarity = XY;
    similarity.each_col() /= x.t(); similarity.each_row() /= y;
    
    return similarity;
}

////////////////////////////////////////////////////////////////////////////////
// Pair matrix columns based on cosine similarity
// Uses a greedy matching

MM::Pairing MM::pairMatrixColumns(const arma::fmat& X, const arma::fmat& Y)
{
    // Check for size issues
    if (X.n_rows != Y.n_rows) {
        throw std::invalid_argument("Utilities::MiscMaths::calculateCosineSimilarity(X,Y): X & Y must have the same number of rows.");
    }
    
    // Intialise
    arma::fmat similarity = calculateCosineSimilarity(X, Y);
    const unsigned int N = std::min(similarity.n_rows, similarity.n_cols);
    Pairing pairing;
    pairing.inds1 = arma::zeros<arma::uvec>(N);
    pairing.inds2 = arma::zeros<arma::uvec>(N);
    pairing.scores = arma::zeros<arma::fvec>(N);
    
    // Iteratively find the best matches (greedy approach)
    for (unsigned int n = 0; n < N; ++n) {
        // Find the max
        const unsigned int index = arma::abs(similarity).index_max();
        const arma::uvec indices = ind2sub(arma::size(similarity), index);
        
        // Record
        pairing.inds1(n) = indices(0);
        pairing.inds2(n) = indices(1);
        pairing.scores(n) = similarity(index);
        
        // Null the similarities to remove these components for the next iteration
        similarity.row(indices(0)).fill( arma::fdatum::nan );
        similarity.col(indices(1)).fill( arma::fdatum::nan );
    }
    
    return pairing;
}

////////////////////////////////////////////////////////////////////////////////

MM::Decomposition MM::calculateVBMatrixFactorisation(const DataTypes::FullRankData data, const unsigned int rank, const FactorisationType factorisationType, const arma::fmat& initialisation, const unsigned int iterations)
{
    Utilities::Timer timer;
    spdlog::trace("Computing matrix factorisation...");
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Key sizes
    const unsigned int V = data.V;
    const unsigned int T = data.T;
    const unsigned int M = rank;
    
    // Storage for hyperpriors (if we need them)
    std::unique_ptr<Modules::MatrixMean_VBPosterior> tau;           // Means
    std::unique_ptr<Modules::MatrixPrecision_VBPosterior> beta;     // Precisions
    std::unique_ptr<Modules::MembershipProbability_VBPosterior> pi; // Memberships
    
    // First, make a constant spatial model
    std::unique_ptr<MFModels::P_VBPosterior> Pconst;
    {
        MFModels::P::P2C Einit;
        Einit.P = initialisation;
        if (factorisationType == FactorisationType::MODES) {
            // Match overall scale to null (tails will still be significant)
            DN::normaliseColumns(Einit.P); Einit.P *= 0.25f;
        }
        Einit.PtP = Einit.P.t() * Einit.P + 0.01f * V * arma::eye<arma::fmat>(M, M);
        Pconst = std::make_unique<VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>>(Einit);
    }
    
    // Make the actual spatial model
    const float dofCorrectionFactor = 1.0f;
    std::unique_ptr<MFModels::P_VBPosterior> P;
    //--------------------------------------------------------------------------
    if (factorisationType == FactorisationType::MODES) {
        typedef MFModels::P::IndependentMixtureModels<0,3> IndependentMixtureModels;
        IndependentMixtureModels::Parameters PPrior;
        PPrior.randomInitialisation = false;
        // Three component model: +ve/-ve signal components, and a low variance
        // null. While we could have used a delta for the null, using a
        // Gaussian gives a better control of the overall scale.
        // N.B. Overall variance: `sum[p_i * (mu_i^2 + sigma_i^2)]` ≈ 1
        // Positive Gaussian signal
        PPrior.gaussians[0].mu = 2.0f * arma::ones<arma::fmat>(V,M);
        PPrior.gaussians[0].sigma2 = std::pow(1.0f, 2) * arma::ones<arma::fmat>(V,M);
        PPrior.gaussians[0].p = 0.08f * arma::ones<arma::fmat>(V,M);
        // Negative Gaussian signal
        PPrior.gaussians[1].mu = -2.0f * arma::ones<arma::fmat>(V,M);
        PPrior.gaussians[1].sigma2 = std::pow(1.0f, 2) * arma::ones<arma::fmat>(V,M);
        PPrior.gaussians[1].p = 0.02f * arma::ones<arma::fmat>(V,M);
        // Null
        PPrior.gaussians[2].mu = arma::zeros<arma::fmat>(V,M);
        PPrior.gaussians[2].sigma2 = std::pow(0.25f, 2) * arma::ones<arma::fmat>(V,M);
        PPrior.gaussians[2].p = 0.90f * arma::ones<arma::fmat>(V,M);
        
        P = std::make_unique<IndependentMixtureModels>(PPrior, dofCorrectionFactor);
    }
    //--------------------------------------------------------------------------
    else if (factorisationType == FactorisationType::PARCELS) {
        // Mean hyperprior
        Modules::MatrixMeans::P2C Etau; Etau.X = 1.0f * arma::ones<arma::fmat>(V,M); Etau.X2 = 1.0f * arma::ones<arma::fmat>(V,M);
        tau = std::make_unique<VBModules::Constant<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>>(Etau);
        
        // Precison hyperprior
        Modules::MatrixPrecisions::P2C Ebeta; Ebeta.X = 1.0f * arma::ones<arma::fmat>(V,M); Ebeta.logX = arma::log(Ebeta.X);
        beta = std::make_unique<VBModules::Constant<Modules::MatrixPrecisions::C2P, Modules::MatrixPrecisions::P2C>>(Ebeta);
        
        // Membership hyperprior
        Modules::MembershipProbabilities::P2C Epi;
        Epi.p = std::vector<arma::fmat>(M, (1.0f / M) * arma::ones<arma::fmat>(V,1));
        Epi.logP = std::vector<arma::fmat>(M, arma::log((1.0f / M) * arma::ones<arma::fmat>(V,1)));
        pi = std::make_unique<VBModules::Constant<Modules::MembershipProbabilities::C2P, Modules::MembershipProbabilities::P2C>>(Epi);
        
        // Actual model
        MFModels::P::Parcellation::Parameters PPrior;
        PPrior.V = V; PPrior.M = M;
        
        P = std::make_unique<MFModels::P::Parcellation>(tau.get(), beta.get(), pi.get(), PPrior, dofCorrectionFactor);
    }
    //--------------------------------------------------------------------------
    
    // Weights
    MFModels::H::P2C EH; EH.h = arma::ones<arma::fvec>(M); EH.hht = arma::ones<arma::fmat>(M, M);
    std::unique_ptr<MFModels::H_VBPosterior> H = std::make_unique<VBModules::Constant<MFModels::H::C2P, MFModels::H::P2C>>(EH);
    
    // Time courses
    // Precision matrix
    VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters alphaAPrior;
    alphaAPrior.a = 1.0e-1f * arma::ones<arma::fvec>(M); alphaAPrior.b = 1.0e-1f * arma::ones<arma::fvec>(M);
    std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> alphaA = std::make_unique<VBModules::PrecisionMatrices::ComponentwiseGammaPrecision>(alphaAPrior);
    // And the time courses themselves
    MFModels::A::MultivariateNormal::Parameters APrior; APrior.M = M; APrior.T = T;
    std::unique_ptr<MFModels::A_VBPosterior> A = std::make_unique<MFModels::A::MultivariateNormal>(alphaA.get(), APrior);
    
    // Noise
    VBDistributions::Gamma::Parameters psiPrior;
    psiPrior.a = 1.0e-1f; psiPrior.b = 1.0e-1f;
    std::unique_ptr<MFModels::Psi_VBPosterior> Psi = std::make_unique<MFModels::Psi::GammaPrecision>(psiPrior);
    
    // And make the MFModel
    std::unique_ptr<MFModel> MFM = std::make_unique<MFModels::FullRankMFModel>(data, Pconst.get(), H.get(), A.get(), Psi.get(), dofCorrectionFactor);
    
    // Convenience lambda to calculate free energy
    auto calculateFreeEnergy = [&] () -> double {
        double F = MFM->getLogLikelihood();
        F -= P->getKL();
        F -= H->getKL();
        F -= A->getKL();
        F -= alphaA->getKL();
        F -= Psi->getKL();
        return F;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Few updates to make sure things start sensibly
    for (unsigned int z = 0; z < 3; ++z) {
        A->update();
        H->update();
        Psi->update();
    }
    
    // Switch to true spatial model
    MFM->setPModel( P.get() );
    for (unsigned int z = 0; z < 3; ++z) {
        P->update();
        H->update();
        Psi->update();
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    double F = calculateFreeEnergy();
    spdlog::trace("Initial free energy: {:.6e}", F);
    
    // And get cracking with the updates!
    for (unsigned int z = 0; z < iterations; ++z) {
        Utilities::Timer loopTimer;
        
        // Update!
        P->update();
        H->update();
        A->update();
        alphaA->update();
        Psi->update();
        
        //spdlog::trace("Iteration {:d}: {:s}", z+1, loopTimer.getTimeElapsed());
        if ( ((z+1) % 25) == 0 ) {
            std::ostringstream summary;
            summary << std::scientific << std::setprecision(3);

            summary << "Summary at end of iteration " << z+1 << std::endl;
            
            summary << "P: " << arma::mean(arma::diagvec(P->getExpectations().PtP)) / V << std::endl;
            summary << "H: " << arma::mean(H->getExpectations().h) << std::endl;
            summary << "A: " << arma::mean(arma::diagvec(A->getExpectations().AAt)) / T << std::endl;
            summary << "Ψ: " << 1.0f / std::sqrt(Psi->getExpectations().psi) << std::endl;
            
            const double oldF = F;
            F = calculateFreeEnergy();
            summary << "F: " << F << std::endl;
            summary << "Δ: " << F - oldF << std::endl;
            
            summary << "T: " << loopTimer.getTimeElapsed();
            
            spdlog::trace(summary.str());
        }
    }
    
    // Collate results
    Decomposition decomposition;
    decomposition.P = P->getExpectations().P;
    if (factorisationType == FactorisationType::MODES) {
        // Zero values that are likely to be drawn from the null
        //decomposition.P.elem( arma::find(arma::abs(decomposition.P) < 1.0f) ).zeros();
    }
    decomposition.h = H->getExpectations().h;
    decomposition.A = A->getExpectations().A;
    decomposition.F = calculateFreeEnergy();
    
    spdlog::trace("Final free energy: {:.6e}", calculateFreeEnergy());
    spdlog::trace("Factorisation finished. Time elapsed: {:s}", timer.getTimeElapsed());
    
    // And return the final map means
    return decomposition;
}

////////////////////////////////////////////////////////////////////////////////
