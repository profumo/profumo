// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2016
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "DataNormalisation.h"

#include <cmath>
#include <stdexcept>
#include <string>

#include "Utilities/LinearAlgebra.h"

namespace DN = PROFUMO::Utilities::DataNormalisation;
namespace linalg = PROFUMO::Utilities::LinearAlgebra;

////////////////////////////////////////////////////////////////////////////////
// Demean
////////////////////////////////////////////////////////////////////////////////

// Removes the data mean
float DN::demeanGlobally(arma::fmat& D)
{
    const float mean = linalg::mean(D);
    
    D -= mean;
    
    return mean;
}

////////////////////////////////////////////////////////////////////////////////

// Removes the data mean from each row
arma::fvec DN::demeanRows(arma::fmat& D)
{
    const arma::fvec means = linalg::mean(D, 1);
    
    D.each_col() -= means;
    
    return means;
}

////////////////////////////////////////////////////////////////////////////////

// Removes the data mean from each column
arma::frowvec DN::demeanColumns(arma::fmat& D)
{
    const arma::frowvec means = linalg::mean(D, 0);
    
    D.each_row() -= means;
    
    return means;
}

////////////////////////////////////////////////////////////////////////////////
// Normalisation
////////////////////////////////////////////////////////////////////////////////

// Helper function to tidy up normalisations
template<class fmat_like>
inline arma::fmat clean_norm(const fmat_like& norm, const float tolerance)
{
    arma::fmat cleaned_norm = norm;
    
    // Prevent division by 0 by removing elements less than tol
    cleaned_norm.elem( arma::find(cleaned_norm < tolerance) ).ones();
    
    // In pathological cases, the variance calculation can underflow giving a 
    // negative variance (which then gets mangled by arma::sqrt() before here)
    cleaned_norm.elem( arma::find_nonfinite(cleaned_norm) ).ones();
    
    return cleaned_norm;
}

////////////////////////////////////////////////////////////////////////////////
// Overall normalisation
////////////////////////////////////////////////////////////////////////////////

float DN::normaliseGlobally(arma::fmat& D, const float tolerance)
{
    // Sum twice as returns an arma::fmat rather than float
    const float normalisation = 1.0f / clean_norm(
            arma::sqrt(
                linalg::mean(linalg::mean(arma::square(D), 0), 1)),
            tolerance
        ).at(0);
    
    D *= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////

arma::fvec DN::normaliseRows(arma::fmat& D, const float tolerance)
{
    const arma::fvec normalisation = 1.0f / clean_norm(
            arma::sqrt(linalg::mean(arma::square(D), 1)),
            tolerance);
    
    D.each_col() %= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////

arma::frowvec DN::normaliseColumns(arma::fmat& D, const float tolerance)
{
    const arma::frowvec normalisation = 1.0f / clean_norm(
            arma::sqrt(linalg::mean(arma::square(D), 0)),
            tolerance);
    
    D.each_row() %= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////
// Signal subspace normalisation
////////////////////////////////////////////////////////////////////////////////

float DN::normaliseSignalSubspaceGlobally(arma::fmat& D, const unsigned int M, const float tolerance)
{
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace captured by the SVD
    // accu(D^2) = Tr(Dt * D) = sum(s^2)
    const float normalisation = 1.0f / clean_norm(
            arma::sqrt(linalg::sum(arma::square(svd.s), 0) / D.n_elem),
            tolerance
        ).at(0);
    
    D *= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////

arma::fvec DN::normaliseSignalSubspaceRows(arma::fmat& D, const unsigned int M, const float tolerance)
{
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace captured by the SVD
    // sum(D^2, 1) = diag(D * Dt) = diag(U * s^2 * Ut)
    const arma::fvec normalisation = 1.0f / clean_norm(
            arma::sqrt(
                linalg::sum(arma::square(svd.U.each_row() % svd.s.t()), 1)
                / D.n_cols),
            tolerance);
    
    D.each_col() %= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////

arma::frowvec DN::normaliseSignalSubspaceColumns(arma::fmat& D, const unsigned int M, const float tolerance)
{
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace captured by the SVD
    // sum(D^2, 1) = diag(Dt * D) = diag(V * s^2 * Vt)
    const arma::frowvec normalisation = 1.0f / clean_norm(
            arma::sqrt(
                linalg::sum(arma::square(svd.V.each_row() % svd.s.t()), 1).t()
                / D.n_rows),
            tolerance);
    
    D.each_row() %= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////
// Noise subspace normalisation
////////////////////////////////////////////////////////////////////////////////

// Rescales data to unit variance
float DN::normaliseNoiseSubspaceGlobally(arma::fmat& D, const unsigned int M, const float tolerance)
{
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace *not* captured by the SVD
    // Subspaces are orthogonal so can compute them separately
    // accu(D^2) = Tr(Dt * D) = sum(s^2)
    const float normalisation = 1.0f / clean_norm(
            arma::sqrt((
                    linalg::sum(linalg::sum(arma::square(D), 0), 1)
                    - linalg::sum(arma::square(svd.s), 0)
                ) / D.n_elem),
            tolerance
        ).at(0);
    
    D *= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////

// Rescales each row of the data to unit variance
arma::fvec DN::normaliseNoiseSubspaceRows(arma::fmat& D, const unsigned int M, const float tolerance)
{
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace *not* captured by the SVD
    // Subspaces are orthogonal so can compute them separately
    // sum(D^2, 1) = diag(D * Dt) = diag(U * s^2 * Ut)
    const arma::fvec normalisation = 1.0f / clean_norm(
            arma::sqrt((
                    linalg::sum(arma::square(D), 1)
                    - linalg::sum(arma::square(svd.U.each_row() % svd.s.t()), 1)
                ) / D.n_cols),
            tolerance);
    
    D.each_col() %= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////

// Rescales each column of the data to unit variance
arma::frowvec DN::normaliseNoiseSubspaceColumns(arma::fmat& D, const unsigned int M, const float tolerance)
{
    const SVD svd = computeRandomSVD(D, M);
    
    // Compute subspace *not* captured by the SVD
    // Subspaces are orthogonal so can compute them separately
    // sum(D^2, 1) = diag(Dt * D) = diag(V * s^2 * Vt)
    const arma::frowvec normalisation = 1.0f / clean_norm(
            arma::sqrt((
                    linalg::sum(arma::square(D), 0)
                    - linalg::sum(arma::square(svd.V.each_row() % svd.s.t()), 1).t()
                ) / D.n_rows),
            tolerance);
    
    D.each_row() %= normalisation;
    
    return normalisation;
}

////////////////////////////////////////////////////////////////////////////////
