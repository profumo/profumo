// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Random.h"

////////////////////////////////////////////////////////////////////////////////

// Get a (shared) random engine
// https://isocpp.org/files/papers/n3551.pdf - Section 7
std::default_random_engine& PROFUMO::Utilities::get_random_engine()
{
    static std::default_random_engine engine{};
    
    return engine;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::Utilities::set_seed(const seed_type seed)
{
    auto engine = get_random_engine();
    engine.seed(seed);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void PROFUMO::Utilities::set_seed_random()
{
    std::random_device rd;
    set_seed(rd());
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
