// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "Timer.h"

#include <sstream>
#include <iostream>
#include <iomanip>

namespace Utils = PROFUMO::Utilities;

////////////////////////////////////////////////////////////////////////////////

Utils::Timer::Timer()
{
    // Start timing
    reset();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

std::string Utils::Timer::getTimeElapsed(const unsigned int precision) const
{
    // Get current time
    const WallTime wallNow = std::chrono::high_resolution_clock::now();
    const std::clock_t cpuNow = clock();
    
    // Calculate time elapsed
    const std::chrono::duration<double> wallTimeElapsed = wallNow - wallStart_;
    const double cpuTimeElapsed = double(cpuNow - cpuStart_) / CLOCKS_PER_SEC;
    
    // Print
    std::ostringstream timingMessage; timingMessage << std::setiosflags(std::ios::fixed) << std::setprecision(precision);
    timingMessage << wallTimeElapsed.count() << "s (" << cpuTimeElapsed << "s CPU)";
    
    return timingMessage.str();
}

////////////////////////////////////////////////////////////////////////////////

void Utils::Timer::printTimeElapsed(const unsigned int precision) const
{
    printTimeElapsed("Time elapsed", precision);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void Utils::Timer::printTimeElapsed(const std::string qualifier, const unsigned int precision) const
{
    std::cout << qualifier << ": " << getTimeElapsed(precision) << std::endl;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void Utils::Timer::reset( )
{
    // Record start times for both clocks
    wallStart_ = std::chrono::high_resolution_clock::now();
    cpuStart_ = std::clock();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
