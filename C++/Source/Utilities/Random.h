// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2019
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Manages global default random device for std::random
// See 'Random Number Generation in C++11'
// https://isocpp.org/files/papers/n3551.pdf

#ifndef UTILITIES_DATA_RANDOM
#define UTILITIES_DATA_RANDOM

#include <random>

namespace PROFUMO
{
    namespace Utilities
    {
        // Get a (shared) random engine
        std::default_random_engine& get_random_engine();
        
        // Set seeds (apes Armadillo interface)
        typedef std::default_random_engine::result_type seed_type;
        void set_seed(const seed_type seed);
        void set_seed_random();
    }
}
#endif
