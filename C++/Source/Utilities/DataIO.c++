// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include "DataIO.h"

#include <vector>
#include <cstdint>
#include <stdexcept>

////////////////////////////////////////////////////////////////////////////////

namespace PROFUMO
{
    namespace Utilities
    {
        // Converts a buffer of arbitrary type to arma::fmat
        template<typename T>
        static inline arma::fmat bufferToMat(const char* buffer, unsigned int n_rows, unsigned int n_cols);
    }
}

////////////////////////////////////////////////////////////////////////////////

// Loads some common neuroimaging file formats
arma::fmat PROFUMO::Utilities::loadDataMatrix(const std::string fileName)
{
    arma::fmat dataMatrix;
    
    ////////////////////////////////////////////////////////////////////////////
    // Start by trying NIFTI/CIFTI reader
    NiftiIO::NiftiHeader header;
    char* buffer = nullptr;  // Can delete https://stackoverflow.com/a/27133124
    std::vector<NiftiIO::NiftiExtension> extensions;
    bool allocateBuffer = true;
    
    // Load data
    bool isNIFTI = false;
    #pragma omp critical(loadData)
    {
    try {
        header = NiftiIO::loadImage(fileName, buffer, extensions, allocateBuffer);
        isNIFTI = true;
    }
    catch (const NiftiIO::NiftiException& /* error */) {
        isNIFTI = false;
    }
    catch (...) {
        delete[] buffer;
        throw;
    }
    }
    
    if (isNIFTI) {
        // Now work out how to transform to matrix
        unsigned int n_rows, n_cols;
        bool isCIFTI = false;
        for (const auto& ext : extensions) {
            if (ext.ecode == NIFTI_ECODE_CIFTI) {isCIFTI = true;}
        }
        if (isCIFTI) {
            // Time
            n_rows = header.dim[5];
            // Space
            n_cols = header.dim[6];
            // TRANSPOSE AFTER
            if (header.dim[1] != 1 || header.dim[2] != 1 || header.dim[3] != 1 || header.dim[4] != 1) {
                throw std::runtime_error(
                        "Utilities::DataIO: CIFTI can only be 2D!"
                        "\n    File: " + fileName);
            }
        }
        else {
            n_rows = header.dim[1] * header.dim[2] * header.dim[3];
            n_cols = header.dim[4];
            if (header.dim[0] > 4) {
                throw std::runtime_error(
                        "Utilities::DataIO: NIFTI can only be 3D or 4D!"
                        "\n    File: " + fileName);
            }
        }
        
        switch (header.datatype) {
            case DT_UINT8 : {
                dataMatrix = bufferToMat<std::uint8_t>(buffer, n_rows, n_cols);
                break;
            }
            
            case DT_UINT16 : {
                dataMatrix = bufferToMat<std::uint16_t>(buffer, n_rows, n_cols);
                break;
            }
            
            case DT_UINT32 : {
                dataMatrix = bufferToMat<std::uint32_t>(buffer, n_rows, n_cols);
                break;
            }
            
            case DT_UINT64 : {
                dataMatrix = bufferToMat<std::uint64_t>(buffer, n_rows, n_cols);
                break;
            }
            
            case DT_INT8 : {
                dataMatrix = bufferToMat<std::int8_t>(buffer, n_rows, n_cols);
                break;
            }
            
            case DT_INT16 : {
                dataMatrix = bufferToMat<std::int16_t>(buffer, n_rows, n_cols);
                break;
            }
            
            case DT_INT32 : {
                dataMatrix = bufferToMat<std::int32_t>(buffer, n_rows, n_cols);
                break;
            }
            
            case DT_INT64 : {
                dataMatrix = bufferToMat<std::int64_t>(buffer, n_rows, n_cols);
                break;
            }
            
            case DT_FLOAT32 : {
                dataMatrix = bufferToMat<float>(buffer, n_rows, n_cols);
                break;
            }
            
            case DT_FLOAT64 : {
                dataMatrix = bufferToMat<double>(buffer, n_rows, n_cols);
                break;
            }
            
            case DT_FLOAT128 : {
                dataMatrix = bufferToMat<long double>(buffer, n_rows, n_cols);
                break;
            }
            
            default : {
                throw std::runtime_error(
                        "Utilities::DataIO: Incompatible NIFTI data type!"
                        "\n    Type: " + header.datatypeString() +
                        "\n    File: " + fileName);
            }
        }
        
        // Flip dimensions for CIFTI
        if (isCIFTI) {
            dataMatrix = dataMatrix.t();
        }
    }
    
    // Free the buffer
    delete[] buffer;
    
    ////////////////////////////////////////////////////////////////////////////
    // Otherwise, assume Armadillo can load it
    
    if (!isNIFTI) {
        bool success = false;
        #pragma omp critical(loadData)
        {
        #pragma omp critical(HDF5)
        {
        success = dataMatrix.load( fileName );
        }
        }
        
        if (success == false) {
            throw std::runtime_error(
                    "Utilities::DataIO: Unrecognised file format!"
                    "\n    File: " + fileName);
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    return dataMatrix;
}

////////////////////////////////////////////////////////////////////////////////

template<typename T>
static inline arma::fmat PROFUMO::Utilities::bufferToMat(const char* buffer, unsigned int n_rows, unsigned int n_cols)
{
    // Would like to use:
    //   const auto tempData = arma::Mat<T>((T*) buffer, ...)
    //   return arma::conv_to<arma::fmat>::from( tempData );
    // But unfortunately Armadillo does not support all the data types we
    // encounter in NIFTI files so have to do it manually :-(
    
    arma::fmat dataMatrix(n_rows, n_cols);
    const T* dataBuffer = (T*) buffer;
    for (unsigned int i=0; i < dataMatrix.n_elem; ++i) {
        dataMatrix[i] = (float) dataBuffer[i];
    }
    
    return dataMatrix;
}

////////////////////////////////////////////////////////////////////////////////
