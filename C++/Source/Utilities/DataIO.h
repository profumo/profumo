// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Some functions for reading/writing data from/to file.

#ifndef UTILITIES_DATA_IO_H
#define UTILITIES_DATA_IO_H

#include <armadillo>
#include <fstream>
#include <string>

#include "NewNifti/NewNifti.h"

namespace PROFUMO
{
    namespace Utilities
    {
        // Loads a data matrix from file
        arma::fmat loadDataMatrix(const std::string fileName);
        
        // Saves any object for which we can use `ofstream << object` to file
        template <typename T>
        void save(const std::string fileName, const T& object);
    }
}

////////////////////////////////////////////////////////////////////////////////

template <typename T>
void PROFUMO::Utilities::save(const std::string fileName, const T& object)
{
    std::ofstream file;
    file.open( fileName.c_str() );
    file << object << std::endl;
    file.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
#endif
