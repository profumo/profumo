// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2015
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Simple class which measures CPU and wall time

// http://en.cppreference.com/w/cpp/chrono

#ifndef UTILITIES_TIMER_H
#define UTILITIES_TIMER_H

#include <chrono>
#include <ctime>
#include <string>

namespace PROFUMO
{
    namespace Utilities
    {
        
        class Timer
        {
        public:
            // Initialises, and starts timing
            Timer();
            
            // Gets a string representation of time taken (precision sets decimal places)
            std::string getTimeElapsed(const unsigned int precision = 2) const;
            
            // Prints 'Time elapsed: <time>' to stdout
            void printTimeElapsed(const unsigned int precision = 2) const;
            // Prints '<qualifier>: <time>' to stdout
            void printTimeElapsed(const std::string qualifier, const unsigned int precision = 2) const;
            
            // Restarts timer
            void reset();
            
        protected:
            typedef std::chrono::time_point<std::chrono::high_resolution_clock> WallTime;
            
            // Stores start points
            std::clock_t cpuStart_;
            WallTime wallStart_;
        };
        
    }
}
#endif
