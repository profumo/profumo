// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Defines a VB distribution. This is designed to be a "dumb" helper class that
// stores some useful update rules but, as it has no concept of the graphical
// model structure, it cannot be used to build a full model by itself.

#ifndef VB_DISTRIBUTION_H
#define VB_DISTRIBUTION_H

#include <cmath>

namespace PROFUMO
{
    
    template<class DataExpectationsType, class ExpectationsType>
    class VBDistribution
    {
    public:
        // Store types for easy external access
        typedef DataExpectationsType DataExpectations;
        typedef ExpectationsType Expectations;
        
        // Update the posterior based on the input data
        virtual void update(const DataExpectations D) = 0;
        
        // Return the expectations of the posterior
        virtual Expectations getExpectations() const = 0;
        
        // KL divergence between prior and posterior
        virtual double getKL() const = 0;
        
        virtual ~VBDistribution() = default;
    };
    
    namespace VBDistributions
    {
        ////////////////////////////////////////////////////////////////////////
        
        template<class DataExpectationsType, class ExpectationsType>
        class MixtureModelDistribution :
            public virtual VBDistribution<DataExpectationsType, ExpectationsType>
        {
        public:
            // Get the "evidence" for this component (to be turned into a probability)
            virtual float getEvidence() const = 0;
        };
        
        //----------------------------------------------------------------------
        
        template<class MixtureModelDistribution>
        struct MixtureModelComponent
        {
        public:
            MixtureModelDistribution distribution;
            const float prior_log_p;
            float posterior_p;
            
            MixtureModelComponent(const MixtureModelDistribution distribution, const float p)
            : distribution(distribution), prior_log_p(std::log(p)), posterior_p(p)
            {return;}
        };
        
        ////////////////////////////////////////////////////////////////////////
    }
}
#endif
