// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

#include <string>
#include <fstream>
#include <sstream>    // std::ostringstream
#include <iomanip>    // std::put_time
#include <sys/stat.h>
#include <locale>     // std::locale, std::isspace, std::isdigit
#include <armadillo>
#include <algorithm>
#include <stdexcept>
#include <chrono>     // std::system_clock
#include <ctime>      // std::time_t, std::localtime

#ifdef _OPENMP
    #include <omp.h>
#endif

#include <boost/optional.hpp>
#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <tclap/CmdLine.h>

#include "ModelManager.h"

#include "Utilities/BuildTime.h"
#include "Utilities/DataIO.h"
#include "Utilities/Random.h"

using json = nlohmann::json;

////////////////////////////////////////////////////////////////////////////////

struct Arguments
{
public:
    std::string command;
    std::string dataLocationsFile;
    unsigned int M;
    std::string outputDir;
    // Spatial
    PROFUMO::ModelManager::SpatialModel spatialModel;
    boost::optional<std::string> groupMeansFile;
    boost::optional<std::string> groupPrecisionsFile;
    boost::optional<std::string> groupMembershipsFile;
    boost::optional<std::string> spatialNeighboursFile;
    // Temporal
    bool useHRF; float TR;  // Doesn't naturally follow optional, yet
    boost::optional<std::string> hrfFile;
    PROFUMO::ModelManager::TemporalPrecisionModel temporalPrecisionModel;
    // Data
    boost::optional<std::string> maskFile;
    boost::optional<unsigned int> lowRank;
    float dofCorrectionFactor;
    bool normaliseData;
    // Initialisation
    boost::optional<std::string> initialMapsFile;
    unsigned int multiStartIterations;
    bool globalInit;
    // Etc
    #ifdef _OPENMP
        bool paralleliseLoading;
    #endif
    boost::optional<PROFUMO::Utilities::seed_type> randomSeed;
};

////////////////////////////////////////////////////////////////////////////////
// Helper functions

Arguments parseArguments(const int argc, const char* argv[]);

void initialiseLogging(const std::string logDir);

void saveConfigFile(const std::string filename, const Arguments& args);

PROFUMO::ModelManager::Options setOptions(const Arguments& args);

PROFUMO::ModelManager::SpatialParameters loadSpatialParameters(const Arguments& args, const boost::optional<arma::uvec>& maskInds);

// Makes sure output directory is legit and ends in .pfm
std::string cleanOutputDir(const std::string outputDir);

// Copy! C++17 filesystem library please hurry up ;-p
void copyFile(const std::string fromFile, const std::string toFile);

// Generates a formatted timestamp
std::string generateTimestamp();
std::string generateTimestamp(const std::string format);

// Loads a set of spatial neighbours from file
std::vector<arma::uvec> loadNeighbours(const std::string neighboursFile);

////////////////////////////////////////////////////////////////////////////////

int main(const int argc, const char* argv[])
{
    ////////////////////////////////////////////////////////////////////////////
    // Parse command line args
    
    const Arguments args = parseArguments(argc, argv);
    
    ////////////////////////////////////////////////////////////////////////////
    // Make key directories and copy key files
    
    // Make output directory
    mkdir( args.outputDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Make directory for preprocessing output
    const std::string preprocessingDir = args.outputDir + "Preprocessing/";
    mkdir( preprocessingDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Make directory for intermediate output
    const std::string intermediatesBaseDir = args.outputDir + "Intermediates/";
    mkdir( intermediatesBaseDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    // Make directory for final output
    const std::string resultsDir = args.outputDir + "FinalModel/";
    mkdir( resultsDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
    
    
    // Copy spec file
    copyFile(args.dataLocationsFile, args.outputDir + "DataLocations.json");
    
    // Mask (if necessary)
    if (args.maskFile) {
        // THIS IS A FUCKING FILE MANIPULATION DISASTER
        // Get the basename
        std::string baseName = *args.maskFile;
        if (baseName.find_last_of("/") != std::string::npos) {
            baseName = baseName.substr( args.maskFile->find_last_of("/") + 1 );
        }
        // And copy!
        // Don't rename mask - we can always use config file to retrieve name
        copyFile(*args.maskFile, preprocessingDir + baseName);
    }
    
    // Neighbours (if necessary)
    if (args.spatialNeighboursFile) {
        copyFile(*args.spatialNeighboursFile, args.outputDir + "SpatialNeighbours.txt");
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Initialise logging
    
    initialiseLogging(args.outputDir);
    
    // Levels: trace / debug / info / warn / err / critical / off
    // 1) PROFUMO.c++ & ModelManager (public): info / debug
    // 2) ModelManager (private): debug / trace
    // 3) Others : trace
    // N.B. (2) can also be assigned to classes called directly by ModelManager
    // public functions (e.g. DataLoader).
    // In general, only spdlog::info() should contain blank lines
    // '-----' should be used for spdlog::debug()
    // Spacing for spdlog::trace() is not a concern given the timestamps
    
    ////////////////////////////////////////////////////////////////////////////
    
    spdlog::info("Beginning PROFUMO analysis.");
    spdlog::info("Started at {}.", generateTimestamp());
    
    // http://www.patorjk.com/software/taag/#p=testall&h=0&f=Big%20Money-ne&t=PROFUMO
    // Increased letter spacing, modified 'F'
    spdlog::info(R"(
  /$$$$$$$   /$$$$$$$    /$$$$$$   /$$$$$$$  /$$   /$$  /$$      /$$   /$$$$$$ 
 | $$__  $$ | $$__  $$  /$$__  $$ | $$____/ | $$  | $$ | $$$    /$$$  /$$__  $$
 | $$  \ $$ | $$  \ $$ | $$  \ $$ | $$      | $$  | $$ | $$$$  /$$$$ | $$  \ $$
 | $$$$$$$/ | $$$$$$$/ | $$  | $$ | $$$$$$  | $$  | $$ | $$ $$/$$ $$ | $$  | $$
 | $$____/  | $$__  $$ | $$  | $$ | $$___/  | $$  | $$ | $$  $$$| $$ | $$  | $$
 | $$       | $$  \ $$ | $$  | $$ | $$      | $$  | $$ | $$\  $ | $$ | $$  | $$
 | $$       | $$  | $$ |  $$$$$$/ | $$      |  $$$$$$/ | $$ \/  | $$ |  $$$$$$/
 |__/       |__/  |__/  \______/  |__/       \______/  |__/     |__/  \______/ 
)");
    
    std::ostringstream parameters;
    parameters << "--------------" << std::endl;
    parameters << "Key parameters" << std::endl;
    parameters << "--------------" << std::endl;
    parameters << "Input data: " << args.dataLocationsFile << std::endl;
    parameters << "Number of modes: " << args.M << std::endl;
    parameters << "Output directory: " << args.outputDir << std::endl;
    parameters << "--------------" << std::endl;
    spdlog::info(parameters.str());
    
    ////////////////////////////////////////////////////////////////////////////
    // Create a config file with key parameters
    
    spdlog::info("Creating config file...");
    
    const std::string configFileName = args.outputDir + "Configuration.json";
    saveConfigFile(configFileName, args);
    
    // And log (read back from file as a sanity check)
    std::ostringstream summary;
    summary << std::string(29, '-') << " Config file contents " << std::string(29, '-') << std::endl;
    std::ifstream contents; contents.open( configFileName.c_str() );
    summary << contents.rdbuf();
    contents.close();
    summary << std::string(80, '-');
    spdlog::debug(summary.str());
    
    spdlog::info("Config file created: {:s}\n", configFileName);
    
    ////////////////////////////////////////////////////////////////////////////
    // Set key options and load any necessary parameters from file
    
    const PROFUMO::ModelManager::Options options = setOptions(args);
    
    const PROFUMO::ModelManager::SpatialParameters spatialParameters
        = loadSpatialParameters(args, options.dataLoader.maskInds);
    
    ////////////////////////////////////////////////////////////////////////////
    // Make full model
    
    PROFUMO::ModelManager::SpatialModel           spatialModel;
    PROFUMO::ModelManager::WeightModel            weightModel;
    PROFUMO::ModelManager::TimeCourseModel        timeCourseModel, initialTimeCourseModel;
    PROFUMO::ModelManager::TemporalPrecisionModel temporalPrecisionModel;
    PROFUMO::ModelManager::NoiseModel             noiseModel;
    // Running PROFUMO-predefined?
    const bool useFixedParameters
        = (args.groupMeansFile || args.groupPrecisionsFile || args.groupMembershipsFile);
    
    // Spatial maps
    spatialModel = args.spatialModel;
    
    // Weights
    weightModel = PROFUMO::ModelManager::WeightModel::RECTIFIED_MULTIVARIATE_NORMAL;
    
    // Time courses
    if (args.useHRF) {
        timeCourseModel = PROFUMO::ModelManager::TimeCourseModel::NOISY_HRF;
    }
    else {
        timeCourseModel = PROFUMO::ModelManager::TimeCourseModel::MULTIVARIATE_NORMAL;
    }
    // Simpler initial model if we are inferring group maps too
    if (useFixedParameters) {
        initialTimeCourseModel = timeCourseModel;
    }
    else {
        initialTimeCourseModel = PROFUMO::ModelManager::TimeCourseModel::MULTIVARIATE_NORMAL;
    }
    
    // Temporal precision matrices
    temporalPrecisionModel = args.temporalPrecisionModel;
    
    // Noise
    noiseModel = PROFUMO::ModelManager::NoiseModel::INDEPENDENT_RUNS;
    
    
    // Make the model!
    PROFUMO::ModelManager model(
            args.dataLocationsFile,
            args.M,
            spatialModel,
            spatialParameters,
            weightModel,
            initialTimeCourseModel,
            temporalPrecisionModel,
            noiseModel,
            options,
            preprocessingDir
        );
    
    ////////////////////////////////////////////////////////////////////////////
    // Solve full model
    
    std::string intermediatesDir;
    unsigned int modelNumber = 1;
    
    // Updates for full inference
    if (!useFixedParameters) {
        
        //----------------------------------------------------------------------
        
        // Initial updates
        
        // First batch with normalised time courses to hold everything in check
        intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
        mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
        
        model.update(10, intermediatesDir, true);  // Normalise weights
        
        
        // And then the next round without normalisation
        intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
        mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
        
        model.update(40, intermediatesDir);
        
        //----------------------------------------------------------------------
        
        // Reboot model - group maps should now be better than decomposed basis
        
        model.reinitialiseModel( spatialModel, spatialParameters, weightModel, timeCourseModel, temporalPrecisionModel );
        
        intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
        mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
        
        model.update(100, intermediatesDir);
        
        //----------------------------------------------------------------------
        
        // Reboot model - subject-specific (artefact?) maps can persist long 
        // after group maps have been eliminated
        
        model.reinitialiseModel( spatialModel, spatialParameters, weightModel, timeCourseModel, temporalPrecisionModel );
        
        intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
        mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
        
        model.update(200, intermediatesDir);
        
        //----------------------------------------------------------------------
        
        // And one final reboot:
        //  + Brings into line with PROFUMO-predefined
        //  + Prevents subject maps from diverging too far from the group
        
        model.reinitialiseModel( spatialModel, spatialParameters, weightModel, timeCourseModel, temporalPrecisionModel );
        
        // At this stage we have a consensus set of group maps, so we can use 
        // the same updates as predefined :-)
    }
        
    // Do the updates!
    intermediatesDir = intermediatesBaseDir + "Model" + std::to_string(modelNumber) + "/";
    mkdir( intermediatesDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ); modelNumber++;
    
    model.update(100, intermediatesDir);
    
    
    // Save!
    model.save(resultsDir);
    
    
    // Free energy
    spdlog::info("Final free energy: {:.8e}\n", model.getFreeEnergy());
    
    ////////////////////////////////////////////////////////////////////////////
    
    spdlog::info("Finished at {}.", generateTimestamp());
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// Helper functions
////////////////////////////////////////////////////////////////////////////////

Arguments parseArguments(const int argc, const char* argv[])
{
    Arguments args;
    
    try {
        
        const std::string EMPTY_DEFAULT = "**DEFAULT**";
        // Simple `boost::optional` wrapper for string arguments
        auto parse_optional = [EMPTY_DEFAULT](const std::string& arg){
            boost::optional<std::string> opt;
            if (arg != EMPTY_DEFAULT) {opt = arg;}
            return opt;
        };
        
        ////////////////////////////////////////////////////////////////////////
        
        // Set everything up (description, delimiter, version)
        TCLAP::CmdLine cmd(
                "PROFUMO: infers PRObabilistic FUnctional MOdes from fMRI data.",
                ' ',
                std::string("0.11.3")
                + "\n + Executable built on "
                + PROFUMO::Utilities::getBuildDate() + " at "
                + PROFUMO::Utilities::getBuildTime()
                #ifdef _OPENMP
                    + "\n + Parallelisation enabled via OpenMP"
                #endif
            );
        
        // Add the required arguments
        // (name, description, required, default, type)
        // N.B. The help message is more useful if type = name
        TCLAP::UnlabeledValueArg<std::string> dataLocationsFile("DataLocations.json", "File containing data locations in JSON format.", true, EMPTY_DEFAULT, "DataLocations.json");
        cmd.add( dataLocationsFile );
        
        TCLAP::UnlabeledValueArg<int> M("M", "Number of modes to infer.", true, 0, "M");
        cmd.add( M );
        
        TCLAP::UnlabeledValueArg<std::string> outputDir("outputDirectory", "Output directory.", true, EMPTY_DEFAULT, "outputDirectory");
        cmd.add( outputDir );
        
        // Optional arguments
        // (flag*, name, description, required, default, type)
        // * flag can only be one character
        // The help text displays these in reverse order!
        
        // Etc
        TCLAP::ValueArg<PROFUMO::Utilities::seed_type> randomSeed("", "randomSeed", "Supply a specific seed to the pseudo-random number generator. Note that the program will not be deterministic if multi-threaded! [Default: random seed generated]", false, 0, "unsigned int");
        cmd.add( randomSeed );
        #ifdef _OPENMP
            TCLAP::SwitchArg loadSequentially("", "loadSequentially", "Turn off the parallelisation of the data loading. This will be slower, but will reduce peak memory usage.", false);
            cmd.add( loadSequentially );
            TCLAP::ValueArg<int> nThreads("", "nThreads", "Use this argument, or set the OMP_NUM_THREADS environment variable, to explicitly set the maximum number of threads. [Default: " + std::to_string(omp_get_max_threads()) + "]", false, 0, "int");
            cmd.add( nThreads );
        #endif
        
        // Initialisation
        TCLAP::SwitchArg globalInit("", "globalInit", "Take account of global signals during initialisation.", false);
        cmd.add( globalInit );
        TCLAP::ValueArg<int> multiStartIterations("", "multiStartIterations", "Number of iterations of the group-level spatial decomposition to do before inferring the full model. [Default: 5]", false, 5, "int");
        cmd.add( multiStartIterations );
        TCLAP::ValueArg<std::string> initialMapsFile("", "initialMaps", "Initialise the decomposition based on the spatial maps specified here", false, EMPTY_DEFAULT, "file");
        cmd.add( initialMapsFile );
        
        // Data
        TCLAP::SwitchArg noNormalisation("", "noDataNormalisation", "BE VERY CAREFUL! This turns off the internal data normalisation, which scales the data such that it matches the generative model. If you are confident in your own preprocessing this can be disabled here...", false);
        cmd.add( noNormalisation );
        TCLAP::ValueArg<float> dof("d", "dofCorrection", "Spatial degrees of freedom correction factor.", false, 1.0f, "float");
        cmd.add( dof );
        TCLAP::ValueArg<int> lowRank("k", "lowRankData", "Use a low rank approximation to the raw data. Supply this argument with the required rank. If not specifed, will use full rank data.", false, 0, "int");
        cmd.add( lowRank );
        TCLAP::ValueArg<std::string> maskFile("m", "mask", "Use to mask the raw data / any pre-specified parameters.", false, EMPTY_DEFAULT, "file");
        cmd.add( maskFile );
        
        // Temporal
        TCLAP::ValuesConstraint<std::string> allowedCMVals({"Subject", "Run", "Condition"});
        TCLAP::ValueArg<std::string> temporalPrecisionModel("", "covModel", "Temporal covariance model type. [Default: Subject]", false, "Subject", &allowedCMVals);
        cmd.add( temporalPrecisionModel );
        // HRF properties
        TCLAP::ValueArg<std::string> hrfFile("", "hrfFile", "Alternative shape for the HRF (uses FLOBS by default). Can only be supplied with the --useHRF argument.", false, EMPTY_DEFAULT, "file");
        cmd.add( hrfFile );
        TCLAP::ValueArg<float> TR("", "useHRF", "Include if an HRF-based temporal model is desired. Supply the TR with this argument.", false, 0.0f, "TR");
        cmd.add( TR );
        
        // Spatial
        TCLAP::ValueArg<std::string> spatialNeighboursFile("", "spatialNeighbours", "File containing the list of spatial neighbours for each voxel.", false, EMPTY_DEFAULT, "file");
        cmd.add( spatialNeighboursFile );
        // Fixed parameters
        TCLAP::ValueArg<std::string> groupPrecisionsFile("", "fixedSpatialPrecisions", "Use this option to supply a fixed set of voxelwise precisions. Subject parameters will then be inferred based on these group values.", false, EMPTY_DEFAULT, "file");
        cmd.add( groupPrecisionsFile );
        TCLAP::ValueArg<std::string> groupMembershipsFile("", "fixedSpatialMemberships", "Use this option to supply a fixed set of voxelwise memberships. Subject parameters will then be inferred based on these group values.", false, EMPTY_DEFAULT, "file");
        cmd.add( groupMembershipsFile );
        TCLAP::ValueArg<std::string> groupMeansFile("", "fixedSpatialMeans", "Use this option to supply a fixed set of voxelwise means. Subject parameters will then be inferred based on these group values.", false, EMPTY_DEFAULT, "file");
        cmd.add( groupMeansFile );
        // Model
        TCLAP::ValuesConstraint<std::string> allowedSMVals({"Modes", "Parcellation"});
        TCLAP::ValueArg<std::string> spatialModel("", "spatialModel", "Spatial model type. [Default: Modes]", false, "Modes", &allowedSMVals);
        cmd.add( spatialModel );
        
        ////////////////////////////////////////////////////////////////////////
        
        // Parse!
        cmd.parse( argc, argv );
        
        ////////////////////////////////////////////////////////////////////////
        
        // And store everything
        
        // Spec file
        args.dataLocationsFile = dataLocationsFile.getValue();
        
        // Modes
        const int iM = M.getValue();
        if ( iM > 0 ) { args.M = (unsigned int) iM; }
        else { throw std::invalid_argument("Number of modes (M) must be a positive integer."); }
        
        // Output dir
        args.outputDir = cleanOutputDir( outputDir.getValue() );
        
        // Spatial
        const std::string sM = spatialModel.getValue();
        if (sM == "Modes") {
            args.spatialModel = PROFUMO::ModelManager::SpatialModel::MODES;
        }
        else if (sM == "Parcellation") {
            args.spatialModel = PROFUMO::ModelManager::SpatialModel::PARCELLATION;
        }
        else {
            throw std::invalid_argument("\"" + sM + "\" is not a valid spatial model.");
        }
        // Fixed parameters
        args.groupMeansFile = parse_optional(groupMeansFile.getValue());
        args.groupPrecisionsFile = parse_optional(groupPrecisionsFile.getValue());
        args.groupMembershipsFile = parse_optional(groupMembershipsFile.getValue());
        // Spatial neighbours
        args.spatialNeighboursFile = parse_optional(spatialNeighboursFile.getValue());
        if ((args.spatialNeighboursFile) && (args.spatialModel != PROFUMO::ModelManager::SpatialModel::PARCELLATION)) {
            throw std::invalid_argument("Spatial neighbourhood only applies to the Parcellation model.");
        }
        
        // Temporal
        args.TR = TR.getValue();
        if (args.TR > 0.0f) { args.useHRF = true; }
        else if (args.TR == 0.0f) { args.useHRF = false; }
        else { throw std::invalid_argument("TR must be positive."); }
        // HRF file
        args.hrfFile = parse_optional(hrfFile.getValue());
        if ((!args.useHRF) && (args.hrfFile)) { throw std::invalid_argument("--hrfFile can only be used with --useHRF!"); }
        // Temporal covariance model
        const std::string tpM = temporalPrecisionModel.getValue();
        if (tpM == "Subject") {
            args.temporalPrecisionModel = PROFUMO::ModelManager::TemporalPrecisionModel::SUBJECT_PRECISION_MATRICES;
        }
        else if (tpM == "Run") {
            args.temporalPrecisionModel = PROFUMO::ModelManager::TemporalPrecisionModel::RUN_PRECISION_MATRICES;
        }
        else if (tpM == "Condition") {
            args.temporalPrecisionModel = PROFUMO::ModelManager::TemporalPrecisionModel::CATEGORISED_RUN_PRECISION_MATRICES;
        }
        else {
            throw std::invalid_argument("\"" + tpM + "\" is not a valid covariance model.");
        }
        
        // Data
        args.maskFile = parse_optional(maskFile.getValue());
        // Low rank data
        const int iK = lowRank.getValue();
        if (iK > 0) { args.lowRank = (unsigned int) iK; }
        else if (iK < 0) { throw std::invalid_argument("Rank of reduced data (k) must be a positive integer."); }
        // DoF
        args.dofCorrectionFactor = dof.getValue();
        if (args.dofCorrectionFactor <= 0.0f) { throw std::invalid_argument("Spatial degrees of freedom correction factor (d) must be positive."); }
        // Normalisation
        args.normaliseData = ! noNormalisation.getValue();
        
        // Initialisation
        args.initialMapsFile = parse_optional(initialMapsFile.getValue());
        // Multi start
        const int iMSI = multiStartIterations.getValue();
        if ( iMSI >= 0 ) { args.multiStartIterations = (unsigned int) iMSI; }
        else { throw std::invalid_argument("Number of multi-start iterations must be a positive integer."); }
        // Global init
        args.globalInit = globalInit.getValue();
        
        // Etc
        #ifdef _OPENMP
            args.paralleliseLoading = ! loadSequentially.getValue();
        #endif
        // Set the number of threads
        #ifdef _OPENMP
            const int nt = nThreads.getValue();
            if ( nt > 0 ) { omp_set_num_threads(nt); }
            else if ( nt < 0 ) { throw std::invalid_argument("'nThreads' must be a positive integer."); }
        #endif
        // Seed random number generators
        const PROFUMO::Utilities::seed_type seed = randomSeed.getValue();
        if (seed != 0) {
            arma::arma_rng::set_seed((arma::arma_rng::seed_type) seed);
            PROFUMO::Utilities::set_seed(seed);
            args.randomSeed = seed;
        }
        else {
            // Set the seeds to a random value
            arma::arma_rng::set_seed_random();
            PROFUMO::Utilities::set_seed_random();
        }
        
        ////////////////////////////////////////////////////////////////////////
    }
    catch (const TCLAP::ArgException& e) {
        std::cerr << "Error: " << e.error() << " for arg " << e.argId() << std::endl;
        throw;
        //return -1;
    }
    
    // And, finally, include full command
    std::ostringstream command;
    for (int i = 0; i < argc; ++i) {
        command << argv[i];
        if (i < argc-1) {
            command << " ";
        }
    }
    args.command = command.str();
    
    return args;
}

////////////////////////////////////////////////////////////////////////////////

void initialiseLogging(const std::string logDir)
{
    // Save all the gory details to file
    auto log_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(
            logDir + "log.txt", true);
    log_sink->set_level(spdlog::level::trace);
    log_sink->set_pattern("[%L] [%Y-%m-%dT%H:%M:%S.%e%z]:\n%v\n");
    
    // But also save a cut down, simply formatted version
    auto output_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(
            logDir + "output.txt", true);
    output_sink->set_level(spdlog::level::debug);
    output_sink->set_pattern("%v");
    
    // Print key info directly to stdout (plain formatting)
    auto stdout_sink = std::make_shared<spdlog::sinks::stdout_sink_mt>();
    stdout_sink->set_level(spdlog::level::info);
    stdout_sink->set_pattern("%v");
    
    // And errors go to stderr, with timestamp etc
    auto stderr_sink = std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
    stderr_sink->set_level(spdlog::level::warn);
    stderr_sink->set_pattern("[%^%l%$] [%Y-%m-%d %H:%M:%S]: %v");
    
    // Combine into a logger, and make it the default
    auto logger = std::make_shared<spdlog::logger>(
                "multi_logger",
                spdlog::sinks_init_list(
                    {log_sink, output_sink, stdout_sink, stderr_sink}));
    logger->set_level(spdlog::level::trace);
    logger->flush_on(spdlog::level::debug);  // Flush often, very unintensive compared to computation
    spdlog::register_logger(logger);
    spdlog::set_default_logger(logger);

    return;
}

////////////////////////////////////////////////////////////////////////////////

void saveConfigFile(const std::string filename, const Arguments& args)
{
    json config;
    
    // Information about the code (time & date, version etc)
    // Use "> " to force some key information to be displayed first
    config["> Description"] = "PROFUMO configuration file";
    config["> Version"] = "0.11.3";
    config["> Version [build time]"] = PROFUMO::Utilities::getBuildTime() + "; " + PROFUMO::Utilities::getBuildDate();
    // Match formats: https://stackoverflow.com/a/2526956
    config["> Timestamp"] = generateTimestamp("%T; %b %e %Y");
    
    // Include full command
    config["Command"] = args.command;
    
    // Default arguments
    config["Data locations file"] = args.dataLocationsFile;
    config["Number of modes"] = args.M;
    config["Output directory"] = args.outputDir;
    
    // Spatial
    switch (args.spatialModel) {
        case PROFUMO::ModelManager::SpatialModel::MODES : {
            config["Spatial model"] = "Modes";
            break;
        }
        case PROFUMO::ModelManager::SpatialModel::PARCELLATION : {
            config["Spatial model"] = "Parcellation";
            break;
        }
        default :
            throw std::invalid_argument("PROFUMO: bad SpatialModel identifier.");
    }
    if (args.groupMeansFile) {
        config["Fixed group parameters"]["Spatial means file"] = *args.groupMeansFile;
    }
    if (args.groupPrecisionsFile) {
        config["Fixed group parameters"]["Spatial precisions file"] = *args.groupPrecisionsFile;
    }
    if (args.groupMembershipsFile) {
        config["Fixed group parameters"]["Spatial memberships file"] = *args.groupMembershipsFile;
    }
    if (args.spatialNeighboursFile) {
        config["Spatial neighbours file"] = *args.spatialNeighboursFile;
    }
    
    // Temporal
    if (args.useHRF) {
        config["Temporal model"] = "HRF based";
        config["HRF parameters"]["TR (s)"] = args.TR;
        if (args.hrfFile) {
            config["HRF parameters"]["HRF file"] = *args.hrfFile;
        }
    } else {
        config["Temporal model"] = "Multivariate normal";
    }
    // Covariance model
    switch (args.temporalPrecisionModel) {
        case PROFUMO::ModelManager::TemporalPrecisionModel::CONSTANT : {
            config["Covariance model"] = "Constant";
            break;
        }
        case PROFUMO::ModelManager::TemporalPrecisionModel::CONSTANT_GROUP : {
            config["Covariance model"] = "Subject (constant group-level prior)";
            break;
        }
        case PROFUMO::ModelManager::TemporalPrecisionModel::GROUP_PRECISION_MATRIX : {
            config["Covariance model"] = "Group";
            break;
        }
        case PROFUMO::ModelManager::TemporalPrecisionModel::SUBJECT_PRECISION_MATRICES : {
            config["Covariance model"] = "Subject";
            break;
        }
        case PROFUMO::ModelManager::TemporalPrecisionModel::RUN_PRECISION_MATRICES : {
            config["Covariance model"] = "Run";
            break;
        }
        case PROFUMO::ModelManager::TemporalPrecisionModel::CATEGORISED_RUN_PRECISION_MATRICES : {
            config["Covariance model"] = "Condition";
            break;
        }
        default :
            throw std::invalid_argument("PROFUMO: bad TemporalPrecisionModel identifier.");
    }
    
    // Data
    if (args.maskFile) {
        config["Mask file"] = *args.maskFile;
    }
    if ( !args.lowRank ) {
        config["Data type"] = "Full rank";
    } else {
        std::ostringstream rank; rank << "Low rank (" << *args.lowRank << " components)";
        config["Data type"] = rank.str();
    }
    config["DoF correction"] = args.dofCorrectionFactor;
    config["Data normalisation"] = args.normaliseData;
    
    // Intialisation
    if (args.initialMapsFile) {
        config["Initial maps file"] = *args.initialMapsFile;
    }
    config["Multi-start iterations"] = args.multiStartIterations;
    config["Global initialisation"] = args.globalInit;
    
    // Etc
    if (args.randomSeed) {
        std::ostringstream seed; seed << *args.randomSeed;
        config["Random seed"] = seed.str();
    }
    
    // Save!
    std::ofstream configFile;
    configFile.open(filename.c_str());
    // Spit out the JSON
    configFile << std::setw(4) << config << std::endl;  // pretty printing via setw, sets number of spaces to indent
    configFile.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::ModelManager::Options setOptions(const Arguments& args)
{
    PROFUMO::ModelManager::Options options;
    
    // Straightforward copies from args
    options.lowRank = args.lowRank;
    options.dataLoader.normaliseData = args.normaliseData;
    #ifdef _OPENMP
        options.dataLoader.paralleliseLoading = args.paralleliseLoading;
    #endif
    options.dofCorrectionFactor = args.dofCorrectionFactor;
    options.multiStartIterations = args.multiStartIterations;
    options.globalInit = args.globalInit;
    if (args.useHRF) {
        options.hrf = PROFUMO::ModelManager::HRFOptions();
        options.hrf->TR = args.TR;
        // Check file exists / set the default (if we are going to use it...)
        std::string hrfFile;
        if (args.hrfFile) {
            hrfFile = *args.hrfFile;
        }
        else {
            // FLOBS default if no file explicitly specified
            // http://stackoverflow.com/a/5867281
            // http://stackoverflow.com/q/3483327
            char const* tmp = std::getenv( "PROFUMODIR" );
            if ( tmp == nullptr ) { throw std::runtime_error("Unable to find `$PROFUMODIR`!"); }
            const std::string fslDir( tmp );
            hrfFile = fslDir + "/HRFs/Default.phrf";
        }
        // And check file exists!
        std::ifstream hrfTest(hrfFile);
        if (!hrfTest.good()) { throw std::runtime_error("Unable to find HRF file! " + hrfFile); };
        options.hrf->file = hrfFile;
    }
    
    // Mask
    if (args.maskFile) {
        // Load mask and transform to indices
        const arma::fmat maskData = PROFUMO::Utilities::loadDataMatrix( *args.maskFile );
        options.dataLoader.maskInds = arma::find( maskData != 0.0f );
    }
    const auto& maskInds = options.dataLoader.maskInds;
    
    // Initial maps
    if (args.initialMapsFile) {
        // Load
        arma::fmat initialMaps = PROFUMO::Utilities::loadDataMatrix( *args.initialMapsFile );
        // Mask, but ignore if the data is already the same size (i.e. it came from an already masked posterior)
        if (maskInds && (initialMaps.n_rows != maskInds->n_elem)) {
            try {
                initialMaps = initialMaps.rows( *maskInds );
            }
            catch (const std::logic_error& /* error */) {
                throw std::logic_error("PROFUMO: Mask and initial maps image sizes incompatible!");
            }
        }
        // Save
        initialMaps.save(args.outputDir + "InitialMaps.hdf5", arma::hdf5_binary);
        // Record
        options.initialMaps = initialMaps;
    }
    
    return options;
}

////////////////////////////////////////////////////////////////////////////////

PROFUMO::ModelManager::SpatialParameters loadSpatialParameters(const Arguments& args, const boost::optional<arma::uvec>& maskInds)
{
    PROFUMO::ModelManager::SpatialParameters spatialParameters;
    
    // Means
    if (args.groupMeansFile) {
        // Load
        arma::fmat groupMeans = PROFUMO::Utilities::loadDataMatrix( *args.groupMeansFile );
        // Mask, but ignore if the data is already the same size (i.e. it came from an already masked posterior)
        if (maskInds && (groupMeans.n_rows != maskInds->n_elem)) {
            try {
                groupMeans = groupMeans.rows( *maskInds );
            }
            catch (const std::logic_error& /* error */) {
                throw std::logic_error("PROFUMO: Mask and spatial mean image sizes incompatible!");
            }
        }
        // Rescale
        /* groupMeans /= arma::stddev( arma::vectorise(groupMeans) );
        groupMeans *= 0.25f; // ... */
        // Save
        groupMeans.save(args.outputDir + "GroupMapMeans.hdf5", arma::hdf5_binary);
        // Record
        spatialParameters.fixedGroupMeans = groupMeans;
    }
    
    // Precisions
    if (args.groupPrecisionsFile) {
        arma::fmat groupPrecisions = PROFUMO::Utilities::loadDataMatrix( *args.groupPrecisionsFile );
        if (maskInds && (groupPrecisions.n_rows != maskInds->n_elem)) {
            try {
                groupPrecisions = groupPrecisions.rows( *maskInds );
            }
            catch (const std::logic_error& /* error */) {
                throw std::logic_error("PROFUMO: Mask and spatial precision image sizes incompatible!");
            }
        }
        groupPrecisions.save(args.outputDir + "GroupMapPrecisions.hdf5", arma::hdf5_binary);
        spatialParameters.fixedGroupPrecisions = groupPrecisions;
    }
    
    // Memberships
    if (args.groupMembershipsFile) {
        arma::fmat groupMemberships = PROFUMO::Utilities::loadDataMatrix( *args.groupMembershipsFile );
        if (maskInds && (groupMemberships.n_rows != maskInds->n_elem)) {
            try {
                groupMemberships = groupMemberships.rows( *maskInds );
            }
            catch (const std::logic_error& /* error */) {
                throw std::logic_error("PROFUMO: Mask and spatial membership image sizes incompatible!");
            }
        }
        // Relax the parcellation ever so slightly - allow some subject variability!
        /* const float relaxation = 0.01f; // Max p = 0.99
        groupMemberships *= (1.0f - relaxation);
        groupMemberships += (relaxation / args.M); */
        groupMemberships.save(args.outputDir + "GroupMapMemberships.hdf5", arma::hdf5_binary);
        spatialParameters.fixedGroupMemberships = groupMemberships;
    }
    
    // Load spatial neighbours
    if (args.spatialNeighboursFile) {
        spatialParameters.spatialNeighbours = loadNeighbours(*args.spatialNeighboursFile);
    }
    
    return spatialParameters;
}

////////////////////////////////////////////////////////////////////////////////

// Makes sure output directory is legit and ends in .pfm
std::string cleanOutputDir(const std::string outputDir)
{
    std::string cleanDir = outputDir;
    
    // Remove any slashes at the end
    while ( cleanDir.back() == '/' ) { cleanDir.pop_back(); }
    
    // Add .pfm if necessary
    if ( cleanDir.length() < 4 || cleanDir.substr( cleanDir.length() - 4) != ".pfm" )
    {
        cleanDir += ".pfm";
    }
    
    // And add the slash back in
    cleanDir += "/";
    
    return cleanDir;
}

////////////////////////////////////////////////////////////////////////////////

void copyFile(const std::string fromFile, const std::string toFile)
{
    // http://stackoverflow.com/q/10195343
    std::ifstream source(fromFile, std::ios::in|std::ios::binary);
    std::ofstream dest(toFile, std::ios::out|std::ios::binary);
    
    dest << source.rdbuf();
    
    source.close();
    dest.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

// Generates a formatted timestamp
std::string generateTimestamp()
{
    // Default format (human readable primarily)
    // 15:09:52 BST (+0100); Fri 21 Jul 2017
    return generateTimestamp("%T %Z (%z); %a %e %b %Y");
}

std::string generateTimestamp(const std::string format)
{
    // This is a mess! No C++1x support for timezones etc, so we have to fall 
    // back on std::localtime from <ctime> to do the dirty work. Urgh...
    // However, will be included in <chrono> in C++20
    const std::time_t t = std::chrono::system_clock::to_time_t(
            std::chrono::system_clock::now());
    
    std::ostringstream timestamp;
    timestamp << std::put_time(std::localtime(&t), format.c_str());
    
    return timestamp.str();
}

////////////////////////////////////////////////////////////////////////////////

std::vector<arma::uvec> loadNeighbours(const std::string neighboursFile)
{
    // Load neighbours from file
    // Open file
    std::ifstream file( neighboursFile.c_str() );
    std::string line;
    
    // First line has number of voxels
    std::getline(file, line);
    
    // Check for mask
    if (line.substr(0,7) != "Voxels:") {
        throw std::invalid_argument("Spatial neighbours file does not contain a size line.");
    }
    const unsigned int N = std::stoul(line.substr(7)); // Everything after "Voxels:"
    
    // Make the store
    std::vector<arma::uvec> neighbourList(N, arma::uvec());
    
    // Parse each line
    while ( std::getline(file, line) ) {
        // Get the index of this set of neighbours
        std::size_t pos = line.find(":");
        const unsigned int index = std::stoul(line.substr(0, pos));
        
        // And parse the neighbours from the remainder of the line
        line = line.substr(pos + 1);
        std::vector<arma::uword> neighbours;
        // http://stackoverflow.com/a/14266139
        do {
            pos = line.find(",");
            const std::string nb = line.substr(0, pos);
            // Convert to uint (but check it isn't just whitespace!)
            // http://stackoverflow.com/a/9642381
            // http://stackoverflow.com/a/15039964
            if ( std::any_of(nb.begin(), nb.end(), static_cast<int(*)(int)>(std::isdigit)) ) {
                neighbours.push_back( std::stoul(nb) );
            }
            line.erase(0, pos + 1);
        } while (pos != std::string::npos);
        
        // And store!
        neighbourList[index] = arma::uvec(neighbours);
    }
    
    return neighbourList;
}
////////////////////////////////////////////////////////////////////////////////
