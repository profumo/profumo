// PROFUMO
// Framework for variational Bayesian inference of modes from fMRI data

// - Sam Harrison 2014
// A separate copy of the licence is also included within the repository.
/*  CCOPYRIGHT  */

// Some useful tests for all sorts of random stuff

#include <iostream>
#include <memory>
#include <armadillo>
#include <cmath>
#include <limits>
#include <algorithm>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <chrono>
#include <thread>  // std::this_thread::sleep_for( std::chrono::seconds(1) );

#ifdef _OPENMP
    #include <omp.h>
#endif

#include "NewNifti/NewNifti.h"

#include "DataTypes.h"
#include "DataLoader.h"

#include "MFModel.h"
#include "MFVariableModels.h"
#include "MFModels/FullRankMFModel.h"
#include "MFModels/LowRankMFModel.h"

#include "MFModels/P/RandConst.h"
#include "MFModels/P/IndependentMixtureModels.h"
#include "MFModels/P/AdditiveGaussian.h"
#include "MFModels/P/DGMM.h"
#include "MFModels/P/Parcellation.h"
#include "MFModels/P/PottsParcellation.h"

#include "MFModels/H/MultivariateNormal.h"
#include "MFModels/H/SpikeSlab.h"

#include "MFModels/A/KroneckerHRF.h"
#include "MFModels/A/MultivariateNormal.h"
#include "MFModels/A/AdditiveMultivariateNormal.h"
#include "MFModels/A/RandConst.h"

#include "MFModels/Psi/GammaPrecision.h"

#include "ModuleList.h"
#include "VBModules/Constant.h"
#include "VBModules/PrecisionMatrices/SharedGammaPrecision.h"
#include "VBModules/PrecisionMatrices/ComponentwiseGammaPrecision.h"
#include "VBModules/PrecisionMatrices/Wishart.h"
#include "VBModules/PrecisionMatrices/HierarchicalWishart.h"
#include "VBModules/MatrixMeans/IndependentGaussian.h"
#include "VBModules/MatrixMeans/IndependentMixtureModel.h"
#include "VBModules/MatrixPrecisions/IndependentGamma.h"
#include "VBModules/MembershipProbabilities/IndependentDirichlet.h"
#include "VBModules/MembershipProbabilities/DynamicDirichlet.h"

#include "VBDistributions/Dirichlet.h"

#include "Utilities/DataNormalisation.h"
#include "Utilities/FastApproximateMaths.h"
#include "Utilities/HRFModelling.h"
#include "Utilities/LinearAlgebra.h"
#include "Utilities/MatrixManipulations.h"
#include "Utilities/MiscMaths.h"
#include "Utilities/Random.h"
#include "Utilities/RandomSVD.h"
#include "Utilities/Timer.h"


using namespace PROFUMO;
namespace linalg = PROFUMO::Utilities::LinearAlgebra;


// Count lines
// find Source/ PROFUMO.c++ Test.c++ -name '*.h' -o -name '*.c++' | xargs wc -l
// find Scripts/ Visualisation/ -name '*.py' -o -name '*.sh' | xargs wc -l

// sloccount C++/Source/ C++/*.c++ Python/ Scripts/ Documentation/
// Doesn't count `.c++`!!
// git clone https://git.fmrib.ox.ac.uk/samh/profumo.git
// cd profumo/
// find . -name "*.c++" -exec bash -c 'mv "$1" "${1%.c++}".cpp' - '{}' \;
// sloccount .

// Check for licence tags
// http://unix.stackexchange.com/q/26836
// grep -rL "CCOPYRIGHT" --include="*.h" --include="*.c++" --include="*.cpp" .
// grep -rL "SHBASECOPYRIGHT" --include="*.py" --include="*.sh" .

// Sort blank lines at EOF
// https://stackoverflow.com/questions/729692/why-should-text-files-end-with-a-newline
// http://stackoverflow.com/a/4865030
// Test: for i in $(find . -name '*.h' -o -name '*.c++'); do  if diff /dev/null "$i" | tail -1 |   grep '^\\ No newline' > /dev/null; then  echo $i;  fi; done
// Fix: for i in $(find . -name '*.h' -o -name '*.c++'); do  if diff /dev/null "$i" | tail -1 |   grep '^\\ No newline' > /dev/null; then echo >> "$i";  fi; done

// Convert between float & double
// https://softwareengineering.stackexchange.com/questions/188721/when-do-you-use-float-and-when-do-you-use-double
// https://stackoverflow.com/questions/2386772/difference-between-float-and-double
// THIS SECTION WILL GET MANGLED BY THE COMMANDS BELOW! COPY IT SOMEWHERE SAFE FIRST!
// https://unix.stackexchange.com/questions/112023/how-can-i-replace-a-string-in-a-files
// http://unix.stackexchange.com/q/102191
// Check for any existing usage of floats / doubles that aren't in a code context
/*
find Source/ PROFUMO.c++ Test.c++ -type f \( -name '*.h' -o -name '*.c++' \) \
  -exec grep -E 'arma::fmat|arma::fvec|arma::frowvec|float' {} +
find Source/ PROFUMO.c++ Test.c++ -type f \( -name '*.h' -o -name '*.c++' \) \
  -exec grep -E 'arma::mat[^ >(&*]|arma::vec[^ >(&*]|arma::rowvec[^ >(&*]|double[^ >(&*]' {} +
*/
// Replace!
/*
find Source/ PROFUMO.c++ Test.c++ -type f \( -name '*.h' -o -name '*.c++' \) \
  -exec sed -i 's/arma::mat/arma::fmat/g; s/arma::vec/arma::fvec/g; s/arma::rowvec/arma::frowvec/g; s/double/float/g; s/arma::fvectorise/arma::vectorise/g' {} +
*/

// Float or double literals? Recommendation: use e.g. 1.0f
// https://stackoverflow.com/questions/7662109/should-we-generally-use-float-literals-for-floats-instead-of-the-simpler-double

// Namespace usage
// http://stackoverflow.com/questions/1452721/why-is-using-namespace-std-considered-bad-practice
// http://stackoverflow.com/questions/8681714/correct-way-to-define-c-namespace-methods-in-cpp-file

// Const pointers etc
// http://stackoverflow.com/questions/3888470/c-const-member-function-that-returns-a-const-pointer-but-what-type-of-const

// Profiling
// http://stackoverflow.com/questions/375913/what-can-i-use-to-profile-c-code-in-linux
// valgrind --tool=callgrind ./(Your binary)
// Then look at with kcachegrind or Gprof2Dot
// e.g. ./gprof2dot.py -f callgrind callgrind.out.x | dot -Tsvg -o output.svg

// How memory layout makes polymorphism work. Awesome.
// http://www.cs.bgu.ac.il/~spl121/Inheritance

// Useful multiple inheritance stuff
// http://www.cprogramming.com/tutorial/multiple_inheritance.html
// http://www.cprogramming.com/tutorial/virtual_inheritance.html
// http://stackoverflow.com/questions/2659116/how-does-virtual-inheritance-solve-the-diamond-problem
// http://stackoverflow.com/questions/18398409/c-inherit-from-multiple-base-classes-with-the-same-virtual-function-name
// http://stackoverflow.com/questions/6944014/c-multiple-inheritance-and-templates

// Pointers v references
// http://www.embedded.com/electronics-blogs/programming-pointers/4023307/References-vs-Pointers
// http://stackoverflow.com/questions/8005514/is-returning-references-of-member-variables-bad-pratice/8005559#8005559
// http://stackoverflow.com/questions/2182408/return-a-const-reference-or-a-copy-in-a-getter-function
// http://stackoverflow.com/questions/7058339/c-when-to-use-references-vs-pointers


int main(int, char**)
{
    std::cout << std::string(80, '*') << std::endl;
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Set the seeds to a random value
    //arma::arma_rng::set_seed_random();
    //Utilities::set_seed_random();
    
    // Set the seeds to a fixed value
    arma::arma_rng::set_seed(42);
    Utilities::set_seed(42);
    // For deterministic programs, need to be single threaded!
    // export OMP_NUM_THREADS=1
    // On Mac, this includes the Accelerate framework as well as OpenMP
    // export VECLIB_MAXIMUM_THREADS=1
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Set the number of threads
    #ifdef _OPENMP
        if (omp_get_max_threads() > 6) {
            omp_set_num_threads(6);
        }
        std::cout << "Multi-threaded using OpenMP (max " << omp_get_max_threads() << " threads)" << std::endl;
    #else
        std::cout << "Single threaded (compiled without OpenMP)" << std::endl;
    #endif
    
    /*
    std::cout << omp_get_max_threads() << std::endl;
    #pragma omp parallel for num_threads(std::min(6, omp_get_max_threads()))
    for (unsigned int i = 0; i < 10; ++i) {
        #pragma omp critical
        {
        std::cout << "Thread: " << i << " of " << omp_get_num_threads() << std::endl;
        }
    }
    */
    
    ////////////////////////////////////////////////////////////////////////////
    // Gaussian
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Gaussian" << std::endl << std::endl;
        
        namespace VBDists = PROFUMO::VBDistributions;
        
        VBDists::Gaussian::Parameters prior;
        prior.mu = 0.0f;
        prior.sigma2 = 1.0f;
        
        VBDists::Gaussian gDist(prior, false);
        std::cout << "sizeof(float): " << sizeof(float) << std::endl;
        std::cout << "sizeof(Gaussian): " << sizeof(gDist) << std::endl;
        std::cout << std::endl;
        
        std::cout << "Prior" << std::endl;
        std::cout << "-----" << std::endl;
        std::cout << "E[x]: " << gDist.getExpectations().x << std::endl;
        std::cout << "E[x2]: " << gDist.getExpectations().x2 << std::endl;
        std::cout << "KL: " << gDist.getKL() << std::endl << std::endl;
        
        std::cout << "Data" << std::endl;
        std::cout << "----" << std::endl;
        VBDistributions::DataExpectations::Gaussian D;
        unsigned int N = 10; float precision = 0.5f; float mean = 1.5f;
        D.psi = N * precision; D.psi_d = N * precision * mean;
        std::cout << "N: " << N << std::endl;
        std::cout << "Mean: " << mean << std::endl;
        std::cout << "Precision: " << precision << std::endl << std::endl;
        gDist.update(D);
        
        std::cout << "Posterior" << std::endl;
        std::cout << "---------" << std::endl;
        std::cout << "E[x]: " << gDist.getExpectations().x << std::endl;
        std::cout << "E[x2]: " << gDist.getExpectations().x2 << std::endl;
        std::cout << "KL: " << gDist.getKL() << std::endl << std::endl;
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Gaussian mixture model
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Gaussian mixture model" << std::endl << std::endl;
        
        namespace VBDists = PROFUMO::VBDistributions;
        
        typedef VBDists::GaussianMixtureModel<1,1> GMM;
        std::array<GMM::DeltaParameters, 1> deltaPriors;
        deltaPriors[0].mu = 0.0f; deltaPriors[0].p = 0.9f;
        std::array<GMM::GaussianParameters, 1> gaussianPriors;
        gaussianPriors[0].mu = 0.0f; gaussianPriors[0].sigma2 = 1.0f; gaussianPriors[0].p = 0.1f;
        
        GMM gmmDist(deltaPriors, gaussianPriors, false);
        std::cout << "sizeof(float): " << sizeof(float) << std::endl;
        std::cout << "sizeof(GMM<1,1>): " << sizeof(gmmDist) << std::endl;
        std::cout << std::endl;
        
        std::cout << "Prior" << std::endl;
        std::cout << "-----" << std::endl;
        std::cout << "E[x]: " << gmmDist.getExpectations().x << std::endl;
        std::cout << "E[x2]: " << gmmDist.getExpectations().x2 << std::endl;
        std::cout << "KL: " << gmmDist.getKL() << std::endl << std::endl;
        
        std::cout << "Data" << std::endl;
        std::cout << "----" << std::endl;
        VBDistributions::DataExpectations::Gaussian D;
        unsigned int N = 10; float precision = 0.5f; float mean = 1.5f;
        D.psi = N * precision; D.psi_d = N * precision * mean;
        std::cout << "N: " << N << std::endl;
        std::cout << "Mean: " << mean << std::endl;
        std::cout << "Precision: " << precision << std::endl << std::endl;
        gmmDist.update(D);
        
        std::cout << "Posterior" << std::endl;
        std::cout << "---------" << std::endl;
        std::cout << "E[x]: " << gmmDist.getExpectations().x << std::endl;
        std::cout << "E[x2]: " << gmmDist.getExpectations().x2 << std::endl;
        std::cout << "KL: " << gmmDist.getKL() << std::endl << std::endl;
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Gamma distribution
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Gamma distribution" << std::endl << std::endl;
        
        namespace VBDists = PROFUMO::VBDistributions;
        
        VBDists::Gamma::Parameters prior;
        prior.a = 1.0f;
        prior.b = 1.0f;
        
        VBDists::Gamma gDist(prior);
        std::cout << "sizeof(float): " << sizeof(float) << std::endl;
        std::cout << "sizeof(Gamma): " << sizeof(gDist) << std::endl;
        std::cout << std::endl;
        
        std::cout << "Prior" << std::endl;
        std::cout << "-----" << std::endl;
        std::cout << "E[std]: " << std::sqrt( 1.0f / gDist.getExpectations().x ) << std::endl;
        std::cout << "KL: " << gDist.getKL() << std::endl << std::endl;
        
        std::cout << "Data" << std::endl;
        std::cout << "----" << std::endl;
        const unsigned int N = 10;
        const arma::fvec r = 3.0f * arma::randn<arma::fvec>(N);
        VBDists::Gamma::DataExpectations D;
        D.n = r.n_elem;
        D.d2 = arma::accu(arma::square(r));
        std::cout << "N: " << N << std::endl;
        std::cout << "std: " << arma::stddev(r) << std::endl << std::endl;
        gDist.update(D);
        
        std::cout << "Posterior" << std::endl;
        std::cout << "---------" << std::endl;
        std::cout << "E[std]: " << std::sqrt( 1.0f / gDist.getExpectations().x ) << std::endl;
        std::cout << "KL: " << gDist.getKL() << std::endl << std::endl;
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Dirichlet distribution
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Dirichlet distribution" << std::endl << std::endl;
        
        namespace VBDists = PROFUMO::VBDistributions;
        
        const unsigned int nClasses = 5;
        VBDists::Dirichlet<nClasses>::Parameters prior;
        std::copy_n(
                arma::fvec(arma::randu<arma::fvec>(nClasses)).begin(),
                nClasses, std::begin(prior.counts));
        
        VBDists::Dirichlet<nClasses> dDist(prior);
        std::cout << "sizeof(float): " << sizeof(float) << std::endl;
        std::cout << "sizeof(Dirichlet<" << nClasses << ">): " << sizeof(dDist) << std::endl;
        std::cout << std::endl;
        
        std::cout << "Prior" << std::endl;
        std::cout << "-----" << std::endl;
        const auto pr_p = dDist.getExpectations().p;
        arma::fvec arma_pr_p(nClasses); for (unsigned int c = 0; c < nClasses; ++c) {arma_pr_p[c] = pr_p[c];}
        std::cout << arma_pr_p;
        std::cout << "sum(P): " << arma::sum(arma_pr_p) << std::endl;
        std::cout << "KL: " << dDist.getKL() << std::endl << std::endl;
        
        std::cout << "Data" << std::endl;
        std::cout << "----" << std::endl;
        VBDists::Dirichlet<nClasses>::DataExpectations D;
        const arma::fvec d = 3.0f * arma::randu<arma::fvec>(nClasses);
        std::copy_n(d.begin(), nClasses, std::begin(D.counts));
        std::cout << arma::fvec(d) << std::endl;
        dDist.update(D);
        
        std::cout << "Posterior" << std::endl;
        std::cout << "---------" << std::endl;
        const auto po_p = dDist.getExpectations().p;
        arma::fvec arma_po_p(nClasses); for (unsigned int c = 0; c < nClasses; ++c) {arma_po_p[c] = po_p[c];}
        std::cout << arma_po_p;
        std::cout << "sum(P): " << arma::sum(arma_po_p) << std::endl;
        std::cout << "KL: " << dDist.getKL() << std::endl << std::endl;
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Profile PtD
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Profile PtD" << std::endl << std::endl;
        
        Utilities::Timer timer;
        
        //Generate some random data
        int V = 90000;
        int T = 1200;
        int M = 100;
        
        arma::fmat D = arma::randn<arma::fmat>(V,T);
        arma::fmat P = arma::randn<arma::fmat>(V,M);
        arma::fmat DP = arma::randn<arma::fmat>(V,300);
        arma::fmat DA = arma::randn<arma::fmat>(300,T);
        
        timer.reset();
        arma::fmat DtP = P.t() * D;
        timer.printTimeElapsed();
        
        P = arma::randn<arma::fmat>(V,M);
        timer.reset();
        DtP = P.t() * D;
        timer.printTimeElapsed();
        
        timer.reset();
        DtP = (P.t() * DP) * DA;
        timer.printTimeElapsed();
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // NIFTI Shizzle
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "NIFTI Shizzle" << std::endl << std::endl;
        
        arma::fmat data;
        
        {
            // Set up NIFTI reader
            NiftiIO Reader;
            std::string filename = "???";
            char* buffer;
            std::vector<NiftiExtension> extensions;
            bool allocateBuffer = true;
            
            // Load data
            NiftiHeader header = Reader.loadImage(filename, buffer, extensions, allocateBuffer);
            
            // Few sanity checks
            std::cout << header.dim[5] << std::endl << header.dim[6] << std::endl;
            std::cout << header.bitsPerVoxel << std::endl;
            
            // Make an arma matrix using this (header.bitsPerVoxel tells us this is a load of floats)
            bool copyAuxMem = false, strict = true;
            arma::fmat tempData = arma::fmat((float*) buffer, header.dim[5], header.dim[6], copyAuxMem, strict);
            
            // Convert to a matrix of floats
            data = arma::conv_to<arma::fmat>::from( tempData.t() );
            
            // Clean up
            delete buffer;
        }
        
        // Demean
        arma::fvec mean = arma::mean(data,1);
        data.each_col() -= mean;
        
        // Normalise variances
        arma::fvec std = arma::stddev(data,0,1);
        data.each_col() /= std;
        
        // Save to file
        std::string fileName = "testNIFTI.txt";
        std::ofstream file;
        file.open( fileName.c_str() );
        file << data;
        file.close();
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // HDF5 Shizzle
    /*
    //#include <hdf5.h>
    //#include <H5Cpp.h>
    // NEED TO ADD -lhdf5_cpp TO LDLIBS IN MAKEFILE!
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "HDF5 Shizzle" << std::endl << std::endl;
        
        // Generate some data
        const unsigned int M = 10000, N = 100;
        arma::fmat X = arma::randn<arma::fmat>(M,N);
        X.elem( arma::find(arma::abs(X) < 1.0e-10f) ).zeros();
        X.elem( arma::find(arma::abs(X) < 1.0f) ).zeros();
        
        // Save with Armadillo
        Utilities::Timer timer;
        X.save("arma.hdf5", arma::hdf5_binary);
        timer.printTimeElapsed(3); timer.reset();
        
        // Save with HDF5
        // https://support.hdfgroup.org/HDF5/Tutor/compress.html
        // https://support.hdfgroup.org/ftp/HDF5/current/src/unpacked/c++/examples/h5tutr_cmprss.cpp
        // arma::diskio_meat
        const hsize_t dims[2] = {M,N};
        const hsize_t chunkDims[2] = {M,1};
        
        H5::H5File *file = new H5::H5File("bespoke.hdf5", H5F_ACC_TRUNC);
        
        H5::DataSpace *dataspace = new H5::DataSpace(2, dims);
        
        H5::DSetCreatPropList *plist = new H5::DSetCreatPropList;
        plist->setChunk(2, chunkDims);
        plist->setDeflate(9);
        
        H5::DataSet *dataset = new H5::DataSet( file->createDataSet("dataset", H5T_NATIVE_DOUBLE, *dataspace, *plist) );
        
        // Write data to dataset.
        dataset->write(X.mem, H5T_NATIVE_DOUBLE);
        
        // Close objects and file.  Either approach will close the HDF5 item.
        delete dataspace;
        delete dataset;
        delete plist;
        //file->close();
        delete file;
        
        timer.printTimeElapsed(3);
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Check the HRF shizzle
    /*
    {
        const std::string hrfFile = "../HRFs/Default.phrf";
        
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "HRF Shizzle" << std::endl << std::endl;
        
        std::cout << Utilities::generateHRF(hrfFile, 10, 2.0f) << std::endl;
        
        MFModels::A::KroneckerHRF::Parameters hrfPrior = Utilities::generateHRFPrior(hrfFile, 1200, 0.72f, 50, 0.01f, 0.05f);
        std::cout << hrfPrior.K->submat(0,0,9,9) << std::endl;
        //hrfPrior.hrf->save("HRF.txt", arma::raw_ascii);
        //hrfPrior.K->save("K.txt", arma::raw_ascii);
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Check the random SVD
    
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Random SVD" << std::endl << std::endl;
        
        // Generate some random data
        const unsigned int M = 10000, N = 1250, C = 50, K = 100;
        //const unsigned int M = 2500, N = 5000, C = 50, K = 100;
        // Data: MxN matrix; C: true subspace rank; K: requested rank.
        arma::fmat Pd = 2.0f * (arma::randu<arma::fmat>(M,C) - 0.5f);
        arma::fmat D1 = Pd * arma::randn<arma::fmat>(C,N) + arma::randn<arma::fmat>(M,N);
        arma::fmat D2 = Pd * arma::randn<arma::fmat>(C,N) + arma::randn<arma::fmat>(M,N);
        arma::fmat D = arma::zeros<arma::fmat>(M,2*N);
        D.cols(0,N-1) = D1; D.cols(N,2*N-1) = D2;
        
        
        // Compute standard SVD
        Utilities::Timer timer;
        arma::fmat U, V; arma::fvec s;
        svd_econ(U,s,V,D);
        U = U.head_cols(K); s = s.head(K); V = V.head_cols(K);
        timer.printTimeElapsed("Full SVD", 3);
        
        // Compute random SVD
        timer.reset();
        SVD svd = Utilities::computeRandomSVD(D, K);
        timer.printTimeElapsed("Random SVD", 3);
        
        // Compute random concatenated SVD
        timer.reset();
        std::vector<const arma::fmat*> Dv; Dv.push_back( &D1 ); Dv.push_back( &D2 );
        ConcatenatedSVD csvd = Utilities::computeRandomConcatenatedSVD(Dv, K);
        timer.printTimeElapsed("Random concatenated SVD", 3);
        
        std::cout << std::endl;
        
        
        // Look at variance explained
        std::cout << "Standard deviations:" << std::endl;
        std::cout << std::endl;
        
        std::cout << "D1: " << arma::stddev(arma::vectorise( D1 )) << std::endl;
        std::cout << "D1 - Full SVD:    " << arma::stddev(arma::vectorise( D1 - U * arma::diagmat(s) * V.rows(0,N-1).t() )) << std::endl;
        std::cout << "D1 - Random SVD:  " << arma::stddev(arma::vectorise( D1 - svd.U * arma::diagmat(svd.s) * svd.V.rows(0,N-1).t() )) << std::endl;
        std::cout << "D1 - Random cSVD: " << arma::stddev(arma::vectorise( D1 - csvd.U * arma::diagmat(csvd.s) * csvd.V[0].t() )) << std::endl;
        
        std::cout << std::endl;
        
        std::cout << "D2: " << arma::stddev(arma::vectorise( D2 )) << std::endl;
        std::cout << "D2 - Full SVD:    " << arma::stddev(arma::vectorise( D2 - U * arma::diagmat(s) * V.rows(N,2*N-1).t() )) << std::endl;
        std::cout << "D2 - Random SVD:  " << arma::stddev(arma::vectorise( D2 - svd.U * arma::diagmat(svd.s) * svd.V.rows(N,2*N-1).t() )) << std::endl;
        std::cout << "D2 - Random cSVD: " << arma::stddev(arma::vectorise( D2 - csvd.U * arma::diagmat(csvd.s) * csvd.V[1].t() )) << std::endl;
        
        std::cout << std::endl;
        
        
        // Look at accuracy
        std::cout << "Signal subspace accuracies:" << std::endl;
        std::cout << std::endl;
        
        arma::fmat D1_svd = U.head_cols(C) * arma::diagmat(s.head(C)) * V.submat(0,0,N-1,C-1).t();
        std::cout << "D1: Full SVD - Random SVD:  " << arma::mean(arma::vectorise(arma::abs( D1_svd - svd.U.head_cols(C) * arma::diagmat(svd.s.head(C)) * svd.V.submat(0,0,N-1,C-1).t() ))) << std::endl;
        std::cout << "D1: Full SVD - Random cSVD: " << arma::mean(arma::vectorise(arma::abs( D1_svd - csvd.U.head_cols(C) * arma::diagmat(csvd.s.head(C)) * csvd.V[0].head_cols(C).t() ))) << std::endl;
        
        std::cout << std::endl;
        
        arma::fmat D2_svd = U.head_cols(C) * arma::diagmat(s.head(C)) * V.submat(N,0,2*N-1,C-1).t();
        std::cout << "D2: Full SVD - Random SVD:  " << arma::mean(arma::vectorise(arma::abs( D2_svd - svd.U.head_cols(C) * arma::diagmat(svd.s.head(C)) * svd.V.submat(N,0,2*N-1,C-1).t() ))) << std::endl;
        std::cout << "D2: Full SVD - Random cSVD: " << arma::mean(arma::vectorise(arma::abs( D2_svd - csvd.U.head_cols(C) * arma::diagmat(csvd.s.head(C)) * csvd.V[1].head_cols(C).t() ))) << std::endl;
        
        std::cout << std::endl;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Check MiscMaths
    /*
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "Maths routines from Utilities" << std::endl << std::endl;
        
        // Check exponential approximation
        std::cout << Utilities::FastApprox::exp( 22.5f ) << " (" << std::exp( 22.5f ) << ")" << std::endl;
        std::cout << Utilities::FastApprox::exp( -22.15f ) << " (" << std::exp( -22.15f ) << ")"  << std::endl;
        
        //for (int i = 0; i < 101; ++i) {
        //    float p = 4.0f + 0.04f * i;
        //    std::cout << p << "(" << std::pow(2,p) << "): " << std::pow(2,p) - Utilities::FastApprox::pow2(p) << " (" << std::pow(2,p) - fastpow2(p) << ")" << std::endl;
        //    std::cout << p << " (" << std::pow(2,p) << "): " << Utilities::FastApprox::pow2(p) << " (" << fastpow2(p) << ")" << std::endl << std::endl;
        //}
        
        int V = 100000, M = 100;
        
        for (int z=0; z<5; ++z) {
            
            // Check accuracy
            arma::fmat A = 10.0f * arma::randn<arma::fmat>(V,M);
            arma::fmat A2 = A;
            arma::fmat B = A;
            arma::fmat C = A;
            
            Utilities::Timer timer;
            //A2 = arma::exp(A2);
            for (auto& a : A2) {
                a = std::exp(a);
            }
            timer.printTimeElapsed();
            A2 = arma::log(A2);
            
            timer.reset();
            for (auto& b : B) {
                b = Utilities::FastApprox::exp(b);
            }
            timer.printTimeElapsed();
            B = arma::log(B);
            
            std:: cout << "A: " << arma::mean(arma::mean( arma::abs( A ) )) << std::endl;
            std:: cout << "A2: " << arma::mean(arma::mean( arma::abs( A2 ) )) << std::endl;
            std:: cout << "A - A2: " << arma::mean(arma::mean( arma::abs( A - A2 ) )) << std::endl;
            std:: cout << "B: " << arma::mean(arma::mean( arma::abs( B ) )) << std::endl;
            std:: cout << "A - B: " << arma::mean(arma::mean( arma::abs( A - B ) )) << std::endl;
            std::cout << std::endl;
            
            
            arma::fmat sA = 10.0f * arma::randn<arma::fmat>(V,M);
            arma::fmat sB = sA;
            arma::fmat sC = sA;
            
            //Random order to update maps
            arma::uvec inds = arma::sort_index( arma::randu<arma::fvec>(M) );
            
            // arma::pow( )
            timer.reset();
            for (arma::uvec::const_iterator m=inds.begin(); m!=inds.end(); ++m) {
                sA.col(*m) = arma::pow(1.0f + arma::trunc_exp(-sA.col(*m)), -1);
            }
            timer.printTimeElapsed();
            
            // 1.0 / arma::fmat
            timer.reset();
            for (arma::uvec::const_iterator m=inds.begin(); m!=inds.end(); ++m) {
                sB.col(*m) = 1.0f / (1.0f + arma::trunc_exp(-sB.col(*m)));
            }
            timer.printTimeElapsed();
            
            timer.reset();
            for (arma::uvec::const_iterator m=inds.begin(); m!=inds.end(); ++m) {
                for (int v = 0; v < V; ++v) {
                    //float& b = sC(v,*m);
                    //if ( b < -10.0f ) {
                    //    b = 0.0f;
                    //}
                    //else if ( b > 10.0f ) {
                    //    b = 1.0f;
                    //}
                    //else {
                    //    b = 1.0f / ( 1.0f + std::exp( -b ) );
                    //}
                    sC(v,*m) = Utilities::MiscMaths::sigmoid(sC(v,*m));
                }
            }
            timer.printTimeElapsed();
            
            std:: cout << "A: " << arma::mean(arma::vectorise( arma::abs( sA ) )) << std::endl;
            std:: cout << "B: " << arma::mean(arma::vectorise( arma::abs( sB ) )) << std::endl;
            std:: cout << "C: " << arma::mean(arma::vectorise( arma::abs( sC ) )) << std::endl;
            std:: cout << "A - B: " << arma::mean(arma::vectorise( arma::abs( sA - sB ) )) << std::endl;
            std:: cout << "A - C: " << arma::mean(arma::vectorise( arma::abs( sA - sC ) )) << std::endl;
            std::cout << std::endl;
        }
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Check cosine similarity
    /*
    {
        const arma::fmat X = arma::randn<arma::fmat>(100,3);
        const arma::fmat Y = arma::randn<arma::fmat>(100,5);
        
        // Sanity check cosine similarity
        std::cout << Utilities::MatrixManipulations::calculateCosineSimilarity(X, Y) << std::endl;
        std::cout << Utilities::MatrixManipulations::calculateCosineSimilarity(X) << std::endl;
        std::cout << Utilities::MatrixManipulations::calculateCosineSimilarity(Y) << std::endl;
        std::cout << Utilities::MatrixManipulations::calculateCosineSimilarity(arma::join_horiz(X,Y)) << std::endl;
        std::cout << std::endl;
        
        // Find a pairing between X and Y
        std::cout << Utilities::MatrixManipulations::calculateCosineSimilarity(X, Y) << std::endl;
        
        Utilities::MatrixManipulations::Pairing pairing 
            = Utilities::MatrixManipulations::pairMatrixColumns(X, Y);
        
        std::cout << pairing.inds1 << std::endl;
        std::cout << pairing.inds2 << std::endl;
        std::cout << pairing.scores << std::endl;
    }
    */
    ////////////////////////////////////////////////////////////////////////////
    // Full MFModel
    
    {
        std::cout << std::string(80, '*') << std::endl;
        std::cout << "MFModel test" << std::endl << std::endl;
        
        std::cout << "Initialising..." << std::endl << std::endl;
        
        //Size of simulated data
        const unsigned int V = 100'000;
        const unsigned int T = 1500;
        const unsigned int N = 30;     // True number of modes
        const unsigned int M = 50;     // Number to infer
        //const float dofCorrectionFactor = 1.0f;
        const float dofCorrectionFactor = 0.1f;
        const float TR = 0.5f;
        
        //**********************************************************************
        // Generate data
        
        // Modes
        arma::fmat Pd = 2.0f * (arma::randu<arma::fmat>(V,N) - 0.5f)
                        + 0.1f * arma::randn<arma::fmat>(V,N);
        arma::fmat Ad = 1.0f * arma::randn<arma::fmat>(N,T);
        //Ad = 0.1f * arma::randn<arma::fmat>(N,N) * Ad;
        arma::fmat D = Pd * Ad + 4.0f * arma::randn<arma::fmat>(V,T) + 10.0f;
        
        /*
        // Parcellation
        arma::fmat Pd = arma::zeros<arma::fmat>(V,N);
        for (int v = 0; v < V; ++v) {
            //int n = v % N; Pd(v,n) = 1.0f;
            int n = std::floor(N * v / V); Pd(v,n) = 1.0f;
        }
        arma::fmat Ad = 1.0f * arma::randn<arma::fmat>(N,T);
        arma::fmat D = Pd * Ad + 1.0f * arma::randn<arma::fmat>(V,T);
        */
        
        std::cout << "std(D): " << arma::stddev( arma::vectorise(D) ) << std::endl;
        std::cout << "std(P*A): " << arma::stddev( arma::vectorise(Pd * Ad) ) << std::endl;
        std::cout << "std(D - P*A): " << arma::stddev( arma::vectorise(D - Pd * Ad) ) << std::endl;
        std::cout << std::endl;
        
        //**********************************************************************
        // Normalise the data
        
        /*
        Utilities::DataNormalisation::demeanGlobally(D);
        std::cout << linalg::accu(D) / (V * T) << std::endl;
        Utilities::DataNormalisation::demeanRows(D);
        std::cout << linalg::sum(D, 1).at(0) / T << std::endl;
        Utilities::DataNormalisation::demeanColumns(D);
        std::cout << linalg::sum(D, 0).at(0) / V << std::endl;
        Utilities::DataNormalisation::normaliseGlobally(D);
        std::cout << linalg::accu(arma::square(D)) / (V * T) << std::endl;
        Utilities::DataNormalisation::normaliseRows(D);
        std::cout << linalg::sum(arma::square(D), 1).at(0) / T << std::endl;
        Utilities::DataNormalisation::normaliseColumns(D);
        std::cout << linalg::sum(arma::square(D), 0).at(0) / V << std::endl;
        Utilities::DataNormalisation::normaliseSignalSubspaceGlobally(D, M);
        Utilities::DataNormalisation::normaliseSignalSubspaceRows(D, M);
        Utilities::DataNormalisation::normaliseSignalSubspaceColumns(D, M);
        Utilities::DataNormalisation::normaliseNoiseSubspaceGlobally(D, M);
        Utilities::DataNormalisation::normaliseNoiseSubspaceRows(D, M);
        Utilities::DataNormalisation::normaliseNoiseSubspaceColumns(D, M);
        */
        
        Utilities::DataNormalisation::demeanRows(D);
        Utilities::DataNormalisation::normaliseRows(D);
        Utilities::DataNormalisation::normaliseNoiseSubspaceRows(D, M);
        Utilities::DataNormalisation::normaliseSignalSubspaceGlobally(D, M);
        D *= std::sqrt((float) M);
        
        //D *= 100.0f;
        
        //std::cout << arma::accu( arma::abs(D) < 1.0e-10f ) << std::endl;
        //D.elem( arma::find(arma::abs(D) < 1.0e-10f) ).zeros();
        
        std::cout << "std(D): " << arma::stddev( arma::vectorise(D) ) << std::endl;
        SVD svd = Utilities::computeRandomSVD(D, M);
        std::cout << "std(signal): " << arma::stddev( arma::vectorise(svd.U * arma::diagmat(svd.s) * svd.V.t()) ) << std::endl;
        //std::cout << "std(signal): " << std::sqrt(linalg::accu(arma::square(svd.U * arma::diagmat(svd.s) * svd.V.t())) / D.n_elem) << std::endl;
        std::cout << "std(noise):  " << arma::stddev( arma::vectorise(D - svd.U * arma::diagmat(svd.s) * svd.V.t()) ) << std::endl;
        //std::cout << "std(noise):  " << std::sqrt(linalg::accu(arma::square(D - svd.U * arma::diagmat(svd.s) * svd.V.t())) / D.n_elem) << std::endl;
        std::cout << std::endl;
        
        /*
        // Full rank data
        DataTypes::FullRankData FRD(D);
        */
        
        
        // Low rank approximation
        unsigned int N_LRD = 300;
        float TrDtD_LRD = linalg::trace_XtX(D);
        // Calculate SVD
        SVD svd_LRD = Utilities::computeRandomSVD(D, N_LRD);
        // Take components
        std::shared_ptr<arma::fmat> Pd_LRD = std::make_shared<arma::fmat>();
        std::shared_ptr<arma::fmat> Ad_LRD = std::make_shared<arma::fmat>();
        *Pd_LRD = svd_LRD.U * arma::diagmat(arma::sqrt( svd_LRD.s ));
        *Ad_LRD = arma::diagmat(arma::sqrt( svd_LRD.s )) * svd_LRD.V.t();
        //std::cout << arma::accu( arma::abs(*Pd_LRD) < 1.0e-10f ) << " - " << arma::accu( arma::abs(*Ad_LRD) < 1.0e-10f ) << std::endl;
        DataTypes::LowRankData LRD(Pd_LRD, Ad_LRD, TrDtD_LRD);
        //std::cout << "D: " << D.submat(0,0,9,9) << std::endl;
        //std::cout << "Pd: " << Pd_LRD->submat(0,0,9,9) << std::endl;
        //std::cout << "Ad: " << Ad_LRD->submat(0,0,9,9) << std::endl;
        std::cout << "Low-rank data:" << std::endl;
        std::cout << "std(D): " << arma::stddev( arma::vectorise(D) ) << std::endl;
        std::cout << "std(Pd): " << arma::stddev( arma::vectorise(*Pd_LRD) ) << std::endl;
        std::cout << "std(Ad): " << arma::stddev( arma::vectorise(*Ad_LRD) ) << std::endl;
        std::cout << std::endl;
        
        
        //**********************************************************************
        // First, make a constant spatial model
        
        MFModels::P_VBPosterior* Pconst;
        
        {
            arma::fmat initialisation = Pd * arma::randn<arma::fmat>(N,M);
            Utilities::DataNormalisation::normaliseGlobally(initialisation);
            initialisation += 0.25f * arma::randn<arma::fmat>(V,M);
            // Orthogonalise - makes a bit more realistic (i.e. we normally
            // start from the SVD)
            initialisation = linalg::closest_orth(initialisation);
            Utilities::DataNormalisation::normaliseColumns(initialisation);
            Utilities::MatrixManipulations::flipColumnSigns(initialisation);
            
            MFModels::P::P2C Einit; Einit.P = initialisation;
            Einit.PtP = Einit.P.t() * Einit.P + 0.01f * V * arma::eye<arma::fmat>(M, M);
            Pconst = new VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>(Einit);
        }
        
        //**********************************************************************
        // P
        
        //const std::string typeP = "Constant";
        //const std::string typeP = "Spike-Slab";
        //const std::string typeP = "TGMM";
        //const std::string typeP = "AdditiveGaussian";
        const std::string typeP = "DGMM";
        //const std::string typeP = "Parcellation";
        std::cout << "P model: " << typeP << std::endl;
        
        MFModels::P_VBPosterior* P;
        Modules::MatrixMean_VBPosterior* tau = nullptr;
        Modules::MatrixPrecision_VBPosterior* beta = nullptr;
        Modules::MatrixPrecision_VBPosterior* kappa = nullptr;
        Modules::MembershipProbability_VBPosterior* pi = nullptr;
        
        if (typeP == "Constant") {
            MFModels::P::P2C EP;
            EP.P = arma::zeros<arma::fmat>(V,M); EP.P.cols(0, N-1) = Pd;
            EP.PtP = EP.P.t() * EP.P;
            EP.PtP += 1.0e-5f * V * arma::eye<arma::fmat>(M,M);
            //EP.PtP += 1.0e-10f * V * arma::diagmat(arma::randu<arma::fvec>(M));
            P = new VBModules::Constant<MFModels::P::C2P, MFModels::P::P2C>(EP);
        }
        
        if (typeP == "Spike-Slab") {
            // Group-level spike-slab
            typedef MFModels::P::IndependentMixtureModels<1,1> IndependentMixtureModels;
            IndependentMixtureModels::Parameters PPrior;
            PPrior.randomInitialisation = true;
            PPrior.deltas[0].mu = arma::zeros<arma::fmat>(V,M);
            PPrior.deltas[0].p = 0.9f * arma::ones<arma::fmat>(V,M);
            PPrior.gaussians[0].mu = 0.5f * arma::ones<arma::fmat>(V,M);
            PPrior.gaussians[0].sigma2 = 1.0f * arma::ones<arma::fmat>(V,M);
            PPrior.gaussians[0].p = 0.1f * arma::ones<arma::fmat>(V,M);
            P = new IndependentMixtureModels(PPrior, dofCorrectionFactor);
        }
        
        if (typeP == "TGMM") {
            // Triple-Gaussian mixture model
            typedef MFModels::P::IndependentMixtureModels<0,3> IndependentMixtureModels;
            IndependentMixtureModels::Parameters PPrior;
            PPrior.randomInitialisation = true;
            PPrior.gaussians[0].mu = 1.0f * arma::ones<arma::fmat>(V,M);
            PPrior.gaussians[0].sigma2 = std::pow(0.5f, 2) * arma::ones<arma::fmat>(V,M);
            PPrior.gaussians[0].p = 0.05f * arma::ones<arma::fmat>(V,M);
            PPrior.gaussians[1].mu = -1.0f * arma::ones<arma::fmat>(V,M);
            PPrior.gaussians[1].sigma2 = std::pow(0.5f, 2) * arma::ones<arma::fmat>(V,M);
            PPrior.gaussians[1].p = 0.025f * arma::ones<arma::fmat>(V,M);
            PPrior.gaussians[2].mu = arma::zeros<arma::fmat>(V,M);
            PPrior.gaussians[2].sigma2 = std::pow(1.0f, 2) * arma::ones<arma::fmat>(V,M);
            PPrior.gaussians[2].p = 0.925f * arma::ones<arma::fmat>(V,M);
            P = new IndependentMixtureModels(PPrior, dofCorrectionFactor);
        }
        
        if (typeP == "AdditiveGaussian") {
            // - Spike-slab group means
            typedef VBModules::MatrixMeans::IndependentMixtureModel<1,1> IndependentMixtureModels;
            IndependentMixtureModels::Parameters tauPrior;
            tauPrior.randomInitialisation = true;
            tauPrior.deltas[0].mu = arma::zeros<arma::fmat>(V,M);
            tauPrior.deltas[0].p = 0.95f * arma::ones<arma::fmat>(V,M);
            tauPrior.gaussians[0].mu = 0.5f * arma::ones<arma::fmat>(V,M);
            tauPrior.gaussians[0].sigma2 = 1.0f * arma::ones<arma::fmat>(V,M);
            tauPrior.gaussians[0].p = 0.05f * arma::ones<arma::fmat>(V,M);
            tau = new IndependentMixtureModels(tauPrior);
            
            MFModels::P::AdditiveGaussian::Parameters PPrior; PPrior.V = V; PPrior.M = M;
            PPrior.precisionPrior.a = 1.0f; PPrior.precisionPrior.b = 1.0f;
            P = new MFModels::P::AdditiveGaussian(tau, PPrior, dofCorrectionFactor);
        }
        
        if (typeP == "DGMM") {
            // - Spike-slab group means
            typedef VBModules::MatrixMeans::IndependentMixtureModel<1,1> IndependentMixtureModels;
            IndependentMixtureModels::Parameters tauPrior;
            tauPrior.randomInitialisation = true;
            tauPrior.deltas[0].mu = arma::zeros<arma::fmat>(V,M);
            tauPrior.deltas[0].p = 0.8f * arma::ones<arma::fmat>(V,M);
            tauPrior.gaussians[0].mu = 0.5f * arma::ones<arma::fmat>(V,M);
            tauPrior.gaussians[0].sigma2 = 1.0f * arma::ones<arma::fmat>(V,M);
            tauPrior.gaussians[0].p = 0.2f * arma::ones<arma::fmat>(V,M);
            tau = new IndependentMixtureModels(tauPrior);
            /*VBModules::MatrixMeans::IndependentGaussian::GaussianParameters tauPrior;
            tauPrior.gaussianPrior.mu = 0.5f * arma::ones<arma::fmat>(V,M);
            tauPrior.gaussianPrior.sigma2 = 1.0f * arma::ones<arma::fmat>(V,M);
            tau = new VBModules::MatrixMeans::IndependentGaussian(tauPrior);*/
            
            VBModules::MatrixPrecisions::IndependentGamma::Parameters betaPrior;
            betaPrior.a = 1.0f * arma::ones<arma::fmat>(V,M);
            betaPrior.b = 1.0f * arma::ones<arma::fmat>(V,M);
            beta = new VBModules::MatrixPrecisions::IndependentGamma(betaPrior);
            
            VBModules::MatrixPrecisions::IndependentGamma::Parameters kappaPrior;
            kappaPrior.a = 0.5f * arma::ones<arma::fmat>(V,1);
            kappaPrior.b = 0.1f * arma::ones<arma::fmat>(V,1);
            kappa = new VBModules::MatrixPrecisions::IndependentGamma(kappaPrior);
            
            typedef VBModules::MembershipProbabilities::IndependentDirichlet<2> IndependentDirichlet;
            IndependentDirichlet::Parameters piPrior;
            piPrior.counts = {0.5f * arma::ones<arma::fmat>(V,M), 0.5f * arma::ones<arma::fmat>(V,M)};
            pi = new IndependentDirichlet(piPrior);
            
            MFModels::P::DGMM::Parameters PPrior; PPrior.V = V; PPrior.M = M;
            P = new MFModels::P::DGMM(tau, beta, kappa, pi, PPrior, dofCorrectionFactor);
        }
        
        if (typeP == "Parcellation") {
            VBModules::MatrixMeans::IndependentGaussian::Parameters tauPrior;
            tauPrior.randomInitialisation = true;
            tauPrior.mu = 0.5f * arma::ones<arma::fmat>(V,M);
            tauPrior.sigma2 = 1.0f * arma::ones<arma::fmat>(V,M);
            tau = new VBModules::MatrixMeans::IndependentGaussian(tauPrior);
            //Modules::MatrixMeans::P2C Etau; Etau.X = 0.5f * arma::randu<arma::fmat>(V,M); Etau.X2 = arma::square(Etau.X);
            //tau = new VBModules::Constant<Modules::MatrixMeans::C2P, Modules::MatrixMeans::P2C>(Etau);
            
            VBModules::MatrixPrecisions::IndependentGamma::Parameters betaPrior;
            betaPrior.a = 1.0f * arma::ones<arma::fmat>(V,M);
            betaPrior.b = 1.0f * arma::ones<arma::fmat>(V,M);
            beta = new VBModules::MatrixPrecisions::IndependentGamma(betaPrior);
            //Modules::MatrixPrecisions::P2C Ebeta; Ebeta.X = 0.01f * arma::ones<arma::fmat>(V,M); Ebeta.logX = arma::log(Ebeta.X);
            //beta = new VBModules::Constant<Modules::MatrixPrecisions::C2P, Modules::MatrixPrecisions::P2C>(Ebeta);
            
            VBModules::MembershipProbabilities::DynamicDirichlet::Parameters piPrior;
            piPrior.counts = std::vector<arma::fmat>(M, 0.01f * arma::ones<arma::fmat>(V,1));
            pi = new VBModules::MembershipProbabilities::DynamicDirichlet(piPrior);
            //Modules::MembershipProbabilities::P2C Epi;
            //Epi.p = std::vector<arma::fmat>(M, (1.0f / M) * arma::ones<arma::fmat>(V,1));
            //Epi.logP = std::vector<arma::fmat>(M, arma::log((1.0f / M) * arma::ones<arma::fmat>(V,1)));
            //Epi.logP = std::vector<arma::fmat>(M, -0.1f * arma::ones<arma::fmat>(V,1));
            //pi = new VBModules::Constant<Modules::MembershipProbabilities::C2P, Modules::MembershipProbabilities::P2C>(Epi);
            
            MFModels::P::Parcellation::Parameters PPrior; PPrior.V = V; PPrior.M = M;
            P = new MFModels::P::Parcellation(tau, beta, pi, PPrior, dofCorrectionFactor);
            /*MFModels::P::PottsParcellation::Parameters PPrior; PPrior.beta = 100.0f; PPrior.V = V; PPrior.M = M;
            for (int v = 0; v < V; ++v) {
                arma::uvec inds;
                if ((v - 3 >= 0) && (v + 3 < V)){
                    inds << v - 1 << v - 2 << v - 3 << v + 1 << v + 2 << v + 3;
                }
                PPrior.couplings->push_back(inds);
            }
            P = new MFModels::P::PottsParcellation(tau, beta, pi, PPrior, dofCorrectionFactor);*/
        }
        
        // Old group-level models:
        // Spike-slab
        // Delta: mu = 0.0;
        // Gaussian: mu = 0.5 (typically); sigma = 1.0 (typically)
        
        // Binary mixture model
        // Delta: mu = {0.0, 1.0};
        
        // Triple-delta mixture model (TDMM)
        // Delta: mu = {-1.0, 0.0, 1.0};
        
        // Triple-Gaussian mixture model (TGMM)
        // Gaussian: mu = {-1.0, 0.0, 1.0}; all sigma = 1.0 (typically)
        
        //**********************************************************************
        // A
        
        //const std::string typeA = "Constant";
        //const std::string typeA = "MVN";
        //const std::string typeA = "Clean HRF";
        const std::string typeA = "Noisy HRF";
        //const std::string typeAlphaA = "No";
        //const std::string typeAlphaA = "Constant";
        //const std::string typeAlphaA = "SharedGamma";
        //const std::string typeAlphaA = "ComponentwiseGamma";
        //const std::string typeAlphaA = "Wishart";
        const std::string typeAlphaA = "HierarchicalWishart";
        std::cout << "A model: " << typeA << " (" << typeAlphaA << " covariance)" << std::endl;
        
        MFModels::A_VBPosterior* A;
        Modules::PrecisionMatrix_VBPosterior* alphaA = nullptr;
        Modules::PrecisionMatrix_VBPosterior* betaA = nullptr;
        
        // Precision matrix
        if (typeAlphaA == "Constant") {
            Modules::PrecisionMatrices::P2C EaA; EaA.X = 1.0f * arma::eye<arma::fmat>(M,M); EaA.logDetX = std::log(1.0f) * M;
            alphaA = new VBModules::Constant<Modules::PrecisionMatrices::C2P, Modules::PrecisionMatrices::P2C>(EaA);
        }
        if (typeAlphaA == "SharedGamma") {
            VBModules::PrecisionMatrices::SharedGammaPrecision::Parameters alphaAPrior;
            alphaAPrior.a = 1.0f; alphaAPrior.b = 1.0f; alphaAPrior.size = M;
            alphaA = new VBModules::PrecisionMatrices::SharedGammaPrecision(alphaAPrior);
        }
        if (typeAlphaA == "ComponentwiseGamma") {
            VBModules::PrecisionMatrices::ComponentwiseGammaPrecision::Parameters alphaAPrior;
            alphaAPrior.a = 1.0f * arma::ones<arma::fvec>(M); alphaAPrior.b = 1.0f * arma::ones<arma::fvec>(M);
            alphaA = new VBModules::PrecisionMatrices::ComponentwiseGammaPrecision(alphaAPrior);
        }
        if (typeAlphaA == "Wishart") {
            VBModules::PrecisionMatrices::Wishart::Parameters alphaAPrior;
            alphaAPrior.a = float(M); alphaAPrior.B = float(M) * arma::eye<arma::fmat>(M,M);
            alphaA = new VBModules::PrecisionMatrices::Wishart(alphaAPrior);
        }
        if (typeAlphaA == "HierarchicalWishart") {
            VBModules::PrecisionMatrices::Wishart::Parameters betaAPrior;
            betaAPrior.a = float(M); betaAPrior.B = arma::eye<arma::fmat>(M,M);
            betaA = new VBModules::PrecisionMatrices::Wishart(betaAPrior);
            VBModules::PrecisionMatrices::HierarchicalWishart::Parameters alphaAPrior;
            alphaAPrior.a = float(M); alphaAPrior.size = M;
            alphaA = new VBModules::PrecisionMatrices::HierarchicalWishart(betaA, alphaAPrior);
        }
        
        
        if (typeA == "Constant") {
            MFModels::A::P2C EA;
            EA.A = arma::zeros<arma::fmat>(M,T); EA.A.rows(0, N-1) = Ad;
            EA.AAt = EA.A * EA.A.t();
            A = new VBModules::Constant<MFModels::A::C2P, MFModels::A::P2C>(EA);
        }
        
        if (typeA == "MVN") {
            MFModels::A::MultivariateNormal::Parameters APrior; APrior.M = M; APrior.T = T;
            A = new MFModels::A::MultivariateNormal(alphaA, APrior);
        }
        
        if (typeA == "Clean HRF") {
            MFModels::A::KroneckerHRF::Parameters hrfPrior = Utilities::generateHRFPrior("../HRFs/Default.phrf", T, TR, M, 0.0025f, 0.025f);
            A = new MFModels::A::KroneckerHRF(alphaA, hrfPrior);
        }
        
        if (typeA == "Noisy HRF") {
            // Make HRF TCs
            MFModels::A::KroneckerHRF::Parameters hrfPrior = Utilities::generateHRFPrior("../HRFs/Default.phrf", T, TR, M, 0.0f, 0.025f);
            std::unique_ptr<MFModels::A_VBPosterior> cleanA = std::make_unique<MFModels::A::KroneckerHRF>(alphaA, hrfPrior);
            // Noise precision matrix
            VBModules::PrecisionMatrices::SharedGammaPrecision::Parameters alphaNoisePrior;
            alphaNoisePrior.a = 0.01f; alphaNoisePrior.b = 0.01f; alphaNoisePrior.size = M;
            std::unique_ptr<Modules::PrecisionMatrix_VBPosterior> alphaNoise = std::make_unique<VBModules::PrecisionMatrices::SharedGammaPrecision>(alphaNoisePrior);
            // Combined TCs
            MFModels::A::AdditiveMultivariateNormal::Parameters APrior; APrior.M = M; APrior.T = T;
            A = new MFModels::A::AdditiveMultivariateNormal(std::move(cleanA), std::move(alphaNoise), APrior);
        }
        
        //**********************************************************************
        // H
        
        //const std::string typeH = "Constant";
        const std::string typeH = "MVN";
        //const std::string typeH = "Spike-Slab";
        std::cout << "H model: " << typeH << std::endl;
        std::cout << std::endl;
        
        MFModels::H_VBPosterior* H;
        Modules::MatrixMean_VBPosterior* muH = nullptr;
        Modules::PrecisionMatrix_VBPosterior* alphaH = nullptr;
        
        if (typeH == "Constant") {
            MFModels::H::P2C EH; EH.h = arma::ones<arma::fvec>(M); EH.hht = arma::ones<arma::fmat>(M, M);
            H = new VBModules::Constant<MFModels::H::C2P, MFModels::H::P2C>(EH);
        }
        
        if (typeH == "MVN") {
            // Means
            VBModules::MatrixMeans::IndependentGaussian::Parameters muHPrior;
            muHPrior.randomInitialisation = false;
            muHPrior.mu = 1.0f * arma::ones<arma::fvec>(M);
            muHPrior.sigma2 = 0.1f * arma::ones<arma::fvec>(M);
            muH = new VBModules::MatrixMeans::IndependentGaussian(muHPrior);
            
            // Precision matrix
            //Modules::PrecisionMatrices::P2C EaH; EaH.X = 100.0f * arma::eye<arma::fmat>(M,M); EaH.logDetX = std::log(100.0f) * M;
            //alphaH = new VBModules::Constant<Modules::PrecisionMatrices::C2P, Modules::PrecisionMatrices::P2C>(EaH);
            VBModules::PrecisionMatrices::Wishart::Parameters alphaHPrior;
            alphaHPrior.a = float(M); alphaHPrior.B = 0.1f * float(M) * arma::eye<arma::fmat>(M,M);
            alphaH = new VBModules::PrecisionMatrices::Wishart(alphaHPrior);
            
            // Pseudo-rectified MVN
            MFModels::H::MultivariateNormal::Parameters HPrior;
            HPrior.M = M; HPrior.rectifyWeights = true;
            HPrior.clipWeights = true; HPrior.clipRange_max = 3.0f;
            H = new MFModels::H::MultivariateNormal(muH, alphaH, HPrior);
        }
        
        if (typeH == "Spike-Slab") {
            // Pseudo-rectified spike-slab
            MFModels::H::SpikeSlab::Parameters HPrior;
            HPrior.M = M; HPrior.mu = 2.0f; HPrior.sigma2 = 1.0f; HPrior.p = 0.95f;
            HPrior.rectifyWeights = true;
            H = new MFModels::H::SpikeSlab(HPrior);
        }
        
        //**********************************************************************
        // Psi
        
        MFModels::Psi_VBPosterior* Psi;
        
        {
            MFModels::Psi::GammaPrecision::Parameters psiPrior;
            psiPrior.a = 1.0f; psiPrior.b = 1.0f;
            Psi = new MFModels::Psi::GammaPrecision(psiPrior);
        }
        
        //**********************************************************************
        // MFM
        
        //MFModel* MFM = new MFModels::FullRankMFModel(FRD, Pconst, H, A, Psi, dofCorrectionFactor);
        
        MFModel* MFM = new MFModels::LowRankMFModel(LRD, Pconst, H, A, Psi, dofCorrectionFactor);
        
        //**********************************************************************
        // Convenience lambdas
        
        auto calculateFreeEnergy = [&] () -> double {
            double F = MFM->getLogLikelihood() - P->getKL() - H->getKL() - A->getKL() - Psi->getKL();
            // P
            if (tau != nullptr)    {F -= dofCorrectionFactor * tau->getKL();}
            if (beta != nullptr)   {F -= dofCorrectionFactor * beta->getKL();}
            if (kappa != nullptr)  {F -= dofCorrectionFactor * kappa->getKL();}
            if (pi != nullptr)     {F -= dofCorrectionFactor * pi->getKL();}
            // A
            if (alphaA != nullptr) {F -= alphaA->getKL();}
            if (betaA != nullptr)  {F -= betaA->getKL();}
            // H
            if (muH != nullptr)    {F -= muH->getKL();}
            if (alphaH != nullptr) {F -= alphaH->getKL();}
            return F;
        };
        
        auto printFreeEnergy = [&] () -> void {
            std::cout << "Free energy: " << calculateFreeEnergy() << std::endl;
            std::cout << "Neg LL: " << - MFM->getLogLikelihood() << std::endl;
            std::cout << "KL P:   " << P->getKL() << std::endl;
            std::cout << "KL H:   " << H->getKL() << std::endl;
            std::cout << "KL A:   " << A->getKL() << std::endl;
            std::cout << "KL Ψ:   " << Psi->getKL() << std::endl;
            // P
            if (tau != nullptr)    {std::cout << "KL τ:   " << dofCorrectionFactor * tau->getKL() << std::endl;}
            if (beta != nullptr)   {std::cout << "KL β:   " << dofCorrectionFactor * beta->getKL() << std::endl;}
            if (kappa != nullptr)  {std::cout << "KL κ:   " << dofCorrectionFactor * kappa->getKL() << std::endl;}
            if (pi != nullptr)     {std::cout << "KL π:   " << dofCorrectionFactor * pi->getKL() << std::endl;}
            // A
            if (alphaA != nullptr) {std::cout << "KL α_A: " << alphaA->getKL() << std::endl;}
            if (betaA != nullptr)  {std::cout << "KL β_A: " << betaA->getKL() << std::endl;}
            // H
            if (muH != nullptr)    {std::cout << "KL μ_H: " << muH->getKL() << std::endl;}
            if (alphaH != nullptr) {std::cout << "KL α_H: " << alphaH->getKL() << std::endl;}
            return;
        };
        
        // Need to add capture to calculate free energy
        auto update = [] (auto* model, const std::string name) -> void {
            //double F1,F2;
            //F1 = calculateFreeEnergy();
            
            Utilities::Timer timer;
            model->update();
            timer.printTimeElapsed(name, 3); timer.reset();
            
            //F2 = calculateFreeEnergy();
            //std::cout << "Diff F: " << F2 - F1 << std::endl;
            
            return;
        };
        
        auto printParameterSummaries = [&] () -> void {
            std::cout << "P: " << arma::median(arma::diagvec(P->getExpectations().PtP)) / V << std::endl;
            std::cout << "H: " << arma::median(H->getExpectations().h) << std::endl;
            std::cout << "A: " << arma::median(arma::diagvec(A->getExpectations().AAt)) / T << std::endl;
            std::cout << "Ψ: " << 1.0f / std::sqrt(Psi->getExpectations().psi) << std::endl;
            return;
        };
        
        //**********************************************************************
        
        std::cout << std::string(80, '-') << std::endl;
        std::cout << "Initial updates..." << std::endl << std::endl;
        
        // Initial updates
        std::cout << "Initialising A/H/Ψ..." << std::endl;
        MFM->setTimeCourseNormalisation(true);
        for (unsigned int z = 0; z < 5; z++) {
            std::cout << "Iteration " << z+1 << std::endl;
            A->update();
            H->update();
            Psi->update();
        }
        MFM->setTimeCourseNormalisation(false);
        std::cout << std::endl;
        
        // Switch to true spatial model
        std::cout << "Initialising P/H/Ψ..." << std::endl;
        MFM->setPModel(P);
        for (unsigned int z = 0; z < 5; ++z) {
            std::cout << "Iteration " << z+1 << std::endl;
            P->update();
            H->update();
            Psi->update();
        }
        std::cout << std::endl;
        
        printParameterSummaries();
        std::cout << std::endl;
        
        //**********************************************************************
        
        std::cout << std::string(80, '-') << std::endl;
        std::cout << "Full inference..." << std::endl << std::endl;
        
        double F, oldF;
        F = calculateFreeEnergy(); oldF = F;
        printFreeEnergy();
        std::cout << std::endl;
        
        for (unsigned int z = 0; z < 100; z++) {
            std::cout << std::string(40, '.') << std::endl << std::endl;
            std::cout << "Iteration " << z+1 << std::endl << std::endl;
            
            // Updates!
            Utilities::Timer timer;
            
            update(A, "A");
            update(H, "H");
            update(P, "P");
            update(H, "H");
            update(Psi, "Ψ");
            
            if (tau != nullptr)    {update(tau, "τ");}
            if (beta != nullptr)   {update(beta, "β");}
            if (kappa != nullptr)  {update(kappa, "κ");}
            if (pi != nullptr)     {update(pi, "π");}
            
            if (alphaA != nullptr) {update(alphaA, "α_A");}
            if (betaA != nullptr)  {update(betaA, "β_A");}
            
            if (muH != nullptr)    {update(muH, "μ_H");}
            if (alphaH != nullptr) {update(alphaH, "α_H");}
            
            timer.printTimeElapsed();
            std::cout << std::endl;
            
            // Parameter summaries
            printParameterSummaries();
            std::cout << std::endl;
            
            // Free energy summary
            printFreeEnergy();
            std::cout << std::endl;
            
            // Free energy change
            F = calculateFreeEnergy();
            double diffF = F - oldF;
            if (diffF >= -10.0 * std::abs(F) * std::numeric_limits<double>::epsilon()) {
                std::cout << "Diff F: " << F - oldF << std::endl;
            }
            else {
                std::cout << "*** Diff F: " << F - oldF << " ***" << "\a" << std::endl;
            }
            oldF = F;
            std::cout << std::endl;
            
            // Gives you a bit of time to read, and stops timing from parallel
            // KL divergence sections messing with update timing
            std::this_thread::sleep_for( std::chrono::milliseconds(500) );
        }
        std::cout << std::string(40, '.') << std::endl << std::endl;
        
        // Extract individual component amplitudes
        arma::fmat amplitudes = arma::zeros<arma::fmat>(M,5);
        amplitudes.col(0) = arma::abs( arma::mean(P->getExpectations().P, 0).t() );
        amplitudes.col(1) = arma::diagvec(P->getExpectations().PtP) / V;
        amplitudes.col(2) = H->getExpectations().h;
        amplitudes.col(3) = arma::abs( arma::mean(A->getExpectations().A, 1) );
        amplitudes.col(4) = arma::diagvec(A->getExpectations().AAt) / T;
        // Sort by the amplitudes of H
        arma::uvec inds = arma::sort_index( amplitudes.col(2), "descend" );
        amplitudes = amplitudes.rows(inds);
        // And print!
        std::cout << "Final component amplitudes [P,PtP,H,A,AAt]:" << std::endl << amplitudes << std::endl;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    std::cout << std::string(80, '*') << std::endl;
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
