# Coding conventions


### Numerical precision

We use single (i.e. 32 bit) precision almost exclusively. This is more than
accurate enough to store the individual parameter values given the noise level
we have, and the VB updates are typically naturally numerical stable (the
update rules are all derived from expectations of log probabilities). In short,
for univariate updates we are typically 'safe'.

However, this is does not address the typical reason for using double (i.e. 64
bit) precision: this is more robust to the accumulation of numerical errors.
While multiplication and division are 'safe', repeated summation can cause
(potentially large) numerical errors. As
[a rule of thumb](https://www.ilikebigbits.com/2017_06_01_float_or_double.html),
if we sum `10^N` numbers then we lose `N` digits (base 10) of precision. As
single precision only has ≈7 digits to begin with, this quickly results in
serious problems (an fMRI scan has something like `10^5` voxels and up to
`10^3` timepoints, and we may study up to `10^3` subjects).

The reason this does not go catastrophically wrong when we sum over voxels (or,
to a lesser extent, timepoints or subjects) is because we typically rely on
BLAS routines under the hood. However, this is not always the case. In the
following example we are essentially calculating the variance of `X`. The first
two routines fall back to `BLAS::dot(X, X)`, whereas the third just sums the
values and is noticeably less accurate:
```cpp
const unsigned int N = 10'000'000;
const arma::fvec X = arma::randn<arma::fvec>(N);
std::cout << "Double (BLAS): "
          << arma::accu(arma::conv_to<arma::mat>::from(X % X)) << std::endl;
std::cout << "Float (BLAS):  "
          << arma::accu(X % X) << std::endl;
std::cout << "Float (naïve): "
          << arma::accu(arma::square(X)) << std::endl;
std::cout << "Float (BLAS): "
          << arma::fmat(arma::ones<arma::frowvec>(N) * arma::square(X)).at(0)
          << std::endl;
std::cout << "Float (naïve): "
          << arma::as_scalar(arma::ones<arma::frowvec>(N) * arma::square(X))
          << std::endl;
std::cout << "Float (naïve): "
          << arma::dot(arma::ones<arma::fvec>(N), arma::square(X))
          << std::endl;
```

How do we stop this happening in the wild? We primarily need to worry if
Armadillo might fall back to a naive implementation, which can happen with
`arma::accu(), arma::sum(), arma::mean(), etc`, but is fundamentally hard to
predict. Furthermore, if the sum is
[well conditioned](https://en.wikipedia.org/wiki/Kahan_summation_algorithm#Accuracy)
(e.g. for approximately zero mean random variables, such that the sum itself
does not grow proportional to `N`) then that will help. We are also protected
to some extent by the way the model naturally splits over voxels / timepoints /
subjects. The largest sum we ever do in one step is `V * T`, as subjects are
always dealt with separately.

Assumptions:
 + Sums over modes (`M`) and subjects (`S`) are 'safe' and do not require
   special treatment.
 + Matrix multiplications are 'safe' as they are dealt with by the underlying
   BLAS libraries, and they are typically well conditioned (most of the
   quantities of interest are zero mean).

Problem areas:
 + Naive sums (e.g. using `arma::sum`) over voxels or timepoints, especially
   when these are poorly conditioned (so over probabilities, variances, etc).

Approach:
 + For problematic sums, try and offload to the underlying BLAS implementation.
 + Try and split over dimensions wherever possible, as per the code snippet
   below.

```cpp
const arma::fmat X2 = arma::square(arma::randn<arma::fmat>(10'000, 10'000));
std::cout << "Double (BLAS): "
          << arma::accu(arma::conv_to<arma::mat>::from(X2)) << std::endl;
std::cout << "Float (naïve): "
          << arma::accu(X2) << std::endl;
std::cout << "Float (BLAS):  "
          << arma::fmat(
                 arma::ones<arma::frowvec>(X2.n_rows)
                 * X2
                 * arma::ones<arma::fvec>(X2.n_cols)
             ).at(0)
          << std::endl;
```

Concrete recommendations:
 + For instances where we collapse down to a scalar, use
   `Utilities::LinearAlgebra::accu(X)`.
 + For intermediate sums, use `Utilities::LinearAlgebra::sum(X, dim)`.
 + `Utilities::LinearAlgebra::mean(X, [dim])` is a thin wrapper around the
   above which may also be useful.
 + If necessary, it is possible to use these tricks directly e.g.
   `arma::ones<arma::frowvec>(X.n_rows) * X`. For example, `linalg::sum(X)`
   returns an `arma::fmat` so forces execution immediately (which may or may
   not be what we want).
 + Try and avoid `arma::as_scalar()`, as this often optimises to a naive
   expression. Similarly, try and avoid e.g. `arma::mean(arma::vectorise(X))`,
   and similarly for `arma::stddev` etc.

The above should be the standard for `MFModels` and `VBModules`. For quantities
that have already been converted to double, `arma::sum(X)` should suffice.
Similarly, in instances where we are only summing over a small number of values
then we can fallback to `arma::sum(X)` if e.g. it is likely to make the code
more efficient.

----------

### C++ Coding Style

Basically as per the link below, but with a few exceptions.
 + <http://geosoft.no/development/cppstyle.html>

Key exceptions:

 + Classes/functions/variables etc: fine to keep abbreviations uppercase e.g. `MFPsiModel`

 + Namespaces: usually just a plural of what it contains e.g. the different
   implementations of abstract class `VBModule` are in the namespace
   `VBModules` (e.g.  `VBModules::SpecificImplementation`)

 + Header guards: include folder structure too, as these need to be unique for
   each file. All capitals with an underscore between every word, keeping
   abbreviations together (e.g. `MF_MODELS_A_KRONECKER_HRF_H`)

 + `const`: Use wherever you can!

 + Log: use log instead of ln (e.g. `logDetX`)

 + Classes / structs: only use structs for pure data containers

 + Not needed parameters: `void function(int /*i*/) {return;}`

Example file layouts:
```cpp
////////////////////////////////////////////////////////////////////////////////
// Some example code

void exampleFunction(Argument* argument)
{
    // Do something
    argument->doSomething();
    
    // Loop over something
    for (int i = 0; i < 100; ++i) {
        if (i == 50) {
            argument->setInt(i);
        }
    }
    
    // Range for
    // http://stackoverflow.com/a/15176127
    // Standard approach, but note that this copies element!
    for (auto element : vector) {
        std::cout << element << std::endl;
    }
    // Observe only, no copying
    for (const auto& element : vector) {
        std::cout << element << std::endl;
    }
    // Modify
    for (auto& element : vector) {
        element += 1;
    }
    
    // Switch statement
    // Note extra braces for individual scoping
    switch (argument->type()) {
        case AAA : {
            argument->doA();
            break;
        }
            
        case BBB : {
            argument->doB();
            break;
        }
        
        // http://stackoverflow.com/q/2201493
        default :
            throw std::invalid_argument(“This is bad.”);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
// Example header file class layout:

namespace PROFUMO
{
    ////////////////////////////////////////////////////////////////////////////
    
    class Example1 :
        public SomeOtherClass
    {
    public:
        Example1();
        void doSomething();
        
    private:
        int hiddenInt_;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Use of struct rather than class signals that this is a pure data 
    // container. 
    struct Example2
    {
    public:
        double data1;
        int data2;
    };
    // http://geosoft.no/development/cppstyle.html
    // "Note that structs are kept in C++ for compatibility with C only, and 
    // avoiding them increases the readability of the code by reducing the 
    // number of constructs used. Use a class instead."
    
    ////////////////////////////////////////////////////////////////////////////
}

////////////////////////////////////////////////////////////////////////////////
```

----------

### Other Style Notes

Git:
 + <https://chris.beams.io/posts/git-commit/>
 + Subject line: 50 characters; imperative; no full stop (i.e. `This commit
   will <subject line>`).
 + Two spaces at start of new (not first) paragraph, lists start with ` + ` and
   keep three space indentation for whole item.


For bash scripts: quote, quote, quote!
 + <http://mywiki.wooledge.org/BashPitfalls>
 + <http://www.shellcheck.net>
 + N.B. Can omit quotes for a simple assignment
   (as per <http://mywiki.wooledge.org/Quotes>)

----------
