PROFUMO: Data management

Basic premise:
All the data we deal with starts its life as arma::mat classes, and in general these are *big*. We need to come up with a data management strategy that doesn’t duplicate data or inadvertently result in lots of expensive copy operations.

Standard workflow:
 + Load data
 + Pre-process data
 + Convert to a more complex form (this requires all data for a given subject at once to, for example, convert to a subject-specific low-rank approximation)
 + Ship the packaged up data out to the models that do the computation

Extra constraints:
 + We want to be able to compute the spatial basis of the data, which requires knowledge of where all the data is at some point during initialisation.



################################################################################
Possible approaches:

## Canonical C++ way ##

 + Each model class is responsible for its own data
 + Just pass round arma::mat or Data classes, and let the compiler worry about what is copied

Pipeline:
 + Parse specFile
 + Upon request, load each subject’s data, and initialise the models using it
 + Once all subjects have been initialised, get a copy of their data, compute the spatial basis, and re-initialise the models

Pros:
 + Never need to worry about dynamic memory
 + Data loader is v. simple, no need to store all data

Cons:
 - May get expensive copies at two stages - when transferring ownership of the data, and if we do computeBasis(std::vector<Data>). To what extent can we rely on the compiler to optimise returns, and for arma::mat to only get shallow copied e.g. in vector<Data>?
 - Need a way of initialising the model for P (including its size) before loading the data! This would require e.g. to set up a dummy P model, load the data and directly pass it to the models, get the data back from the models and compute the SVD, re-initialise P with the spatial basis. This is very messy, and e.g. requires the class holding all the models do know how to compute the basis for the different types of data they store, rather than letting the data loader do this.



## Smart pointer way ##

 + Data is stored on the heap, and we just pass round shared_ptr<Data>

Pipeline:
 + Parse specFile, load all data and store preprocessed and transformed representations on the heap
 + Calculate spatial basis as all data is pre-loaded
 + When requested, return pointer to data and initialise models

Pros:
 + No copies of actual data
 + Doesn’t matter if data loader and models have data at the same time
 + Can share data between models (unlikely!)

Cons:
 - Dynamic memory management issues
 - Data loader is complex as has to store data too
 - The models have to dereference the data when it is used, which makes the code messier



################################################################################
Conclusions:

 + C++11 makes *moving* large objects around much more predictable - we can really forget about passing in references to be modified rather than just returning etc

 + This doesn’t help us if we just want to “look” e.g. we need to compute the SVD over all the data the models have stored. We could do [ vD.push_back(model.getData()); svd(vD) ] but we can’t get around the fact that this requires a copy of the data to be placed in vD - the benefit C++11 gives us is that vD won’t be needlessly copied later. In the single data case we would do [ svd(const &D) ], but we can’t pass a vector of references. In lieu of say an observer_ptr we still have to rely on pointers for this. They won’t manage the memory (good) but it is unclear what they do and it relies on functions not deleting them (bad).

 + The shared spatial basis in the low rank data model is a textbook use case for shared_ptr.

 + In reality, when just working with the full data, there are serious advantages to the canonical (no dynamic memory) approach. But, it seriously disrupts the initialisation of the models themselves, and it becomes much harder to safely initialise the graphical model (i.e. without placeholder classes that we replace once all the data is loaded and we can compute the spatial basis).

 + There are advantages to not mixing management models - so what goes for one data type got for all data types



################################################################################
Decision:

 + All data is managed by shared_ptr

 + If you want to look at data, use a raw pointer (until a look but don’t touch object comes along)

 + If you need to modify the data, use a reference (how does this work in pointer land?)


################################################################################
Notes:

Can we do dynamic polymorphism on the stack?
http://stackoverflow.com/q/486581

Should we pass by value, pointer, const reference etc?
http://stackoverflow.com/q/18086609
http://stackoverflow.com/a/15600615

What happens with a vector if the objects it holds are *big*:
http://stackoverflow.com/a/2275091
http://stackoverflow.com/q/8259107

*Really* good overview of smart pointers (and when raw pointers are ok):
https://mbevin.wordpress.com/2012/11/18/smart-pointers/
http://stackoverflow.com/a/15649077
http://stackoverflow.com/q/19929970
https://msdn.microsoft.com/en-us/library/hh279669.aspx
http://programmers.stackexchange.com/q/133302
https://herbsutter.com/2013/06/05/gotw-91-solution-smart-pointer-parameters/

Proposal for an observer_ptr (access without ownership):
http://en.cppreference.com/w/cpp/experimental/observer_ptr
http://open-std.org/jtc1/sc22/wg21/docs/papers/2014/n3840.pdf
http://stackoverflow.com/q/31862412

How to actually use vectors of pointers (for polymorphic reasons):
http://stackoverflow.com/a/3477305

Sounds like in the STL move semantics mean that a lot of the problems with vectors etc go away
https://mbevin.wordpress.com/2012/11/20/move-semantics/
http://stackoverflow.com/q/3134831
http://upcoder.com/2/efficient-vectors-of-vectors/
http://www.cprogramming.com/c++11/rvalue-references-and-move-semantics-in-c++11.html
http://stackoverflow.com/q/16823615

Key point:
Smart pointers deal with ownership, raw pointers are still ok for looking at stuff
